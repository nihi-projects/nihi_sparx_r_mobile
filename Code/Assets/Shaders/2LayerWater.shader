Shader "Custom/2LayerWater" {

Properties {
    _Blend ("Blend", Range (0, 1) ) = 0.5 
    _MainTex ("Texture 1", 2D) = "" 
    _Texture2 ("Texture 2", 2D) = ""
//    _Texture3 ("Texture 3", 2D) = ""
	}

SubShader { 
    Pass {
	        SetTexture[_MainTex]{
	        	Matrix[_Matrix1]
	        }
	        SetTexture[_Texture2] { 
	        	Matrix[_Matrix2]
	            ConstantColor (0,0,0, [_Blend]) 
	            Combine texture Lerp(constant) previous
	        }
//	        SetTexture[_Texture3] { 
//	            ConstantColor (0,0,0, [_Blend]) 
//	            Combine texture Lerp(constant) previous
//	        }
	    }
	} 

	
}

