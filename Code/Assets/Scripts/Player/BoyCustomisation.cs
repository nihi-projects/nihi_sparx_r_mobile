using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BoyCustomisation : MonoBehaviour {
	
	//Public variables
	public GUISkin m_skin = null;
	public GameObject m_boyCharacterPrf = null;
	
	//Private variables
	//Hair style
	private int m_iTotalHairStyleNumber = 11;
	private int m_iCurrentHairStyleNum = 1;
	
	//Hair color
	private int m_iTotalHairColorNum = 10;
	private int m_iCurrentHairColorNum = 1;
	private Color m_rCurrentHairColor;
	
	//Skin Color
	private int m_iTotalSkinColorNum = 4;
	private int m_iCurrentSkinColorNum = 1;
	
	//Eye Color
	private int m_iTotalEyeColorNum = 6;
	private int m_iCurrentEyeColorNum = 1;
	private Color m_rCurrentEyeColor;
	
	//Cloth Color
	private int m_iTotalClothColorNum = 5;
	private int m_iCurrentClothColorNum = 1;
	private Color m_rCurrentColorColor;
	
	//Trim textures
	public Texture2D m_tTrimTexture1;
	public Texture2D m_tTrimTexture2;
	public Texture2D m_tTrimTexture3;
	public Texture2D m_tTrimTexture4;
	
	private int m_iTotalTrimNum = 5;
	private int m_iCurrentTrimNum = 1;
	private Texture2D m_tCurrentTrimTexture;
	
	//Trim color
	private int m_iTotalTrimColorNum = 5;
	private int m_iCurrentTrimColorNum = 1;
	private Color m_rCurrentTrimColor;
	
	//Backpack
	private int m_iTotalBackpackNum = 2;
	private int m_iCurrentBackpackNum = 1;

	
	private Vector3 m_vBoysCustomisePosition;
	private GameObject m_boyObjectClone = null;

	private List<SkinnedMeshRenderer> m_lBoyHairMeshes = new List<SkinnedMeshRenderer>();
	
	private float m_rotationAngle = 0.0f;
	
	private CapturedDataInput instance;
	
	// Use this for initialization
	void Start () {
		//Connecting to the server
		//instance    = CapturedDataInput.GetInstance;
		instance = GameObject.Find("CapturedDataInputHolder").GetComponent<CapturedDataInput>();
		
//		m_skin          = (GUISkin) Resources.Load("Skins/SparxSkin");
//		
//		m_tTrimTexture1 = (Texture2D)Resources.Load ("Textures/Characters/Boy/boy_body_trim_1", typeof(Texture2D));
//		m_tTrimTexture2 = (Texture2D)Resources.Load ("Textures/Characters/Boy/boy_body_trim_2", typeof(Texture2D));
//		m_tTrimTexture3 = (Texture2D)Resources.Load ("Textures/Characters/Boy/boy_body_trim_3", typeof(Texture2D));
//		m_tTrimTexture4 = (Texture2D)Resources.Load ("Textures/Characters/Boy/boy_body_trim_4", typeof(Texture2D));
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void OnGUI()
	{
		GUI.depth = -1;
		GUI.skin = m_skin;
		
		GUI.Label(new Rect(Screen.width * -0.01f, Screen.height * -0.06f, Screen.width * 1.05f, Screen.height * 1.07f), "", "frontendfg");
		
		// Display the text on the left side and on the right side
		GUI.Label(new Rect(Screen.width * 0.2f, Screen.height * 0.15f, 100, 20), "Personal", "TitleMenuFont");
		
		//Hair Style
		if(GUI.Button(new Rect(Screen.width * 0.2f, 200, 100, 20), "Hair Style")){
			//Change the position of the boy object
			m_boyObjectClone.transform.position = new Vector3(-0.0f, -0.6f, 1.0f);
				
			//Disable the old hair style
			m_boyObjectClone.transform.Find("boy_hair_" + m_iCurrentHairStyleNum.ToString()).GetComponent<SkinnedMeshRenderer>().enabled = false;
			
			m_iCurrentHairStyleNum++;
			
			//Go back to the first hair style
			if(m_iCurrentHairStyleNum > m_iTotalHairStyleNumber){
				m_iCurrentHairStyleNum = 1;
			}
			
			//Enable the new hair style
			m_boyObjectClone.transform.Find("boy_hair_" + m_iCurrentHairStyleNum.ToString()).GetComponent<SkinnedMeshRenderer>().enabled = true;
		}
		
		//Hair Colour
		if(GUI.Button(new Rect(Screen.width * 0.2f, 260, 100, 20), "Hair Colour")){
			//Change the position of the boy object
			m_boyObjectClone.transform.position = new Vector3(-0.0f, -0.6f, 1.0f);
			
			m_iCurrentHairColorNum++;
				
			if(m_iCurrentHairColorNum > m_iTotalHairColorNum){
				m_iCurrentHairColorNum = 1;
			}
			Color newHairColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
			switch(m_iCurrentHairColorNum){
				case 1:
					//black
					newHairColor = new Color(0.11f, 0.11f, 0.11f, 1.0f);
					break;
				case 2:
					//brunette
					newHairColor = new Color(0.25f, 0.17f, 0.1f, 1.0f);
					break;
				case 3:
					//blonde
					newHairColor = new Color(0.56f, 0.49f, 0.26f, 1.0f);
					break;
				case 4:
					//ginger
					newHairColor = new Color(0.48f, 0.26f, 0.11f, 1.0f);
					break;
				case 5:
					//red
					newHairColor = new Color(0.35f, 0.1f, 0.05f, 1.0f);
					break;
				case 6:
					//blue
					newHairColor = new Color(0.21f, 0.38f, 0.54f, 1.0f);
					break;
				case 7:
					//green
					newHairColor = new Color(0.35f, 0.5f, 0.21f, 1.0f);
					break;
				case 8:
					//pink
					newHairColor = new Color(0.63f, 0.34f, 0.49f, 1.0f);
					break;
				case 9:
					//purple
					newHairColor = new Color(0.46f, 0.28f, 0.5f, 1.0f);
					break;
				case 10:
					//grey
					newHairColor = new Color(0.49f, 0.49f, 0.52f, 1.0f);
					break;
			}
			//Find the hair mesh, then change its colour
			m_boyObjectClone.transform.Find("boy_hair_" + m_iCurrentHairStyleNum.ToString()).GetComponent<Renderer>().material.color = newHairColor;
		}
		
		//Skin Colour
		if(GUI.Button(new Rect(Screen.width * 0.2f, 320, 100, 20), "Skin Colour")){
			//Change the position of the boy object
			m_boyObjectClone.transform.position = new Vector3(-0.0f, -0.6f, 1.0f);
			
			m_iCurrentSkinColorNum++;
				
			if(m_iCurrentSkinColorNum > m_iTotalSkinColorNum){
				m_iCurrentSkinColorNum = 1;
			}
			
			Color newSkinColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
			switch(m_iCurrentSkinColorNum){
				case 1:
					//pale
					newSkinColor = new Color(0.6f, 0.6f, 0.6f, 1.0f);
					break;
				case 2:
					//tan
					newSkinColor = new Color(0.49f, 0.4f, 0.35f, 1.0f);
					break;
				case 3:
					//pacific
					newSkinColor = new Color(0.3f, 0.23f, 0.19f, 1.0f);
					break;
				case 4:
					//african
					newSkinColor = new Color(0.18f, 0.168f, 0.164f, 1.0f);
					break;
			}
			
			//Find the skin mesh, then change its colour
			m_boyObjectClone.transform.Find("boy_body").GetComponent<Renderer>().materials[1].SetColor("_Color",newSkinColor);
			m_boyObjectClone.transform.Find("boy_head").GetComponent<Renderer>().materials[0].SetColor("_Color",newSkinColor);
		}
		
		//Eye Colour
		if(GUI.Button(new Rect(Screen.width * 0.2f, 380, 100, 20), "Eye Colour")){
			//Change the position of the boy object
			m_boyObjectClone.transform.position = new Vector3(-0.0f, -0.6f, 1.0f);
			
			m_iCurrentEyeColorNum++;
				
			if(m_iCurrentEyeColorNum > m_iTotalEyeColorNum){
				m_iCurrentEyeColorNum = 1;
			}
			Color newEyeColor = new Color(0.0f, 0.0f, 0.0f, 0.0f);
			switch(m_iCurrentEyeColorNum){
				case 1:
					newEyeColor = new Color(0.55f, 0.73f, 0.94f, 1.0f);
					break;
				case 2:
					newEyeColor = new Color(0.77f, 0.84f, 0.95f, 1.0f);
					break;
				case 3:
					newEyeColor = new Color(0.84f, 0.64f, 0.47f, 1.0f);
					break;
				//case 4:
					//newEyeColor = m_rCurrentHairColor;
					//break;
				case 4:
					newEyeColor = new Color(0.59f, 0.32f, 0.19f, 1.0f);
					break;
				case 5:
					newEyeColor = new Color(0.51f, 0.86f, 0.54f, 1.0f);
					break;
				case 6:
					newEyeColor = new Color(0.95f, 0.42f, 0.42f, 1.0f);
					break;
				
			}
			//Find the hair mesh, then change its colour
			m_boyObjectClone.transform.Find("boy_head").GetComponent<Renderer>().materials[2].SetColor("_Color",newEyeColor);
		}
		
		// Display the text on the left side and on the right side
		GUI.Label(new Rect(Screen.width * 0.66f, Screen.height * 0.15f, 120, 20), "Clothing", "TitleMenuFont");
		
		//Cloth Colour
		if(GUI.Button(new Rect(Screen.width * 0.66f, 200, 120, 20), "Colour")){
			//Change the position of the boy object
			m_boyObjectClone.transform.position = new Vector3(0.0f, 0.15f, 3f);
			
			m_iCurrentClothColorNum++;
				
			if(m_iCurrentClothColorNum > m_iTotalClothColorNum){
				m_iCurrentClothColorNum = 1;
			}
			Color newClothColor = new Color(0.35f, 0.54f, 0.9f, 1.0f);
			switch(m_iCurrentClothColorNum){
				case 1:
					newClothColor = new Color(0.82f, 0.2f, 0.2f, 1.0f);
					break;
				case 2:
					newClothColor = new Color(1.0f, 0.87f, 0.0f, 1.0f);
					break;
				case 3:
					newClothColor = new Color(0.77f, 0.44f, 0.97f, 1.0f);
					break;
				case 4:
					newClothColor = new Color(0.35f, 0.54f, 0.9f, 1.0f);
					//newClothColor = m_rCurrentHairColor;
					break;
				case 5:
					newClothColor = new Color(0.1f, 1.0f, 0.4f, 1.0f);
					break;
			}
			//Find the hair mesh, then change its colour
			m_boyObjectClone.transform.Find("boy_body").GetComponent<Renderer>().materials[0].SetColor("_Color",newClothColor);	
		}
		
		//Trim Texture
		if(GUI.Button(new Rect(Screen.width * 0.66f, 260, 120, 20), "Trim")){
			//Change the position of the boy object
			m_boyObjectClone.transform.position = new Vector3(0.0f, 0.15f, 3f);
			
			m_iCurrentTrimNum++;
				
			if(m_iCurrentTrimNum > m_iTotalTrimNum){
				m_iCurrentTrimNum = 1;
			}

			switch(m_iCurrentTrimNum){
				case 1:
					m_tCurrentTrimTexture = m_tTrimTexture1;
					break;
				case 2:
					m_tCurrentTrimTexture = m_tTrimTexture2;
					break;
				case 3:
					m_tCurrentTrimTexture = m_tTrimTexture3;
					break;
				case 4:
					m_tCurrentTrimTexture = m_tTrimTexture4;
					break;
			}
			
			//Find the trim
			m_boyObjectClone.transform.Find("boy_body").GetComponent<Renderer>().materials[4].SetTexture("_MainTex", m_tCurrentTrimTexture); 
		}
		
		//Trim Colour
		if(GUI.Button(new Rect(Screen.width * 0.66f, 320, 120, 20), "Trim Colour")){
			//Change the position of the boy object
			m_boyObjectClone.transform.position = new Vector3(0.0f, 0.15f, 3f);	
			
			m_iCurrentTrimColorNum++;
				
			if(m_iCurrentTrimColorNum > m_iTotalTrimColorNum){
				m_iCurrentTrimColorNum = 1;
			}
			Color newTrimColor = new Color(0.52f, 0.45f, 0.0f, 1.0f);
			switch(m_iCurrentTrimColorNum){
				case 1:
					newTrimColor = new Color(0.52f, 0.45f, 0.0f, 1.0f);
					break;
				case 2:
					newTrimColor = new Color(0.49f, 0.02f, 0.02f, 1.0f);
					break;
				case 3:
					newTrimColor = new Color(0.1f, 0.48f, 0.68f, 1.0f);
					break;
				case 4:
					newTrimColor = new Color(0.34f, 0.08f, 0.64f, 1.0f);
					break;
				case 5:
					newTrimColor = new Color(0.1f, .5f, 0.05f, 1.0f);
					break;
			}
			//Find the hair mesh, then change its colour
			m_boyObjectClone.transform.Find("boy_body").GetComponent<Renderer>().materials[4].SetColor("_Color",newTrimColor);	
		}
		
		//Backpack
		if(GUI.Button(new Rect(Screen.width * 0.66f, 380, 120, 20), "Backpack")){
			//Change the position of the boy object
			m_boyObjectClone.transform.position = new Vector3(0.0f, 0.15f, 3f);
			
			//Disable the old hair style
			m_boyObjectClone.transform.Find("boy_bag_" + m_iCurrentBackpackNum.ToString()).GetComponent<SkinnedMeshRenderer>().enabled = false;
			
			m_iCurrentBackpackNum++;
				
			if(m_iCurrentBackpackNum > m_iTotalBackpackNum){
				m_iCurrentBackpackNum = 1;
			}
			
			//Enable the new hair style
			m_boyObjectClone.transform.Find("boy_bag_" + m_iCurrentBackpackNum.ToString()).GetComponent<SkinnedMeshRenderer>().enabled = true;
		}
		
		//Confirm
		if(GUI.Button(new Rect(Screen.width * 0.66f, Screen.height * 0.7f, 80, 20), "Confirm", "CustomScreenButtons")){
			this.enabled = false;
			//Go to the guide splash screen
			GameObject.Find ("GUI").GetComponent<TheGuideSplashScreen>().enabled = true;
			
			Global.CurrentPlayerName = "Boy";
			Global.userHairStyleNum = m_iCurrentHairStyleNum;
			Global.userHairColor = m_boyObjectClone.transform.Find("boy_hair_" + m_iCurrentHairStyleNum.ToString()).GetComponent<Renderer>().material.color;
			Global.userSkinColor = m_boyObjectClone.transform.Find("boy_body").GetComponent<Renderer>().materials[1].color;
			Global.userEyeColor = m_boyObjectClone.transform.Find("boy_head").GetComponent<Renderer>().materials[2].color;
			Global.userClothColor = m_boyObjectClone.transform.Find("boy_body").GetComponent<Renderer>().materials[0].color;
			Global.userTrimTextureNum = m_iCurrentTrimNum;
			Global.userTrimColor = m_boyObjectClone.transform.Find("boy_body").GetComponent<Renderer>().materials[4].color;
			Global.userBackPackNum = m_iCurrentBackpackNum;
			
			//SAVE LEVEL
			StartCoroutine(instance.SaveUserLevelToServer_NewJsonSchema(Global.CurrentLevelNumber));
		}
		
		//Cancel
		if(GUI.Button(new Rect(Screen.width * 0.25f, Screen.height * 0.7f, 80, 20), "Cancel", "CustomScreenButtons")){
			//Destory the boy object clone on the boy customization screen
			GameObject.Destroy(m_boyObjectClone);
			
			this.enabled = false;
			GameObject.Find("L1GUI").GetComponent<CharacterSelection>().enabled = true;
		}
		
		//Left Rotation
		if(GUI.Button(new Rect(Screen.width * 0.36f, Screen.height * 0.8f, 64, 64), "", "SpinL")){
			m_rotationAngle = -90.0f;
			
			m_boyObjectClone.transform.Rotate(new Vector3(0.0f, m_rotationAngle, 0.0f));
			Debug.Log ("Left rotation angle = " + m_rotationAngle);
		}
		
		//Right Rotation
		if(GUI.Button(new Rect(Screen.width * 0.58f, Screen.height * 0.8f, 64, 64), "", "SpinR")){
			m_rotationAngle = 90.0f;
			
			m_boyObjectClone.transform.Rotate(new Vector3(0.0f, m_rotationAngle, 0.0f));
			Debug.Log ("Right rotation angle = " + m_rotationAngle);
		}
	}
	
	//The boy and girl clones need to be created every time when
	//this script is called.
	public void OnEnable(){
		//Character's customisation position
		m_vBoysCustomisePosition = new Vector3(0.0f, 0.15f, 3f);
		
		//Load the boy object on the screen
		m_boyObjectClone = (GameObject) Instantiate(m_boyCharacterPrf, m_vBoysCustomisePosition, Quaternion.identity);
		m_boyObjectClone.transform.rotation = new Quaternion(0.0f, 180.0f, 0.0f, 1.0f);
		Destroy(m_boyObjectClone.GetComponent<PlayerMovement>());
		
		m_rCurrentHairColor = m_boyObjectClone.transform.Find("boy_hair_1").GetComponent<SkinnedMeshRenderer>().material.color;
		m_rCurrentTrimColor = m_boyObjectClone.transform.Find("boy_body").GetComponent<Renderer>().materials[4].color;
		m_tCurrentTrimTexture = m_tTrimTexture4;
		
		SkinnedMeshRenderer hairMesh = null;
			
		//Get the all the hair style children
		for(int i = 1; i < m_iTotalHairStyleNumber; i++){
			//Go through each hair style in  the boy object
			hairMesh = m_boyObjectClone.transform.Find("boy_hair_" + i.ToString()).GetComponent<SkinnedMeshRenderer>();
			m_lBoyHairMeshes.Add(hairMesh);
		}
	}
	
	public Texture2D GetTexture(int index)
	{
		switch(index){
			case 1:
				return (m_tTrimTexture1);
				break;
			case 2:
				return (m_tTrimTexture2);
				break;
			case 3:
				return (m_tTrimTexture3);
				break;
			case 4:
				return (m_tTrimTexture4);
				break;
		};
		
		return (null);
	}
}
