using UnityEngine;
using System.Collections;

public class ClickCharacter : MonoBehaviour {
	
	private CharacterSelection m_characterSelection;
		
		
	// Use this for initialization
	void Start () {
		m_characterSelection = GameObject.Find ("GUI").GetComponent<CharacterSelection>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnMouseDown(){
		//Disable that screen
		m_characterSelection.enabled = false;
		
		//Destroy the boy clone
		//Destroy(m_characterSelection.m_boyObjectClone, 3.0f);
		
		GameObject.Find ("GUI").GetComponent<BoyCustomisation>().enabled = true;
	}
}
