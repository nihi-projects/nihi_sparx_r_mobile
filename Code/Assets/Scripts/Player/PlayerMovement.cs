using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;
public enum EMovement
{
	LEFT,
	RIGHT,
	UP,
	DOWN,
	MAX_MOVEMENT
};
public class PlayerMovement : MonoBehaviour {
	
	// publics
	public float m_fSpeed;
	public float m_fRunningSpeed        = 6.0f;
	public float m_fWalkingSpeed        = 3.0f;
	public float m_fRotationSpeed       = 1.0f;
	
	public bool m_bWalkOnly             = false;
	public bool m_bMovement             = false;
	public bool m_bMovingToPosition     = false;
	public bool m_bTuiMovementAttach    = true;
	
	// privates
	private Vector3 m_vTargetPosition;
	private Vector3 m_vMoveFromPosition;
	private Vector3 m_vTargetLookAt;
	
	private string m_strInteractionName = "";
	
	private float m_fTime = 0.0f;
	private float m_fNormalisedSpeed = 1.0f;
	
	private bool m_bMoveTowardsNPC      = false;
	private bool m_bRotateTowardsTarget = false;
	
	private static GameObject bagGlowMat;
	public Material[] mats;
	public static Material[] allMats;

	private GameObject mobileControl;
	
	// Use this for initialization
	void Start () {

		mobileControl = GameObject.Find("MobileSingleStickControl");

		m_bTuiMovementAttach = true;

		allMats = new Material[mats.Length];
		for(int i = 0; i < mats.Length; i ++){
			allMats[i] = mats[i];
		}

		if(!Application.isMobilePlatform){
			if(mobileControl != null){
				mobileControl.SetActive(false);
			}
		}else{
			mobileControl.SetActive(true);
		}
	}
	
	// Update is called once per frame
	void Update ()
	{

		gemCheck();

		if(!m_bMovement && !m_bRotateTowardsTarget)
		{
			SoundPlayer.StopLoop("walk");
			SoundPlayer.StopLoop("run");
			//if (animation.IsPlaying("walk") || animation.IsPlaying("run"))
			//{
			//	animation.Play("idle");
			//}
			return;
		}

		if(Global.CurrentLevelNumber == 2){
			Vector3 eulerAngles = Global.GetPlayer().GetComponent<Transform>().rotation.eulerAngles;
			eulerAngles.z = 0;
			Global.GetPlayer().GetComponent<Transform>().rotation = Quaternion.Euler(eulerAngles);
		}
		
		if(m_bRotateTowardsTarget)
		{
			m_fTime += Time.deltaTime * 0.5f;
			Quaternion rotation = Quaternion.LookRotation(m_vTargetLookAt - transform.position);
			transform.rotation  = Quaternion.Slerp(transform.rotation, rotation, m_fTime);
			if(m_fTime >= 1.0f)
			{
				m_bRotateTowardsTarget = false;
				m_fTime = 0.0f;
				Global.GetPlayer().GetComponent<Animation>().Stop("walk");
			}
		}
		else if(m_bMovingToPosition)
		{
			GetComponent<Animation>().Play("walk");
			SoundPlayer.PlayLoopSound("walk", 1.0f);
			
			m_fTime += Time.deltaTime / m_fNormalisedSpeed;
			
			transform.position = Vector3.Lerp(m_vMoveFromPosition , m_vTargetPosition, m_fTime);
			
			Quaternion rotation = Quaternion.LookRotation(m_vTargetLookAt - transform.position);
			transform.rotation = Quaternion.Slerp(transform.rotation, rotation, m_fTime);
			
			//If the player reaches the destination
			if (m_fTime >= 1.0f)
			{
				m_bMovingToPosition = false;
				if(!Global.GetPlayer().GetComponent<Animation>().IsPlaying("take")){
					GetComponent<Animation>().Play("idle");
					Debug.Log ("The player is playing idle...");
				}
				
				m_fTime = 1.0f;
				
				transform.position = Vector3.Lerp(m_vMoveFromPosition , m_vTargetPosition, m_fTime);
				transform.rotation = Quaternion.Slerp(transform.rotation, rotation, m_fTime);
				
				if (m_bMoveTowardsNPC == true)
				{
					m_bRotateTowardsTarget = true;
					m_bMoveTowardsNPC = false;
				}
			}
		}
		else
		{
			bool Running = false;
			
			double moveCap = 0.5;
			
			//Bind the Tui and Tui fly floows the player
			AttachTuiMovement();

			if (Application.isMobilePlatform) {
				if ((!m_bWalkOnly && Input.GetKey (KeyCode.LeftShift) || !m_bWalkOnly && Input.GetKey (KeyCode.RightShift) || !m_bWalkOnly && CrossPlatformInputManager.GetAxis ("SprintVert") > moveCap)) {
					Running = true;
				}
			} else {
				if ((!m_bWalkOnly && Input.GetKey (KeyCode.LeftShift) || !m_bWalkOnly && Input.GetKey (KeyCode.RightShift))) {
					Running = true;
				}
			}


			// Check arrow keys down.
			EMovement movement = EMovement.MAX_MOVEMENT;
			if(Input.GetKey(KeyCode.LeftArrow)|| Input.GetKey(KeyCode.A) || CrossPlatformInputManager.GetAxis ("Horizontal") < -moveCap)
			{
				movement = EMovement.LEFT;
				Move (movement, Running);
			}
			if(Input.GetKey(KeyCode.RightArrow)|| Input.GetKey(KeyCode.D) || CrossPlatformInputManager.GetAxis ("Horizontal") > moveCap)
			{
				movement = EMovement.RIGHT;
				Move (movement, Running);
			}
			if(Input.GetKey(KeyCode.UpArrow)|| Input.GetKey(KeyCode.W) || CrossPlatformInputManager.GetAxis ("Vertical") > moveCap)
			{
				movement = EMovement.UP;
				Move (movement, Running);
			}
			if(Input.GetKey(KeyCode.DownArrow)|| Input.GetKey(KeyCode.S) || CrossPlatformInputManager.GetAxis ("Vertical") < -moveCap)
			{
				movement = EMovement.DOWN;
				Move (movement, Running);
			}
			
			if(movement == EMovement.MAX_MOVEMENT)
			{
				if(!GetComponent<Animation>().IsPlaying("ride"))
				{
					GetComponent<Animation>().Play("idle");
					SoundPlayer.StopLoop("walk");
					SoundPlayer.StopLoop("run");
				}
			}
			else
			{
				if(GetComponent<Animation>().IsPlaying ("idle"))
				{
					GetComponent<Animation>().Stop();
					Debug.Log ("190ANIMATION - STOPED!");
				}
				
				bool onLadder = false;
				GameObject level4 = GameObject.Find("Level4");
				if (level4 != null)
				{
					LadderInteraction[] ladders = level4.GetComponentsInChildren<LadderInteraction>();
					foreach (LadderInteraction ladder in ladders)
					{
						if (ladder.m_bOnThisLadder)
						{
							onLadder = true;
							break;
						}
					}
				}
				
				if (!onLadder)
				{
					if(Running)
					{
						GetComponent<Animation>().Play("run");
						SoundPlayer.PlayLoopSound("run", 1.0f);
					}
					else
					{
						GetComponent<Animation>().Play("walk");
						SoundPlayer.PlayLoopSound("walk", 0.8f);
					}
				}
			}
		}
	}

	static public void gemCheck(){
		if(Global.m_bGotCurrentLevelGem == true && ReleaseGem.bagOff == false){
			if(Global.GetPlayer().name == "Boy"){
				if(Global.userBackPackNum == 1){
					bagGlowMat = GameObject.Find("boy_bag_1");
					if(Global.CurrentLevelNumber == 1){
						bagGlowMat.GetComponent<Renderer>().material = allMats[13];
					}else if(Global.CurrentLevelNumber == 2){
						bagGlowMat.GetComponent<Renderer>().material = allMats[9];
					}else if(Global.CurrentLevelNumber == 3){
						bagGlowMat.GetComponent<Renderer>().material = allMats[14];
					}else if(Global.CurrentLevelNumber == 4){
						bagGlowMat.GetComponent<Renderer>().material = allMats[11];
					}else if(Global.CurrentLevelNumber == 5){
						bagGlowMat.GetComponent<Renderer>().material = allMats[10];
					}else if(Global.CurrentLevelNumber == 6){
						bagGlowMat.GetComponent<Renderer>().material = allMats[15];
					}else if(Global.CurrentLevelNumber == 7){
						bagGlowMat.GetComponent<Renderer>().material = allMats[12];
					}	
				}else{
					bagGlowMat = GameObject.Find("boy_bag_2");
					if(Global.CurrentLevelNumber == 1){
						bagGlowMat.GetComponent<Renderer>().material = allMats[6];
					}else if(Global.CurrentLevelNumber == 2){
						bagGlowMat.GetComponent<Renderer>().material = allMats[2];
					}else if(Global.CurrentLevelNumber == 3){
						bagGlowMat.GetComponent<Renderer>().material = allMats[7];
					}else if(Global.CurrentLevelNumber == 4){
						bagGlowMat.GetComponent<Renderer>().material = allMats[4];
					}else if(Global.CurrentLevelNumber == 5){
						bagGlowMat.GetComponent<Renderer>().material = allMats[3];
					}else if(Global.CurrentLevelNumber == 6){
						bagGlowMat.GetComponent<Renderer>().material = allMats[8];
					}else if(Global.CurrentLevelNumber == 7){
						bagGlowMat.GetComponent<Renderer>().material = allMats[5];
					}
				}
			}else if(Global.GetPlayer().name == "Girl"){
				if(Global.userBackPackNum == 1){
					bagGlowMat = GameObject.Find("girl_bag_1");
					if(Global.CurrentLevelNumber == 1){
						bagGlowMat.GetComponent<Renderer>().material = allMats[13];
					}else if(Global.CurrentLevelNumber == 2){
						bagGlowMat.GetComponent<Renderer>().material = allMats[9];
					}else if(Global.CurrentLevelNumber == 3){
						bagGlowMat.GetComponent<Renderer>().material = allMats[14];
					}else if(Global.CurrentLevelNumber == 4){
						bagGlowMat.GetComponent<Renderer>().material = allMats[11];
					}else if(Global.CurrentLevelNumber == 5){
						bagGlowMat.GetComponent<Renderer>().material = allMats[10];
					}else if(Global.CurrentLevelNumber == 6){
						bagGlowMat.GetComponent<Renderer>().material = allMats[15];
					}else if(Global.CurrentLevelNumber == 7){
						bagGlowMat.GetComponent<Renderer>().material = allMats[12];
					}	
				}else{
					bagGlowMat = GameObject.Find("girl_bag_2");
					if(Global.CurrentLevelNumber == 1){
						bagGlowMat.GetComponent<Renderer>().material = allMats[6];
					}else if(Global.CurrentLevelNumber == 2){
						bagGlowMat.GetComponent<Renderer>().material = allMats[2];
					}else if(Global.CurrentLevelNumber == 3){
						bagGlowMat.GetComponent<Renderer>().material = allMats[7];
					}else if(Global.CurrentLevelNumber == 4){
						bagGlowMat.GetComponent<Renderer>().material = allMats[4];
					}else if(Global.CurrentLevelNumber == 5){
						bagGlowMat.GetComponent<Renderer>().material = allMats[3];
					}else if(Global.CurrentLevelNumber == 6){
						bagGlowMat.GetComponent<Renderer>().material = allMats[8];
					}else if(Global.CurrentLevelNumber == 7){
						bagGlowMat.GetComponent<Renderer>().material = allMats[5];
					}
				}
			}
		}else{
			if(Global.GetPlayer().name == "Boy"){
				if(Global.userBackPackNum == 1){
					bagGlowMat = GameObject.Find("boy_bag_1");
					bagGlowMat.GetComponent<Renderer>().material = allMats[0];
				}else{
					bagGlowMat = GameObject.Find("boy_bag_2");
					bagGlowMat.GetComponent<Renderer>().material = allMats[1];
				}
			}else{
				if(Global.userBackPackNum == 1){
					bagGlowMat = GameObject.Find("girl_bag_1");
					bagGlowMat.GetComponent<Renderer>().material = allMats[0];
				}else{
					bagGlowMat = GameObject.Find("girl_bag_2");
					bagGlowMat.GetComponent<Renderer>().material = allMats[1];
				}
			}
		}
		
	}

	public void Move(EMovement move, bool bRun)
	{
		if(move == EMovement.MAX_MOVEMENT)
		{
			return;
		}
		m_fSpeed = bRun ? m_fRunningSpeed : m_fWalkingSpeed;
		if(move == EMovement.LEFT)
		{
			gameObject.transform.Rotate(0.0f, -m_fRotationSpeed, 0.0f);
		}
		else if(move == EMovement.RIGHT)
		{
			gameObject.transform.Rotate(0.0f, m_fRotationSpeed, 0.0f);
		}
		else if(move == EMovement.UP)
		{
			gameObject.GetComponent<CharacterController>().SimpleMove(transform.forward * m_fSpeed);
		}
		else if(move == EMovement.DOWN)
		{
			gameObject.GetComponent<CharacterController>().SimpleMove(-transform.forward * m_fSpeed);
		}
	}
	
	public void MoveToPosition(Vector3 Position, Vector3 TargetNPCPos, string NameOfNPC)
	{
		MoveToPosition(Position, TargetNPCPos);
		m_bMoveTowardsNPC = true;
		m_strInteractionName = NameOfNPC;
	}
	
	public void MoveToPosition(Vector3 Position, Vector3 LookAtPos)
	{
		m_vTargetLookAt = LookAtPos;
		m_vTargetLookAt.y = transform.position.y;
		m_vTargetPosition = Position;
		if (m_bMovingToPosition == false)
		{
			m_fTime = 0.0f;
			m_vMoveFromPosition = transform.position;
			float dist = Vector3.Distance(m_vMoveFromPosition, Position);
			m_fNormalisedSpeed = dist / m_fWalkingSpeed;
		}
		m_bMovingToPosition = true;	
	}
	
	public void AttachTuiMovement()
	{
		Vector3 tuiFlyToPosition = new Vector3(0.0f, 0.0f, 0.0f);
		
		//If Tui needs to move along with the player
		if(m_bTuiMovementAttach){
			if(Application.loadedLevelName == "Level7_Canyon" && !Global.loadedSavePoint.Contains("GameMid")){
				tuiFlyToPosition = Global.GetPlayer().transform.position;
				tuiFlyToPosition.x -= 1.2f;
				tuiFlyToPosition.y += 1.0f;
				tuiFlyToPosition.z -= 0.5f;
				GameObject hope = GameObject.Find ("Hope");
				if (hope)
				{
					hope.transform.position = tuiFlyToPosition;
					hope.transform.localRotation = gameObject.transform.rotation;
				}
			}
		}
	}
}
