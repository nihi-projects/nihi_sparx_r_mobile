using UnityEngine;
using System.Collections;

public class Global : MonoBehaviour {

	private static Global instanceRef;
	
	public static string CurrentInteractNPC      = "Guide";
	public static string m_sCurrentFakeNPC       = "";
	public static string CurrentPlayerName       = "";
	public static bool GemObtainedForTheLevel    = false;
	public static int m_L6CurrentDoorNum         = 1;
	
	//This temp value is used to save the temply interact character
	public static string TempTalkCharacter        = CurrentInteractNPC;
	public static bool m_bGotCurrentLevelGem      = false;
	
	//This variable tells whether the player visited the island or not
	public static bool LevelSVisited              = false;
	public static int CurrentLevelNumber          = 1;
	
	//This variable is used  to record the previous scene name before it transfer to the script
	public static string PreviousSceneName        = "";

	public static bool m_bGemStartsRaising		  = false;
	public static bool m_bGemIsFullyReleased      = false;
	
	public static GameObject[] GUIObjects         = new GameObject[7];
	
	//User In Game Data (For Server)
	public static int    userCurrentLevelNum      = 1;
	public static int[]  userFeedBackScore        = new int[3];
	
	public static int    userHairStyleNum         = 1;
	public static Color  userHairColor            = new Color(1.0f, 1.0f, 1.0f, 1.0f);
	public static Color  userSkinColor      	  = new Color(1.0f, 1.0f, 1.0f, 1.0f);
	public static Color  userEyeColor       	  = new Color(1.0f, 1.0f, 1.0f, 1.0f);
	public static Color  userClothColor     	  = new Color(1.0f, 1.0f, 1.0f, 1.0f);
	public static int    userTrimTextureNum 	  = 0;
	public static Color  userTrimColor      	  = new Color(1.0f, 1.0f, 1.0f, 1.0f);
	public static int    userBackPackNum    	  = 0;
	
	//User Notebook data blocks(For Server)
	public static string[] userL1NoteBookDataBlocks = new string[6];
	public static string[] userL1NoteBookManualBlocks = new string[3];
	
	public static string[] userL2NoteBookDataBlocks = new string[6];
	public static string[] userL2NoteBookManualBlocks = new string[3];
	
	public static string[] userL3NoteBookDataBlockOne = new string[6];
	public static string[] userL3NoteBookDataBlockTwo = new string[6];
	public static string[] userL3NoteBookDataBlockThree = new string[1];
	
	public static string[] userL4NoteBookDataBlockOne = new string[1];
	public static string[] userL4NoteBookDataBlockTwo = new string[6];
	public static string[] userL4NoteBookStepsBlocks = new string[5];
	
	public static string[] userL5NoteBookDataBlocks = new string[6];
	public static string[] userL5NoteBookManualBlocks = new string[3];
	
	public static string[] userL6NoteBookDataBlocks = new string[1];
	public static string[] userL6NoteBookManualBlocks = new string[3];
	public static string[] userL6NoteBookRapaBlocks = new string[4];
	
	public static string[] userL7NoteBookDataBlocks = new string[1];

	public static string lastLevelNoteBookData;
	
	//Give away object
	private static string m_giveAwayNPCName       = "";
	private static string m_giveAwayAniName       = "";
	private static string m_giveAwayObjName       = "";
	private static Vector3 m_giveAwayObjAppearPos;
	private static bool m_gObjectStartGivingAway  = false;
	
	//Pick up object
	public static string m_pickNPCName            = "";
	public static string m_PickAniName            = "";
	private static bool m_gObjectTakingStart      = false;

	public static bool m_ObjectTaken              = false;

	public static string multiChoice;
	public static string arrowKey;

	public static bool playerReadyCheck 		  = false;
	
	//save point
	/*public static string lastSavePoint;*/
	 
	//Game Saving use
	public static string m_sSavePointCode         = "";
	public static string tuiScene;
	public static bool m_bTalkedWithMentor;
	public static bool toggleGUI;
	public static bool levelComplete;
	public static int savePointLoadLevelNumber;
	public static string loadedSavePoint          = "";
	public static bool levelStarted				  = false;
	public static Vector3 loadPos;

	//Minigame Level 7 Help
	public static bool m_bPlayerAcceptHelp = false;
	//Minigame Level 7 Help
	public static bool m_bPlayerAfterFire = false;

	//Testing - Set TestingOnline = True to enable testing. If false, the other variables do not need changing.
	public static bool TestingOnline				= false;
	public static int TestProgress					= 4;
	public static string TestSavePoint				= "L5Game";


	public static string[] questionName = {"Things I want to change", "Things that stood out for me today", "Things I am going to do", "Things that I find relaxing", "My triggers are", "My distraction skills", "What I would like to try out from today", "My problem is", "Im going to figure it out using STEPS", "My Sparks are", "I SPOT these annoying Gnats", "Things that stood out for me today", "A thought I'd like to swap", "RAPA",  "What I would like to try out from today", "What I would like to take from today"};


	public static string versionNumber            = "Version: SPARX_R v0.0.0b1";

	public static bool Global_bshowVersionOnScreen            = false;

	//Implementation
	void Awake(){
		
		//DontDestroyOnLoad(gameObject);//FIX DUPLICATE AND INDISTICT KILLING OBJECTS
		if (instanceRef == null) {
			instanceRef = this;
			DontDestroyOnLoad (gameObject);
		}else{
			DestroyImmediate (gameObject);
		}
		playerCheck();
		Debug.Log(versionNumber);
	}
	
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {

		keyCheck();
		if(TestingOnline){
			checkNPC();
		}

		if(m_gObjectStartGivingAway){
			if (GameObject.Find(m_pickNPCName) == Global.GetPlayer())
			{
				GameObject.Find(m_pickNPCName).GetComponent<PlayerMovement>().m_bMovement = false;
			}
			if(!GameObject.Find(m_giveAwayNPCName).GetComponent<Animation>().IsPlaying(m_giveAwayAniName)){
				GameObject.Find(m_giveAwayObjName).transform.position = m_giveAwayObjAppearPos;
				if (GameObject.Find(m_giveAwayObjName).GetComponent<ParticleSystem>())
				{
					GameObject.Find(m_giveAwayObjName).GetComponent<ParticleSystem>().enableEmission = true;
				}
				m_gObjectStartGivingAway = false;
				m_ObjectTaken = false;
				//Start picking up the object
				NPCPickUpObject(m_pickNPCName, m_PickAniName, m_giveAwayObjName);
			}
		}
		if(m_gObjectTakingStart){
			//If player finished taking
			if(!GameObject.Find (m_pickNPCName).GetComponent<Animation>().IsPlaying(m_PickAniName)){
				//Set the camera back after take the object given by NPC
				GameObject.Find("Main Camera").GetComponent<CameraMovement>().m_bCameraLookAt = false;
				GameObject giveObject = GameObject.Find(m_giveAwayObjName);
				if (giveObject.GetComponent<ParticleSystem>())
				{
					giveObject.GetComponent<ParticleSystem>().enableEmission = false;
					GameObject.Find ("L2GUI").GetComponent<TerminateCassFiremanScene>().m_firePersonConversationFinished = true;
				}
				else
				{
					if (giveObject.name == "Gem7" || giveObject.name == "Gem2")
					{
						giveObject.GetComponent<GetGem>().enabled = true;
					}
					else if (giveObject.name != "Shield")
					{
						giveObject.GetComponent<MeshRenderer>().enabled = false;
					}
					else
					{
						foreach (MeshRenderer renderer in giveObject.GetComponentsInChildren<MeshRenderer>())
						{
							renderer.enabled = false;
						}
					}
				}
				
				if (GameObject.Find(m_pickNPCName) == Global.GetPlayer())
				{
					GameObject.Find(m_pickNPCName).GetComponent<PlayerMovement>().m_bMovement = true;
				}
				
				m_ObjectTaken = true;
				m_gObjectTakingStart = false;
			}
		}
	}

	public void playerCheck(){
		if(Application.loadedLevel != 0){
			while(!playerReadyCheck){
				GetPlayer();
			}
		}
	}

	static public void checkNPC(){
		Debug.Log ("CurrentInteractNPC: " + CurrentInteractNPC);
		Debug.Log ("interact Object: " + TerminateTalkScene.interactObject);
	}

	static public void keyCheck(){

		//Debug.Log ("<TalkScene>" + GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentScene);
		if(GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentScene.Length > 7){
			//if (GameObject.Find ("GUI").GetComponent<TalkScenes> ().m_currentScene.Substring (0, 7) != "L4Spark") {
				if (Input.GetKeyUp (KeyCode.Alpha1) || Input.GetKeyUp (KeyCode.A)) {
					multiChoice = "A";
				} else if (Input.GetKeyUp (KeyCode.Alpha2) || Input.GetKeyUp (KeyCode.B)) {
					multiChoice = "B";
				} else if (Input.GetKeyUp (KeyCode.Alpha3) || Input.GetKeyUp (KeyCode.C)) {
					if (GameObject.Find ("GUI").GetComponent<TalkScenes> ().m_currentScene == "L4GuideScene5" || GameObject.Find ("GUI").GetComponent<TalkScenes> ().m_currentScene == "L3GuideScene42"){
						multiChoice = "D";
					} else {
						multiChoice = "C";
					}

				} else if (Input.GetKeyUp (KeyCode.Alpha4) || Input.GetKeyUp (KeyCode.D)) {
					if (GameObject.Find ("GUI").GetComponent<TalkScenes> ().m_currentScene == "L4GuideScene5" || GameObject.Find ("GUI").GetComponent<TalkScenes> ().m_currentScene == "L3GuideScene42"){
						multiChoice = "";
					} else {
						multiChoice = "D";
					}
				} else {
					multiChoice = "";
				}
			
				if (Input.GetKeyUp (KeyCode.RightArrow)) {
					arrowKey = "right";
				} else if (Input.GetKeyUp (KeyCode.LeftArrow)) {
					arrowKey = "left";
				} else {
					arrowKey = "";
				}
			//}
		}/*else {
			
		if (Input.GetKeyUp (KeyCode.Alpha1) || Input.GetKeyUp (KeyCode.A)) {
			multiChoice = "A";
		} else if (Input.GetKeyUp (KeyCode.Alpha2) || Input.GetKeyUp (KeyCode.B)) {
			multiChoice = "B";
		} else if (Input.GetKeyUp (KeyCode.Alpha3) || Input.GetKeyUp (KeyCode.C)) {
			if (GameObject.Find ("GUI").GetComponent<TalkScenes> ().m_currentScene == "L4GuideScene5" || GameObject.Find ("GUI").GetComponent<TalkScenes> ().m_currentScene == "L3GuideScene42"){
				multiChoice = "D";
			} else {
				multiChoice = "C";
			}
		} else if (Input.GetKeyUp (KeyCode.Alpha4) || Input.GetKeyUp (KeyCode.D)) {
			if (GameObject.Find ("GUI").GetComponent<TalkScenes> ().m_currentScene == "L4GuideScene5" || GameObject.Find ("GUI").GetComponent<TalkScenes> ().m_currentScene == "L3GuideScene42"){
				multiChoice = "";
			} else {
				multiChoice = "D";
			}
		} else {
			multiChoice = "";
		}

		if (Input.GetKeyUp (KeyCode.RightArrow)) {
			arrowKey = "right";
		} else if (Input.GetKeyUp (KeyCode.LeftArrow)) {
			arrowKey = "left";
		} else {
			arrowKey = "";
		}

		}*/
	}

	static public void levelSSavePoint(int _levelNum){
		//levelSSavePoint(Global.CurrentLevelNumber);
		if(LevelSVisited && m_bGotCurrentLevelGem && Application.loadedLevel == 1){
			if(_levelNum == 1){
				m_sSavePointCode = "LevelsSP2";
			}else if(_levelNum == 2){
				m_sSavePointCode = "LevelsSP2";
			}else if(_levelNum == 3){
				m_sSavePointCode = "LevelsSP2";
			}else if(_levelNum == 4){
				m_sSavePointCode = "LevelsSP2";
			}else if(_levelNum == 5){
				m_sSavePointCode = "LevelsSP2";
			}else if(_levelNum == 6){
				m_sSavePointCode = "LevelsSP2";
			}else if(_levelNum == 7){
				m_sSavePointCode = "LevelsSP2";
			}else{
				m_sSavePointCode = "saveFailed";
			}
		}else if(Application.loadedLevel != 1){
			if(_levelNum == 1){
				m_sSavePointCode = "L1Game";
			}else if(_levelNum == 2){
				m_sSavePointCode = "L2Game";
			}else if(_levelNum == 3){
				m_sSavePointCode = "L3Game";
			}else if(_levelNum == 4){
				m_sSavePointCode = "L4Game";
			}else if(_levelNum == 5){
				m_sSavePointCode = "L5Game";
			}else if(_levelNum == 6){
				m_sSavePointCode = "L6Game";
			}else if(_levelNum == 7){
				m_sSavePointCode = "L7Game";
			}else{
				m_sSavePointCode = "saveFailed";
			}
		}else if(Application.loadedLevel == 1){
			if(_levelNum == 1){
				m_sSavePointCode = "LevelsSP1";
			}else if(_levelNum == 2){
				m_sSavePointCode = "LevelsSP1";
			}else if(_levelNum == 3){
				m_sSavePointCode = "LevelsSP1";
			}else if(_levelNum == 4){
				m_sSavePointCode = "LevelsSP1";
			}else if(_levelNum == 5){
				m_sSavePointCode = "LevelsSP1";
			}else if(_levelNum == 6){
				m_sSavePointCode = "LevelsSP1";
			}else if(_levelNum == 7){
				m_sSavePointCode = "LevelsSP1";
			}else{
				m_sSavePointCode = "saveFailed";
			}
		}
	}
	
	static public string GetCurrentLevelID()
	{
		string CurrentLevel = "";
		switch(Application.loadedLevelName)
		{
		case "Level1_Cave":
			CurrentLevel = "Level1";
			break; 
		case "LevelS":
			CurrentLevel = "LevelS";
			break;
		case "Level2_Ice":
			CurrentLevel = "Level2";
			break;
		case "Level3_Lava":
			CurrentLevel = "Level3";
			break;
		case "Level4_Cliff":
			CurrentLevel = "Level4";
			break;
		case "Level5_Swamp":
			CurrentLevel = "Level5";
			break;
		case "Level6_Bridge":
			CurrentLevel = "Level6";
			break;
		case "Level7_Canyon":
			CurrentLevel = "Level7";
			break;
		default:break;
		}
		
		return(CurrentLevel);
	}
	
	//
	static public string GetNextLevelName()
	{
		string NextLevel = "";
		if(CurrentLevelNumber == 1){
			switch(Application.loadedLevelName)
			{
				case "LevelS":
					NextLevel = "Level1_Cave";
					break;
				default:break;
			}
		}
		else if(CurrentLevelNumber == 2){
			switch(Application.loadedLevelName)
			{
				case "LevelS":
					NextLevel = "Level2_Ice";
					break;
				default:break;
			}
		}
		else if(CurrentLevelNumber == 3){
			switch(Application.loadedLevelName)
			{
				case "LevelS":
					NextLevel = "Level3_Lava";
					break;
				default:break;
			}
		}
		else if(CurrentLevelNumber == 4){
			switch(Application.loadedLevelName)
			{
				case "LevelS":
					NextLevel = "Level4_Cliff";
					break;
				default:break;
			}
		}
		else if(CurrentLevelNumber == 5){
			switch(Application.loadedLevelName)
			{
				case "LevelS":
					NextLevel = "Level5_Swamp";
					break;
				default:break;
			}
		}
		else if(CurrentLevelNumber == 6){
			switch(Application.loadedLevelName)
			{
				case "LevelS":
					NextLevel = "Level6_Bridge";
					break;
				default:break;
			}
		}
		else if(CurrentLevelNumber == 7){
			switch(Application.loadedLevelName)
			{
				case "LevelS":
					NextLevel = "Level7_Canyon";
					break;
				default:break;
			}
		}
		
		return(NextLevel);	
	}
	
	//
	static public GameObject GetPlayer()
	{
		GameObject CurrentPlayer = null;
		if(CurrentPlayerName == "Boy"){
			CurrentPlayer = GameObject.Find("Boy");
			playerReadyCheck = true;
		}
		else if(CurrentPlayerName == "Girl"){
			CurrentPlayer = GameObject.Find ("Girl");
			playerReadyCheck = true;
		}
		
		//Set the camera for the player
		if(CurrentPlayer != null){
			GameObject.Find ("Main Camera").GetComponent<CameraMovement>().PlayerTransform = CurrentPlayer.transform;
		}
		
		return CurrentPlayer;
	}
	
	//
	static public Vector3 GetPosition(string strNameOfObj, string strLevelName)
	{
		return(GameObject.Find (strLevelName).transform.FindChild(strNameOfObj).position);
	}
	
	static public Quaternion GetRotation(string strNameOfObj, string strLevelName)
	{
		return(GameObject.Find (strLevelName).transform.FindChild(strNameOfObj).rotation);
	}
	
	//
	static public int GetNumOfNPCInLevel()
	{
		int iNum = 0;
		iNum = GameObject.FindGameObjectsWithTag("NPC").Length;
		return(iNum);
	}
	
	//
	static public void SnapCameraBasedOnID(string ID)
	{
		Vector3 CameraTarget = Vector3.zero;
		Vector3 CameraPosition = Vector3.zero;
		// Get the level object in the game
		GameObject Level = GameObject.Find (GetCurrentLevelID());
		foreach(Transform item in Level.transform)
		{
			string itemName = item.name;
			
			if(itemName.Contains(ID))
			{
				if(itemName.Contains (".Target"))
				{
					CameraTarget = item.transform.position;
				}
				else
				{
					CameraPosition = item.transform.position;
				}
			}
		}
		GameObject.Find ("Main Camera").GetComponent<CameraMovement>().SnapCameraTo(CameraTarget, CameraPosition);

	}
	
	//
	static public void MoveCameraBasedOnID(string ID)
	{
		Vector3 CameraTarget = Vector3.zero;
		Vector3 CameraPosition = Vector3.zero;
		// Get the level object in the game
		GameObject Level = GameObject.Find (GetCurrentLevelID());
		foreach(Transform item in Level.transform)
		{
			string itemName = item.name;
			
			if(itemName.Contains(ID))
			{
				if(itemName.Contains (".Target"))
				{
					CameraTarget = item.transform.position;
				}
				else
				{
					CameraPosition = item.transform.position;
				}
			}
		}
		GameObject.Find ("Main Camera").GetComponent<CameraMovement>().SetCameraTo(CameraTarget, CameraPosition);
	}
	
	/*
	 * This function will return any object position in the scene
	 * This camera position can either be original position and the 
	 * target position.
    */
	static public Vector3 GetCameraObjPosition(string ID, bool _isTarget)
	{
		Vector3 cameraPos = Vector3.zero;
		
		GameObject Level = GameObject.Find (GetCurrentLevelID());
		foreach(Transform item in Level.transform)
		{
			string itemName = item.name;
			
			if(itemName.Contains(ID))
			{
				if(itemName.Contains (".Target") && _isTarget == true)
				{
					cameraPos = item.transform.position;
				}
				else
				{
					cameraPos = item.transform.position;
				}
			}
		}
		return cameraPos;
	}
	
	static public GameObject GetCameraObj(string ID, bool _isTarget)
	{
		GameObject Level = GameObject.Find (GetCurrentLevelID());
		foreach(Transform item in Level.transform)
		{
			string itemName = item.name;
			
			if(itemName.Contains(ID))
			{
				if(itemName.Contains (".Target") && _isTarget == true)
				{
					return (item.gameObject);
				}
				else
				{
					return (item.gameObject);
				}
			}
		}
		return null;
	}
	
	//
	static public Vector3 GetObjectPositionByID(string ID)
	{
		Vector3 Pos = Vector3.zero;
		GameObject Level = GameObject.Find (GetCurrentLevelID());
		foreach(Transform item in Level.transform)
		{
			string itemName = item.name;
			
			if(itemName.Contains(ID))
			{
				Pos = item.position;
			}
		}
		return(Pos);
	}
	
	//
	static public GameObject GetGameObjectByID(string ID)
	{
		GameObject gameObject = null;
		GameObject Level = GameObject.Find (GetCurrentLevelID());
		for(int i = 0; i < Level.transform.childCount; ++i)
		{
			string itemName = Level.transform.GetChild(i).name;
			
			if(itemName.Contains(ID)){
				gameObject = Level.transform.GetChild(i).gameObject;	
			}
		}
		
		return(gameObject);
	}
	
	//
	static public Vector3 GetObjectPositionByID(string ID, GameObject SetToPos)
	{
		Vector3 Pos = Vector3.zero;
		GameObject Level = GameObject.Find (GetCurrentLevelID());
		foreach(Transform item in Level.transform)
		{
			string itemName = item.name;
			
			if(itemName.Contains(ID))
			{
				Pos = item.position;
				SetToPos.transform.position = Pos;
			}
		}
		return(Pos);
	}
	
	//
	static public PlayerEffect GetPlayerEffect()
	{
		return(GetPlayer().GetComponentInChildren<PlayerEffect>());
	}
	
	//
	static public void CameraSnapBackToPlayer()
	{
		Camera.main.GetComponent<CameraMovement>().SnapBackToPlayer(); 
	}
	
	//
	static public string GetMousePosToObj()
	{
		Ray ray = Camera.main.ScreenPointToRay( Input.mousePosition );
		RaycastHit hit;
		
		int layer = LayerMask.NameToLayer("path");
		LayerMask ignore = ~((1 << layer));
		if( Physics.Raycast( ray, out hit, 20.0f, ignore) )
		{
			return(hit.collider.gameObject.name);
			Debug.Log ("The clicked game object name is ==== " + hit.collider.gameObject.name);
		}
		return("");
	}
	
	//
	static public GameObject GetMouseToObject()
	{
		Ray ray = Camera.main.ScreenPointToRay( Input.mousePosition );
		RaycastHit hit;
		
		int layer = LayerMask.NameToLayer("path");
		LayerMask ignore = ~((1 << layer));
		if( Physics.Raycast( ray, out hit, 20.0f, ignore) )
		{
			return(hit.collider.gameObject);
		}
		return(null);
	}
	
	//
	static public string[] GetMousePosToAllObj()
	{
		Ray ray = Camera.main.ScreenPointToRay( Input.mousePosition );
		RaycastHit[] hit;
		
		int layer = LayerMask.NameToLayer("path");
		LayerMask ignore = ~((1 << layer));
		hit = Physics.RaycastAll(ray, 20.0f, ignore);
		string[] hitObj = new string[hit.Length];
		
		for(uint i = 0; i < hitObj.Length;++i)
		{
			hitObj[i] = hit[i].collider.gameObject.name;
		}
		return(hitObj);
	}
	
	static public void NPCGiveAwayObject(string NPCNname, string _animationName, string _objectName)
	{
		m_giveAwayNPCName = NPCNname;
		m_giveAwayAniName = _animationName;
		m_giveAwayObjName = _objectName;
		
		GameObject.Find (NPCNname).GetComponent<Animation>().Play(_animationName);
		
		Vector3 ObjectAppearPosition = GameObject.Find (NPCNname).transform.position;
		if (GameObject.Find(_objectName).GetComponent<ParticleSystem>() != null)
		{
			ObjectAppearPosition.z += 0.4f;
		}
		else
		{
			if (_objectName == "Gem7")
			{
				ObjectAppearPosition.x += 1.2f;
				ObjectAppearPosition.z += 1.5f;
			}
			else if (_objectName == "Gem2")
			{
				ObjectAppearPosition.z += 0.7f;
			}
			else if (_objectName != "Shield")
			{
				ObjectAppearPosition.x -= 0.7f;
				ObjectAppearPosition.y += 0.2f;
			}
			else
			{
				ObjectAppearPosition.x += 0.7f;
				ObjectAppearPosition.y += 0.2f;
				ObjectAppearPosition.z += 1.0f;
			}
		}
		m_giveAwayObjAppearPos = ObjectAppearPosition;
		
		m_gObjectStartGivingAway = true;
	}
	
	static public void NPCPickUpObject(string _NPCNname, string _animationName, string _objectName)
	{
		GameObject character = GameObject.Find(_NPCNname);
		character.GetComponent<Animation>().Play(_animationName);
		m_gObjectTakingStart = true;
		
		if (character == Global.GetPlayer())
		{
			character.GetComponent<PlayerMovement>().m_bMovement = false;
		}
	}

	public static string doTextFieldCheck(string textfieldName, string textFieldResult, Rect textfieldSize){
		return doTextFieldCheck (textfieldName, textFieldResult, textfieldSize, "Click here to enter text", 32);
	}
	public static string doTextFieldCheck(string textfieldName, string textFieldResult, Rect textfieldSize, int maxSize){
		return doTextFieldCheck (textfieldName, textFieldResult, textfieldSize, "Click here to enter text", maxSize);
	}
	public static string doTextFieldCheck(string textfieldName, string textFieldResult, Rect textfieldSize, string defaultText){
		return doTextFieldCheck (textfieldName, textFieldResult, textfieldSize, defaultText, 32);
	}
	public static string doTextFieldCheck(string textfieldName, string textFieldResult, Rect textfieldSize, string defaultText, int maxSize){
		string theResult = textFieldResult;
		if (GUI.GetNameOfFocusedControl () == textfieldName) {
			if (textFieldResult == defaultText) {
				theResult = GUI.TextField(textfieldSize, "", maxSize);
			}
		}
		return theResult;
	}
}
