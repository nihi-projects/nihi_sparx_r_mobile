using UnityEngine;
using System.Collections;

public class FireSpritFlyAway : MonoBehaviour {

	public Vector3 m_vTargetPosition;
	public bool m_bMovingToTarget;
	
	Vector3 m_start = Vector3.zero;
	float m_fTime = -0.2f;
	
	// Use this for initialization
	void Start () {
		m_start = transform.position;
	}
	
	// Update is called once per frame
	void Update (){
		if(m_bMovingToTarget){
			//FireSprit look at
			gameObject.transform.LookAt(m_vTargetPosition);
			
			m_fTime += Time.deltaTime * 0.3f;
			
			//Flying
			transform.position = Vector3.Lerp (m_start, m_vTargetPosition, m_fTime);
			
			//Destination is reached
			if(m_fTime > 1.0f){
				m_bMovingToTarget = false;
				gameObject.SetActive(false);
			}
		}
	}
	
	/*
	 * This function set the Tui fly to the target position
	 * */
	public void FireSpritFlyTo(string _flyTargetPositionID)
	{
		m_start = transform.position;
		GetComponent<Animation>().Play ("fly");
		
		m_vTargetPosition = Global.GetObjectPositionByID(_flyTargetPositionID);
		m_bMovingToTarget = true;
	}
}
