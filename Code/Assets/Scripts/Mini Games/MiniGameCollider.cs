using UnityEngine;
using System.Collections;

public class MiniGameCollider : MonoBehaviour {
	
	public int index = 0;
	bool bTriggered = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnTriggerStay(Collider collisionInfo)
	{
		if(bTriggered)
		{
			return;
		}
		if(collisionInfo.gameObject.name == Global.GetPlayer ().name)
		{
			if(Input.GetKey (KeyCode.Space))
			{
				GameObject.Find(Global.GetCurrentLevelID()).GetComponent<LevelSixScript>().MiniGameEnable(index);
				bTriggered = true;
			}
		}
	}
}
