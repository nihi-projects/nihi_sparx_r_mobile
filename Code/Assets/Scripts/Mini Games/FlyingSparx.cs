using UnityEngine;
using System.Collections;

public class FlyingSparx : MonoBehaviour {
	
	float fSpawnDelay;
	Vector3 Target;
	public bool bSpawn = false;
	bool bFlying = false;
	// Use this for initialization
	void Start () {
		fSpawnDelay = Random.Range(0.0f, 10.0f);
	}
	
	// Update is called once per frame
	void Update () {
		if(bSpawn)
		{
			Spawn();
		}
		else
		{
			fSpawnDelay -= Time.deltaTime;
			if(fSpawnDelay <= 0.0f)
			{
				bSpawn = true;
			}
		}
		// Movement
		if(bFlying)
		{
			transform.position = Vector3.Lerp (transform.position, Target, Time.deltaTime * 0.7f);
			if(Vector3.Distance(transform.position, Target) <= 1.0f)
			{
				bFlying = false;
			}
		}
	}	

	public void Spawn()
	{
		// Make it disappear, start respawn timer again.
		Vector3 LeftSideOfScreen, RightSideOfScreen;
		float fromy, toy;
		fromy = Random.Range(100.0f, 500.0f);
		toy = Random.Range(100.0f, 500.0f);
		
		LeftSideOfScreen = Camera.main.ScreenToWorldPoint(new Vector3(-180.0f,fromy, Camera.main.nearClipPlane + 5.0f));
		RightSideOfScreen = Camera.main.ScreenToWorldPoint(new Vector3(Camera.main.pixelWidth + 180.0f, toy, Camera.main.nearClipPlane + 5.0f));
		
		fSpawnDelay = Random.Range(0.0f, 5.0f);
		
		int i = Random.Range(0, 2);
		if(0 == i)
		{
			transform.position = LeftSideOfScreen;
			Target = RightSideOfScreen;
		}
		else
		{
			transform.position = RightSideOfScreen;
			Target = LeftSideOfScreen;
		}
		fSpawnDelay = Random.Range(0.0f, 10.0f);
		bFlying = true;
		bSpawn = false;
	}
	public void Kill()
	{
		this.enabled = false;
		this.gameObject.SetActive(false);
	}
}
