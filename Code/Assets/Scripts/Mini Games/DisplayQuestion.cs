using UnityEngine;
using System.Collections;

public class DisplayQuestion : MonoBehaviour {
	public bool bCorrectAnswer = false;
	public bool bFinishedAnswering = false;
	public GUISkin GUIskin;
	bool bActivated = false;
	
	public string[] Options;
	public string CorrectOption;
	public string Question;
	public GameObject CorrespondingSparx;
	public float fStartVoice, fEndVoice;
	public string VoiceFile;
	public AudioClip L4GnatsAudioClip; 
	AudioSource m_Voice;
	public float fHeight = 20.0f;
	public string Name;
	float countdown = 1.0f;
	public bool m_bjustPlay = false;//Diego
	// Use this for initialization
	void Start () {
		Question.Replace("\\n", "\n");
		for(int i = 0; i < Options.Length; ++i)
		{
			Options[i].Replace("\\n", "\n");
		}
		
//		if (GUIskin == null)
//		{
//			GUIskin = (GUISkin)Resources.Load("Skins/SparxSkin");
//		}
	}
	
	// Update is called once per frame
	void Update () {	
		if(bActivated)
		{
			Debug.Log(m_Voice.time + " <Eval> " +fEndVoice);
			if (m_bjustPlay)//FIX_D
			{//FIX_D
				if (m_Voice.time < fEndVoice)//FIX_D
				{//FIX_D
					m_bjustPlay = false;//FIX_D
					Debug.Log("bJustPlay True to False: " + m_Voice.time);
					if(m_Voice.time <= fStartVoice){
						Debug.Log ("SET START AUDIO AND PLAY AGAIN");
						m_Voice.time = fStartVoice;
						m_Voice.Play();
					}
				}
				else {
					Debug.Log("OriginalValue: " + m_Voice.time);
					Debug.Log("ValueGuiven: " + fStartVoice);
					m_Voice.time = fStartVoice;
				}
				//FIX_D
			}//FIX_D
			else//FIX_D
			{//FIX_D
				Debug.Log("bJustPlay False: " + m_Voice.time);

				if(m_Voice.time >= fEndVoice)
				{
					m_Voice.Stop ();
					Debug.Log("STOP1");
				}


			}//FIX_D


/*
			if(m_Voice.time >= fEndVoice)
			{
				m_Voice.Stop ();
			}*/
		}
		else
		{
			if(!bCorrectAnswer)
			{
				countdown -= Time.deltaTime;
				if(countdown <= 0.0f)
				{
					Activate();
				}
			}
		}
	}
	public void Activate()
	{
		bActivated = true;
		bFinishedAnswering = false;
		
		m_Voice = GameObject.Find (Name).GetComponent<AudioSource>();
		//m_Voice.clip = Resources.Load("AudioSounds/Voice/" + VoiceFile) as AudioClip;
		m_Voice.clip = L4GnatsAudioClip;
		m_Voice.time = fStartVoice;
		m_Voice.Play();
		m_bjustPlay = true;//FIX_D
	}
	public void Reset()
	{
		bActivated = false;
		bFinishedAnswering = false;
	}
	
	void OnGUI()
	{
		int nMultiChoice = 100;

		if(bFinishedAnswering || !bActivated)
		{
			return;
		}
		
		GUI.skin = GUIskin;
		
//		GUI.skin.button.wordWrap = true;
//		GUI.skin.button.alignment = TextAnchor.UpperLeft;
//		GUI.skin.button.normal.background = null;
//		GUI.skin.button.hover.background = null;
//		GUI.skin.button.active.background = null;
//		GUI.skin.button.font = Resources.Load ("Main Font") as Font;
//		GUI.color = Color.black;
		
		GUIStyle temp = GUI.skin.GetStyle("Label");
		GUI.Label(new Rect(0,Screen.height*0.63f,Screen.width,Screen.height*0.3f), "", "NormalSizeDilogBox");
		GUI.Label(new Rect(Screen.width*0.17f,Screen.height*0.80f,620,135), "", "NormalSizeDilogBox");
		temp.normal.textColor = new Color(0.44f, 0.44f, 0.44f, 1.0f);
		GUI.Label(new Rect(Screen.width*0.17f,Screen.height*0.69f,725,100), "Gnat", "Label");
		temp.normal.textColor = new Color(0.0f, 0.0f, 0.0f, 1.0f);
		GUI.Label(new Rect(Screen.width*0.17f,Screen.height*0.72f,725,100), Question, "Label");
		float y = Screen.height*0.84f;
		for(int i = 0; i < Options.Length;++i)
		{
			if(GUI.Button (new Rect(250, y, 480, fHeight), Options[i],"Label"))
			{
				if(Options[i] == CorrectOption)
				{
					bCorrectAnswer = true;
				}
				bFinishedAnswering = true;
				m_Voice.Stop();
				Debug.Log ("147MVOICE - STOPED!");
				if (GameObject.Find ("TalkPaper") != null) {
					GameObject.Find ("TalkPaper").GetComponent<GUITexture> ().enabled = false;
				}
				if (GameObject.Find ("TalkPaperSmall") != null) {
					GameObject.Find ("TalkPaperSmall").GetComponent<GUITexture> ().enabled = false;
				}
				bActivated = false;
				countdown = 1.0f;
				m_Voice.Stop ();
			}

			/*if (Input.GetKeyDown (KeyCode.Alpha1) || Input.GetKeyDown (KeyCode.A)) {
				nMultiChoice = 0;
			}  else if (Input.GetKeyDown (KeyCode.Alpha2) || Input.GetKeyDown (KeyCode.B)) {
				nMultiChoice = 1;
			}  else if (Input.GetKeyDown (KeyCode.Alpha3) || Input.GetKeyDown (KeyCode.C)) {
				nMultiChoice = 2;
			}  else if (Input.GetKeyDown (KeyCode.Alpha4) || Input.GetKeyDown (KeyCode.D)) {
				nMultiChoice = 3;
			}  else {
				nMultiChoice = 100;
			}

			if(nMultiChoice < 3)
			{
				if(Options[nMultiChoice] == CorrectOption)
				{
					bCorrectAnswer = true;
				}
				bFinishedAnswering = true;
				m_Voice.Stop();
				GameObject.Find ("TalkPaper").GetComponent<GUITexture>().enabled = false;
				GameObject.Find ("TalkPaperSmall").GetComponent<GUITexture>().enabled = false;
				bActivated = false;
				countdown = 1.0f;
				m_Voice.Stop ();
			}*/


			y += fHeight;
		}
	}
}
