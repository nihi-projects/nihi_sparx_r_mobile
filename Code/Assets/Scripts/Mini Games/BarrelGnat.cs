using UnityEngine;
using System.Collections;

public enum BarrelGnatStatus
{
	FlyToPosition,
	InConversation,
	FlyingAway,
	Standby,
}

public class BarrelGnat : MonoBehaviour {
	public string GnatWords;
	public float StartTime, EndTime;
	public Transform CorrectBarrel;
	Vector3 CenterPos, FlyAwayPos;
	public BarrelGnatStatus Status;
	AudioSource m_VoiceAS;
	
	
	// Use this for initialization
	void Start () {
		Reset ();
	}
	public void Reset()	
	{
		Status = BarrelGnatStatus.FlyToPosition;
		transform.position = GameObject.Find ("FlyFrom").transform.position;
		CenterPos = GameObject.Find ("FlyToCenter").transform.position;
		FlyAwayPos = GameObject.Find ("FlyToLeft").transform.position;
		m_VoiceAS = GameObject.Find("Cube").GetComponent<AudioSource>();
		GnatWords = GnatWords.Replace("\\n", "\n");
	}
	
	// Update is called once per frame
	void Update () {
		if(BarrelGnatStatus.FlyToPosition== Status)
		{
			transform.position = Vector3.Lerp (transform.position, 
										 	   CenterPos,
											   Time.deltaTime);
		}
		if(Vector3.Distance(transform.position, CenterPos) <= 1.0f && 
			Status == BarrelGnatStatus.FlyToPosition)
		{
			
			Status = BarrelGnatStatus.InConversation;
			m_VoiceAS.time = StartTime;
			
			// Setup conversation
			m_VoiceAS.Play();
			GameObject.Find ("TalkPaper").GetComponent<GUITexture>().enabled = true;
			GameObject.Find ("Conversation").GetComponent<GUIText>().text = GnatWords;
		}
		if(Status == BarrelGnatStatus.InConversation)
		{
			if(m_VoiceAS.time >= EndTime)
			{
				m_VoiceAS.Stop ();
				Status = BarrelGnatStatus.Standby;
			}
		}
		if(Status == BarrelGnatStatus.Standby)
		{
			//m_VoiceAS.Stop	
		}
	}
}
