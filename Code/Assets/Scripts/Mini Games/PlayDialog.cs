using UnityEngine;
using System.Collections;

public class PlayDialog : MonoBehaviour {
	
	public string Dialog;
	string Name;
	public float fStartingAnchor, fFinishingAnchor;
	public bool StartPlaying = false;
	public bool IsPlaying = false;
	public string VoiceFile;
	public AudioClip VoiceAudilClip;
	AudioSource m_Voice;
	public GameObject CorrectBarrel;
	public string GameID;
	
	static Texture button = null;
	public Texture2D buttonTexture;
	public GUISkin skin;
	
	// Use this for initialization
	void Start () {
		Dialog = Dialog.Replace("\\n", "\n");
		
		if (button == null)
		{
			//button = (Texture2D)Resources.Load ("UI/next");
			button = buttonTexture;
		}
		
		
		if (Dialog.Contains("Spark"))
		{
			Name = "Spark";
			Dialog = Dialog.Remove(0, 5);
		}
		else 
		{
			Name = "Gnat";
			Dialog = Dialog.Remove(0, 4);
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnGUI()
	{
		GUI.skin = skin;
		if(IsPlaying)
		{
			if(m_Voice.time >= fFinishingAnchor)
			{
				m_Voice.Stop ();
			}
			GUI.skin.button.normal.background = null;
			GUI.skin.button.hover.background = null;
			GUI.skin.button.active.background = null;
			Rect pos = new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, 60, 60);
			
			GUIStyle temp = GUI.skin.GetStyle("Label");
			int fontsize = temp.fontSize;
			temp.fontSize = 16;
			/*if (Name == "Gnat")
			{
				temp.normal.textColor = new Color(0.44f, 0.44f, 0.44f, 1.0f);
			}
			else
			{
				temp.normal.textColor = new Color(0.17f, 0.66f, 0.78f, 1.0f);
			}*/
			//temp.normal.textColor = Color.black;
			GUI.Label(new Rect(0,Screen.height*0.63f,Screen.width,Screen.height*0.3f), "", "NormalSizeDilogBox");
			GUI.Label(new Rect(Screen.width*0.18f,Screen.height*0.71f,725,100), Name, "Label");
			GUI.Label(new Rect(Screen.width*0.15f,Screen.height*0.72f,725,100), Dialog);
			//temp.normal.textColor = new Color(0.0f, 0.0f, 0.9f, 1.0f);
			temp.fontSize = fontsize;
			
			if(GUI.Button(pos, button) || Global.arrowKey == "right"){
				Global.arrowKey = "";
				//temp.normal.textColor = new Color(0.0f, 0.0f, 0.0f, 1.0f);
				IsPlaying = false;
				m_Voice.Stop();
				Debug.Log ("86M_VOICE - STOPED!");

				//GameObject.Find ("TalkPaper").GetComponent<GUITexture>().enabled = false;
				//GameObject.Find ("TalkPaperSmall").guiTexture.enabled = false;
				//GameObject.Find ("Conversation").GetComponent<GUIText>().text = "";
			}
		}
	}
	public void StartDialog()
	{
		StartPlaying = true;
		IsPlaying = true;
		m_Voice = GameObject.Find (GameID).GetComponent<AudioSource>();
		//m_Voice.clip = Resources.Load("AudioSounds/Voice/" + VoiceFile) as AudioClip;
		m_Voice.clip = VoiceAudilClip;
		m_Voice.time = fStartingAnchor;
		m_Voice.Play();
		
		//GameObject.Find ("TalkPaper").GetComponent<GUITexture>().enabled = true;
		//GameObject.Find ("TalkPaperSmall").guiTexture.enabled = true;
		//GameObject.Find ("Conversation").GetComponent<GUIText>().enabled = true;
		//GameObject.Find ("Conversation").GetComponent<GUIText>().text = Dialog;
	}
}
