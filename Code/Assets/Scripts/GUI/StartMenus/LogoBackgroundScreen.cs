using UnityEngine;
using System;
using System.Collections;

public class LogoBackgroundScreen : MonoBehaviour {
	//Public variables
	//Private variables
	public GUISkin m_skin                = null;
	private bool m_bGotLoginTokenDone    = false;
	public static bool m_bGotDynamicTokenDone  = false;
	public static bool m_bGotDynamicURLDone  = false;
	private bool m_bGotLoadAvatarDone  = false;
	private bool m_bHasLevelNumber       = false;
	
	private string m_sGameConfigURL      = "";
	// Use this for initialization
	private CapturedDataInput instance;

	public void Start () {
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		
		//Connecting to the server
		//instance = CapturedDataInput.GetInstance;
		instance = GameObject.Find("CapturedDataInputHolder").GetComponent<CapturedDataInput>();
		//Get token from fixed username and password
		if(Application.isEditor){
			//StartCoroutine(instance.GetUserLogInToServer()); //CHANGES_KEY_NEW_JSON_SCHEMA 
			StartCoroutine(instance.GetUserLogInToServer_NewSchema()); //CHANGES_KEY_NEW_JSON_SCHEMA 
		}
		//StartCoroutine(instance.GetUserLogInToServer());
		//Get token from configURL
		if(!Application.isEditor){
			//Call the javascript method in HTML page
			Application.ExternalCall("unityGameLoaded", "GUI");
		}
		try{
			m_skin.label.normal.textColor = new Color(0.0f, 0.0f, 0.0f, 1.0f);
		} catch (Exception e){
			Debug.Log("ERROR - something about textColor");
		}
	}
		
	// Update is called once per frame
	public void Update () {

		/*string s = " Hello\"";
		s = s.Replace("\"","");*/

		//STATIC URL
		if(Application.isEditor){
			if(instance.m_logInDataRetriever.isDone && !m_bGotLoginTokenDone && string.IsNullOrEmpty(instance.m_logInDataRetriever.error)){
				instance.GetTokenAfterLogin_NewJsonSchema(); //EDITOR GETS TOKEN FROM SERVER
				Debug.Log("<Token from URL>");
				m_bGotLoginTokenDone = true;
			}
		}

		//Keep getting login token
		if(instance.m_sGameLoginToken == ""){
			/*if(Application.isEditor){
				instance.GetTokenAfterLogin_NewJsonSchema(); //EDITOR GETS DYNAMIC URL FROM SERVER
				Debug.Log("<Token from Server>");
			}*/
			//instance.GetTokenAfterLogin();
			if(!Application.isEditor){
				Application.ExternalCall("unityGameLoaded", "GUI");
				Debug.Log("<Token from Config>");
				m_bGotLoginTokenDone = true;
			}
		} 

		if (instance.m_sGameLoginToken != "") {
			//if(!Application.isEditor && !m_bGotDynamicTokenDone){//CHANGES_KEY_NEW_JSON_SCHEMA
			if (!m_bGotDynamicTokenDone) {//CHANGES_KEY_NEW_JSON_SCHEMA EDITOR AND WEBGL GET DYNAMIC URL 
				//Send off the gameConfigURL in order to get dynamic URL
				StartCoroutine (instance.SendOffGameConfigURL_NewJsonSchema (m_sGameConfigURL)); //EDITOR GETS DYNAMIC URL FROM SERVER
				m_bGotDynamicTokenDone = true;
				Debug.Log ("<Token & Dynamic URL from Server>");
			}
		} 
		//DYNAMIC URL
		//Keep getting webplayer dynamicURL and dynamicURL token
		//Debug.Log (">>>> TOKEN - bGotDynamicTo" + m_bGotDynamicTokenDone + "<>" + !Application.isEditor );
		if(m_bGotDynamicTokenDone && instance.m_dynamiURLRetriever.isDone){
			//Clearing the Dynamic URL to make sure it has to take one 
			//For webplayer build only
			//if(!Application.isEditor){//CHANGES_KEY_NEW_JSON_SCHEMA
			if (!m_bGotDynamicURLDone && instance.m_dynamiURLRetriever.isDone) {
				//Debug.Log (">>>> TOKEN - IN");
				instance.GetTokenAndDynamicURL_NewJsonSchema ();
				m_bGotDynamicURLDone = true;
				Debug.Log ("<Token & Dynamic URL from Server>");
			}
			//}//CHANGES_KEY_NEW_JSON_SCHEMA
		}
		  
		// CHANGE THE SAVE POINT TEST PORPOUSES  
		/*if (m_bGotDynamicTokenDone && !m_bGotLoadAvatarDone && m_bGotDynamicURLDone) {
			StartCoroutine (instance.savePointToServer_NewJsonSchema (3, "[0,0,0]", "L3GameMid1", false));
		}*/

		//"LevelsSP2"
		//"levelEnd"
		//"L2Game"
		//"L2GameMid1"
		//"L2GameMid2"
		// CHANGE THE SAVE POINT TEST PORPOUSES

		if(m_bGotDynamicTokenDone && !m_bGotLoadAvatarDone && m_bGotDynamicURLDone){//CHANGES_KEY_NEW_JSON_SCHEMA INSERT
			StartCoroutine(instance.ReadUserLevelDataFromServer_NewJsonSchema(Global.CurrentLevelNumber));//CHANGES_KEY_NEW_JSON_SCHEMA INSERT
			Debug.Log("<Character from Server>");
			m_bGotLoadAvatarDone = true;
		}//CHANGES_KEY_NEW_JSON_SCHEMA INSERT

		if(instance.m_sGameDynamicToken != "" && instance.m_ProcessDataGetRetriever == null && m_bGotDynamicURLDone && instance.m_LevelDataGetRetriever.isDone){
				//LOAD PROGRESS, Level num and Score
				StartCoroutine(instance.ReadUserProgressDataFromServer_NewJsonSchema(Global.CurrentLevelNumber));
				Debug.Log("<Progress from Server>");
			}
			
		if(instance.m_ProcessDataGetRetriever != null && instance.m_bFoundLevelNumber && m_bGotDynamicURLDone && instance.m_ProcessDataGetRetriever.isDone){
				if(Global.CurrentLevelNumber > 1){
					//LOAD NOTEBOOK CONTENT IN GAME
				if(instance.m_ProcessNotebookDataGetRetriever == null){
						StartCoroutine(instance.ReadUserProgressNoteBookDataFromServer_NewJsonSchema(Global.CurrentLevelNumber));
						Debug.Log("<Notebook from Server>");
					}
				} 
			}  
			//save point loading call
		if (instance.m_sLoadURLReturn == null && instance.m_sGameDynamicToken != "" && m_bGotDynamicURLDone && instance.m_ProcessDataGetRetriever != null) {
			if (instance.m_ProcessDataGetRetriever.isDone) {
				Debug.Log ("Call - loadSavePointFromServer");
				StartCoroutine (instance.loadSavePointFromServer_NewJsonSchema ());
				Debug.Log ("<Saving point>");
			}
		}
		//} //CHANGES_KEY_NEW_JSON_SCHEMA

		//turn off the start menus if the player got the gem, only when the guide scene loads again
		if(Global.LevelSVisited && Application.loadedLevelName == "GuideScene"){
			this.enabled = false;
			GetComponent<LogosScreen>().enabled = false;
			GetComponent<TalkScenes>().enabled  = true;
		}


	}

	/*
	*
	* This function gets the token from the config file
	*
	*/
	public void setConfig(string _gameConfigUrl)
	{		
		//Get the token sent from the config file
		int equalSignIndex = _gameConfigUrl.IndexOf('=');
		string token = _gameConfigUrl.Substring(equalSignIndex + 1);
		instance.m_sGameLoginToken = token;
		
		//Get dynamic URL here(Only works for webplayer)
		m_sGameConfigURL = _gameConfigUrl;
		Debug.Log ("DEBUG @ setConfig: " + m_sGameConfigURL + ". (token is:" + token + ")");
	}
	
	/*
	*
	* OnGUI Function.
	*
	*/
	public void OnGUI () 
	{
		GUI.depth = -1;
		GUI.skin = m_skin;
		
		// start the logo flash black background
		GUI.Box(new Rect(0,0,Screen.width,Screen.height),"","SplashScreenBackground");	
	}

}
