using UnityEngine;
using System;
using System.Collections;

public class LevelNumScreen : MonoBehaviour {

	//Public variables
	public GUISkin m_skin = null;
	
	//Private variables
	private bool m_bLogoFadeInStart   = true;
	private float m_fFadeInStartTime  = 0.0f;
	private float m_fLogoStayTime     = 0.0f;
	private bool m_bLogoFadeOutStart  = true;
	private float m_fFadeOutTime      = 0.0f;
	private float m_fFadeInLogoAlpha  = 0.0f;
	private float m_fFadeOutLogoAlpha = 1.0f;
	
	private bool m_bScriptIsKilled = false;
	
	private CapturedDataInput instance;
	
	void Awake(){
		//instance = CapturedDataInput.GetInstance;
		instance = GameObject.Find("CapturedDataInputHolder").GetComponent<CapturedDataInput>();
		if(LogoBackgroundScreen.m_bGotDynamicTokenDone &&  LogoBackgroundScreen.m_bGotDynamicURLDone){//CHANGES_KEY_NEW_JSON_SCHEMA INSERT
			StartCoroutine(instance.ReadUserLevelDataFromServer_NewJsonSchema(Global.CurrentLevelNumber));//CHANGES_KEY_NEW_JSON_SCHEMA COMMENT
		}//CHANGES_KEY_NEW_JSON_SCHEMA INSERT
	}
	
	
	/*
	*
	* Start function
	*
	*/
	public void Start () {
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
	}
	
	/*
	*
	* Update function
	*
	*/
	public void Update () {
	}
	
	public void OnEnable(){
		m_fFadeInStartTime = Time.time;
		if(!string.IsNullOrEmpty(Global.loadedSavePoint)){
			//loadLevel(Global.savePointLoadLevelNumber, Global.loadSavePoint);
			Debug.Log ("Loading Save Point");
			LoadOtherSPs();
		}
	}
	
	/*
	*
	* Fade In Wonders Logo
	* Display the fist splash screen image
	*/
	public void OnGUI(){
		if(m_bScriptIsKilled == false){
			GUI.depth = -2;
			GUI.skin = m_skin;
			
			if(m_bLogoFadeInStart){
				m_fFadeInLogoAlpha = Mathf.Lerp(0.0f, 1.0f, (Time.time - m_fFadeInStartTime) * 0.3f);
				GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, m_fFadeInLogoAlpha);
				m_fLogoStayTime = Time.time;
			}
			
			if(m_fFadeInLogoAlpha == 1.0f && m_bLogoFadeInStart){
				m_fLogoStayTime = Time.time;
				m_bLogoFadeInStart = false;
			}
			
			//Keep the logo stay on the screen for 5 secs, then start to fade out again
			if(Time.time - m_fLogoStayTime > 2.0f && m_bLogoFadeOutStart){
				m_fFadeOutTime = Time.time;
				m_bLogoFadeOutStart = false;
			}
			
			if(m_bLogoFadeOutStart == false){
				m_fFadeOutLogoAlpha = Mathf.Lerp(1.0f, 0.0f, (Time.time - m_fFadeOutTime) * 0.3f);
				GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, m_fFadeOutLogoAlpha);
			}
			
			GUI.Label(new Rect(0,0,Screen.width,Screen.height), "", "LevelNumberScreen");
			try{
				m_skin.FindStyle("Label").fontSize = 60;
			} catch (Exception e){
				Debug.Log("ERROR - Something about a label");
			}
			GUI.Label(new Rect(Screen.width*0.3958f,Screen.height*0.4667f,Screen.width*0.7552f,Screen.height*0.1667f), "Level " + Global.CurrentLevelNumber);
			
			if(m_fFadeOutLogoAlpha == 0.0f){
				m_bScriptIsKilled = true;
				
				GetComponent<LogoBackgroundScreen>().enabled = false;
				
				m_skin.FindStyle("Label").fontSize = 16;
				if(Application.loadedLevel == 0 && Global.levelComplete == true && Global.CurrentLevelNumber != 1){
					Debug.Log ("Not level 1, Guide Scene, Level Complete = true. Talk Scenes enabled");
					GetComponent<TalkScenes>().enabled = true;
				}else if(Application.loadedLevel == 0 && Global.loadedSavePoint == ""){
					Debug.Log ("Guide Scene, save point empty. Talk Scenes enabled");
					GetComponent<TalkScenes>().enabled = true;
				}
				this.enabled = false;
			}
		}
	}

	//Load right scene base on SP code
	public void LoadOtherSPs()
	{
		if(!Global.levelComplete){
			//Load levels
			if (Global.loadedSavePoint.Contains("Levels")) {
				if(Global.loadedSavePoint.Contains("LevelsSP2")){
					Global.m_bGotCurrentLevelGem = true;
					Global.LevelSVisited = true;
				}
				
				if(Global.CurrentLevelNumber == 1){
					Global.CurrentInteractNPC = "guardian";
					GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentScene = "GuideEndTransition";
					GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled = false;
				}
				if(Global.CurrentLevelNumber == 2){
					GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentScene = "GuideEndTransition";
					GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled = false;
				}
				Application.LoadLevel("LevelS");
				Debug.Log ("Loading Levels...");
			}
			
			//Load Game level
			if (Global.loadedSavePoint.Contains("L1Game")) {
				Application.LoadLevel("Level1_Cave");
				Debug.Log ("Loading Level1_Cave...");
			}
			if (Global.loadedSavePoint.Contains("L2Game")) {
				Application.LoadLevel("Level2_Ice");
				Debug.Log ("Loading Level2_Ice...");
			}
			if (Global.loadedSavePoint.Contains("L3Game")) {
				Application.LoadLevel("Level3_Lava");
				Debug.Log ("Loading Level3_Lava...");
			}
			if (Global.loadedSavePoint.Contains("L4Game")) {
				Application.LoadLevel("Level4_Cliff");
				Debug.Log ("Loading Level4_Cliff...");
			}
			if (Global.loadedSavePoint.Contains("L5Game")) {
				Application.LoadLevel("Level5_Swamp");
				Debug.Log ("Loading Level5_Swamp...");
			}
			if (Global.loadedSavePoint.Contains("L6Game")) {
				Application.LoadLevel("Level6_Bridge");
				Debug.Log ("Loading Level6_Bridge...");
			}
			if (Global.loadedSavePoint.Contains("L7Game")) {
				Application.LoadLevel("Level7_Canyon");
				Debug.Log ("Loading Level7_Canyon...");
			}
		}
	}
}
