using UnityEngine;
using System.Collections;

public class FeelingDownScreen : MonoBehaviour {

	public GUISkin m_skin = null;
	
	
	public Texture2D m_tFeelingDownTexture;
	
	
	// Use this for initialization
	void Start () {
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		
		//m_tFeelingDownTexture = (Texture2D)Resources.Load ("UI/mg_l2_s_mg2", typeof(Texture2D));
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	/*
	*
	* OnGUI Function.
	*
	*/
	public void OnGUI () 
	{		
		GUI.skin = m_skin;
		GUI.depth = -1;
		
		GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
		GUI.Label(new Rect(155.0f, 55.0f, 650.0f, 501.0f), m_tFeelingDownTexture);
		GUI.Label(new Rect(210.0f, 200.0f, 200.0f, 20.0f),"Feeling Down");
		GUI.Label(new Rect(640.0f, 280.0f, 200.0f, 20.0f),"No Energy");
		GUI.Label(new Rect(270.0f, 360.0f, 200.0f, 20.0f),"Doing Less");
		GUI.Label(new Rect(580.0f, 440.0f, 200.0f, 20.0f),"Feeling Worse");
		
		if(GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
					Global.arrowKey = "";

			this.enabled = false;
			
			GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled = true;
			//Update the current scene name
			UpdateCurrentSceneName();
		}
	}
	
	/*
	*
	* This function updates the current scene name after click the next button on the image
	* ThoughsAndFeeling image screen.
	*
	*/
	public void UpdateCurrentSceneName()
	{
		//Before go to the island scene
		if(Global.LevelSVisited == false){
			//level 1
			if(Global.CurrentLevelNumber == 1){
				
			}
			//level 2
			if(Global.CurrentLevelNumber == 2){
				GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentScene = "L2GuideScene11";
			}
		}
		//After visited the island scene
		else{
			//level 1
			if(Global.CurrentLevelNumber == 1){
				
			}
			//level 2
			if(Global.CurrentLevelNumber == 2){
				
			}
		}
	}
}
