using UnityEngine;
using System.Collections;

public class ResultBarchart : MonoBehaviour {
	
	public TextAsset m_xmlFilePathTextAsset;
	public GUISkin m_skin = null;
	
	public Texture2D m_tBarchartLines;
	public Texture2D m_tBarchartBar;
	public Texture2D[] m_tBarColours;
	public Texture2D m_tBarGreen;
	public Texture2D m_tBarOrange;
	public Texture2D m_tBarRed;

	private bool m_bShowBarchartText = false;
	private TalkScenes m_talkScene;
	private string displayText = "";
	
	private int severityScore = 0;
	private int questionCount = 0;
	
	private float[] scorePercentage;
	
	static int m_iLevel1Score = 14;
	static int m_iLevel4Score = 14;
	static int m_iLevel7Score = 14;
	
	private Rect m_rBarRect1;
	private Rect m_rBarRect4;
	private Rect m_rBarRect7;
	
	
	// Use this for initialization
	void Start () {
		//m_skin           = (GUISkin) Resources.Load("Skins/SparxSkin");
		//m_tBarchartLines = (Texture2D)Resources.Load ("UI/moodgraph_bg", typeof(Texture2D));
		//m_tBarchartBar   = (Texture2D)Resources.Load ("UI/moodgraph_bar", typeof(Texture2D));
		
		//m_tBarColours = new Texture2D[3]{(Texture2D)Resources.Load("UI/moodgraph_bar_green", typeof(Texture2D)), 
										 //(Texture2D)Resources.Load("UI/moodgraph_bar_orange", typeof(Texture2D)), 
										 //(Texture2D)Resources.Load("UI/moodgraph_bar_red", typeof(Texture2D))};
		m_tBarColours = new Texture2D[3]{m_tBarGreen, m_tBarOrange, m_tBarRed};
	}
	
	// Update is called once per frame
	void Update () {

	}
	
	/*
	*
	* OnEnable Function.
	*
	*/
	public void OnEnable()
	{
		scorePercentage = new float[3];
		//NOTE: The barchart data is loaded from the server
		//from the begining of  the game, it is in the LogosScreen
		//file
		
		if (Global.CurrentLevelNumber > 1){
			m_iLevel1Score = Global.userFeedBackScore[0];
		}
		if (Global.CurrentLevelNumber > 4){
			m_iLevel4Score = Global.userFeedBackScore[1];
		}
		
		if(Global.CurrentLevelNumber == 1){
			Global.userFeedBackScore[0] = severityScore;
		}
		if(Global.CurrentLevelNumber == 4){
			Global.userFeedBackScore[1] = severityScore;
		}
		if(Global.CurrentLevelNumber == 7){
			Global.userFeedBackScore[2] = severityScore;
		}
		
		BarsDisplay(Global.CurrentLevelNumber);
		
		severityScore = 0;
		
		//Get the talk scene component
		m_talkScene = GameObject.Find("GUI").GetComponent<TalkScenes>();
	}
	
	/*
	*
	* OnGUI Function.
	*
	*/
	public void OnGUI () {		
		GUI.skin = m_skin;
		GUI.depth = -1;
		
		GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
		
		//Display the bar chart here
		GUI.Label(new Rect(120.0f, 0.0f, 960.0f, 600.0f), m_tBarchartLines);

		//Define the rect for the (bar)
		GUIStyle barStyle = m_skin.GetStyle("Bars");
		
		barStyle.normal.background = SetTextureByScore(m_iLevel1Score);
		barStyle.border.top = (int)(18.0f * scorePercentage[0]);
		barStyle.border.bottom = (int)(18.0f * scorePercentage[0]);
		GUI.Label(m_rBarRect1, "", "Bars");
		if (Global.CurrentLevelNumber >= 4)
		{
			barStyle.normal.background = SetTextureByScore(m_iLevel4Score);
			barStyle.border.top = (int)(18.0f * scorePercentage[1]);
			barStyle.border.bottom = (int)(18.0f * scorePercentage[1]);
			GUI.Label(m_rBarRect4, "", "Bars");
			
			if (Global.CurrentLevelNumber >= 7)
			{
				barStyle.normal.background = SetTextureByScore(m_iLevel7Score);
				barStyle.border.top = (int)(18.0f * scorePercentage[2]);
				barStyle.border.bottom = (int)(18.0f * scorePercentage[2]);
				GUI.Label(m_rBarRect7, "", "Bars");
			}
		}
		
		
		//Display the ranking numbers in the bar chart
		GUI.Label(new Rect(280.0f, 430.0f, 100.0f, 20.0f), "Level 1");
		GUI.Label(new Rect(484.0f, 430.0f, 100.0f, 20.0f), "Level 4");
		GUI.Label(new Rect(688.0f, 430.0f, 100.0f, 20.0f), "Level 7");
		
		// bar chart labels
		//GUI.Label(new Rect(170.0f, 220.0f, 100.0f, 20.0f), "M\n o\n o\n d");
		GUI.Label(new Rect(90.0f, 245.0f, 140.0f, 20.0f), "Level of\nstress/depression","CenteredFont");
		GUI.Label(new Rect(465.0f, 460.0f, 150.0f, 20.0f), "SPARX levels");
		
		//The next button
		if(GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
					Global.arrowKey = "";

			//update the current scene name
			UpdateCurrentSceneName();
			//Load the guide talk scene again
			GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;
			
			this.enabled =false;
		}
	}
	
	/*
	*
	* This function updates the current scene name after click the next button on the image
	* ThoughsAndFeeling image screen.
	*
	*/
	public void UpdateCurrentSceneName()
	{
		if(Global.LevelSVisited == false)
		{
			int Level1Answer = GetAnswerValue(Global.userFeedBackScore[0]);
			
			//Level 1
			if(Global.CurrentLevelNumber == 1)
			{
				if(Level1Answer == 0)
				{
					GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentScene = "GuideScene9_A1";
				}
				else if(Level1Answer == 1)
				{
					GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentScene = "GuideScene9_A2";
				}
				else if(Level1Answer == 2)
				{
					GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentScene = "GuideScene9_A3";
				}
                else
                {
                    GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentScene = "GuideScene9_A5";
                }
                return;
                //else if(Level1Answer == 3)
				//{
				//	GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentScene = "GuideScene9_A4";
				//}
			}
			
			int Level4Answer = GetAnswerValue(Global.userFeedBackScore[1]);
			
			if(Global.CurrentLevelNumber == 4)
			{
				if(Level4Answer == 0)
				{
					//ANGELA CHONG ASK TO REMOVE THE COMPARISION BETWEEN LEVELS TOTAL SCOREs
					//if (Level1Answer >= Level4Answer)
					//{
						GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentScene = "L4GuideScene9_A1";
					//}
					//else
					//{
					//	GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentScene = "L4GuideScene9_A2";
					//}
				}
				else if(Level4Answer == 1)
				{
					//if (Level1Answer >= Level4Answer)
					//{
					//	GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentScene = "L4GuideScene9_A3";
					//}
					//else
					//{
						GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentScene = "L4GuideScene9_A4";
					//}
				}
				else if(Level4Answer == 2)
				{
					//if (Level1Answer >= Level4Answer)
					//{
					//	GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentScene = "L4GuideScene9_A5";
					//}
					//else
					//{
						GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentScene = "L4GuideScene9_A6";
					//}
				}
				else// if(Level4Answer == 3)
				{
					/*if (Level1Answer >= Level4Answer)
					{
						GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentScene = "L4GuideScene9_A7";
					}
					else
					{
						GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentScene = "L4GuideScene9_A8";
					}
				}
				else
				{*/
					GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentScene = "L4GuideScene9_A9";
				}
				Debug.Log ("PHQA Scene: " + GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentScene);
				return ;
			}
			
			if(Global.CurrentLevelNumber == 7)
			{
				if(Global.userFeedBackScore[2] < 5)
				{
					GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentScene = "L7GuideScene9_A1";
				}
				else if(Global.userFeedBackScore[2] < 10)
				{
					GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentScene = "L7GuideScene9_A2";
				}
				else if(Global.userFeedBackScore[2] < 15)
				{
					GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentScene = "L7GuideScene9_A3";
				}
				else// if(Global.userFeedBackScore[2] < 20)
				{
					/*GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentScene = "L7GuideScene9_A4";
				}
				else
				{*/
					GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentScene = "L7GuideScene9_A5";
				}
			}
		}
	}
	
	int GetAnswerValue(int score)
	{
		if (score < 5)
		{
			return (0);
		}
		else if (score < 10)
		{
			return (1);
		}
		else if (score < 15)
		{
			return (2);
		}
		else if (score < 20)
		{
			return (3);
		}
		else
		{
			return (4);
		}
	}

	/*
	*
	* This function is used to display the bars in the bar chart
	*/
	public void BarsDisplay(int _currentLevelNum)
	{
		if (_currentLevelNum == 1)
		{
			m_iLevel1Score = Global.userFeedBackScore[0];
		}
		else if (_currentLevelNum == 4)
		{
			m_iLevel4Score = Global.userFeedBackScore[1];
		}
		else if (_currentLevelNum == 7)
		{
			m_iLevel7Score = Global.userFeedBackScore[2];
		}
		
		scorePercentage[0] = (float)(m_iLevel1Score + 1) / ((float)(questionCount * 3) + 1);
		float temp = scorePercentage[0] * 300.0f;
		m_rBarRect1 = new Rect(290.0f, 410.0f - temp, 30.0f, temp);
		
		scorePercentage[1] = (float)(m_iLevel4Score + 1) / ((float)(questionCount * 3) + 1);
		temp = scorePercentage[1] * 300.0f;
		m_rBarRect4 = new Rect(495.0f, 410.0f - temp, 30.0f, temp);
		
		scorePercentage[2] = (float)(m_iLevel7Score + 1) / ((float)(questionCount * 3) + 1);
		temp = scorePercentage[2] * 300.0f;
		m_rBarRect7 = new Rect(697.0f, 410.0f - temp, 30.0f, temp);
	}
	
	public void AddScore(int iScore)
	{
		severityScore += iScore;
		++questionCount;
	}
	
	public void RemoveScore(int iScore)
	{
		severityScore -= iScore;
		--questionCount;
	}
	
	Texture2D SetTextureByScore(int score)
	{
		if (score > 18)
		{
			return (m_tBarColours[2]);
		}
		else if (score > 9)
		{
			return (m_tBarColours[1]);
		}
		else
		{
			return (m_tBarColours[0]);
		}
	}
}
