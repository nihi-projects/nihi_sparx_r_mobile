using UnityEngine;
using System.Collections;

public class L5EnterTexts : MonoBehaviour {

	public GUISkin m_skin = null;
	
	static private float m_inputWidth = 200.0f;
	static private float m_inputHeight = 30.0f;
	private Rect[] m_inputRects = 
	{
		new Rect(380.0f, 210.0f, m_inputWidth, m_inputHeight),
		new Rect(380.0f, 240.0f, m_inputWidth, m_inputHeight),
		new Rect(380.0f, 270.0f, m_inputWidth, m_inputHeight),
		new Rect(380.0f, 300.0f, m_inputWidth, m_inputHeight),
		new Rect(380.0f, 330.0f, m_inputWidth, m_inputHeight),
		new Rect(380.0f, 360.0f, m_inputWidth, m_inputHeight),
	};
	
	static public string[] m_inputTexts = 
	{
		"Click here to enter text.", 
		"Click here to enter text.", 
		"Click here to enter text.", 
		"Click here to enter text.", 
		"Click here to enter text.", 
		"Click here to enter text."
	};
	
	// Use this for initialization
	void Start () 
	{
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
	
	/*
	*
	* OnGUI Function.
	*
	*/
	public void OnGUI () 
	{		
		GUI.skin = m_skin;
		GUI.depth = -1;
		
		// the background of the mingame
		GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
		
		GUI.Label(new Rect(180, 150, 600, 30), "I SPOT these annoying Gnats (Gloomy Negative Automatic Thoughts):","Titles");

		// draw the text input fields
		for (int i = 0; i < m_inputRects.Length; ++i)
		{
			m_inputTexts[i] = GUI.TextField(m_inputRects[i], m_inputTexts[i], 44);
		}
				
		if(GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
					Global.arrowKey = "";

			this.enabled = false;
			
			GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;
			//Update the current scene name
			GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentScene = "L5GuideScene26";
			
		}
	}
}
