using UnityEngine;
using System.Collections;

public class L4EnterTexts : MonoBehaviour {

	public GUISkin m_skin = null;
	
	static private float m_inputWidth = 300.0f;
	static private float m_inputHeight = 30.0f;
	static public bool m_bTriggered = false;
	private Rect m_inputRects = new Rect(340.0f, 250.0f, m_inputWidth, m_inputHeight);
	
	static public string m_inputTexts = "Click here to enter text";
	
	// Use this for initialization
	void Start () 
	{
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
	}
	
	// Update is called once per frame
	void Update () 
	{
		m_bTriggered = true;
	}

	/*
	*
	* OnGUI Function.
	*
	*/
	public void OnGUI () 
	{
		GUI.skin = m_skin;
		GUI.depth = -1;
		
		// the background of the mingame
		GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
		
		GUI.Label(new Rect(180, 150, 600, 30), "Enter the problem that \nyou are currently struggling with:","Titles");

		// draw the text input fields
		GUI.SetNextControlName ("text1");
		m_inputTexts = GUI.TextField(m_inputRects, m_inputTexts, 64);
		if (GUI.GetNameOfFocusedControl () != "") {
			m_inputTexts = Global.doTextFieldCheck ("text1", m_inputTexts, m_inputRects);
		}

		if(GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
					Global.arrowKey = "";

			this.enabled = false;
			
			GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;
			//Update the current scene name
			GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentScene = "L4GuideScene25";
			
		}
	}
}
