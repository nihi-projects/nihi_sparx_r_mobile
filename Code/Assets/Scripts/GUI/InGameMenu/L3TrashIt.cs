using UnityEngine;
using System.Collections;

public class L3TrashIt : MonoBehaviour {

	public GUISkin m_skin = null;
	
	public Texture2D m_tTrashItTexture;
	
	// Use this for initialization
	void Start () {
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		
		//m_tTrashItTexture = (Texture2D)Resources.Load ("UI/mg_l3_e_mg3", typeof(Texture2D));
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	/*
	*
	* OnGUI Function.
	*
	*/
	public void OnGUI () 
	{		
		GUI.skin = m_skin;
		GUI.depth = -1;
		
		GUI.Label(new Rect(90, 90, 400, 30), "Unhelpful thought? Trash it!");
		GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
		GUI.Label(new Rect(155.0f, 55.0f, 650.0f, 501.0f), m_tTrashItTexture);
		GUI.Label(new Rect(360.0f, 340.0f, 240.0f, 20.0f),"Trash it","PictureText");
		
		if(GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
					Global.arrowKey = "";

			this.enabled = false;
			
			GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;
			//Update the current scene name
			GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentScene = "L3GuideScene32";
		}
	}
}
