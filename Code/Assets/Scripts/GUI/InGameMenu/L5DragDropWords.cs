using UnityEngine;
using System.Collections;

public class L5DragDropWords : MonoBehaviour 
{
	Rect k_zeroRect = new Rect(0.0f, 0.0f, 0.0f, 0.0f);
	// publics
	public GUISkin m_skin = null;

	
	// privates
	private string m_conversationBoxText = "";
	
	private Vector2[] m_KeywordDimensions = 
	{
		new Vector2(80.0f, 30.0f), // Disaster
		new Vector2(80.0f, 30.0f), // Downer
		new Vector2(80.0f, 30.0f), // All-or-Nothing
		new Vector2(80.0f, 30.0f), // Guilty
		new Vector2(80.0f, 30.0f), // Perfectionist
		new Vector2(80.0f, 30.0f), // Mind Reader
	};
	
	static private float m_collisiondimension = 10.0f;
	private DraggableObject[] m_wordsRects = 
	{
		new DraggableObject(new Rect(120.0f, 150.0f, 130.0f, 30.0f), 
			"Disaster"),
		new DraggableObject(new Rect(120.0f, 200.0f, 130.0f, 30.0f), 
			"Downer"),
		new DraggableObject(new Rect(120.0f, 250.0f, 130.0f, 30.0f), 
			"All-or-Nothing"),
		new DraggableObject(new Rect(120.0f, 300.0f, 130.0f, 30.0f), 
			"Guilty"),
		new DraggableObject(new Rect(120.0f, 350.0f, 130.0f, 30.0f), 
			"Perfectionist"),
		new DraggableObject(new Rect(120.0f, 400.0f, 130.0f, 30.0f), 
			"Mind Reader"),
	};
	
	//KeyWord destination positions 
	static private float m_dextinationRectWidth = 260.0f;
	static private float m_dextinationRectHeight = 155.0f;
	
	private Rect[] m_destinationRect = 
	{
		new Rect(300.0f, 67.0f, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(300.0f, 210.0f, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(300.0f, 380.0f, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(570.0f, 87.0f, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(570.0f, 240.0f, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(570.0f, 360.0f, m_dextinationRectWidth, m_dextinationRectHeight)
/*
		new Rect(Screen.width*0.2930f, Screen.height*0.1463f, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(Screen.width*0.2930f, Screen.height*0.3746f, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(Screen.width*0.2930f, Screen.height*0.5708f, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(Screen.width*0.5566f, Screen.height*0.1463f, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(Screen.width*0.5566f, Screen.height*0.3746f, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(Screen.width*0.5566f, Screen.height*0.5708f, m_dextinationRectWidth, m_dextinationRectHeight)
*/		
	};

	private string[] m_destinationText = 
	{
		"Attemps to make you feel guilty about things you have no control over.\n'We are losing every game and it's my fault'.", 
		"Only perfect will do.\n'I can't miss a single practise this season'\nor\n'We have to win all our games'.\n", 
		"Ignore the positives and only see things that go wrong, not right. It's the opposite of a silver lining.\n'We won our game but anyone could beat that team'.\n", 
		"Blows everything out of proportion.\n'We lost this game, it's the end of the world!'\n", 
		"Everything is seen as extremes (often has words like 'always' or 'never'), it's either wonderful or horrible, with nothing in between.\n'The referee is always unfair to our team'. or 'if someone isn’t nice to me at all times, then they are not my real friend'", 
		"We think we know what is going to happen and what everyone else is thinking.\n'I bet I won't make the team because the coach thinks I'll drop the ball' or 'He doesn’t like me, I bet he thinks I’m ugly'."
	};
	
	private Vector2[] m_finalTextPos = 
	{
		new Vector2(430.0f, 140.0f),
		new Vector2(430.0f, 250.0f),
		new Vector2(430.0f, 420.0f),
		new Vector2(700.0f, 140.0f),
		new Vector2(700.0f, 250.0f),
		new Vector2(700.0f, 420.0f)
/*
		new Vector2(Screen.width*0.4199f, Screen.height*0.3050f),
		new Vector2(Screen.width*0.4199f, Screen.height*0.5182f),
		new Vector2(Screen.width*0.4199f, Screen.height*0.7135f),
		new Vector2(Screen.width*0.6836f, Screen.height*0.2700f),
		new Vector2(Screen.width*0.6836f, Screen.height*0.5182f),
		new Vector2(Screen.width*0.6836f, Screen.height*0.7357f)
*/
	};
	
	private int[] m_correctIndex = 
	{
		3,
		4, 
		1, 
		0, 
		2, 
		5
	};
	
	private bool[] m_placed = 
	{
		false, false, false, false, false, false
	};
	
	private string m_currentClickedString = "";
	
	private int m_currentIndex = -1;
	
	private bool b_dragging = false;
	
	public Texture2D m_tConversationBackground;
	
	// Use this for initialization
	void Start () 
	{		
		Global.levelStarted = false;
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		//m_tConversationBackground  = (Texture2D)Resources.Load ("UI/talk_paper", typeof(Texture2D));
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (b_dragging)
		{
			// check to see if the moveable rect is found
			if (m_currentIndex != -1)
			{
				// save the mouse pos for ease of access
				float mouseX = Input.mousePosition.x;
				float mouseY = Input.mousePosition.y;
				m_wordsRects[m_currentIndex].movable.Set(mouseX - m_KeywordDimensions[m_currentIndex].x/2, 
												     mouseY * (-1) + Screen.height - m_KeywordDimensions[m_currentIndex].y/2, 
												     m_collisiondimension, 
												     m_collisiondimension);
			}
		}
		else
		{
			KeyWordDrag();
		}
		
		DestinationDrop();
	}
	
	public void OnGUI () 
	{		
		GUI.skin = m_skin;
		GUI.depth = -1;
		
		// the background of the mingame
		GUI.Label(new Rect(0.0f, 0.0f, 960.0f, 600.0f), "", "FullSizeDialogBox");
		
		// The middle area
		GUI.Label(new Rect(180, 50, 600, 80), "Match each Gnat (negative thought) to its definition and example:", "Titles");
		bool done = true;
		for (int i = 0; i < m_destinationRect.Length; ++i)
		{
			GUI.Label(m_destinationRect[i], "", "BoxMatch");
			//GUI.Box(m_destinationRect[i], "");
			GUI.Label(m_destinationRect[i], m_destinationText[i], "CenteredFont12");
			
			if (!m_placed[i])
			{
				done = false;
			}
		}
		
		for (int i = 0; i < m_wordsRects.Length; ++i)
		{
			Rect temp = new Rect(m_wordsRects[i].movable.x, m_wordsRects[i].movable.y, 
								m_wordsRects[i].original.width, m_wordsRects[i].original.height);
			GUI.Label(temp, m_wordsRects[i].words, "CenteredFont");
		}
		
		//The next button
		if (true == done)
		{
			if(GUI.Button(new Rect(800, 495, 60 ,60), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
					Global.arrowKey = "";

				this.enabled = false;
				
				GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;
				GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentScene = "L5GuideScene21";
			}
		}
	}
	
	/*
	 * 
	 * Thie function drag the keyword and move with the mouse.
	 * 
	*/
	public void KeyWordDrag()
	{
		// release early if there is no active keyword box being moved.
		if (m_currentIndex != -1) { return ;}
		
		//If the mouse button is clicked and if the mouse position is on the keywords
		if(Input.GetMouseButton(0))
		{
			// save the mouse pos for ease of access
			float mouseX = Input.mousePosition.x;
			float mouseY = Input.mousePosition.y;
			
			// boolean to see if a box has been clicked on.
			bool clicked = false;
			
			// loop through all the original rects to see if the user has clicked on one of the boxes.
			for (int i = 0; i < m_wordsRects.Length; ++i)
			{
				if (m_wordsRects[i].original.Contains(new Vector2(mouseX, Screen.height - mouseY)))
				{
					// set the current active key text
					// check if the current text has been set
					if (m_currentClickedString == "")
					{
						m_currentClickedString = m_wordsRects[i].words;
						m_currentIndex = i;
						clicked = true;
						b_dragging = true;
						break;
					}
				}
			}	
			
			// this is an early release from the function
			if (clicked == true) { return ;}
		}
	}
	
	/*
	 * 
	 * Thie function drop the keyword to the destination area.
	 * 
	*/
	public void DestinationDrop()
	{
		// release early if there is no active keyword box being moved.
		if (m_currentIndex == -1) { return ;}
		
		//If the mouse button is UnClicked and if  the mouse position is on the fill field
		if(Input.GetMouseButtonUp(0))
		{
			b_dragging = false;
			
			// boolean for if the text was placed in the box.
			bool successful = false;
			
			// loop through the destination rects to see which box the word is being placed in
			for (int i = 0; i < m_destinationRect.Length; ++i)
			{
				float mouseX = Input.mousePosition.x;
				float mouseY = Input.mousePosition.y;
				
				// check if this destination box contains the current clicked keyword box
				if(m_destinationRect[i].Contains(new Vector2(mouseX, Screen.height - mouseY)))
				{
					// if the destination text is empty
					if(m_correctIndex[i] == m_currentIndex)
					{
						// set the destination text and remove the clicked text.
						m_currentClickedString = "";
						successful = true;
						m_placed[i] = true;
						
						float width = m_wordsRects[m_currentIndex].original.width;
						float height = m_wordsRects[m_currentIndex].original.height;
						m_wordsRects[m_currentIndex].movable.Set(m_finalTextPos[i].x - width/2, 
																 m_finalTextPos[i].y - height/2, 
																 width, height);

						m_currentIndex = -1;
					}
					break;
				}
			}
			
			// another early release because everything is done already
			if (successful) { return ;}
			
			// execute this code if the user missed the destination box.
			ResetCurrentString();
		}
	}
	
	/*
	 * 
	 * This function resets the missed keyword text and text rect back to their original positions
	 * 
	 */
	private void ResetCurrentString()
	{
		// set the movable rect to the original area, set the keyword back to the current,
		// reset the current keyword and break the loop.
		m_wordsRects[m_currentIndex].movable = m_wordsRects[m_currentIndex].original;
		m_currentClickedString = "";
		m_currentIndex = -1;
	}
}
