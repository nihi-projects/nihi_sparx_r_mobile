using UnityEngine;
using System.Collections;

public class Goodbye : MonoBehaviour {
	
	public GUISkin m_skin            = null;
	private bool m_bPlayerIsWaving    = false;
	private bool m_bShowGoodbyeScreen = true;
	private bool m_bSavePointRoutine = false;
	public bool m_bGameWasQuit;
	private CapturedDataInput instance;
	
	// Use this for initialization
	void Start () 
	{
		m_bSavePointRoutine = false;
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		//Connecting to the server
		//instance = CapturedDataInput.GetInstance;
		instance = GameObject.Find("CapturedDataInputHolder").GetComponent<CapturedDataInput>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (m_bPlayerIsWaving && !GameObject.Find ("Guide").GetComponent<Animation> ().IsPlaying ("wave")) {
			if (!m_bGameWasQuit) {
				if (!m_bSavePointRoutine) {
					instance.m_sSaveURLReturn = null;
					SaveToServer ();
					m_bSavePointRoutine = true;
				}
				if (instance.m_sSaveURLReturn.isDone && m_bSavePointRoutine) {
					Debug.Log ("This level is finished!");
					if (!Application.isEditor) {
						Application.ExternalCall ("gameLevelEnd");
					}
					this.enabled = false;
				}
			} else {
				Debug.Log ("This level was quit!");
				Debug.Log ("This level is finished!");
				if (!Application.isEditor) {
					Application.ExternalCall ("gameLevelEnd");
				}
				this.enabled = false;
			}
		}
	}

	void SaveToServer(){
		
		Debug.Log("trigger save point saving.");
		
		bool levelComplete = true;
		string currentSavePoint = "levelEnd";
		string feedbackData = "[" + Global.userFeedBackScore[0] + ", " + Global.userFeedBackScore[1] + ", " + Global.userFeedBackScore[2] + "]";
		StartCoroutine(instance.SavePointEventToServer_NewJsonSchema(5,Global.CurrentLevelNumber,currentSavePoint));
		StartCoroutine(instance.savePointToServer_NewJsonSchema(Global.CurrentLevelNumber, feedbackData, currentSavePoint, levelComplete));
	}
	
	public void OnGUI () 
	{		
		if(m_bShowGoodbyeScreen){
			GUI.skin = m_skin;
			GUI.depth = -1;

			GUI.Label(new Rect(0.0f, 0.0f, 960.0f, 600.0f), "", "GoodByeScreen");

			if(GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
				
			//if(GUI.Button(new Rect(800, 495, 60, 60), "", "dilogBoxNextButton") || Global.arrowKey == "right"){

//			GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "GoodByeScreen");
			
//			if(GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
				Global.arrowKey = "";
				GameObject.Find ("Guide").GetComponent<Animation>().Play("wave");
				m_bShowGoodbyeScreen = false;
				m_bPlayerIsWaving = true;
			}
		}
	}
}
