using UnityEngine;
using System.Collections;

public class L6AnalyseNegativeStatement : MonoBehaviour {

	public GUISkin m_skin = null;
	
	static public string[] m_sChoices = {"Click here to enter text", "Click here to enter text", "Click here to enter text", "Click here to enter text"};
	
	// Use this for initialization
	void Start () {
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void OnGUI () 
	{		
		GUI.skin = m_skin;
		GUI.depth = -1;
		
		//1st negative statement analyze
		if(GetComponent<L6NegativeStatement>().m_bNegativeStatementSelectList[0]){
			GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
			GUI.Label(new Rect(270.0f,170.0f,600.0f,200.0f), "Analyse the negative statement you chose using RAPA.\n\nMy life sucks...");
			
			GUI.Label(new Rect(420.0f,253.0f,600.0f,200.0f), "Reality Check:");
			m_sChoices[0] = GUI.TextField(new Rect(270.0f,263.0f,400.0f,30.0f), m_sChoices[0]);
			
			GUI.Label(new Rect(420.0f,307.0f,600.0f,200.0f), "Another view:");
			m_sChoices[1] = GUI.TextField(new Rect(270.0f,317.0f,400.0f,30.0f), m_sChoices[1]);
			
			GUI.Label(new Rect(420.0f,363.0f,600.0f,200.0f), "Perspective:");
			m_sChoices[2] = GUI.TextField(new Rect(270.0f,373.0f,400.0f,30.0f), m_sChoices[2]);
			
			GUI.Label(new Rect(420.0f,417.0f,600.0f,200.0f), "Action:");
			m_sChoices[3] = GUI.TextField(new Rect(270.0f,427.0f,400.0f,30.0f), m_sChoices[3]);
		}
		
		//2nd negative statement analyze
		if(GetComponent<L6NegativeStatement>().m_bNegativeStatementSelectList[1]){
			GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
			GUI.Label(new Rect(270.0f,170.0f,600.0f,200.0f), "Analyse the negative statement you chose using RAPA.\n\nNobody likes me...");
			
			GUI.Label(new Rect(420.0f,253.0f,600.0f,200.0f), "Reality Check:");
			m_sChoices[0] = GUI.TextField(new Rect(270.0f,263.0f,400.0f,30.0f), m_sChoices[0]);
			
			GUI.Label(new Rect(420.0f,307.0f,600.0f,200.0f), "Another view:");
			m_sChoices[1] = GUI.TextField(new Rect(270.0f,317.0f,400.0f,30.0f), m_sChoices[1]);
			
			GUI.Label(new Rect(420.0f,363.0f,600.0f,200.0f), "Perspective:");
			m_sChoices[2] = GUI.TextField(new Rect(270.0f,373.0f,400.0f,30.0f), m_sChoices[2]);
			
			GUI.Label(new Rect(420.0f,417.0f,600.0f,200.0f), "Action:");
			m_sChoices[3] = GUI.TextField(new Rect(270.0f,427.0f,400.0f,30.0f), m_sChoices[3]);
		}
		
		//3nd negative statement analyze
		if(GetComponent<L6NegativeStatement>().m_bNegativeStatementSelectList[2]){
			GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
			GUI.Label(new Rect(270.0f,170.0f,600.0f,200.0f), "Analyse the negative statement you chose using RAPA.\n\nSPARX is useless...");
			
			GUI.Label(new Rect(420.0f,253.0f,600.0f,200.0f), "Reality Check:");
			m_sChoices[0] = GUI.TextField(new Rect(270.0f,263.0f,400.0f,30.0f), m_sChoices[0]);
			
			GUI.Label(new Rect(420.0f,307.0f,600.0f,200.0f), "Another view:");
			m_sChoices[1] = GUI.TextField(new Rect(270.0f,317.0f,400.0f,30.0f), m_sChoices[1]);
			
			GUI.Label(new Rect(420.0f,363.0f,600.0f,200.0f), "Perspective:");
			m_sChoices[2] = GUI.TextField(new Rect(270.0f,373.0f,400.0f,30.0f), m_sChoices[2]);
			
			GUI.Label(new Rect(420.0f,417.0f,600.0f,200.0f), "Action:");
			m_sChoices[3] = GUI.TextField(new Rect(270.0f,427.0f,400.0f,30.0f), m_sChoices[3]);
		}
		
		//4th negative statement analyze
		if(GetComponent<L6NegativeStatement>().m_bNegativeStatementSelectList[3]){
			GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
			GUI.Label(new Rect(270.0f,170.0f,600.0f,200.0f), "Analyse the negative statement you chose using RAPA.\n\nI'll never feel good again...");
			
			GUI.Label(new Rect(420.0f,253.0f,600.0f,200.0f), "Reality Check:");
			m_sChoices[0] = GUI.TextField(new Rect(270.0f,263.0f,400.0f,30.0f), m_sChoices[0]);
			
			GUI.Label(new Rect(420.0f,307.0f,600.0f,200.0f), "Another view:");
			m_sChoices[1] = GUI.TextField(new Rect(270.0f,317.0f,400.0f,30.0f), m_sChoices[1]);
			
			GUI.Label(new Rect(420.0f,363.0f,600.0f,200.0f), "Perspective:");
			m_sChoices[2] = GUI.TextField(new Rect(270.0f,373.0f,400.0f,30.0f), m_sChoices[2]);
			
			GUI.Label(new Rect(420.0f,417.0f,600.0f,200.0f), "Action:");
			m_sChoices[3] = GUI.TextField(new Rect(270.0f,427.0f,400.0f,30.0f), m_sChoices[3]);
		}
		
		//5th negative statement analyze
		if(GetComponent<L6NegativeStatement>().m_bNegativeStatementSelectList[4]){
			GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
			GUI.Label(new Rect(270.0f,170.0f,600.0f,200.0f), "Analyse your own negative statement using RAPA.");
			
			GUI.Label(new Rect(420.0f,253.0f,600.0f,200.0f), "Reality Check:");
			m_sChoices[0] = GUI.TextField(new Rect(270.0f,263.0f,400.0f,30.0f), m_sChoices[0]);
			
			GUI.Label(new Rect(420.0f,307.0f,600.0f,200.0f), "Another view:");
			m_sChoices[1] = GUI.TextField(new Rect(270.0f,317.0f,400.0f,30.0f), m_sChoices[1]);
			
			GUI.Label(new Rect(420.0f,363.0f,600.0f,200.0f), "Perspective:");
			m_sChoices[2] = GUI.TextField(new Rect(270.0f,373.0f,400.0f,30.0f), m_sChoices[2]);
			
			GUI.Label(new Rect(420.0f,417.0f,600.0f,200.0f), "Action:");
			m_sChoices[3] = GUI.TextField(new Rect(270.0f,427.0f,400.0f,30.0f), m_sChoices[3]);
		}
		
		//The next button
		if(GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
					Global.arrowKey = "";

			this.enabled =false;
			
			//Load the guide talk scene again
			GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;
			GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentScene = "L6GuideScene24";
		}
	}
}
