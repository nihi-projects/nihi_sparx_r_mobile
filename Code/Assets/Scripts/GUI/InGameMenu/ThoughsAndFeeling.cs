using UnityEngine;
using System.Collections;

public class ThoughsAndFeeling : MonoBehaviour {
	
	public GUISkin m_skin = null;
	
	public Texture2D m_tBarchartBackground;
	public Texture2D m_tThroughAndFeelingTexture;
	
	// Use this for initialization
	void Start () {
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		
		//m_tBarchartBackground = (Texture2D)Resources.Load ("UI/frontendbg", typeof(Texture2D));
		//m_tThroughAndFeelingTexture = (Texture2D)Resources.Load ("UI/mg_l1_s_mg1", typeof(Texture2D));
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	/*
	*
	* OnGUI Function.
	*
	*/
	public void OnGUI () 
	{		
		GUI.skin = m_skin;
		GUI.depth = -1;
		
		GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
		GUI.Label(new Rect(155.0f, 55.0f, 650.0f, 501.0f), m_tThroughAndFeelingTexture);
		
		if(GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
					Global.arrowKey = "";

			this.enabled = false;
			
			GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled = true;
			//Update the current scene name
			UpdateCurrentSceneName();
		}
	}
	
	/*
	*
	* This function updates the current scene name after click the next button on the image
	* ThoughsAndFeeling image screen.
	*
	*/
	public void UpdateCurrentSceneName()
	{
		if(Global.PreviousSceneName == "GuideScene10")
		{
			GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentScene = "GuideScene11";
		}
		else if(Global.PreviousSceneName == "GuideScene22")
		{
			GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentScene = "GuideScene23";
		}
	}
}
