using UnityEngine;
using System.Collections;

public class L7TakingHome : MonoBehaviour 
{
	// public
	public GUISkin m_skin = null;
	
	
	public Texture[] m_textures;
	
	static public Texture m_tChoice;

	private string[] takeHomeArray 	=  {"REALITY CHECK. ANOTHER VIEW. PERSPECTIVE. Think ACTION", "I know that even if I feel really down, sooner or later hope will return.", 
										"I'll ask for help.", "I'll let strong feelings pass.", "I know that I have all these skills for dealing with low mood.", 
										"I'll make changes even though I don't always feel like it."};

	// privates
	private Rect[] m_boxes = 
	{
		new Rect(230.0f, 110.0f, 150.0f, 150.0f),
		new Rect(400.0f, 110.0f, 150.0f, 150.0f),
		new Rect(570.0f, 110.0f, 150.0f, 150.0f),
		new Rect(230.0f, 280.0f, 150.0f, 150.0f),
		new Rect(400.0f, 280.0f, 150.0f, 150.0f),
		new Rect(570.0f, 280.0f, 150.0f, 150.0f)
	};
	
	private string m_conversationBoxText = "";
	
	private int m_selectedIndex = -1;
	
	public Texture2D m_tConversationBackground;
	
	// Use this for initialization
	void Start () 
	{
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		
		//m_tConversationBackground = (Texture2D)Resources.Load ("UI/talk_paper", typeof(Texture2D));
	}
	
	// Update is called once per frame
	void Update () 
	{
		//The default conversation box text
		m_conversationBoxText = WordsHoverOverText(-1);
		
		for (int i = 0; i < m_boxes.Length; ++i)
		{
			if(m_boxes[i].Contains(new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{
		         m_conversationBoxText = WordsHoverOverText(i);
				break;
			}
		}
		
		CheckClick();
	}
	
	void OnGUI()
	{
		GUI.skin = m_skin;
		GUI.depth = -1;
		
		// the background of the mingame
		GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
		
		//Conversation box
		GUI.Label(new Rect(265,470,620,135), m_tConversationBackground);
		GUI.Label(new Rect(300.0f, 497.0f, 360.0f, 80.0f), m_conversationBoxText, "CenteredFont");
		
		for (int i = 0; i < m_boxes.Length; ++i)
		{
			GUI.Label(m_boxes[i], m_textures[i]);
		}
		
		if (m_selectedIndex != -1)
		{
			Rect temp = new Rect(m_boxes[m_selectedIndex].x - 10.0f, m_boxes[m_selectedIndex].y - 10.0f,
								 m_boxes[m_selectedIndex].width + 20.0f, m_boxes[m_selectedIndex].height + 20.0f);
			GUI.Label(temp, m_textures[m_selectedIndex]);
		}
		bool done = true;
		
		//The next button
		if (true == done)
		{
			if(GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
					Global.arrowKey = "";

				m_tChoice = m_textures[m_selectedIndex];

				if(m_tChoice.name == "taketoday_1"){
					Global.userL7NoteBookDataBlocks[0] = takeHomeArray[0];
				}else if(m_tChoice.name == "taketoday_2"){
					Global.userL7NoteBookDataBlocks[0] = takeHomeArray[1];
				}else if(m_tChoice.name == "taketoday_3"){
					Global.userL7NoteBookDataBlocks[0] = takeHomeArray[2];
				}else if(m_tChoice.name == "taketoday_4"){
					Global.userL7NoteBookDataBlocks[0] = takeHomeArray[3];
				}else if(m_tChoice.name == "taketoday_5"){
					Global.userL7NoteBookDataBlocks[0] = takeHomeArray[4];
				}else if(m_tChoice.name == "taketoday_6"){
					Global.userL7NoteBookDataBlocks[0] = takeHomeArray[5];
				}else{
					Global.userL7NoteBookDataBlocks[0] = "";
				}
				this.enabled = false;
				
				GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;
				GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentScene = "L7GuideScene17";
			}
		}
	}
	
	void CheckClick()
	{
		//If the mouse button is clicked and if the mouse position is on the keywords
		if(Input.GetMouseButtonUp(0))
		{
			// save the mouse pos for ease of access
			float mouseX = Input.mousePosition.x;
			float mouseY = Input.mousePosition.y;
			
			// loop through all the original rects to see if the user has clicked on one of the boxes.
			for (int i = 0; i < m_boxes.Length; ++i)
			{
				if (m_boxes[i].Contains(new Vector2(mouseX, Screen.height - mouseY)))
				{
					if (m_selectedIndex == i)
					{
						m_selectedIndex = -1;
					}
					else 
					{
						m_selectedIndex = i;
					}
				}
			}
		}
	}
	
	public string WordsHoverOverText(int _index)
	{
		string keuWordExplaination = "";
			
		switch(_index)
		{
			case 0:
				keuWordExplaination = "REALITY CHECK.\nANOTHER VIEW.\nPERSPECTIVE.\nThink ACTION";
				break;
			case 1:
				keuWordExplaination = "I know that even if I feel really down, sooner or later hope will return.";
				break;
			case 2:
				keuWordExplaination = "I'll ask for help.";
				break;
			case 3:
				keuWordExplaination = "I'll let strong feelings pass.";
				break;
			case 4:
				keuWordExplaination = "I know that I have all these skills for dealing with low mood.";
				break;
			case 5:
				keuWordExplaination = "I'll make changes even though I don't always feel like it.";
				break;
			default:
			    keuWordExplaination = "...";
				break;
		}
			
		return keuWordExplaination;
	}
}
