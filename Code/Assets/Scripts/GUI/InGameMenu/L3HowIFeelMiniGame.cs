using UnityEngine;
using System.Collections;

public class L3HowIFeelMiniGame : MonoBehaviour 
{
	// publics
	public GUISkin m_skin = null;
	public Texture[] m_tActiveFaces;
	public Texture[] m_tDeactiveFaces;
	
	// privates
	private string[] m_worksDone = 
	{
		"I put off my assignments, chores or work.",
		"I finished my assignments, chores or work.",
		"I sat on my own doing nothing.",
		"I did something fun with family/friends.",
		"I got some exercise/played sports.",
		"I sat around inside."
	};
	
	static private float m_textWidth = 300.0f;
	static private float m_textHeight = 50.0f;
	private Rect[] m_wordsRect = 
	{
		new Rect(150.0f, 170.0f, m_textWidth, m_textHeight),
		new Rect(150.0f, 220.0f, m_textWidth, m_textHeight),
		new Rect(150.0f, 270.0f, m_textWidth, m_textHeight),
		new Rect(150.0f, 320.0f, m_textWidth, m_textHeight),
		new Rect(150.0f, 370.0f, m_textWidth, m_textHeight),
		new Rect(150.0f, 420.0f, m_textWidth, m_textHeight)
	};
	
	private int[] m_ActiveFace = 
	{
		-1, -1, -1, -1, -1, -1
	};
	
	static private float m_fFaceSize = 50.0f;
	private Rect[,] m_faceRects =
	{
		{new Rect(500.0f, 150.0f, m_fFaceSize, m_fFaceSize), new Rect(600.0f, 150.0f, m_fFaceSize, m_fFaceSize), new Rect(700.0f, 150.0f, m_fFaceSize, m_fFaceSize)},
		{new Rect(500.0f, 200.0f, m_fFaceSize, m_fFaceSize), new Rect(600.0f, 200.0f, m_fFaceSize, m_fFaceSize), new Rect(700.0f, 200.0f, m_fFaceSize, m_fFaceSize)},
		{new Rect(500.0f, 250.0f, m_fFaceSize, m_fFaceSize), new Rect(600.0f, 250.0f, m_fFaceSize, m_fFaceSize), new Rect(700.0f, 250.0f, m_fFaceSize, m_fFaceSize)},
		{new Rect(500.0f, 300.0f, m_fFaceSize, m_fFaceSize), new Rect(600.0f, 300.0f, m_fFaceSize, m_fFaceSize), new Rect(700.0f, 300.0f, m_fFaceSize, m_fFaceSize)},
		{new Rect(500.0f, 350.0f, m_fFaceSize, m_fFaceSize), new Rect(600.0f, 350.0f, m_fFaceSize, m_fFaceSize), new Rect(700.0f, 350.0f, m_fFaceSize, m_fFaceSize)},
		{new Rect(500.0f, 400.0f, m_fFaceSize, m_fFaceSize), new Rect(600.0f, 400.0f, m_fFaceSize, m_fFaceSize), new Rect(700.0f, 400.0f, m_fFaceSize, m_fFaceSize)}
	};
	
	// Use this for initialization
	void Start () 
	{
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
	}
	
	// Update is called once per frame
	void Update () 
	{
		CheckFaceClick();
	}
	
	public void OnGUI () 
	{		
		GUI.skin = m_skin;
		GUI.depth = -1;
		
		GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
		GUI.Label(new Rect(200.0f,100.0f,100.0f,50.0f), "What I did", "Titles");
		GUI.Label(new Rect(575.0f,100.0f,100.0f,50.0f), "How I felt", "Titles");
		
		//Conversation box
		GUI.Label(new Rect(265,470,440,135), "", "MiddleAreaBackGround");
		GUI.Label(new Rect(300.0f, 497.0f, 360.0f, 80.0f), "Click on the faces which best match how you felt", "CenteredFont");
		//GUI.Label(new Rect(300.0f,470.0f,350.0f,50.0f), "Click on the faces which best match how you felt", "CenteredFont");
		
		for (int i = 0; i < 6; ++i)
		{
			for (int j = 0; j < 3; ++j)
			{
				if (j == m_ActiveFace[i])
				{
					GUI.Label(m_faceRects[i,j], m_tActiveFaces[j]);
				}
				else
				{
					GUI.Label(m_faceRects[i,j], m_tDeactiveFaces[j]);
				}
			}
			
			GUI.Label(m_wordsRect[i], m_worksDone[i]);
		}
		
		//The next button
		if(GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
			Global.arrowKey = "";
			this.enabled =false;
			
			//Load the guide talk scene again
			GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled = true;
			//update the current scene name
			UpdateCurrentSceneName();
		}
	}
	
	void CheckFaceClick()
	{
		if(Input.GetMouseButtonUp(0))
		{
			for (int i = 0; i < 6; ++i)
			{
				bool found = false;
				for (int j = 0; j < 3; ++j)
				{	
					// save the mouse pos for ease of access
					float mouseX = Input.mousePosition.x;
					float mouseY = Input.mousePosition.y;
					if (m_faceRects[i,j].Contains(new Vector2(mouseX, Screen.height - mouseY)))
					{
						m_ActiveFace[i] = j;
						found = true;
						break;
					}
				}
				
				if (found) { break;}
			}
		}
	}
	
	public void UpdateCurrentSceneName()
	{
		if(Global.CurrentLevelNumber == 3)
		{
			GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentScene = "L3GuideScene8";
		}
	}
}
