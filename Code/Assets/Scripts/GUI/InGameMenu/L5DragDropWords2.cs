using UnityEngine;
using System.Collections;

public class L5DragDropWords2 : MonoBehaviour 
{
	Rect k_zeroRect = new Rect(0.0f, 0.0f, 0.0f, 0.0f);
	// publics
	public GUISkin m_skin = null;
	
	
	// privates
	private string m_conversationBoxText = "";
	
	private Vector2[] m_KeywordDimensions = 
	{
		new Vector2(75.0f, 30.0f), // Disaster
		new Vector2(70.0f, 30.0f), // Downer
		new Vector2(70.0f, 30.0f), // All-or-Nothing
		new Vector2(70.0f, 30.0f), // Guilty
		new Vector2(110.0f, 30.0f), // Perfectionist
		new Vector2(70.0f, 30.0f), // Mind Reader
	};
	
	static private float m_collisiondimension = 10.0f;
	private DraggableObject[] m_wordsRects = 
	{
		new DraggableObject(new Rect(120.0f, 150.0f, 130.0f, 30.0f), 
			"Disaster"),
		new DraggableObject(new Rect(120.0f, 200.0f, 130.0f, 30.0f), 
			"Downer"),
		new DraggableObject(new Rect(120.0f, 250.0f, 130.0f, 30.0f), 
			"All-or-Nothing"),
		new DraggableObject(new Rect(120.0f, 300.0f, 130.0f, 30.0f), 
			"Guilty"),
		new DraggableObject(new Rect(120.0f, 350.0f, 130.0f, 30.0f), 
			"Perfectionist"),
		new DraggableObject(new Rect(120.0f, 400.0f, 130.0f, 30.0f), 
			"Mind Reader"),
	};
	
	//KeyWord destination positions 
	static private float m_dextinationRectWidth = 250.0f;
	static private float m_dextinationRectHeight = 105.0f;
	
	private Rect[] m_destinationRect = 
	{
		new Rect(300.0f, 120.0f, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(300.0f, 245.0f, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(300.0f, 365.0f, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(570.0f, 120.0f, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(570.0f, 245.0f, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(570.0f, 365.0f, m_dextinationRectWidth, m_dextinationRectHeight)

/*		new Rect(Screen.width*0.2930f, Screen.height*0.1563f, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(Screen.width*0.2930f, Screen.height*0.3846f, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(Screen.width*0.2930f, Screen.height*0.5808f, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(Screen.width*0.5566f, Screen.height*0.1563f, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(Screen.width*0.5566f, Screen.height*0.3846f, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(Screen.width*0.5566f, Screen.height*0.5808f, m_dextinationRectWidth, m_dextinationRectHeight)
*/
	};

	private string[] m_destinationText = 
	{
		"'Am I making a bigger deal out \nof this than it really is?'\n\n", 
		"'Am I thinking things are my fault even \nwhen it's not up to me?'\n\n", 
		"'Am I just looking at the downside and \nignoring the positive?'\n\n", 
		"'Am I seeing things as extremes with no \nmiddle ground?'\n\n", 
		"'Am I trying to read people's minds?'\n\n\n", 
		"'Am I expecting myself to be perfect?'\n\n\n"
	};
	
	private Vector2[] m_finalTextPos = 
	{
		new Vector2(425.0f, 140.0f),
		new Vector2(430.0f, 260.0f),
		new Vector2(430.0f, 380.0f),
		new Vector2(700.0f, 140.0f),
		new Vector2(700.0f, 260.0f),
		new Vector2(700.0f, 380.0f)

		/*new Vector2(Screen.width*0.4199f, Screen.height*0.3050f),
		new Vector2(Screen.width*0.4199f, Screen.height*0.5182f),
		new Vector2(Screen.width*0.4199f, Screen.height*0.7135f),
		new Vector2(Screen.width*0.6836f, Screen.height*0.2700f),
		new Vector2(Screen.width*0.6836f, Screen.height*0.5182f),
		new Vector2(Screen.width*0.6836f, Screen.height*0.7357f)*/
	};
	
	private int[] m_correctIndex = 
	{
		0,
		3, 
		1, 
		2, 
		5, 
		4
	};
	
	private bool[] m_placed = 
	{
		false, false, false, false, false, false
	};
	
	private string m_currentClickedString = "";
	
	private int m_currentIndex = -1;
	
	private bool b_dragging = false;
	
	public Texture2D m_tFillBoxBackground;
	
	// Use this for initialization
	void Start () 
	{				
		Global.levelStarted = false; //used to reload after some Guide End Scene GUI elements close.
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		
		//m_tFillBoxBackground = (Texture2D)Resources.Load ("UI/talk_paper", typeof(Texture2D));
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (b_dragging)
		{
			// check to see if the moveable rect is found
			if (m_currentIndex != -1)
			{
				// save the mouse pos for ease of access
				float mouseX = Input.mousePosition.x;
				float mouseY = Input.mousePosition.y;
				m_wordsRects[m_currentIndex].movable.Set(mouseX - m_KeywordDimensions[m_currentIndex].x/2, 
												     mouseY * (-1) + Screen.height - m_KeywordDimensions[m_currentIndex].y/2, 
												     m_collisiondimension, 
												     m_collisiondimension);
			}
		}
		else
		{
			KeyWordDrag();
		}
		
		DestinationDrop();
	}
	
	public void OnGUI () 
	{		
		GUI.skin = m_skin;
		GUI.depth = -1;
		
		// the background of the mingame
		GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
		
		// The middle area
		GUI.Label(new Rect(180, 60, 600, 80), "Match each type of Gnat (negative thought) to a question that challenges it.", "Titles");
		bool done = true;
		for (int i = 0; i < m_destinationRect.Length; ++i)
		{
			GUI.Label(m_destinationRect[i], "", "BoxMatch");
			//GUI.Box(m_destinationRect[i], "");
			GUI.Label(m_destinationRect[i], m_destinationText[i], "CenteredFont12");
			
			if (!m_placed[i])
			{
				done = false;
			}
		}
		
		for (int i = 0; i < m_wordsRects.Length; ++i)
		{
			Rect temp = new Rect(m_wordsRects[i].movable.x, m_wordsRects[i].movable.y, 
								m_wordsRects[i].original.width, m_wordsRects[i].original.height);
			GUI.Label(temp, m_wordsRects[i].words, "CenteredFont");
		}
		
		//The next button
		if (true == done)
		{
			if(GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
					Global.arrowKey = "";

				this.enabled = false;
				
				GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;
				GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentScene = "L5GuideScene22";
			}
		}
	}
	
	/*
	 * 
	 * Thie function drag the keyword and move with the mouse.
	 * 
	*/
	public void KeyWordDrag()
	{
		// release early if there is no active keyword box being moved.
		if (m_currentIndex != -1) { return ;}
		
		//If the mouse button is clicked and if the mouse position is on the keywords
		if(Input.GetMouseButton(0))
		{
			// save the mouse pos for ease of access
			float mouseX = Input.mousePosition.x;
			float mouseY = Input.mousePosition.y;
			
			// boolean to see if a box has been clicked on.
			bool clicked = false;
			
			// loop through all the original rects to see if the user has clicked on one of the boxes.
			for (int i = 0; i < m_wordsRects.Length; ++i)
			{
				if (m_wordsRects[i].original.Contains(new Vector2(mouseX, Screen.height - mouseY)))
				{
					// set the current active key text
					// check if the current text has been set
					if (m_currentClickedString == "")
					{
						m_currentClickedString = m_wordsRects[i].words;
						m_currentIndex = i;
						clicked = true;
						b_dragging = true;
						break;
					}
				}
			}	
			
			// this is an early release from the function
			if (clicked == true) { return ;}
		}
	}
	
	/*
	 * 
	 * Thie function drop the keyword to the destination area.
	 * 
	*/
	public void DestinationDrop()
	{
		// release early if there is no active keyword box being moved.
		if (m_currentIndex == -1) { return ;}
		
		//If the mouse button is UnClicked and if  the mouse position is on the fill field
		if(Input.GetMouseButtonUp(0))
		{
			b_dragging = false;
			
			// boolean for if the text was placed in the box.
			bool successful = false;
			
			// loop through the destination rects to see which box the word is being placed in
			for (int i = 0; i < m_destinationRect.Length; ++i)
			{
				float mouseX = Input.mousePosition.x;
				float mouseY = Input.mousePosition.y;
				
				// check if this destination box contains the current clicked keyword box
				if(m_destinationRect[i].Contains(new Vector2(mouseX, Screen.height - mouseY)))
				{
					// if the destination text is empty
					if(m_correctIndex[i] == m_currentIndex)
					{
						// set the destination text and remove the clicked text.
						m_currentClickedString = "";
						successful = true;
						m_placed[i] = true;
						
						float width = m_wordsRects[m_currentIndex].original.width;
						float height = m_wordsRects[m_currentIndex].original.height;
						m_wordsRects[m_currentIndex].movable.Set(m_finalTextPos[i].x - width/2, 
																 m_finalTextPos[i].y - height/2, 
																 width, height);

						m_currentIndex = -1;
					}
					break;
				}
			}
			
			// another early release because everything is done already
			if (successful) { return ;}
			
			// execute this code if the user missed the destination box.
			ResetCurrentString();
		}
	}
	
	/*
	 * 
	 * This function resets the missed keyword text and text rect back to their original positions
	 * 
	 */
	private void ResetCurrentString()
	{
		// set the movable rect to the original area, set the keyword back to the current,
		// reset the current keyword and break the loop.
		m_wordsRects[m_currentIndex].movable = m_wordsRects[m_currentIndex].original;
		m_currentClickedString = "";
		m_currentIndex = -1;
	}
}
