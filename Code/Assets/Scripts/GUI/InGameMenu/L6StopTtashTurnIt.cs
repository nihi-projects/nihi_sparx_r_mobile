using UnityEngine;
using System.Collections;

public class L6StopTtashTurnIt : MonoBehaviour {
	
	public GUISkin m_skin = null;
	
	
	public Texture2D m_tStopITrashTurntTexture;
	
	// Use this for initialization
	void Start () 
	{
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		
		//m_tStopITrashTurntTexture = (Texture2D)Resources.Load ("UI/mg_l5_e_mg1", typeof(Texture2D));	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void OnGUI () 
	{		
		GUI.skin = m_skin;
		GUI.depth = -1;
		
		GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
		GUI.Label(new Rect(220.0f, 90.0f, 700.0f, 400.0f), m_tStopITrashTurntTexture);
		GUI.Label(new Rect(240.0f, 210.0f, 200.0f, 20.0f),"Stop it","PictureTextSmall");
		GUI.Label(new Rect(525.0f, 225.0f, 200.0f, 20.0f),"Trash it","PictureTextSmall");
		
		if(GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
					Global.arrowKey = "";

			this.enabled = false;
			
			GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;
			//Update the current scene name
			GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentScene = "L6GuideScene12_1_1";
		}
	}
}
