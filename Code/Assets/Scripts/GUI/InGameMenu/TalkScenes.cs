using UnityEngine;
using System.Collections;
using System.Globalization;

public class TalkScenes: MonoBehaviour {

	//private static TalkScenes instanceRef;
	
	//Puiblic variables
	public string m_currentScene            = "";
	public string m_currentTuiScene         = "";
	
	public bool m_bGuardianNeedsLoad        = true;
	public bool m_voiceCanStart             = true;

	public bool m_SoundFilesAlreadyLoaded   = false;

	public bool m_bTalkedWithGuardian       = false;
	public bool m_bTalkedWithMentor         = false;
	
	public bool m_bL7NeedHelpOptionA        = false;
	public bool m_bL7UnNeedHelpOptionB      = false;
	
	public float m_autoSceneStartTime       = 0.0f;
	public bool m_bCassFiremanTalkStart     = false;
			
	public int[] m_PHQAAnswers;
	
	public int m_iTestingLevelNum           = 0;
		
	public bool m_bNpcSubMultiChoiceOptionA = false;
	public bool m_bNpcSubMultiChoiceOptionB = false;
	public bool m_bNpcSubMultiChoiceOptionC = false;
    public bool m_bNpcSubMultiChoiceOptionD = false;
    public bool m_bjustPlay = false;//Diego

	bool m_debounce = true;
	
	public bool[] m_bChiceSelectedDetector;

	//Private variables
	//Normal character txt files
	//----------------------------Conversation XML data files--------------------------
	private TextAsset m_guideXmlFilePathTextAsset;
	private TextAsset m_mentorXMLFilePathTextAsset;
	private TextAsset m_hopeXmlFilePathTextAsset;
	
	//1st level
	private TextAsset m_guardianXmlFilePathTextAsset;
	private TextAsset m_1npcXmlFilePathTextAsset;
	private TextAsset m_travelerXmlFilePathTextAsset;
	//2nd level
	private TextAsset m_cassXmlFilePathTextAsset;
	private TextAsset m_fireXmlFilePathTextAsset;
	private TextAsset m_yetiXmlFilePathTextAsset;
	private TextAsset m_fireperson2XmlFilePathTextAsset;
	//3nd level
	private TextAsset m_firespiritXmlFilePathTextAsset;
	private TextAsset m_npcXmlFilePathTextAsset;
	private TextAsset m_npc2XmlFilePathTextAsset;
	//4nd level
	private TextAsset m_darroXmlFilePathTextAsset;
	private TextAsset m_gnatsXmlFilePathTextAsset;
	private TextAsset m_sparksXmlFilePathTextAsset;
	//5nd level
	private TextAsset m_swapguyXmlFilePathTextAsset;
	private TextAsset m_ntsXmlFilePathTextAsset;
	//6nd level
	private TextAsset m_bridgeWomanXmlFilePathTextAsset;
	private TextAsset m_templeGuidianXmlFilePathTextAsset;
	//7nd level
	private TextAsset m_examinerXmlFilePathTextAsset;
	private TextAsset m_swapGuyXmlFilePathTextAsset;
	private TextAsset m_playerNeedsHelpTextAsset;
	private TextAsset m_cassL7XmlFilePathTextAsset;
	private TextAsset m_darroL7XmlFilePathTextAsset;
	private TextAsset m_firepersonL7XmlFilePathTextAsset;
	private TextAsset m_kogXmlFilePathTextAsset;
	
	private int m_iPerPHQAQuestionScore  = 0;
	private int m_iPHQAQuestionNum       = 1;
	private int m_iQ9Score				 = 0;
	public GUISkin m_skin               = null;
	
	private LoadTalkSceneXMLData m_loadSceneData;


	//----------------------------Audio files--------------------------------
	//Level 1 Audio list
	public AudioClip[] L1AudioFileList;
	//Level 2 Audio list
	public AudioClip[] L2AudioFileList;
	//Level 3 Audio list
	public AudioClip[] L3AudioFileList;
	//Level 4 Audio list
	public AudioClip[] L4AudioFileList;
	//Level 5 Audio list
	public AudioClip[] L5AudioFileList;
	//Level 6 Audio list
	public AudioClip[] L6AudioFileList;
	//level 7 Audio list
	public AudioClip[] L7AudioFileList;
	
	public AudioSource m_characterVoiceAS;
	
	private bool m_bAutoConversationUnFinished = true;
	private bool m_bNPC1AnimationPlayOnce      = false;
	private bool m_bNPC2AnimationPlayOnce      = false;
	
	private bool m_bMultiChoice1OptionA = false;
	private bool m_bMultiChoice1OptionB = false;
	private bool m_bMultiChoice1OptionC = false;
	private bool m_bMultiChoice1OptionD = false;
	
	private bool m_bTuiSubMultiChoiceOptionA = false;
	private bool m_bTuiSubMultiChoiceOptionB = false;
	private bool m_bTuiSubMultiChoiceOptionC = false;
	private bool m_bTuiSubMultiChoiceOptionD = false;
	
	private int m_PHQAAnswerIndex   = 0;
	private CapturedDataInput instance;


	 
	//For web player test only
	public string m_sTestWholeDynamic   = "";
	public string m_sTestDynamicToken   = "";
	public string m_sTestDynamicURL     = "";

	private bool m_BoolSkipError = false;
	//Cass skip Error
	public bool m_BoolSkipVoiceErrorLevel2 = false;
	public bool m_BoolSkipVoiceErrorLevel4 = false;
	public bool m_BoolSkipVoiceErrorLevel4Pass = false;


	private float m_CurrentVoiceTime = 0f;
	private bool m_bCurrentVoiceTimePlaying = false;
	private int m_nCurrentVoicePlayingCounter = 0;
	private int m_nCurrentVoicePlayingWell = 0;
	private bool m_bCurrentVoiceRightStopped = false;
		
	//Implementation
	/*
	*
	* Awake function.
	*
	* The Awake function is run before Start.
	*
	*/

	public void Awake()
	{
		/*if (instanceRef == null) {
			instanceRef = this;
			DontDestroyOnLoad (gameObject);
		}else{
			DestroyImmediate (this.gameObject);
		}*/

		//Debug.Log ("Talk Scenes Awake: " + Global.CurrentLevelNumber);
		//This function load all the conversation contexts
		m_SoundFilesAlreadyLoaded = false;
		LoadLevelConversation(Global.CurrentLevelNumber);
		
		//Load the xml character conversation content here
		m_loadSceneData = GetComponent<LoadTalkSceneXMLData>();
	}
	
	/*
	*
	* Start function
	*
	*/

	public void Start ()
	{	
		//System.Globalization.CultureInfo.CurrentCulture.ClearCachedData ();

		//DontDestroyOnLoad(gameObject);//FIX DUPLICATE AND INDISTICT KILLING OBJECTS

		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		
		//For local game test only
		//if(Application.isEditor){
		//	Global.CurrentLevelNumber = m_iTestingLevelNum;
     	//	}
		
		//Fill up the GUI objects list
		for(int i = 1; i <= 7; i++){
			Global.GUIObjects[i-1] = GameObject.Find ("L" + i.ToString() + "GUI");
		}
		for(int j = 1; j <= 7; j++){
			//Never set the L1GUI to inactive
			if(j != Global.CurrentLevelNumber){
				Global.GUIObjects[j-1].SetActive(false);
			}
		}
		
		//Initialise the option selection array
		m_bChiceSelectedDetector = new bool[4];
		m_PHQAAnswers = new int[9];
		for(int i = 0; i< 9; i++){
			m_PHQAAnswers[i] = 0;
		}
		//AudioSource
		m_characterVoiceAS = (AudioSource)gameObject.AddComponent<AudioSource>();
		
		//Connecting to the server
		//instance = CapturedDataInput.GetInstance;
		instance = GameObject.Find("CapturedDataInputHolder").GetComponent<CapturedDataInput>();

	}
	
	/*
	*
	* Update function
	*
	*/
/*    public void Update()
    {
		if (!m_bjustPlay) {
			if (m_CurrentVoiceTime == m_characterVoiceAS.time) {
				m_nCurrentVoicePlayingCounter++;
			} else {
				m_nCurrentVoicePlayingWell++;
				m_nCurrentVoicePlayingCounter = 0;
			} 
		} else {
			m_nCurrentVoicePlayingWell = 0;
			m_bCurrentVoiceRightStopped = false;
			m_nCurrentVoicePlayingCounter = 0;
		}
		m_CurrentVoiceTime = m_characterVoiceAS.time;
		Debug.Log ("NOPLAYINGCOUNTER>" + m_nCurrentVoicePlayingCounter);
		Debug.Log ("PLAYINGCOUNTER>" + m_nCurrentVoicePlayingWell);
		Debug.Log(m_currentScene);
        //Debug.Log(m_characterVoiceAS.time + " <MainEval> " + m_loadSceneData.GetVoiceEndTime(m_currentScene));
        //Debug.Log("bJustPlay_False ");
        //Stop Tui's voice
        if (Global.CurrentInteractNPC == "Hope")
        {
		Debug.Log(m_characterVoiceAS.time + " <Eval Hope> " + m_loadSceneData.GetVoiceEndTime(m_currentTuiScene));               
            if (m_bjustPlay)//FIX_D
                {//FIX_D
					m_bjustPlay = false;//FIX_D
                    if (m_characterVoiceAS.time < m_loadSceneData.GetVoiceEndTime(m_currentTuiScene))//FIX_D
                    {//FIX_D
                       
                        Debug.Log("bJustPlay_False: " + m_characterVoiceAS.time);
						/*if (m_characterVoiceAS.time < m_loadSceneData.GetVoiceStartTime (m_currentTuiScene)) {
								m_characterVoiceAS.Stop();
								Debug.Log ("SET START AUDIO AND PLAY AGAIN Start:" + m_loadSceneData.GetVoiceStartTime (m_currentTuiScene) + "<End>:" + m_loadSceneData.GetVoiceEndTime (m_currentTuiScene));
								m_characterVoiceAS.time = m_loadSceneData.GetVoiceStartTime (m_currentTuiScene);
								m_characterVoiceAS.Play();
								m_nCurrentVoicePlayingCounter = 0;
						}*/
/*                    }
                    else {
                        Debug.Log("OriginalValue: " + m_characterVoiceAS.time);
                        Debug.Log("ValueGuiven: " + m_loadSceneData.GetVoiceStartTime(m_currentTuiScene));
					m_characterVoiceAS.Stop();
						Debug.Log ("SET START AUDIO AND PLAY AGAIN Current less than start:" + m_loadSceneData.GetVoiceStartTime (m_currentTuiScene) + "<End>:" + m_loadSceneData.GetVoiceEndTime (m_currentTuiScene));
						m_characterVoiceAS.time = m_loadSceneData.GetVoiceStartTime (m_currentTuiScene);
						m_characterVoiceAS.Play();
						m_nCurrentVoicePlayingCounter = 0;
                    }
                    //FIX_D
                }//FIX_D
                else//FIX_D
                {//FIX_D
				 
					if (m_characterVoiceAS.time >= m_loadSceneData.GetVoiceEndTime(m_currentTuiScene))
						{
							m_characterVoiceAS.Stop();
							m_bCurrentVoiceRightStopped = true;
							Debug.Log("STOP1");
						}

					if (m_nCurrentVoicePlayingCounter > 20 && !m_bCurrentVoiceRightStopped && m_nCurrentVoicePlayingWell < 22) {
					m_characterVoiceAS.Stop();
							Debug.Log ("SET START AUDIO AND PLAY AGAIN counter 5:" + m_loadSceneData.GetVoiceStartTime (m_currentTuiScene) + "<End>:" + m_loadSceneData.GetVoiceEndTime (m_currentTuiScene));
							m_characterVoiceAS.time = m_loadSceneData.GetVoiceStartTime (m_currentTuiScene);
							m_characterVoiceAS.Play();
							m_nCurrentVoicePlayingCounter = 0;
							m_bjustPlay = false;
						}

                }//FIX_D
        }
        else if (!Global.CurrentInteractNPC.Contains("Fake"))
        {
		Debug.Log ("EVAL T:" + m_characterVoiceAS.time + "<E>:" + m_loadSceneData.GetVoiceEndTime (m_currentScene) + "<S>:" + m_loadSceneData.GetVoiceStartTime (m_currentScene));
            if (m_bjustPlay)//FIX_D
            {//FIX_D
				m_bjustPlay = false;//FIX_D
                if (m_characterVoiceAS.time < m_loadSceneData.GetVoiceEndTime(m_currentScene))//FIX_D
                {//FIX_D
                    
                    Debug.Log("bJustPlay_False: " + m_characterVoiceAS.time);
					/*if (m_characterVoiceAS.time < m_loadSceneData.GetVoiceStartTime (m_currentScene)) {
						m_characterVoiceAS.Stop();
						Debug.Log ("SET START AUDIO AND PLAY AGAIN Start:" + m_loadSceneData.GetVoiceStartTime (m_currentScene) + "<End>:" + m_loadSceneData.GetVoiceEndTime (m_currentScene));
						m_characterVoiceAS.time = m_loadSceneData.GetVoiceStartTime (m_currentScene);
						m_characterVoiceAS.Play();
						m_nCurrentVoicePlayingCounter = 0;
						}*/
                        //Debug.Log("bJustPlay_False ");
/*                    }
                    else
                    {
                        Debug.Log("OriginalValue: " + m_characterVoiceAS.time);
                        Debug.Log("ValueGuiven: " + m_loadSceneData.GetVoiceStartTime(m_currentScene));
						Debug.Log ("SET START AUDIO AND PLAY AGAIN  Current less than start:" + m_loadSceneData.GetVoiceStartTime (m_currentScene) + "<End>:" + m_loadSceneData.GetVoiceEndTime (m_currentScene));
					m_characterVoiceAS.Stop();
						m_characterVoiceAS.time = m_loadSceneData.GetVoiceStartTime (m_currentScene);
						m_characterVoiceAS.Play();
						m_nCurrentVoicePlayingCounter = 0;
                    }
                //FIX_D
            }//FIX_D
            else//FIX_D
            {//FIX_D

				Debug.Log ("START AUDIO:" + m_loadSceneData.GetVoiceStartTime (m_currentScene));
				Debug.Log ("END AUDIO:" + m_loadSceneData.GetVoiceEndTime (m_currentScene));
				Debug.Log ("CURRENT TIME:" + m_characterVoiceAS.time);
				//Debug.Log ("LENGTH AUDIO:" +  m_characterVoiceAS.clip.length);

				if (m_characterVoiceAS.time >= m_loadSceneData.GetVoiceEndTime(m_currentScene))
                    {
                        //Debug.Log(m_characterVoiceAS.time + " <Eval> " + m_loadSceneData.GetVoiceEndTime(m_currentScene));
						m_characterVoiceAS.Stop();
						m_bCurrentVoiceRightStopped = true;
                        Debug.Log("STOPAUSE2");
                       
                        if (!(GameObject.Find("Cass") && GameObject.Find("Cass").GetComponent<Animation>().IsPlaying("walk")) && Global.CurrentInteractNPC != "FirespiritA" &&
                            Global.CurrentInteractNPC != "KingOfGnats" && Global.CurrentInteractNPC != "OptimizationDoorPoint")
                        {
                            GameObject.Find(Global.CurrentInteractNPC).GetComponent<Animation>().Play("idle");
                        }
                    }

			if (m_nCurrentVoicePlayingCounter > 40 && !m_bCurrentVoiceRightStopped && m_nCurrentVoicePlayingWell < 41) {
				Debug.Log ("SET START AUDIO AND PLAY AGAIN counter T:" + m_characterVoiceAS.time + "<E>:" + m_loadSceneData.GetVoiceEndTime (m_currentScene) + "<S>:" + m_loadSceneData.GetVoiceStartTime (m_currentScene) + "<L>:" + m_characterVoiceAS.clip.length);
					m_characterVoiceAS.Stop();
				m_characterVoiceAS.time = m_loadSceneData.GetVoiceStartTime (m_currentScene);
				m_characterVoiceAS.Play();
				m_nCurrentVoicePlayingCounter = 0;
				m_bjustPlay = false;
			}

            }//FIX_D
        }

        if (Global.CurrentInteractNPC == "FirespiritA")
        {
            if (!Global.GetPlayer().GetComponent<Animation>().IsPlaying("idle"))
            {
                Global.GetPlayer().GetComponent<Animation>().Play("idle");
            }
            Global.GetPlayer().GetComponent<PlayerMovement>().m_bMovement = false;
        }

    }
*/	
	/*
	*
	* This function will be called everytime when this script is enabled
	*
	*/

	public void Update()
	{
			/*
			Debug.Log ("CURRENT TIME:" + m_characterVoiceAS.time);
			Debug.Log("Currentsce:" + m_currentScene);
			Debug.Log("CurrentTUIsce:" + m_currentTuiScene);
			GUI.depth = -1;
			GUI.skin = m_skin;
			if (Global.Global_bshowVersionOnScreen){
				GUI.Label(new Rect(0, 0, 200, 50), Global.versionNumber);
			}
			*/
			//Debug.Log("------------------------ONGUIII:-----------------------------");
			//HOPE ONLY have Statement and SubMultichoise conversation types
			if(Global.CurrentInteractNPC == "Hope"){
				//------------------------------------------------------------Tui Statement Display-----------------------------------------
				if(m_loadSceneData.GetQuestionType(m_currentTuiScene).Equals("Statement")){
					//Switch the camera
					if(!m_loadSceneData.GetCameraID(m_currentTuiScene).Equals("")){
					//	Global.MoveCameraBasedOnID(m_loadSceneData.GetCameraID(m_currentTuiScene));
					}

					//Stop the voice
					Debug.Log("<SoundFilesLoaded>:" + m_SoundFilesAlreadyLoaded);
					if(m_voiceCanStart && m_SoundFilesAlreadyLoaded){
						if(m_currentTuiScene != ""){
							Debug.Log("STATEMENT VARS1:" + m_currentScene + " " + m_loadSceneData.GetSoundFileName(m_currentScene));
							PlayVoice(m_currentTuiScene, m_loadSceneData.GetSoundFileName(m_currentTuiScene));
						}
						m_voiceCanStart = false;
					}
					//Display the statement conversation
					//DisplayStateMentText();
				}
				//------------------------------------------------------------Tui SubMultiChoice Display-----------------------------------------
				if(m_loadSceneData.GetQuestionType(m_currentTuiScene).Equals("SubMultiChoiceQuestion")){
					//Switch the camera
					if(!m_loadSceneData.GetCameraID(m_currentTuiScene).Equals("")){
					//	Global.MoveCameraBasedOnID(m_loadSceneData.GetCameraID(m_currentTuiScene));
					}
					//if(){
					if (GameObject.Find ("Main Camera").GetComponent<CameraMovement> ().m_bCameraLookAt && m_currentScene == "TerminateTalkScene" && m_currentTuiScene == "L3HopeScene2" && Vector3.Distance (GameObject.Find("Main Camera").transform.position, Global.GetPlayer().transform.position) > 7) {
						m_BoolSkipError = true;
						m_voiceCanStart = true;
						GameObject.Find ("Main Camera").GetComponent<CameraMovement> ().m_bCameraLookAt = false;
					} else {
						m_BoolSkipError = false;
					}
					Debug.Log(Vector3.Distance (GameObject.Find("Main Camera").transform.position, Global.GetPlayer().transform.position));

					//Stop the voice
					Debug.Log("<SoundFilesLoaded>:" + m_SoundFilesAlreadyLoaded);
					if(m_voiceCanStart && m_SoundFilesAlreadyLoaded){
						if(m_currentTuiScene != "" && !m_BoolSkipError){
							Debug.Log("STATEMENT VARS2:" + m_currentScene + " " + m_loadSceneData.GetSoundFileName(m_currentScene));
							PlayVoice(m_currentTuiScene, m_loadSceneData.GetSoundFileName(m_currentTuiScene));
						}
						m_voiceCanStart = false;
						if (m_BoolSkipError) {
							m_voiceCanStart = true;
						}
					}

					//DisplaySubMultichoicetext();
				}
			}
			//NON HOPE
			else{

				//------------------------------------------------------------Non Hope Statement Display-----------------------------------------
				//Debug.Log("NON HOPE---------------------:" + m_loadSceneData.GetQuestionType(m_currentScene).ToString());
				if(m_loadSceneData.GetQuestionType(m_currentScene).Equals("Statement")){
					//The 1st NPC's animation
					if(m_bNPC1AnimationPlayOnce &&
						m_loadSceneData.GetObjectAnimationName(m_currentScene) != ""){
						//GameObject.Find (Global.CurrentInteractNPC).GetComponent<Animation>()
						//	.Play(m_loadSceneData.GetObjectAnimationName(m_currentScene));

						//m_bNPC1AnimationPlayOnce = false;
					}

					//The 2nd NPC's animation
					if(m_bNPC2AnimationPlayOnce && 
						m_loadSceneData.GetOtherObjAniName(m_currentScene) != ""){
						//GameObject.Find (m_loadSceneData.GetOtherObjCharacterName(m_currentScene)).GetComponent<Animation>()
						//	.Play(m_loadSceneData.GetOtherObjAniName(m_currentScene));

						//m_bNPC2AnimationPlayOnce = false;
					}

					//Switch the camera
					if(!m_loadSceneData.GetCameraID(m_currentScene).Equals("")){
						//Global.MoveCameraBasedOnID(m_loadSceneData.GetCameraID(m_currentScene));
					}

					//Start the voice
					Debug.Log("<SoundFilesLoaded>:" + m_SoundFilesAlreadyLoaded);
					if(m_voiceCanStart && m_SoundFilesAlreadyLoaded){
						Debug.Log("STATEMENT VARS3:" + m_currentScene + " " + m_loadSceneData.GetSoundFileName(m_currentScene));
						//if (m_currentScene != "AutoL2FireScene3") {
							//PlayVoice("GuideScene9_A5", "voice_guide_PHQ");
							PlayVoice (m_currentScene, m_loadSceneData.GetSoundFileName (m_currentScene));
							m_voiceCanStart = false;
						//}						
					}
					//Display the statement conversation
					//DisplayStateMentText();
				}
				//------------------------------------------------------------Non Hope SubMultiChoice Display-----------------------------------------
				if(m_loadSceneData.GetQuestionType(m_currentScene).Equals("SubMultiChoiceQuestion")){
					//Animation play
					if(m_loadSceneData.GetObjectAnimationName(m_currentScene) != ""){
						//Play the animation if it exists
						//GameObject.Find (Global.CurrentInteractNPC).GetComponent<Animation>().Play(m_loadSceneData.GetObjectAnimationName(m_currentScene));
					}

					//Switch the camera
					if(!m_loadSceneData.GetCameraID(m_currentScene).Equals("")){
						//Global.MoveCameraBasedOnID(m_loadSceneData.GetCameraID(m_currentScene));
					}
					//Stop the voice
					Debug.Log("<SoundFilesLoaded>:" + m_SoundFilesAlreadyLoaded);
					if (m_voiceCanStart && m_SoundFilesAlreadyLoaded) {
						Debug.Log (m_currentScene + "<>" + m_currentScene + "<>" + GameObject.Find ("GUI").GetComponent<TerminateTalkScene>().enabled);
						Debug.Log ("STATEMENT VARS4:" + m_currentScene + " " + m_loadSceneData.GetSoundFileName (m_currentScene));
						PlayVoice (m_currentScene, m_loadSceneData.GetSoundFileName (m_currentScene));
						//m_characterVoiceAS.Stop();
						m_voiceCanStart = false;
					}

					//DisplaySubMultichoicetext();
				}
				//------------------------------------------------------------Multichoice Display------------------------------------------
				if(m_loadSceneData.GetQuestionType(m_currentScene).Equals("MultiChoice")){
					//GameObject.Find (Global.CurrentInteractNPC).GetComponent<Animation>().Play("idle");
					//DisplayMultiChoiseText(Global.CurrentLevelNumber);
				}
				//------------------------------------------------------------NonVoice Statement Display------------------------------------
				if(m_loadSceneData.GetQuestionType(m_currentScene).Equals("NonVoiceStatement")){
					//DisplayNonVoiceText();
				}
			}












		/*--------------------------------------------------------------------------------------------------------------------------------	
		-------------------------------                                                                          -----------------------
		-------------------------------  GUI                                                                         -----------------------
		--------------------------------------------------------------------------------------------------------------------------------	
		*/

		/* Debug.Log("TRACE" + Global.CurrentInteractNPC.ToString());
		Debug.Log("JUST_PLAY");
		Debug.Log(m_bjustPlay);
		//Debug.Log(m_characterVoiceAS.time + " <Eval> " + m_loadSceneData.GetVoiceEndTime(m_currentScene));
		Debug.Log("EVALRESULT");
		Debug.Log(m_characterVoiceAS.time < m_loadSceneData.GetVoiceEndTime(m_currentScene));*/

		//Stop Tui's voice
		if (Global.CurrentInteractNPC == "Hope")
		{
			Debug.Log(m_characterVoiceAS.time + " <Eval> " + m_loadSceneData.GetVoiceEndTime(m_currentTuiScene));
			if (m_bjustPlay)//FIX_D
			{//FIX_D

				if (m_characterVoiceAS.time < m_loadSceneData.GetVoiceEndTime(m_currentTuiScene))//FIX_D
				{//FIX_D
					m_bjustPlay = false;//FIX_D

					Debug.Log("bJustPlay_False ");
				}
				else
				{
					Debug.Log(m_characterVoiceAS.time + " <bJustPlay TRUE = > " + m_loadSceneData.GetVoiceEndTime(m_currentTuiScene));
					m_characterVoiceAS.time = m_loadSceneData.GetVoiceStartTime(m_currentTuiScene);
				}
				//FIX_D
			}//FIX_D
			else//FIX_D
			{//FIX_D
				if (m_characterVoiceAS.time >= m_loadSceneData.GetVoiceEndTime(m_currentTuiScene))
				{
					m_characterVoiceAS.Stop();
					Debug.Log("STOP1-------------------------------------------");
				}
			}//FIX_D
		}
		else if (!Global.CurrentInteractNPC.Contains("Fake"))
		{
			Debug.Log(m_characterVoiceAS.time + " <Eval> " + m_loadSceneData.GetVoiceEndTime(m_currentScene));
			if (m_bjustPlay)//FIX_D
			{//FIX_D

				if (m_characterVoiceAS.time < m_loadSceneData.GetVoiceEndTime(m_currentScene))//FIX_D
				{//FIX_D
					m_bjustPlay = false;//FIX_D

					Debug.Log("bJustPlay_False ");
				}
				else
				{
					Debug.Log(m_characterVoiceAS.time + " <bJustPlay TRUE = > " + m_loadSceneData.GetVoiceEndTime(m_currentScene));
					m_characterVoiceAS.time = m_loadSceneData.GetVoiceStartTime(m_currentScene);
				}
				//FIX_D
			}//FIX_D
			else//FIX_D
			{  
			///FIX_D
			if (m_characterVoiceAS.time >= m_loadSceneData.GetVoiceEndTime(m_currentScene))
				{
					if (!m_currentScene.Contains("Auto")) {
						Debug.Log (m_characterVoiceAS.time + " <LOOK> " + m_loadSceneData.GetVoiceEndTime (m_currentScene));

						m_characterVoiceAS.Stop ();
						Debug.Log ("STOP2-------------------------------------------");
						if (!(GameObject.Find ("Cass") && GameObject.Find ("Cass").GetComponent<Animation> ().IsPlaying ("walk")) && Global.CurrentInteractNPC != "FirespiritA" &&
						   Global.CurrentInteractNPC != "KingOfGnats" && Global.CurrentInteractNPC != "OptimizationDoorPoint") {
							GameObject.Find (Global.CurrentInteractNPC).GetComponent<Animation> ().Play ("idle");
						}
					}
				}
			//}//FIX_D
		}

		if (Global.CurrentInteractNPC == "FirespiritA")
		{
			if (!Global.GetPlayer().GetComponent<Animation>().IsPlaying("idle"))
			{
				Global.GetPlayer().GetComponent<Animation>().Play("idle");
			}
			Global.GetPlayer().GetComponent<PlayerMovement>().m_bMovement = false;
		}

	}
						}
	public void OnEnable(){

		if(Global.CurrentLevelNumber != 1){
			Debug.Log ("Talk Scenes OnEnable: " + Global.CurrentLevelNumber);
			//This function load all the conversation contexts
			m_SoundFilesAlreadyLoaded = false;
			LoadLevelConversation(Global.CurrentLevelNumber);
			
			//Load the xml character conversation content here
			m_loadSceneData = GetComponent<LoadTalkSceneXMLData>();
		}
		if(Global.levelStarted == false){
			//Guide Interacted
			if(Global.CurrentInteractNPC == "Guide"){
				
				//Before the player go to the LevelS
				if(Global.LevelSVisited == false && Application.loadedLevel == 0){
					if(Global.CurrentLevelNumber == 1){
						m_currentScene = "GuideScene1";		
						
						Debug.Log ("Level 1 Guide conversation data is loaded...");
					}
					if(Global.CurrentLevelNumber == 2){
						m_currentScene = "L2GuideScene1";		
						
						Debug.Log ("Level 2 Guide conversation data is loaded...");
					}
					if(Global.CurrentLevelNumber == 3){
						m_currentScene = "L3GuideScene1";		
						
						Debug.Log ("Level 3 Guide conversation data is loaded...");
					}
					if(Global.CurrentLevelNumber == 4){
						m_currentScene = "L4GuideScene1";	
						
						Debug.Log ("Level 4 Guide conversation data is loaded...");
					}
					if(Global.CurrentLevelNumber == 5){
						m_currentScene = "L5GuideScene1";
						
						Debug.Log ("Level 5 Guide conversation data is loaded...");
					}
					if(Global.CurrentLevelNumber == 6){
						m_currentScene = "L6GuideScene1";	
						
						Debug.Log ("Level 6 Guide conversation data is loaded...");
					}
					if(Global.CurrentLevelNumber == 7){
						m_currentScene = "L7GuideScene1";
						
						Debug.Log ("Level 7 Guide conversation data is loaded...");
					}
				}
				//After the player visited the LevelS
				else{
					if(Global.CurrentLevelNumber == 1){
						m_currentScene = "GuideScene13";		
						Debug.Log ("Level 1 Guide conversation data continue is loaded...");
					}
					if(Global.CurrentLevelNumber == 2){
						m_currentScene = "L2GuideScene13";		
						Debug.Log ("Level 2 Guide conversation data continue is loaded...");
					}
					if(Global.CurrentLevelNumber == 3){
						m_currentScene = "L3GuideScene14";		
						Debug.Log ("Level 3 Guide conversation data continue is loaded...");
					}
					if(Global.CurrentLevelNumber == 4){
						m_currentScene = "L4GuideScene17";		
						Debug.Log ("Level 4 Guide conversation data continue is loaded...");
					}
					if(Global.CurrentLevelNumber == 5){
						m_currentScene = "L5GuideScene18";		
						Debug.Log ("Level 5 Guide conversation data continue is loaded...");
					}
					if(Global.CurrentLevelNumber == 6){
						m_currentScene = "L6GuideScene11";		
						Debug.Log ("Level 6 Guide conversation data continue is loaded...");
					}
					if(Global.CurrentLevelNumber == 7){
						m_currentScene = "L7GuideScene11";		
						Debug.Log ("Level 7 Guide conversation data continue is loaded...");
					}
				}
				
				m_loadSceneData.GeneralReadFromXML(m_guideXmlFilePathTextAsset);
				Debug.Log ("Loaded Guide XML: " + m_loadSceneData.GetCharacterName(m_currentScene));
				Global.levelStarted = true;
			}
		}

		string currentInteractingNPC = Global.CurrentInteractNPC;
		
		if (Global.GetPlayer() && m_currentScene != "TerminateTalkScene" && m_currentTuiScene != "TuiTerminateScene")
		{
			Global.GetPlayer().GetComponent<PlayerMovement>().m_bMovement = false;
		}

		//-------------------------------------------------Load all the (LEVEL 1) character data------------------------------------------------------(Global.levelStarted == false){
		if(Global.CurrentLevelNumber == 1){
			//Guardian
			if(Global.CurrentInteractNPC == "guardian" && Global.TempTalkCharacter != "guardian"){
				Global.TempTalkCharacter = "guardian";
				
				if(Global.LevelSVisited == false){
					m_currentScene = "GuardianScene1";
					m_bTalkedWithGuardian = true;
				}
			
				m_loadSceneData.GeneralReadFromXML(m_guardianXmlFilePathTextAsset);
				Debug.Log ("Level 1 Guardian conversation data is loaded...");
			}
			//Mentor
			if(Global.CurrentInteractNPC == "Mentor" && Global.TempTalkCharacter != "Mentor"){
				Global.TempTalkCharacter = "Mentor";
				
				if(Global.LevelSVisited == false){
					m_currentScene = "MentorScene1";
					m_bTalkedWithMentor = true;
					Global.m_bTalkedWithMentor = true;
				}
				else{
					m_currentScene = "MentorScene4";
					m_bTalkedWithMentor = false;
					Global.m_bTalkedWithMentor = false;
				}
				m_loadSceneData.GeneralReadFromXML(m_mentorXMLFilePathTextAsset);
				Debug.Log ("Level 1 Mentor conversation data is loaded...");
			}
			//Npc
			if(Global.CurrentInteractNPC == "1npc" && Global.TempTalkCharacter != "1npc"){
				Global.TempTalkCharacter = "1npc";
				
				if(Global.LevelSVisited == false){
					m_currentScene = "RangerScene1";
				}
				
				m_loadSceneData.GeneralReadFromXML(m_1npcXmlFilePathTextAsset);
				Debug.Log ("Level 1 Npc conversation data is loaded...");
			}
			//Hope
			if(Global.CurrentInteractNPC == "Hope" && Global.TempTalkCharacter != "Hope"){
				Global.TempTalkCharacter = "Hope";
				
				if(Global.LevelSVisited == false && !Global.loadedSavePoint.Contains("GameMid")){
					m_currentTuiScene = "HopeScene1";
				}
				
				m_loadSceneData.GeneralReadFromXML(m_hopeXmlFilePathTextAsset);
				Debug.Log ("Level 1 Hope conversation data is loaded...");
			}
			//Traveller
			if(Global.CurrentInteractNPC == "Traveller" && Global.TempTalkCharacter != "Traveller"){
				Global.TempTalkCharacter = "Traveller";
				
				if(Global.LevelSVisited == false){
					m_currentScene = "TravellerScene1";
				}
				
				m_loadSceneData.GeneralReadFromXML(m_travelerXmlFilePathTextAsset);
				Debug.Log ("Level 1 Traveller conversation data is loaded...");
			}
		}
		//-------------------------------------------------Load all the (LEVEL 2) character data------------------------------------------------------------
		if(Global.CurrentLevelNumber == 2){
			//Mentor
			if(Global.CurrentInteractNPC == "Mentor" && Global.TempTalkCharacter != "Mentor"){
				Global.TempTalkCharacter = "Mentor";
				
				if(Global.LevelSVisited == false){
					m_currentScene = "L2MentorScene1";
					m_bTalkedWithMentor = true;
					Global.m_bTalkedWithMentor = true;
				}
				else{
					m_currentScene = "L2MentorScene5";
					m_bTalkedWithMentor = false;
					Global.m_bTalkedWithMentor = false;
				}
				
				m_loadSceneData.GeneralReadFromXML(m_mentorXMLFilePathTextAsset);
				Debug.Log ("Level 2 Mentor conversation data is loaded...");
			}
			//Hope
			if(Global.CurrentInteractNPC == "Hope" && Global.TempTalkCharacter != "Hope"){
				Global.TempTalkCharacter = "Hope";
				
				if(Global.LevelSVisited == false && !Global.loadedSavePoint.Contains("GameMid")){
					if(!Global.GetGameObjectByID("32003").GetComponent<IceCaveDoor>().m_bGotL2OpenDoorFire){
						m_currentTuiScene = "L2HopeScene1";
					}
				}
				
				m_loadSceneData.GeneralReadFromXML(m_hopeXmlFilePathTextAsset);
				Debug.Log ("Level 2 Hope conversation data is loaded...");
			}
			//Cass
			if(Global.CurrentInteractNPC == "Cass" && Global.TempTalkCharacter != "Cass"){
				Global.TempTalkCharacter = "Cass";
				
				if(Global.LevelSVisited == false){
					m_currentScene = "L2CassScene1";
				}
				
				m_loadSceneData.GeneralReadFromXML(m_cassXmlFilePathTextAsset);
				m_loadSceneData.GeneralReadFromXML(m_fireXmlFilePathTextAsset);
				Debug.Log ("Level 2 Cass conversation data is loaded...");
			}
			//Fireperson
			if(Global.CurrentInteractNPC == "Fireperson" && Global.TempTalkCharacter != "Fireperson"){
				Global.TempTalkCharacter = "Fireperson";
				
				if(Global.LevelSVisited == false){
					m_currentScene = "L2FireScene1";
					GameObject.Find ("Cass").GetComponent<CharacterInteraction>().enabled = true;
				}
				
				m_loadSceneData.GeneralReadFromXML(m_fireXmlFilePathTextAsset);
				Debug.Log ("Level 2 Fire Person conversation data is loaded...");
			}
			//Yeti
			if(Global.CurrentInteractNPC == "Yeti" && Global.TempTalkCharacter != "Yeti"){
				Global.TempTalkCharacter = "Yeti";
				
				if(Global.LevelSVisited == false){
					m_currentScene = "L2YetiScene1";
				}
				
				m_loadSceneData.GeneralReadFromXML(m_yetiXmlFilePathTextAsset);
				Debug.Log ("Level 2 Yeti conversation data is loaded...");
			}
			//Fireperson2
			if(Global.CurrentInteractNPC == "Fireperson2" && Global.TempTalkCharacter != "Fireperson2"){
				Global.TempTalkCharacter = "Fireperson2";
				
				if(Global.LevelSVisited == false){
					m_currentScene = "L2Fireman2Scene1";
				}
				
				m_loadSceneData.GeneralReadFromXML(m_fireperson2XmlFilePathTextAsset);
				Debug.Log ("Level 2 Fireperson2 conversation data is loaded...");
			}
		}
		//-------------------------------------------------Load all the (LEVEL 3) character data------------------------------------------------------------
		if(Global.CurrentLevelNumber == 3){
			//Mentor
			if(Global.CurrentInteractNPC == "Mentor" && Global.TempTalkCharacter != "Mentor"){
				Global.TempTalkCharacter = "Mentor";
				
				if(Global.LevelSVisited == false){
					m_currentScene = "L3MentorScene1";
					m_bTalkedWithMentor = true;
					Global.m_bTalkedWithMentor = true;
				}
				else{
					m_currentScene = "L3MentorScene5";
					m_bTalkedWithMentor = false;
					Global.m_bTalkedWithMentor = false;
				}
				
				m_loadSceneData.GeneralReadFromXML(m_mentorXMLFilePathTextAsset);
				Debug.Log ("Level 3 Mentor conversation data is loaded...");
			}
			//npclava
			if(Global.CurrentInteractNPC == "npclava" && Global.TempTalkCharacter != "npclava"){
				Global.TempTalkCharacter = "npclava";
				
				if(Global.LevelSVisited == false){
					m_currentScene = "L3NpcScene1";
				}
				
				m_loadSceneData.GeneralReadFromXML(m_npcXmlFilePathTextAsset);
				Debug.Log ("Level 3 npclava conversation data is loaded...");
			}
			//Hope
			if(Global.CurrentInteractNPC == "Hope" && Global.TempTalkCharacter != "Hope"){
				Global.TempTalkCharacter = "Hope";
				
				if(Global.LevelSVisited == false && !Global.loadedSavePoint.Contains("GameMid")){
					m_currentTuiScene = "L3HopeScene1";
				}
				
				m_loadSceneData.GeneralReadFromXML(m_hopeXmlFilePathTextAsset);
				Debug.Log ("Level 3 Hope conversation data is loaded...");
			}
			if(Global.CurrentInteractNPC == "FakeTuiBeforeMiniGames" && Global.TempTalkCharacter != "Hope"){
				Global.TempTalkCharacter = "Hope";
				
				if(Global.LevelSVisited == false && !Global.loadedSavePoint.Contains("GameMid")){
					m_currentTuiScene = "L3HopeScene1";
				}
				
				m_loadSceneData.GeneralReadFromXML(m_hopeXmlFilePathTextAsset);
				Debug.Log ("Level 3 Hope conversation data is loaded...");
			}
			//FireSpirit
			if(Global.CurrentInteractNPC == "FirespiritA" && Global.TempTalkCharacter != "FirespiritA"){
				Global.TempTalkCharacter = "FirespiritA";
				
				if(Global.LevelSVisited == false && !Global.loadedSavePoint.Contains("GameMid")){
					m_currentScene = "L3FirepspiritScene1";
				}
				
				m_loadSceneData.GeneralReadFromXML(m_firespiritXmlFilePathTextAsset);
				Debug.Log ("Level 3 FirepspiritA conversation data is loaded...");
			}
			//npc2
			if(Global.CurrentInteractNPC == "npclava2" && Global.TempTalkCharacter != "npclava2"){
				Global.TempTalkCharacter = "npclava2";
				
				if(Global.LevelSVisited == false){
					m_currentScene = "L3Npc2Scene1";
				}
				
				m_loadSceneData.GeneralReadFromXML(m_npc2XmlFilePathTextAsset);
				Debug.Log ("Level 3 npclava2 conversation data is loaded...");
			}
		}
		//-------------------------------------------------Load all the (LEVEL 4) character data------------------------------------------------------------
		if(Global.CurrentLevelNumber == 4){
			//Mentor
			if(Global.CurrentInteractNPC == "Mentor" && Global.TempTalkCharacter != "Mentor"){
				Global.TempTalkCharacter = "Mentor";
				
				if(Global.LevelSVisited == false){
					m_currentScene = "L4MentorScene1";
					m_bTalkedWithMentor = true;
					Global.m_bTalkedWithMentor = true;
				}
				else{
					m_currentScene = "L4MentorScene5";
					m_bTalkedWithMentor = false;
					Global.m_bTalkedWithMentor = false;
				}
				
				m_loadSceneData.GeneralReadFromXML(m_mentorXMLFilePathTextAsset);
				Debug.Log ("Level 4 Mentor conversation data is loaded...");
			}
			//Hope
			if(Global.CurrentInteractNPC == "Hope" && Global.TempTalkCharacter != "Hope"){
				Global.TempTalkCharacter = "Hope";
				
				if(Global.LevelSVisited == false){
					m_currentTuiScene = "L4HopeScene1";
				}
				
				m_loadSceneData.GeneralReadFromXML(m_hopeXmlFilePathTextAsset);
				Debug.Log ("Level 4 Hope conversation data is loaded...");
			}
			//Darro
			if(Global.CurrentInteractNPC == "Darro" && Global.TempTalkCharacter != "Darro"){
				Global.TempTalkCharacter = "Darro";
				
				if(Global.LevelSVisited == false){
					m_currentScene = "L4DarroScene1";
				}
				
				m_loadSceneData.GeneralReadFromXML(m_darroXmlFilePathTextAsset);
				Debug.Log ("Level 4 Darro conversation data is loaded...");
			}
			//Spark1
			string[] names = {"Spark1", "Spark2", "Spark3", "Spark4"};
			for (int i = 0; i < names.Length; ++i)
			{
				if(Global.CurrentInteractNPC == names[i] && Global.TempTalkCharacter != names[i]){
					Global.TempTalkCharacter = names[i];
					
					if(Global.LevelSVisited == false){
						//m_currentScene = "L4SparkScene" + (i + 1).ToString();
					}
					
					m_loadSceneData.GeneralReadFromXML(m_sparksXmlFilePathTextAsset);
					Debug.Log ("Level 4 spark conversation data is loaded...");
					break;
				}
			}
		}
		//-------------------------------------------------Load all the (LEVEL 5) character data------------------------------------------------------------
		if(Global.CurrentLevelNumber == 5){
			//Mentor
			if(Global.CurrentInteractNPC == "Mentor" && Global.TempTalkCharacter != "Mentor"){
				Global.TempTalkCharacter = "Mentor";
				
				if(Global.LevelSVisited == false){
					m_currentScene = "L5MentorScene1";
					m_bTalkedWithMentor = true;
					Global.m_bTalkedWithMentor = true;
				}
				else{
					m_currentScene = "L5MentorScene6";
					m_bTalkedWithMentor = false;
					Global.m_bTalkedWithMentor = false;
				}
				
				m_loadSceneData.GeneralReadFromXML(m_mentorXMLFilePathTextAsset);
				Debug.Log ("Level 5 Mentor conversation data is loaded...");
			}
			//Swampguy
			if(Global.CurrentInteractNPC == "Swampguy" && Global.TempTalkCharacter != "Swampguy"){
				Global.TempTalkCharacter = "Swampguy";
				
				if(Global.LevelSVisited == false){
					m_currentScene = "L5SwampguyScene1";
				}
				
				m_loadSceneData.GeneralReadFromXML(m_swapguyXmlFilePathTextAsset);
				Debug.Log ("Level 5 Swampguy conversation data is loaded...");
			}
			//Hope
			if(Global.CurrentInteractNPC == "Hope" && Global.TempTalkCharacter != "Hope"){
				Global.TempTalkCharacter = "Hope";
				
				if(Global.LevelSVisited == false){
					m_currentTuiScene = "L5HopeScene1";
				}
				
				m_loadSceneData.GeneralReadFromXML(m_hopeXmlFilePathTextAsset);
				Debug.Log ("Level 5 Hope conversation data is loaded...");
			}
		}
		//-------------------------------------------------Load all the (LEVEL 6) character data------------------------------------------------------------			//-------------------------------------------------Load all the (LEVEL 7) character data------------------------------------------------------------
		if(Global.CurrentLevelNumber == 6){
			//Mentor
			if(Global.CurrentInteractNPC == "Mentor" && Global.TempTalkCharacter != "Mentor"){
				Global.TempTalkCharacter = "Mentor";
				
				if(Global.LevelSVisited == false){
					m_currentScene = "L6MentorScene1";
					m_bTalkedWithMentor = true;
					Global.m_bTalkedWithMentor = true;
				}
				else{
					m_currentScene = "L6MentorScene5";
					m_bTalkedWithMentor = false;
					Global.m_bTalkedWithMentor = false;
				}
				
				m_loadSceneData.GeneralReadFromXML(m_mentorXMLFilePathTextAsset);
				Debug.Log ("Level 6 Mentor conversation data is loaded...");
			}
			//Gatekeeper_female
			if(Global.CurrentInteractNPC == "Gatekeeper_female" && Global.TempTalkCharacter != "Gatekeeper_female"){
				Global.TempTalkCharacter = "Gatekeeper_female";
				
				if(Global.LevelSVisited == false){
					m_currentScene = "L6WomanBridgeScene1";
				}
				
				m_loadSceneData.GeneralReadFromXML(m_bridgeWomanXmlFilePathTextAsset);
				Debug.Log ("Level 6 Gatekeeper_female conversation data is loaded...");
			}
			//Hope
			if(Global.CurrentInteractNPC == "Hope" && Global.TempTalkCharacter != "Hope"){
				Global.TempTalkCharacter = "Hope";
				
				if(Global.LevelSVisited == false && !Global.loadedSavePoint.Contains("GameMid")){
					m_currentTuiScene = "L6HopeScene1";
				}
				
				m_loadSceneData.GeneralReadFromXML(m_hopeXmlFilePathTextAsset);
				Debug.Log ("Level 6 Hope conversation data is loaded...");
			}
			//Temple Guardian
			if(Global.CurrentInteractNPC == "6_npc2" && Global.TempTalkCharacter != "6_npc2"){
				Global.TempTalkCharacter = "6_npc2";
				
				if(Global.LevelSVisited == false){
					m_currentScene = "L6TempleGuardianScene1";
				}
				
				m_loadSceneData.GeneralReadFromXML(m_templeGuidianXmlFilePathTextAsset);
				Debug.Log ("Level 6 Temple Guardian conversation data is loaded...");
			}
		}
		//-------------------------------------------------Load all the (LEVEL 7) character data------------------------------------------------------------			//-------------------------------------------------Load all the (LEVEL 7) character data------------------------------------------------------------
		if(Global.CurrentLevelNumber == 7){
			//Mentor
			if(Global.CurrentInteractNPC == "Mentor" && Global.TempTalkCharacter != "Mentor"){
				Global.TempTalkCharacter = "Mentor";
				
				if(Global.LevelSVisited == false){
					m_currentScene = "L7MentorScene1";
					m_bTalkedWithMentor = true;
					Global.m_bTalkedWithMentor = true;
				}
				else{
					m_currentScene = "L7MentorScene3";
					m_bTalkedWithMentor = false;
					Global.m_bTalkedWithMentor = false;
				}
				
				m_loadSceneData.GeneralReadFromXML(m_mentorXMLFilePathTextAsset);
				Debug.Log ("Level 7 Mentor conversation data is loaded...");
			}
			//Traveller
			if(Global.CurrentInteractNPC == "Traveller" && Global.TempTalkCharacter != "Traveller"){
				Global.TempTalkCharacter = "Traveller";
				
				if(Global.LevelSVisited == false){
					m_currentScene = "L7ExaminerScene1";
				}
				
				m_loadSceneData.GeneralReadFromXML(m_examinerXmlFilePathTextAsset);
				Debug.Log ("Level 7 Traveller conversation data is loaded...");
			}
			//Hope
			if(Global.CurrentInteractNPC == "Hope" && Global.TempTalkCharacter != "Hope"){
				Global.TempTalkCharacter = "Hope";
				
				if(Global.LevelSVisited == false){
					m_currentTuiScene = "L7HopeScene1";
				}
				
				m_loadSceneData.GeneralReadFromXML(m_hopeXmlFilePathTextAsset);
				Debug.Log ("Level 7 Hope conversation data is loaded...");
			}
			//Swampguy
			if(Global.CurrentInteractNPC == "Swampguy" && Global.TempTalkCharacter != "Swampguy"){
				Global.TempTalkCharacter = "Swampguy";
				
				if(Global.LevelSVisited == false){
					m_currentScene = "L7GuardScene1";
				}
				
				m_loadSceneData.GeneralReadFromXML(m_swapGuyXmlFilePathTextAsset);
				Debug.Log ("Level 7 Swampguy conversation data is loaded...");
			}
			//Ask for help
			if(Global.CurrentInteractNPC == "Boy" || Global.CurrentInteractNPC == "Girl"){
				m_currentScene = "L7PlayerScene1";
				
				m_loadSceneData.GeneralReadFromXML(m_playerNeedsHelpTextAsset);
				Debug.Log ("Level 7 player ask for help conversation data is loaded...");
			}
			//Cass
			if(Global.CurrentInteractNPC == "Cass" && Global.TempTalkCharacter != "Cass"){
				Global.TempTalkCharacter = "Cass";
				
				if(Global.LevelSVisited == false){
					m_currentScene = "L7CassScene1";
				}
				
				m_loadSceneData.GeneralReadFromXML(m_cassL7XmlFilePathTextAsset);
				Debug.Log ("Level 7 Cass conversation data is loaded...");
			}
			//Darro
			if(Global.CurrentInteractNPC == "Darro" && Global.TempTalkCharacter != "Darro"){
				Global.TempTalkCharacter = "Darro";
				
				if(Global.LevelSVisited == false){
					m_currentScene = "L7DarroScene1";
				}
				
				m_loadSceneData.GeneralReadFromXML(m_darroL7XmlFilePathTextAsset);
				Debug.Log ("Level 7 Darro conversation data is loaded...");
			}
			//Fireperson
			if(Global.CurrentInteractNPC == "Fireperson" && Global.TempTalkCharacter != "Fireperson"){
				Global.TempTalkCharacter = "Fireperson";
				
				if(Global.LevelSVisited == false){
					m_currentScene = "L7CanyonDwellerScene1";
				}
				
				m_loadSceneData.GeneralReadFromXML(m_firepersonL7XmlFilePathTextAsset);
				Debug.Log ("Level 7 FirePerson conversation data is loaded...");
			}
			//KOG
			if(Global.CurrentInteractNPC == "KingOfGnats" && Global.TempTalkCharacter != "KingOfGnats"){
				Global.TempTalkCharacter = "KingOfGnats";
				
				if(Global.LevelSVisited == false){
					m_currentScene = "L7KogScene1";
				}
				
				m_loadSceneData.GeneralReadFromXML(m_kogXmlFilePathTextAsset);
				Debug.Log ("Level 7 KOG conversation data is loaded...");
			}
		}
	}
	
	/*
	*
	* OnGUI function create the GUI of the conversation box
	* This OnGUI function displays all the conversation context
	* in the dialog box.
	*
	*/
							
	public void OnGUI()
	{	
		Debug.Log ("CURRENT TIME:" + m_characterVoiceAS.time);
		Debug.Log("Currentsce:" + m_currentScene);
		Debug.Log("CurrentTUIsce:" + m_currentTuiScene);
		GUI.depth = -1;
		GUI.skin = m_skin;

		if (Global.Global_bshowVersionOnScreen && Global.CurrentLevelNumber == 1 && UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == "GuideScene"){
			GUI.Label(new Rect(0, 0, 200, 50), Global.versionNumber);
		}

		#if UNITY_WEBGL && !UNITY_EDITOR 
							Debug.Log (System.DateTime.UtcNow.AddHours(0).Year.ToString().PadLeft(2,'0') + "-" + System.DateTime.UtcNow.AddHours(0).Month.ToString().PadLeft(2,'0') + "-" + System.DateTime.UtcNow.AddHours(0).Day.ToString().PadLeft(2,'0') + "T" + System.DateTime.UtcNow.AddHours(0).Hour.ToString().PadLeft(2,'0') + ":" + System.DateTime.UtcNow.AddHours(0).Minute.ToString().PadLeft(2,'0') + ":" + System.DateTime.UtcNow.AddHours(0).Second.ToString().PadLeft(2,'0'));
		#else
							Debug.Log (System.DateTime.UtcNow.Year.ToString().PadLeft(2,'0') + "-" + System.DateTime.UtcNow.Month.ToString().PadLeft(2,'0') + "-" + System.DateTime.UtcNow.Day.ToString().PadLeft(2,'0') + "T" + System.DateTime.UtcNow.Hour.ToString().PadLeft(2,'0') + ":" + System.DateTime.UtcNow.Minute.ToString().PadLeft(2,'0') + ":" + System.DateTime.UtcNow.Second.ToString().PadLeft(2,'0'));		
		#endif
		
		//GUI.Label (new Rect (0, 0, 200, 50), System.DateTime.UtcNow.AddHours(0).Year.ToString().PadLeft(2,'0') + "-" + System.DateTime.UtcNow.AddHours(0).Month.ToString().PadLeft(2,'0') + "-" + System.DateTime.UtcNow.AddHours(0).Day.ToString().PadLeft(2,'0') + "T" + System.DateTime.UtcNow.AddHours(0).Hour.ToString().PadLeft(2,'0') + ":" + System.DateTime.UtcNow.AddHours(0).Minute.ToString().PadLeft(2,'0') + ":" + System.DateTime.UtcNow.AddHours(0).Second.ToString().PadLeft(2,'0'));
        //Debug.Log("------------------------ONGUIII:-----------------------------");
		//HOPE ONLY have Statement and SubMultichoise conversation types
		if(Global.CurrentInteractNPC == "Hope"){
			//------------------------------------------------------------Tui Statement Display-----------------------------------------
			if(m_loadSceneData.GetQuestionType(m_currentTuiScene).Equals("Statement")){
				//Switch the camera
				if(!m_loadSceneData.GetCameraID(m_currentTuiScene).Equals("")){
					Global.MoveCameraBasedOnID(m_loadSceneData.GetCameraID(m_currentTuiScene));
				}
				
				//Stop the voice
				Debug.Log("<SoundFilesLoaded>:" + m_SoundFilesAlreadyLoaded);
				if(m_voiceCanStart && m_SoundFilesAlreadyLoaded){
					if(m_currentTuiScene != ""){
                        Debug.Log("STATEMENT VARS1:" + m_currentScene + " " + m_loadSceneData.GetSoundFileName(m_currentScene));
						//PlayVoice(m_currentTuiScene, m_loadSceneData.GetSoundFileName(m_currentTuiScene));
					}
					//m_voiceCanStart = false;
				}
				
				//Display the statement conversation
				DisplayStateMentText();
			}
			//------------------------------------------------------------Tui SubMultiChoice Display-----------------------------------------
			if(m_loadSceneData.GetQuestionType(m_currentTuiScene).Equals("SubMultiChoiceQuestion")){
				//Switch the camera
				if(!m_loadSceneData.GetCameraID(m_currentTuiScene).Equals("")){
					Global.MoveCameraBasedOnID(m_loadSceneData.GetCameraID(m_currentTuiScene));
				}

				//if(){
				if (GameObject.Find ("Main Camera").GetComponent<CameraMovement> ().m_bCameraLookAt && m_currentScene == "TerminateTalkScene" && m_currentTuiScene == "L3HopeScene2" && Vector3.Distance (GameObject.Find("Main Camera").transform.position, Global.GetPlayer().transform.position) > 7) {
					m_BoolSkipError = true;
					m_voiceCanStart = true;
					GameObject.Find ("Main Camera").GetComponent<CameraMovement> ().m_bCameraLookAt = false;
				} else {
					m_BoolSkipError = false;

				}
				Debug.Log(Vector3.Distance (GameObject.Find("Main Camera").transform.position, Global.GetPlayer().transform.position));

				//Stop the voice
				Debug.Log("<SoundFilesLoaded>:" + m_SoundFilesAlreadyLoaded);
				if(m_voiceCanStart && m_SoundFilesAlreadyLoaded){
					if(m_currentTuiScene != "" && !m_BoolSkipError){
                        Debug.Log("STATEMENT VARS2:" + m_currentScene + " " + m_loadSceneData.GetSoundFileName(m_currentScene));
						//PlayVoice(m_currentTuiScene, m_loadSceneData.GetSoundFileName(m_currentTuiScene));
					}
					//m_voiceCanStart = false;
					if (m_BoolSkipError) {
						//m_voiceCanStart = true;
					}
				}
				
				DisplaySubMultichoicetext();
			}
		}
		//NON HOPE
		else{

			//------------------------------------------------------------Non Hope Statement Display-----------------------------------------
            //Debug.Log("NON HOPE---------------------:" + m_loadSceneData.GetQuestionType(m_currentScene).ToString());
            if(m_loadSceneData.GetQuestionType(m_currentScene).Equals("Statement")){
				//The 1st NPC's animation
				if(m_bNPC1AnimationPlayOnce &&
					m_loadSceneData.GetObjectAnimationName(m_currentScene) != ""){
					GameObject.Find (Global.CurrentInteractNPC).GetComponent<Animation>()
						.Play(m_loadSceneData.GetObjectAnimationName(m_currentScene));
					
					m_bNPC1AnimationPlayOnce = false;
				}
				
				//The 2nd NPC's animation
				if(m_bNPC2AnimationPlayOnce && 
					m_loadSceneData.GetOtherObjAniName(m_currentScene) != ""){
					GameObject.Find (m_loadSceneData.GetOtherObjCharacterName(m_currentScene)).GetComponent<Animation>()
						.Play(m_loadSceneData.GetOtherObjAniName(m_currentScene));
					
					m_bNPC2AnimationPlayOnce = false;
				}
				
				//Switch the camera
				if(!m_loadSceneData.GetCameraID(m_currentScene).Equals("")){
					Global.MoveCameraBasedOnID(m_loadSceneData.GetCameraID(m_currentScene));
				}

				//Start the voice
				Debug.Log("<SoundFilesLoaded>:" + m_SoundFilesAlreadyLoaded);
				if(m_voiceCanStart && m_SoundFilesAlreadyLoaded){
                    Debug.Log("STATEMENT VARS3:" + m_currentScene + " " + m_loadSceneData.GetSoundFileName(m_currentScene));
						//PlayVoice("GuideScene9_A5", "voice_guide_PHQ");
						//PlayVoice (m_currentScene, m_loadSceneData.GetSoundFileName (m_currentScene));
						//m_voiceCanStart = false;
				}
				
				//Display the statement conversation
				DisplayStateMentText();
			}
			//------------------------------------------------------------Non Hope SubMultiChoice Display-----------------------------------------
			if(m_loadSceneData.GetQuestionType(m_currentScene).Equals("SubMultiChoiceQuestion")){
				//Animation play
				if(m_loadSceneData.GetObjectAnimationName(m_currentScene) != ""){
					//Play the animation if it exists
					GameObject.Find (Global.CurrentInteractNPC).GetComponent<Animation>().Play(m_loadSceneData.GetObjectAnimationName(m_currentScene));
				}
				
				//Switch the camera
				if (!m_loadSceneData.GetCameraID (m_currentScene).Equals ("") && !m_BoolSkipVoiceErrorLevel2) {
					Global.MoveCameraBasedOnID (m_loadSceneData.GetCameraID (m_currentScene));
				} else {
					m_BoolSkipVoiceErrorLevel2 = false;
				}
				//Stop the voice
				Debug.Log("<SoundFilesLoaded>:" + m_SoundFilesAlreadyLoaded);
				if (m_voiceCanStart && m_SoundFilesAlreadyLoaded) {
					Debug.Log (m_currentScene + "<>" + m_currentScene + "<>" + GameObject.Find ("GUI").GetComponent<TerminateTalkScene>().enabled);
					Debug.Log ("STATEMENT VARS4:" + m_currentScene + " " + m_loadSceneData.GetSoundFileName (m_currentScene));
					//PlayVoice (m_currentScene, m_loadSceneData.GetSoundFileName (m_currentScene));
					//m_characterVoiceAS.Stop();
					//m_voiceCanStart = false;
				}
				
				DisplaySubMultichoicetext();
			}
			//------------------------------------------------------------Multichoice Display------------------------------------------
			if(m_loadSceneData.GetQuestionType(m_currentScene).Equals("MultiChoice")){
				GameObject.Find (Global.CurrentInteractNPC).GetComponent<Animation>().Play("idle");
				DisplayMultiChoiseText(Global.CurrentLevelNumber);
			}
			//------------------------------------------------------------NonVoice Statement Display------------------------------------
			if(m_loadSceneData.GetQuestionType(m_currentScene).Equals("NonVoiceStatement")){
				DisplayNonVoiceText();
			}
		}


	
	
	}

	/*
	*
	* This function displays the statement text
	*
	*/
	/* //FIXING TEXT POSITIONING
	public void DisplayStateMentText()
	{			
		SwitchCharacterNameColor();
		
		//Normal size dilog box
		if(m_loadSceneData.LoadVoiceText(m_currentScene).Length < 450){
			GUI.Label(new Rect(0,Screen.height*0.63f,Screen.width,Screen.height*0.3f), "", "NormalSizeDilogBox");
			//Hope
			if(Global.CurrentInteractNPC == "Hope"){
				//Display character's name
				GUI.Label(new Rect(Screen.width*0.15f,Screen.height*0.69f,725,100), m_loadSceneData.GetCharacterName(m_currentTuiScene), "CharacterNameColor");
				GUI.Label(new Rect(Screen.width*0.16f,Screen.height*0.72f,700,100), m_loadSceneData.LoadVoiceText(m_currentTuiScene));
			}
			//Non Hope
			else{
				//Display character's name
				GUI.Label(new Rect(Screen.width*0.13f,Screen.height*0.69f,725,100), m_loadSceneData.GetCharacterName(m_currentScene), "CharacterNameColor");
				
				if(IsAutoTalkScene(m_currentScene)){
					GUI.Label(new Rect(Screen.width*0.16f,Screen.height*0.72f,700,100), m_loadSceneData.LoadVoiceText(m_currentScene));
					AutoConversationSceneSwitch();
				}
				else{
					GUI.Label(new Rect(Screen.width*0.16f,Screen.height*0.72f,700,100), m_loadSceneData.LoadVoiceText(m_currentScene));
				}
			}
		}
		//Large size dialog box
		else{
			GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
			//Hope
			if(Global.CurrentInteractNPC == "Hope"){
			}
			//Non Hope
			else{
				if(IsAutoTalkScene(m_currentScene)){//Not necessary code, but just in case
					GUI.Label(new Rect(130.0f, 75.0f, 700.0f, 400.0f), m_loadSceneData.LoadVoiceText(m_currentScene));
					AutoConversationSceneSwitch();
				}
				else{
					GUI.Label(new Rect(130.0f, 75.0f, 700.0f, 400.0f), m_loadSceneData.LoadVoiceText(m_currentScene));
				}
			}
		}
		
		//Hope-----------------------------------------------------------------------------------------------------------
		if(Global.CurrentInteractNPC == "Hope"){
			//Previous button
			if(m_loadSceneData.PreviousButtonExist(m_currentTuiScene)){
				if(GUI.Button(new Rect(Screen.height * 0.13f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxPreButton") || Global.arrowKey == "left"){
					//Stop the orginal voice, and loads up new voice
					m_characterVoiceAS.Stop();
					
					//Update the new scene (Hope)
					m_currentTuiScene = m_loadSceneData.GetPreSceneName(m_currentTuiScene);
					m_voiceCanStart = true;
				}
			}
			
			//Next button
			if(m_loadSceneData.NextButtonExist(m_currentTuiScene)){
				if(GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
					Global.arrowKey = "";

					
					//Stop the orginal voice, and loads up new voice
					m_characterVoiceAS.Stop();
					
					//Update the new scene (Hope)
					m_currentTuiScene = m_loadSceneData.GetNextSceneName(m_currentTuiScene);
					m_voiceCanStart = true;
				}
			}
		}
		//Non Hope-----------------------------------------------------------------------------------------------------------
		else{
			//Previous button
			if(m_loadSceneData.PreviousButtonExist(m_currentScene)){
				if(GUI.Button(new Rect(Screen.height * 0.13f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxPreButton") || Global.arrowKey == "left"){
					//Stop the orginal voice, and loads up new voice
					m_characterVoiceAS.Stop();
					
					//Update the new scene (Non Hope)
					m_currentScene = m_loadSceneData.GetPreSceneName(m_currentScene);
					m_voiceCanStart = true;
				}
			}
			
			//Next button
//			if(m_loadSceneData.NextButtonExist(m_currentScene) && 
//				!GameObject.Find(Global.CurrentInteractNPC).animation.IsPlaying("give") &&
//				(!Global.GetPlayer() || Global.GetPlayer() && !Global.GetPlayer().animation.IsPlaying("take"))){
			if(m_loadSceneData.NextButtonExist(m_currentScene)){
				if(GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
					Global.arrowKey = "";

					//Stop the orginal voice, and loads up new voice
					m_characterVoiceAS.Stop();
					
					if (Global.CurrentLevelNumber == 3)
					{
						if (m_currentScene == "L3MentorScene2")
						{
							Global.NPCGiveAwayObject("Mentor", "give", "_[id]30008_blinc");
							Global.m_pickNPCName = Global.GetPlayer().name;
							Global.m_PickAniName = "take";
						}
					}
					else if (Global.CurrentLevelNumber == 6)
					{
						if (m_currentScene == "L6MentorScene3")
						{
							Global.NPCGiveAwayObject("Mentor", "give", "_[id]30009_rapakey");
							Global.m_pickNPCName = Global.GetPlayer().name;
							Global.m_PickAniName = "take";
						}
					}
					
					//Update the new scene (Non Hope)
					if (Global.CurrentInteractNPC == "Guide" && m_currentScene.Contains("_A"))
					{
						/*if (m_iQ9Score == 1)
						{
							m_currentScene = "GuideScene9_A10";
						}
						else if (m_iQ9Score == 2 || m_iQ9Score == 3)
						{*/
						/* //FIXING TEXT POSITIONING
		
						if(m_iQ9Score > 1){
							m_currentScene = "GuideScene9_A11";
						}
						else
						{
							m_currentScene = m_loadSceneData.GetNextSceneName(m_currentScene);
						}
						m_iQ9Score = 0;
					}
					else
					{
						m_currentScene = m_loadSceneData.GetNextSceneName(m_currentScene);
					}
					m_voiceCanStart = true;
					
					m_bNPC1AnimationPlayOnce = true;
					m_bNPC2AnimationPlayOnce = true;
					
					
				}
			}
		}
	}
	
	/*
	*
	* This function displays the multichoice text
	*
	*/
	/* //FIXING TEXT POSITIONING

	public void DisplayMultiChoiseText(int _levelNum)
	{
		SwitchCharacterNameColor();
		
		ResultBarchart chart = GetComponent<ResultBarchart>();
		
		GUI.Label(new Rect(0,Screen.height*0.63f,Screen.width,Screen.height*0.3f), "", "NormalSizeDilogBox");
		
		//Display character's name
		GUI.Label(new Rect(Screen.width*0.13f,Screen.height*0.69f,725,100), m_loadSceneData.GetCharacterName(m_currentScene), "CharacterNameColor");
		
		//Questions box
		GUI.Label(new Rect(Screen.width*0.2f,Screen.height*0.80f,620,135), "", "NormalSizeDilogBox");
		
		//Question 1
		GUI.Label(new Rect(Screen.width*0.16f,Screen.height*0.72f,700,100), m_loadSceneData.GetQuestionText(m_currentScene, 1));
		
		bool showButton = false;

		//Option A
		if(m_loadSceneData.GetQuestionOptions(m_currentScene, 1, "A") != ""){
			if(GUI.Toggle(new Rect(Screen.width*0.25f, Screen.height*0.84f, 480, 20), m_bMultiChoice1OptionA, m_loadSceneData.GetQuestionOptions(m_currentScene, 1, "A")) || Global.multiChoice == "A"){
				if(Global.multiChoice == "A"){
					Global.multiChoice = "";
				}
				m_bMultiChoice1OptionA = true;
				m_bMultiChoice1OptionB = false;
				m_bMultiChoice1OptionC = false;
				m_bMultiChoice1OptionD = false;
				showButton = true;
			}
		}
		
		//Option B
		if(m_loadSceneData.GetQuestionOptions(m_currentScene, 1, "B") != ""){
			if(GUI.Toggle(new Rect(Screen.width*0.25f, Screen.height*0.86f, 480, 20), m_bMultiChoice1OptionB, m_loadSceneData.GetQuestionOptions(m_currentScene, 1, "B")) || Global.multiChoice == "B"){
				if(Global.multiChoice == "B"){
					Global.multiChoice = "";
				}		
				m_bMultiChoice1OptionA = false;
				m_bMultiChoice1OptionB = true;
				m_bMultiChoice1OptionC = false;
				m_bMultiChoice1OptionD = false;
				showButton = true;
				if (m_currentScene.Contains("_9"))
				{
					m_iQ9Score = 1;
				}
			}
		}
		
		//Option C
		if(m_loadSceneData.GetQuestionOptions(m_currentScene, 1, "C") != ""){
			if(GUI.Toggle(new Rect(Screen.width*0.25f, Screen.height*0.88f, 480, 20), m_bMultiChoice1OptionC, m_loadSceneData.GetQuestionOptions(m_currentScene, 1, "C")) || Global.multiChoice == "C"){
				if(Global.multiChoice == "C"){
					Global.multiChoice = "";
				}
				m_bMultiChoice1OptionA = false;
				m_bMultiChoice1OptionB = false;
				m_bMultiChoice1OptionC = true;
				m_bMultiChoice1OptionD = false;
				showButton = true;
				if (m_currentScene.Contains("_9"))
				{
					m_iQ9Score = 2;
				}
			}
		}
		
		//Option D
		if(m_loadSceneData.GetQuestionOptions(m_currentScene, 1, "D") != ""){
			if(GUI.Toggle(new Rect(Screen.width*0.25f, Screen.height*0.9f, 480, 20), m_bMultiChoice1OptionD, m_loadSceneData.GetQuestionOptions(m_currentScene, 1, "D")) || Global.multiChoice == "D"){
				if(Global.multiChoice == "D"){
					Global.multiChoice = "";
				}			
				m_bMultiChoice1OptionA = false;
				m_bMultiChoice1OptionB = false;
				m_bMultiChoice1OptionC = false;
				m_bMultiChoice1OptionD = true;
				showButton = true;
				if (m_currentScene.Contains("_9"))
				{
					m_iQ9Score = 3;
				}
			}
		}
		
		//Previous button
		if(m_loadSceneData.PreviousButtonExist(m_currentScene)){
			if(GUI.Button(new Rect(Screen.height * 0.13f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxPreButton") || Global.arrowKey == "left"){
				//Stop the orginal voice, and loads up new voice
				m_characterVoiceAS.Stop();
				
				//Update the new scene (Non Hope)
				m_currentScene = m_loadSceneData.GetPreSceneName(m_currentScene);
				m_voiceCanStart = true;
				
				//Reset  the variables
				m_bMultiChoice1OptionA = false;
				m_bMultiChoice1OptionB = false;
				m_bMultiChoice1OptionC = false;
				m_bMultiChoice1OptionD = false;
				
				if(m_PHQAAnswerIndex > 0){
					m_PHQAAnswerIndex--;
					chart.RemoveScore(m_PHQAAnswers[m_PHQAAnswerIndex]);
				}
			}
		}
		
		//Next button
		if(m_loadSceneData.NextButtonExist(m_currentScene) && showButton){
			if(GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
					Global.arrowKey = "";
				if (m_bMultiChoice1OptionA == true){
                    Debug.Log("PHQA:" + m_iPerPHQAQuestionScore);
                    chart.AddScore(0);
					m_iPerPHQAQuestionScore = 0;
				}
				else if (m_bMultiChoice1OptionB == true){
                    Debug.Log("PHQB:" + m_iPerPHQAQuestionScore);
                    chart.AddScore(1);
					m_iPerPHQAQuestionScore = 1;
				}
				else if (m_bMultiChoice1OptionC == true){
                    Debug.Log("PHQC:" + m_iPerPHQAQuestionScore);
                    chart.AddScore(2);
					m_iPerPHQAQuestionScore = 2;
				}
				else if (m_bMultiChoice1OptionD == true){
                    Debug.Log("PHQD:" + m_iPerPHQAQuestionScore);
                    chart.AddScore(3);
					m_iPerPHQAQuestionScore = 3;
				}
				
				m_currentScene = m_loadSceneData.GetNextSceneName(m_currentScene);
                Debug.Log("Current Scene" + m_currentScene);
				//The voice will be played again if the question type is statement or subMultichoice
				m_voiceCanStart = true;
				
				//Reset  the variables
				m_bMultiChoice1OptionA = false;
				m_bMultiChoice1OptionB = false;
				m_bMultiChoice1OptionC = false;
				m_bMultiChoice1OptionD = false;

				if(m_PHQAAnswers.Length == 0){
					m_PHQAAnswers = new int[9];
				}
				m_PHQAAnswers[m_PHQAAnswerIndex] = m_iPerPHQAQuestionScore;
				
				//Save the PHQA data once all the multi choice questions are answered
				if(m_PHQAAnswerIndex == 8){
					//SAVE PHQA EVENT
					StartCoroutine(instance.SaveUserPHQAEventToServer_NewJsonSchema(1, Global.CurrentLevelNumber, 
																	     m_PHQAAnswers[0], m_PHQAAnswers[1], m_PHQAAnswers[2], 
															 		     m_PHQAAnswers[3], m_PHQAAnswers[4], m_PHQAAnswers[5], 
															 		     m_PHQAAnswers[6], m_PHQAAnswers[7], m_PHQAAnswers[8]));
				}
				
				m_PHQAAnswerIndex += 1;
			}
		}
	}
	
	/*
	*
	* This function displays the nonVoice text
	*
	*/
	/* //FIXING TEXT POSITIONING

	public void DisplayNonVoiceText()
	{
		SwitchCharacterNameColor();
		
		//Display character's name
		GUI.Label(new Rect(Screen.width*0.13f,Screen.height*0.69f,725,100), m_loadSceneData.GetCharacterName(m_currentScene), "CharacterNameColor");
		
		//It is used to display the normal size dilog box
		if(m_loadSceneData.LoadNonVoiceText(m_currentScene).Length < 100){
			GUI.Label(new Rect(0,Screen.height*0.63f,Screen.width,Screen.height*0.3f), "", "NormalSizeDilogBox");
			GUI.Label(new Rect(120,407,725,100), m_loadSceneData.LoadNonVoiceText(m_currentScene));
		}
		
		//Large size dialog box
		else{
			GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
			GUI.Label(new Rect(130.0f, 75.0f, 700.0f, 400.0f), m_loadSceneData.LoadNonVoiceText(m_currentScene));
		}
		
		//Next button
		if(m_loadSceneData.NextButtonExist(m_currentScene)){
			if(m_loadSceneData.NextButtonExist(m_currentScene)){
				if(GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
					Global.arrowKey = "";

					m_currentScene = m_loadSceneData.GetNextSceneName(m_currentScene);
					
					//The voice will be played again if the question type is statement or subMultichoice
					m_voiceCanStart = true;
				}
			}
		}
	}
	
	/*
	*
	* This function displays the subMultichoice text
	*
	*/
	/* //FIXING TEXT POSITIONING

	public void DisplaySubMultichoicetext()
	{	
		string tempSceneName = "";
		string hopeTempSceneName = "";
		
		SwitchCharacterNameColor();
		//Hope-------------------------------------------------------------------------------------
		if(Global.CurrentInteractNPC == "Hope"){
			//Choose the background GUI style and display text position
			if(m_loadSceneData.LoadVoiceText(m_currentTuiScene).Length < 180){
				GUI.Label(new Rect(0,Screen.height*0.63f,Screen.width,Screen.height*0.3f), "", "NormalSizeDilogBox");
				//Display character's name
				GUI.Label(new Rect(Screen.width*0.15f,Screen.height*0.69f,725,100), m_loadSceneData.GetCharacterName(m_currentTuiScene), "CharacterNameColor");
				GUI.Label(new Rect(Screen.width*0.16f,Screen.height*0.72f,700,100), m_loadSceneData.LoadVoiceText(m_currentTuiScene));
				//questio box
				GUI.Label(new Rect(Screen.width*0.2f,Screen.height*0.80f,620,135), "", "NormalSizeDilogBox");
			}
			else{
				GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
				//Display character's name
				GUI.Label(new Rect(Screen.width*0.15f,Screen.height*0.69f,725,100), m_loadSceneData.GetCharacterName(m_currentTuiScene), "CharacterNameColor");
				GUI.Label(new Rect(200.0f, 150.0f, 600.0f, 300.0f), m_loadSceneData.LoadVoiceText(m_currentTuiScene));
			}
			
			//Options
			if(m_loadSceneData.SubMultiChoiceGetQuestionOptions(m_currentTuiScene, "A") != ""){
				if(GUI.Toggle(new Rect(Screen.width*0.25f, Screen.height*0.84f, 480, 20), m_bTuiSubMultiChoiceOptionA, m_loadSceneData.SubMultiChoiceGetQuestionOptions(m_currentTuiScene, "A")) || Global.multiChoice == "A"){
					if(Global.multiChoice == "A"){
						Global.multiChoice = "";
					}
					m_bTuiSubMultiChoiceOptionA = true;
					m_bTuiSubMultiChoiceOptionB = false;
					m_bTuiSubMultiChoiceOptionC = false;
					m_bTuiSubMultiChoiceOptionD = false;
									
					hopeTempSceneName = m_loadSceneData.GetSubMultichoiceNextSceneName(m_currentTuiScene, "A");
				}
			}
			if(m_loadSceneData.SubMultiChoiceGetQuestionOptions(m_currentTuiScene, "B") != ""){
				if(GUI.Toggle(new Rect(Screen.width*0.25f, Screen.height*0.86f, 480, 20), m_bTuiSubMultiChoiceOptionB, m_loadSceneData.SubMultiChoiceGetQuestionOptions(m_currentTuiScene, "B")) || Global.multiChoice == "B"){
					if(Global.multiChoice == "B"){
						Global.multiChoice = "";
					}
					m_bTuiSubMultiChoiceOptionA = false;
					m_bTuiSubMultiChoiceOptionB = true;
					m_bTuiSubMultiChoiceOptionC = false;
					m_bTuiSubMultiChoiceOptionD = false;
					
					hopeTempSceneName = m_loadSceneData.GetSubMultichoiceNextSceneName(m_currentTuiScene, "B");
				}
			}
			if(m_loadSceneData.SubMultiChoiceGetQuestionOptions(m_currentTuiScene, "C") != ""){
				if(GUI.Toggle(new Rect(Screen.width*0.25f, Screen.height*0.88f, 480, 20), m_bTuiSubMultiChoiceOptionC, m_loadSceneData.SubMultiChoiceGetQuestionOptions(m_currentTuiScene, "C")) || Global.multiChoice == "C"){
					if(Global.multiChoice == "C"){
						Global.multiChoice = "";
					}
					m_bTuiSubMultiChoiceOptionA = false;
					m_bTuiSubMultiChoiceOptionB = false;
					m_bTuiSubMultiChoiceOptionC = true;
					m_bTuiSubMultiChoiceOptionD = false;
					
					hopeTempSceneName = m_loadSceneData.GetSubMultichoiceNextSceneName(m_currentTuiScene, "C");
				}
			}
			if(m_loadSceneData.SubMultiChoiceGetQuestionOptions(m_currentTuiScene, "D") != ""){
				if(GUI.Toggle(new Rect(Screen.width*0.25f, Screen.height*0.9f, 480, 20), m_bTuiSubMultiChoiceOptionD, m_loadSceneData.SubMultiChoiceGetQuestionOptions(m_currentTuiScene, "D")) || Global.multiChoice == "D"){
					if(Global.multiChoice == "D"){
						Global.multiChoice = "";
					}
					m_bTuiSubMultiChoiceOptionA = false;
					m_bTuiSubMultiChoiceOptionB = false;
					m_bTuiSubMultiChoiceOptionC = false;
					m_bTuiSubMultiChoiceOptionD = true;
					
					hopeTempSceneName = m_loadSceneData.GetSubMultichoiceNextSceneName(m_currentTuiScene, "D");
				}
			}
			
			//Previous button
			if(m_loadSceneData.PreviousButtonExist(m_currentTuiScene)){
				if(GUI.Button(new Rect(Screen.height * 0.13f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxPreButton") || Global.arrowKey == "left"){
					
					//Stop the orginal voice, and loads up new voice
					m_characterVoiceAS.Stop();
					m_currentTuiScene = m_loadSceneData.GetPreSceneName(m_currentTuiScene);
					m_voiceCanStart = true;
				}
			}
			//Next button
			if(m_loadSceneData.NextButtonExist(m_currentTuiScene)){
				if(IsOptionsSelected()){
					if(GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
					Global.arrowKey = "";

						if(Global.CurrentLevelNumber == 4){
							Debug.Log ("Releasing Camera");
							GameObject.Find("Main Camera").GetComponent<CameraMovement>().m_bCameraLookAt = false;
						}
						string selectedLabel = "";
						
						if(m_bTuiSubMultiChoiceOptionA){
							selectedLabel = "A";
						}
						if(m_bTuiSubMultiChoiceOptionB){
							selectedLabel = "B";
						}
						if(m_bTuiSubMultiChoiceOptionC){
							selectedLabel = "C";
						}
						if(m_bTuiSubMultiChoiceOptionD){
							selectedLabel = "D";
						}
						
						if(m_loadSceneData.GetQuestionType(m_currentTuiScene).Equals("SubMultiChoiceQuestion")){
							if(m_loadSceneData.GetSubMultichoiceGotoType(m_currentTuiScene, selectedLabel).Equals("Imag")){
								//Load the script
								m_loadSceneData.SwitchScriptByName(m_loadSceneData.GetSubMultichoiceNextSceneName(m_currentTuiScene, selectedLabel));
							}
							else{
								//Load to another scene
								m_currentTuiScene = hopeTempSceneName;
							}
						}
						
						//The voice will be played again if the question type is statement or subMultichoice
						//Stop the orginal voice, and loads up new voice
                        Debug.Log("Stopped by Multichoise");
						m_characterVoiceAS.Stop();
						m_voiceCanStart = true;
						
						//Reset the variables
						m_bTuiSubMultiChoiceOptionA = false;
						m_bTuiSubMultiChoiceOptionB = false;
						m_bTuiSubMultiChoiceOptionC = false;
						m_bTuiSubMultiChoiceOptionD = false;
					}
				}
			}//End of next button
		}
		//Non Hope-------------------------------------------------------------------------------------
		else{
			//Debug.Log (m_loadSceneData.SubMultiChoiceGetQuestionOptions(m_currentScene, "C").Length);
			if(m_loadSceneData.SubMultiChoiceGetQuestionOptions(m_currentScene, "C").Length == 0){
				//Debug.Log ("C len = 0");
				m_bNpcSubMultiChoiceOptionC = false;
			}
			//Choose the background GUI style and display text position
			if(m_loadSceneData.LoadVoiceText(m_currentScene).Length < 240){
				if(m_currentScene != "L7PlayerScene1"){//When player ask for help, this box is not needed
					GUI.Label(new Rect(0,Screen.height*0.63f,Screen.width,Screen.height*0.3f), "", "NormalSizeDilogBox");
				}
				
				//Display character's name
				GUI.Label(new Rect(Screen.width*0.13f,Screen.height*0.69f,725,100), m_loadSceneData.GetCharacterName(m_currentScene), "CharacterNameColor");
				GUI.Label(new Rect(Screen.width*0.16f,Screen.height*0.72f,700,100), m_loadSceneData.LoadVoiceText(m_currentScene));
				//Answer box
				GUI.Label(new Rect(Screen.width*0.2f,Screen.height*0.80f,620,135), "", "NormalSizeDilogBox");
			}
			else{
				GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
				//Display character's name
				GUI.Label(new Rect(Screen.width*0.13f,Screen.height*0.69f,725,100), m_loadSceneData.GetCharacterName(m_currentScene), "CharacterNameColor");
				GUI.Label(new Rect(200.0f, 150.0f, 600.0f, 300.0f), m_loadSceneData.LoadVoiceText(m_currentScene));
			}
			
			//Options
			if(m_loadSceneData.SubMultiChoiceGetQuestionOptions(m_currentScene, "A") != ""){
				if(GUI.Toggle(new Rect(Screen.width*0.25f, Screen.height*0.84f, 480, 20), m_bNpcSubMultiChoiceOptionA, m_loadSceneData.SubMultiChoiceGetQuestionOptions(m_currentScene, "A")) || Global.multiChoice == "A"){
					if(Global.multiChoice == "A"){
						Global.multiChoice = "";
					}
					m_bNpcSubMultiChoiceOptionA = true;
					m_bNpcSubMultiChoiceOptionB = false;
					m_bNpcSubMultiChoiceOptionC = false;
					m_bNpcSubMultiChoiceOptionD = false;
					
					if (m_currentScene == "L4GuideScene24")
					{
						L4EnterTexts.m_bTriggered = true;
					}
					
					//For level 7 only
					m_bL7NeedHelpOptionA = true;
					m_bL7UnNeedHelpOptionB = false;
					
					//This code is used to record which option is being selected
					OptionSelecetionReset(0);
						
					tempSceneName = m_loadSceneData.GetSubMultichoiceNextSceneName(m_currentScene, "A");
				}
			}
			string optionA = m_loadSceneData.SubMultiChoiceGetQuestionOptions(m_currentScene, "A");
			Rect Bbox = new Rect(Screen.width*0.25f, Screen.height*0.86f, 480, 20);
			if (optionA.Contains("\n"))
			{
				Bbox = new Rect(Screen.width*0.25f, Screen.height*0.88f, 480, 20);
			}
			if(m_loadSceneData.SubMultiChoiceGetQuestionOptions(m_currentScene, "B") != ""){
				if(GUI.Toggle(Bbox, m_bNpcSubMultiChoiceOptionB, m_loadSceneData.SubMultiChoiceGetQuestionOptions(m_currentScene, "B")) || Global.multiChoice == "B"){
					if(Global.multiChoice == "B"){
						Global.multiChoice = "";
					}
					m_bNpcSubMultiChoiceOptionA = false;
					m_bNpcSubMultiChoiceOptionB = true;
					m_bNpcSubMultiChoiceOptionC = false;
					m_bNpcSubMultiChoiceOptionD = false;
					
					if (m_currentScene == "L4GuideScene24")
					{
						L4EnterTexts.m_bTriggered = false;
					}
					
					//For level 7 only
					m_bL7UnNeedHelpOptionB = true;
					m_bL7NeedHelpOptionA = false;
					
					//This code is used to record which option is being selected
					OptionSelecetionReset(1);
					
					tempSceneName = m_loadSceneData.GetSubMultichoiceNextSceneName(m_currentScene, "B");
				}
			}
			if(m_loadSceneData.SubMultiChoiceGetQuestionOptions(m_currentScene, "C") != ""){
				if(GUI.Toggle(new Rect(Screen.width*0.25f, Screen.height*0.88f, 480, 20), m_bNpcSubMultiChoiceOptionC, m_loadSceneData.SubMultiChoiceGetQuestionOptions(m_currentScene, "C")) || Global.multiChoice == "C"){
					if(Global.multiChoice == "C"){
						Global.multiChoice = "";
					}
					m_bNpcSubMultiChoiceOptionA = false;
					m_bNpcSubMultiChoiceOptionB = false;
					m_bNpcSubMultiChoiceOptionC = true;
					m_bNpcSubMultiChoiceOptionD = false;
					
					//This code is used to record which option is being selected
					OptionSelecetionReset(2);
					
					tempSceneName = m_loadSceneData.GetSubMultichoiceNextSceneName(m_currentScene, "C");
				}
			}
			if(m_loadSceneData.SubMultiChoiceGetQuestionOptions(m_currentScene, "D") != ""){
				if(GUI.Toggle(new Rect(Screen.width*0.25f, Screen.height*0.9f, 480, 20), m_bNpcSubMultiChoiceOptionD, m_loadSceneData.SubMultiChoiceGetQuestionOptions(m_currentScene, "D")) || Global.multiChoice == "D"){
					if(Global.multiChoice == "D"){
						Global.multiChoice = "";
					}
					m_bNpcSubMultiChoiceOptionA = false;
					m_bNpcSubMultiChoiceOptionB = false;
					m_bNpcSubMultiChoiceOptionC = false;
					m_bNpcSubMultiChoiceOptionD = true;
					
					//This code is used to record which option is being selected
					OptionSelecetionReset(3);
					
					tempSceneName = m_loadSceneData.GetSubMultichoiceNextSceneName(m_currentScene, "D");
				}
			}
			
			//Previous button
			if(m_loadSceneData.PreviousButtonExist(m_currentScene) == true){
				if(GUI.Button(new Rect(Screen.height * 0.13f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxPreButton") || Global.arrowKey == "left"){
					
					//Stop the orginal voice, and loads up new voice
					m_characterVoiceAS.Stop();
					m_currentScene = m_loadSceneData.GetPreSceneName(m_currentScene);	
					m_voiceCanStart = true;
				}
			}
			//Next button
			if(m_loadSceneData.NextButtonExist(m_currentScene)){
				if(IsOptionsSelected()){
					Rect rectQuetionBox;
					if(m_currentScene != "L7PlayerScene1"){
						rectQuetionBox = new Rect(Screen.width*0.82f, Screen.height * 0.7813f, 60, 60);
					}
					else{
						rectQuetionBox = new Rect(Screen.width*0.7f, Screen.height * 0.85f, 60, 60);
					}
//					Debug.Log ("Length A: " + m_loadSceneData.SubMultiChoiceGetQuestionOptions(m_currentScene, "A").Length);
//					Debug.Log ("Length B: " + m_loadSceneData.SubMultiChoiceGetQuestionOptions(m_currentScene, "B").Length);
//					Debug.Log ("Length C: " + m_loadSceneData.SubMultiChoiceGetQuestionOptions(m_currentScene, "C").Length);
//					Debug.Log ("Length D: " + m_loadSceneData.SubMultiChoiceGetQuestionOptions(m_currentScene, "D").Length);
					if(GUI.Button(rectQuetionBox, "", "dilogBoxNextButton") || Global.arrowKey == "right"){
						Global.arrowKey = "";

						string selectedLabel = "";
						
						if(m_bNpcSubMultiChoiceOptionA){
							selectedLabel = "A";
						}
						if(m_bNpcSubMultiChoiceOptionB){
							selectedLabel = "B";
						}
						if(m_bNpcSubMultiChoiceOptionC){
							selectedLabel = "C";
						}
						if(m_bNpcSubMultiChoiceOptionD){
							selectedLabel = "D";
						}
						
						if(m_loadSceneData.GetQuestionType(m_currentScene).Equals("SubMultiChoiceQuestion")){
							if(m_loadSceneData.GetSubMultichoiceGotoType(m_currentScene, selectedLabel).Equals("Imag")){
								//Load the script
								m_loadSceneData.SwitchScriptByName(m_loadSceneData.GetSubMultichoiceNextSceneName(m_currentScene, selectedLabel));
							}
							else{
								//Load to another scene
								m_currentScene = tempSceneName;
							}
						}
						
						//The voice will be played again if the question type is statement or subMultichoice
						//Stop the orginal voice, and loads up new voice
						m_characterVoiceAS.Stop();
						m_voiceCanStart = true;
						
						//Reset the variables
						m_bNpcSubMultiChoiceOptionA = false;
						m_bNpcSubMultiChoiceOptionB = false;
						m_bNpcSubMultiChoiceOptionC = false;
						m_bNpcSubMultiChoiceOptionD = false;
						
						m_bL7NeedHelpOptionA = false;
						m_bL7UnNeedHelpOptionB = false;
					}
				}
			}
		}
	}
	
	/*
	*
	* This function is used to check whether any of the submultichoice 
	* options is being selected
	*
	*/
	/*
	*
	* This function displays the statement text
	*
	*/
	public void DisplayStateMentText()
	{			
		SwitchCharacterNameColor();

		//Normal size dilog box
		if(m_loadSceneData.LoadVoiceText(m_currentScene).Length < 450){
			GUI.Label(new Rect(30,350,920,250), "", "NormalSizeDilogBox");
			//Hope
			if(Global.CurrentInteractNPC == "Hope"){
				//Display character's name
				GUI.Label(new Rect(120,400,725,100), m_loadSceneData.GetCharacterName(m_currentTuiScene), "CharacterNameColor");
				GUI.Label(new Rect(120,416,725,100), m_loadSceneData.LoadVoiceText(m_currentTuiScene));
			}
			//Non Hope
			else{
				//Display character's name
				GUI.Label(new Rect(120,400,725,100), m_loadSceneData.GetCharacterName(m_currentScene), "CharacterNameColor");

				if(IsAutoTalkScene(m_currentScene)){
					GUI.Label(new Rect(120,418,725,100), m_loadSceneData.LoadVoiceText(m_currentScene));
					AutoConversationSceneSwitch();
				}
				else{
					GUI.Label(new Rect(120,418,725,100), m_loadSceneData.LoadVoiceText(m_currentScene));
				}
			}

			if(m_currentScene == "L7DarroScene1"){
				Global.GetPlayer().GetComponent<PlayerMovement>().m_bMovement = true;
			}
		}
		//Large size dialog box
		else{
			GUI.Label(new Rect(0.0f, 0.0f, 960.0f, 600.0f), "", "FullSizeDialogBox");
			//Hope
			if(Global.CurrentInteractNPC == "Hope"){
			}
			//Non Hope
			else{
				if(IsAutoTalkScene(m_currentScene)){//Not necessary code, but just in case
					GUI.Label(new Rect(130.0f, 75.0f, 700.0f, 400.0f), m_loadSceneData.LoadVoiceText(m_currentScene));
					AutoConversationSceneSwitch();
				}
				else{
					GUI.Label(new Rect(130.0f, 75.0f, 700.0f, 400.0f), m_loadSceneData.LoadVoiceText(m_currentScene));
				}
			}
		}

		//Hope-----------------------------------------------------------------------------------------------------------
		if(Global.CurrentInteractNPC == "Hope"){
			//Previous button
			if(m_loadSceneData.PreviousButtonExist(m_currentTuiScene)){
				if(GUI.Button(new Rect(113, 495, 60, 60), "", "dilogBoxPreButton") || Global.arrowKey == "left"){
					//Stop the orginal voice, and loads up new voice
					m_characterVoiceAS.Stop();
					Debug.Log ("CHARVIOCE 1863 - STOPED!");
					//Update the new scene (Hope)
					m_currentTuiScene = m_loadSceneData.GetPreSceneName(m_currentTuiScene);
					m_voiceCanStart = true;
				}
			}

			//Next button
			if(m_loadSceneData.NextButtonExist(m_currentTuiScene)){
				if(GUI.Button(new Rect(800, 495, 60, 60), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
					Global.arrowKey = "";


					//Stop the orginal voice, and loads up new voice
					m_characterVoiceAS.Stop();
					Debug.Log ("CHARVIOCE 1879 - STOPED!");
					//Update the new scene (Hope)
					m_currentTuiScene = m_loadSceneData.GetNextSceneName(m_currentTuiScene);
					m_voiceCanStart = true;
				}
			}
		}
		//Non Hope-----------------------------------------------------------------------------------------------------------
		else{
			//Previous button
			if(m_loadSceneData.PreviousButtonExist(m_currentScene)){
				if(GUI.Button(new Rect(113, 495, 60, 60), "", "dilogBoxPreButton") || Global.arrowKey == "left"){
					//Stop the orginal voice, and loads up new voice
					m_characterVoiceAS.Stop();
					Debug.Log ("CHARVIOCE 1893 - STOPED!");

					//Update the new scene (Non Hope)
					m_currentScene = m_loadSceneData.GetPreSceneName(m_currentScene);
					m_voiceCanStart = true;
				}
			}

			//Next button
			//			if(m_loadSceneData.NextButtonExist(m_currentScene) && 
			//				!GameObject.Find(Global.CurrentInteractNPC).animation.IsPlaying("give") &&
			//				(!Global.GetPlayer() || Global.GetPlayer() && !Global.GetPlayer().animation.IsPlaying("take"))){
			if(m_loadSceneData.NextButtonExist(m_currentScene)){
				if (GUI.Button (new Rect (800, 495, 60, 60), "", "dilogBoxNextButton") || Global.arrowKey == "right") {
					Global.arrowKey = "";

					//Stop the orginal voice, and loads up new voice
					m_characterVoiceAS.Stop ();
					Debug.Log ("CHARVIOCE 1909 - STOPED!");

					if (Global.CurrentLevelNumber == 3) {
						if (m_currentScene == "L3MentorScene2") {
							Global.NPCGiveAwayObject ("Mentor", "give", "_[id]30008_blinc");
							Global.m_pickNPCName = Global.GetPlayer ().name;
							Global.m_PickAniName = "take";
						}
					} else if (Global.CurrentLevelNumber == 6) {
						if (m_currentScene == "L6MentorScene3") {
							Global.NPCGiveAwayObject ("Mentor", "give", "_[id]30009_rapakey");
							Global.m_pickNPCName = Global.GetPlayer ().name;
							Global.m_PickAniName = "take";
						}
					}
					
					if(Global.CurrentLevelNumber == 7){

						if (Global.CurrentInteractNPC == "Guide" && m_currentScene.Contains ("_A")) {
							/*if (m_iQ9Score == 1)
						{
							m_currentScene = "GuideScene9_A10";
						}
						else if (m_iQ9Score == 2 || m_iQ9Score == 3)*/
							if (m_iQ9Score == 0) {
								m_currentScene = "L7GuideScene3_1"; //REQUIREMENT FOR SPARX_R If Q9 (L7.020) =1,2,3 then L7.026 is displayed, then goes to L7.027 and continues on from there. If Q9 (L7.020) =0 go to L7.030
							}
							else if (m_iQ9Score == 1 || m_iQ9Score == 2 || m_iQ9Score == 3) {
								m_currentScene = "GuideScene9_A11";
							} else {
								m_currentScene = m_loadSceneData.GetNextSceneName (m_currentScene);
							}
							m_iQ9Score = 0;
						} else {
							m_currentScene = m_loadSceneData.GetNextSceneName (m_currentScene);
						}

					} else {
						//Update the new scene (Non Hope)
						if (Global.CurrentInteractNPC == "Guide" && m_currentScene.Contains ("_A")) {
							/*if (m_iQ9Score == 1)
							{
								m_currentScene = "GuideScene9_A10";
							}
							else if (m_iQ9Score == 2 || m_iQ9Score == 3)*/
							if (m_iQ9Score == 1 || m_iQ9Score == 2 || m_iQ9Score == 3) {
								m_currentScene = "GuideScene9_A11";
							} else {
								m_currentScene = m_loadSceneData.GetNextSceneName (m_currentScene);
							}
							m_iQ9Score = 0;
						} else {
							m_currentScene = m_loadSceneData.GetNextSceneName (m_currentScene);
						}
																
					}

					m_voiceCanStart = true;

					m_bNPC1AnimationPlayOnce = true;
					m_bNPC2AnimationPlayOnce = true;


				}
			}
		}
	}

	/*
	*
	* This function displays the multichoice text
	*
	*/
	public void DisplayMultiChoiseText(int _levelNum)
	{
		SwitchCharacterNameColor();

		ResultBarchart chart = GetComponent<ResultBarchart>();

		GUI.Label(new Rect(30,350,920,250), "", "NormalSizeDilogBox");

		//Display character's name
		GUI.Label(new Rect(120,400,725,100), m_loadSceneData.GetCharacterName(m_currentScene), "CharacterNameColor");

		//Questions box
		GUI.Label(new Rect(180,470,620,135), "", "NormalSizeDilogBox");

		//Question 1
		GUI.Label(new Rect(120,418,725,100), m_loadSceneData.GetQuestionText(m_currentScene, 1));

		bool showButton = false;

		//Debug.Log ("TALKSCENE DEBUG @ LINE 1053: " + m_iQ9Score.ToString ());

		//Option A
		if(m_loadSceneData.GetQuestionOptions(m_currentScene, 1, "A") != ""){
			if(GUI.Toggle(new Rect(235, 500, 480, 20), m_bMultiChoice1OptionA, m_loadSceneData.GetQuestionOptions(m_currentScene, 1, "A")) || Global.multiChoice == "A"){
				if(Global.multiChoice == "A"){
					Global.multiChoice = "";
				}
				m_bMultiChoice1OptionA = true;
				m_bMultiChoice1OptionB = false;
				m_bMultiChoice1OptionC = false;
				m_bMultiChoice1OptionD = false;
				showButton = true;
				if (m_currentScene.Contains("_9"))
				{
					m_iQ9Score = 0;
				}
			}
		}

		//Option B
		if(m_loadSceneData.GetQuestionOptions(m_currentScene, 1, "B") != ""){
			if(GUI.Toggle(new Rect(235, 518, 480, 20), m_bMultiChoice1OptionB, m_loadSceneData.GetQuestionOptions(m_currentScene, 1, "B")) || Global.multiChoice == "B"){
				if(Global.multiChoice == "B"){
					Global.multiChoice = "";
				}		
				m_bMultiChoice1OptionA = false;
				m_bMultiChoice1OptionB = true;
				m_bMultiChoice1OptionC = false;
				m_bMultiChoice1OptionD = false;
				showButton = true;
				if (m_currentScene.Contains("_9"))
				{
					m_iQ9Score = 1;
				}
			}
		}

		//Option C
		if(m_loadSceneData.GetQuestionOptions(m_currentScene, 1, "C") != ""){
			if(GUI.Toggle(new Rect(235, 536, 480, 20), m_bMultiChoice1OptionC, m_loadSceneData.GetQuestionOptions(m_currentScene, 1, "C")) || Global.multiChoice == "C"){
				if(Global.multiChoice == "C"){
					Global.multiChoice = "";
				}
				m_bMultiChoice1OptionA = false;
				m_bMultiChoice1OptionB = false;
				m_bMultiChoice1OptionC = true;
				m_bMultiChoice1OptionD = false;
				showButton = true;
				if (m_currentScene.Contains("_9"))
				{
					m_iQ9Score = 2;
				}
			}
		}

		//Option D
		if(m_loadSceneData.GetQuestionOptions(m_currentScene, 1, "D") != ""){
			if(GUI.Toggle(new Rect(235, 554, 480, 20), m_bMultiChoice1OptionD, m_loadSceneData.GetQuestionOptions(m_currentScene, 1, "D")) || Global.multiChoice == "D"){
				if(Global.multiChoice == "D"){
					Global.multiChoice = "";
				}			
				m_bMultiChoice1OptionA = false;
				m_bMultiChoice1OptionB = false;
				m_bMultiChoice1OptionC = false;
				m_bMultiChoice1OptionD = true;
				showButton = true;
				if (m_currentScene.Contains("_9"))
				{
					m_iQ9Score = 3;
				}
			}
		}

		//Previous button
		if(m_loadSceneData.PreviousButtonExist(m_currentScene)){
			if(GUI.Button(new Rect(113, 495, 60, 60), "", "dilogBoxPreButton") || Global.arrowKey == "left"){
				//Stop the orginal voice, and loads up new voice
				m_characterVoiceAS.Stop();
				Debug.Log ("CHARVIOCE 2063 - STOPED!");
				//Update the new scene (Non Hope)
				m_currentScene = m_loadSceneData.GetPreSceneName(m_currentScene);
				m_voiceCanStart = true;

				//Reset  the variables
				m_bMultiChoice1OptionA = false;
				m_bMultiChoice1OptionB = false;
				m_bMultiChoice1OptionC = false;
				m_bMultiChoice1OptionD = false;

				if(m_PHQAAnswerIndex > 0){
					m_PHQAAnswerIndex--;
					chart.RemoveScore(m_PHQAAnswers[m_PHQAAnswerIndex]);
				}
			}
		}

		//Next button
		if(m_loadSceneData.NextButtonExist(m_currentScene) && showButton){
			if(GUI.Button(new Rect(800, 495, 60, 60), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
				Global.arrowKey = "";

				if (m_bMultiChoice1OptionA == true){
					chart.AddScore(0);
					m_iPerPHQAQuestionScore = 0;
				}
				else if (m_bMultiChoice1OptionB == true){
					chart.AddScore(1);
					m_iPerPHQAQuestionScore = 1;
				}
				else if (m_bMultiChoice1OptionC == true){
					chart.AddScore(2);
					m_iPerPHQAQuestionScore = 2;
				}
				else if (m_bMultiChoice1OptionD == true){
					chart.AddScore(3);
					m_iPerPHQAQuestionScore = 3;
				}

				m_currentScene = m_loadSceneData.GetNextSceneName(m_currentScene);

				//The voice will be played again if the question type is statement or subMultichoice
				m_voiceCanStart = true;

				//Reset  the variables
				m_bMultiChoice1OptionA = false;
				m_bMultiChoice1OptionB = false;
				m_bMultiChoice1OptionC = false;
				m_bMultiChoice1OptionD = false;

				if(m_PHQAAnswers.Length == 0){
					m_PHQAAnswers = new int[9];
				}
				m_PHQAAnswers[m_PHQAAnswerIndex] = m_iPerPHQAQuestionScore;

				//CapturedDataInput instance2 = CapturedDataInput.GetInstance;
				//Save the PHQA data once all the multi choice questions are answered
				if(m_PHQAAnswerIndex == 8){
					//SAVE PHQA EVENT
					StartCoroutine(instance.SaveUserPHQAEventToServer_NewJsonSchema(1, Global.CurrentLevelNumber, 
						m_PHQAAnswers[0], m_PHQAAnswers[1], m_PHQAAnswers[2], 
						m_PHQAAnswers[3], m_PHQAAnswers[4], m_PHQAAnswers[5], 
						m_PHQAAnswers[6], m_PHQAAnswers[7], m_PHQAAnswers[8]));
				}

				m_PHQAAnswerIndex += 1;
			}
		}
	}

	/*
	*
	* This function displays the nonVoice text
	*
	*/
	public void DisplayNonVoiceText()
	{
		SwitchCharacterNameColor();

		//Display character's name
		GUI.Label(new Rect(120,400,725,100), m_loadSceneData.GetCharacterName(m_currentScene), "CharacterNameColor");

		//It is used to display the normal size dilog box
		if(m_loadSceneData.LoadNonVoiceText(m_currentScene).Length < 100){
			GUI.Label(new Rect(30,350,920,250), "", "NormalSizeDilogBox");
			GUI.Label(new Rect(120,407,725,100), m_loadSceneData.LoadNonVoiceText(m_currentScene));
		}

		//Large size dialog box
		else{
			GUI.Label(new Rect(0.0f, 0.0f, 960.0f, 600.0f), "", "FullSizeDialogBox");
			GUI.Label(new Rect(130.0f, 75.0f, 700.0f, 400.0f), m_loadSceneData.LoadNonVoiceText(m_currentScene));
		}

		//Next button
		if(m_loadSceneData.NextButtonExist(m_currentScene)){
			if(m_loadSceneData.NextButtonExist(m_currentScene)){
				if(GUI.Button(new Rect(800, 495, 60, 60), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
					Global.arrowKey = "";

					m_currentScene = m_loadSceneData.GetNextSceneName(m_currentScene);

					//The voice will be played again if the question type is statement or subMultichoice
					m_voiceCanStart = true;
				}
			}
		}
	}

	/*
	*
	* This function displays the subMultichoice text
	*
	*/
	public void DisplaySubMultichoicetext()
	{	
		string tempSceneName = "";
		string hopeTempSceneName = "";

		SwitchCharacterNameColor();
		//Hope-------------------------------------------------------------------------------------
		if(Global.CurrentInteractNPC == "Hope"){
			//Choose the background GUI style and display text position
			if(m_loadSceneData.LoadVoiceText(m_currentTuiScene).Length < 180){
				GUI.Label(new Rect(30,350,920,250), "", "NormalSizeDilogBox");
				//Display character's name
				GUI.Label(new Rect(120,400,725,100), m_loadSceneData.GetCharacterName(m_currentTuiScene), "CharacterNameColor");
				GUI.Label(new Rect(120,418,725,100), m_loadSceneData.LoadVoiceText(m_currentTuiScene));
				//questio box
				GUI.Label(new Rect(180,470,620,135), "", "NormalSizeDilogBox");
			}
			else{
				GUI.Label(new Rect(0.0f, 0.0f, 960.0f, 600.0f), "", "FullSizeDialogBox");
				//Display character's name
				GUI.Label(new Rect(120,400,725,100), m_loadSceneData.GetCharacterName(m_currentTuiScene), "CharacterNameColor");
				GUI.Label(new Rect(200.0f, 150.0f, 600.0f, 300.0f), m_loadSceneData.LoadVoiceText(m_currentTuiScene));
			}

			//Options
			if(m_loadSceneData.SubMultiChoiceGetQuestionOptions(m_currentTuiScene, "A") != ""){
				if(GUI.Toggle(new Rect(235, 500, 480, 20), m_bTuiSubMultiChoiceOptionA, m_loadSceneData.SubMultiChoiceGetQuestionOptions(m_currentTuiScene, "A")) || Global.multiChoice == "A"){
					if(Global.multiChoice == "A"){
						Global.multiChoice = "";
					}
					m_bTuiSubMultiChoiceOptionA = true;
					m_bTuiSubMultiChoiceOptionB = false;
					m_bTuiSubMultiChoiceOptionC = false;
					m_bTuiSubMultiChoiceOptionD = false;

					hopeTempSceneName = m_loadSceneData.GetSubMultichoiceNextSceneName(m_currentTuiScene, "A");
				}
			}
			if(m_loadSceneData.SubMultiChoiceGetQuestionOptions(m_currentTuiScene, "B") != ""){
				if(GUI.Toggle(new Rect(235, 520, 480, 20), m_bTuiSubMultiChoiceOptionB, m_loadSceneData.SubMultiChoiceGetQuestionOptions(m_currentTuiScene, "B")) || Global.multiChoice == "B"){
					if(Global.multiChoice == "B"){
						Global.multiChoice = "";
					}
					m_bTuiSubMultiChoiceOptionA = false;
					m_bTuiSubMultiChoiceOptionB = true;
					m_bTuiSubMultiChoiceOptionC = false;
					m_bTuiSubMultiChoiceOptionD = false;

					hopeTempSceneName = m_loadSceneData.GetSubMultichoiceNextSceneName(m_currentTuiScene, "B");
				}
			}
			if(m_loadSceneData.SubMultiChoiceGetQuestionOptions(m_currentTuiScene, "C") != ""){
				if(GUI.Toggle(new Rect(235, 540, 480, 20), m_bTuiSubMultiChoiceOptionC, m_loadSceneData.SubMultiChoiceGetQuestionOptions(m_currentTuiScene, "C")) || Global.multiChoice == "C"){
					if(Global.multiChoice == "C"){
						Global.multiChoice = "";
					}
					m_bTuiSubMultiChoiceOptionA = false;
					m_bTuiSubMultiChoiceOptionB = false;
					m_bTuiSubMultiChoiceOptionC = true;
					m_bTuiSubMultiChoiceOptionD = false;

					hopeTempSceneName = m_loadSceneData.GetSubMultichoiceNextSceneName(m_currentTuiScene, "C");
				}
			}
			if(m_loadSceneData.SubMultiChoiceGetQuestionOptions(m_currentTuiScene, "D") != ""){
				if(GUI.Toggle(new Rect(235, 560, 480, 20), m_bTuiSubMultiChoiceOptionD, m_loadSceneData.SubMultiChoiceGetQuestionOptions(m_currentTuiScene, "D")) || Global.multiChoice == "D"){
					if(Global.multiChoice == "D"){
						Global.multiChoice = "";
					}
					m_bTuiSubMultiChoiceOptionA = false;
					m_bTuiSubMultiChoiceOptionB = false;
					m_bTuiSubMultiChoiceOptionC = false;
					m_bTuiSubMultiChoiceOptionD = true;

					hopeTempSceneName = m_loadSceneData.GetSubMultichoiceNextSceneName(m_currentTuiScene, "D");
				}
			}

			//Previous button
			if(m_loadSceneData.PreviousButtonExist(m_currentTuiScene)){
				if(GUI.Button(new Rect(113, 495, 60, 60), "", "dilogBoxPreButton") || Global.arrowKey == "left"){

					//Stop the orginal voice, and loads up new voice
					m_characterVoiceAS.Stop();
					Debug.Log ("CHARVIOCE 2261 - STOPED!");
					m_currentTuiScene = m_loadSceneData.GetPreSceneName(m_currentTuiScene);
					m_voiceCanStart = true;
				}
			}
			//Next button
			if(m_loadSceneData.NextButtonExist(m_currentTuiScene)){
				if(IsOptionsSelected()){
					if(GUI.Button(new Rect(800, 495, 60, 60), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
						Global.arrowKey = "";

						string selectedLabel = "";

						if(m_bTuiSubMultiChoiceOptionA){
							selectedLabel = "A";
						}
						if(m_bTuiSubMultiChoiceOptionB){
							selectedLabel = "B";
						}
						if(m_bTuiSubMultiChoiceOptionC){
							selectedLabel = "C";
						}
						if(m_bTuiSubMultiChoiceOptionD){
							selectedLabel = "D";
						}

						if(m_loadSceneData.GetQuestionType(m_currentTuiScene).Equals("SubMultiChoiceQuestion")){
							if(m_loadSceneData.GetSubMultichoiceGotoType(m_currentTuiScene, selectedLabel).Equals("Imag")){
								//Load the script
								m_loadSceneData.SwitchScriptByName(m_loadSceneData.GetSubMultichoiceNextSceneName(m_currentTuiScene, selectedLabel));
							}
							else{
								//Load to another scene
								m_currentTuiScene = hopeTempSceneName;
							}
						}

						//The voice will be played again if the question type is statement or subMultichoice
						//Stop the orginal voice, and loads up new voice
						m_characterVoiceAS.Stop();
						Debug.Log ("CHARVIOCE 2300 - STOPED!");
						m_voiceCanStart = true;

						//Reset the variables
						m_bTuiSubMultiChoiceOptionA = false;
						m_bTuiSubMultiChoiceOptionB = false;
						m_bTuiSubMultiChoiceOptionC = false;
						m_bTuiSubMultiChoiceOptionD = false;
					}
				}
			}//End of next button
		}
		//Non Hope-------------------------------------------------------------------------------------
		else{
			//Debug.Log (m_loadSceneData.SubMultiChoiceGetQuestionOptions(m_currentScene, "C").Length);
			if(m_loadSceneData.SubMultiChoiceGetQuestionOptions(m_currentScene, "C").Length == 0){
				//Debug.Log ("C len = 0");
				m_bNpcSubMultiChoiceOptionC = false;
			}
			//Choose the background GUI style and display text position
			if(m_loadSceneData.LoadVoiceText(m_currentScene).Length < 240){
				if(m_currentScene != "L7PlayerScene1"){//When player ask for help, this box is not needed
					GUI.Label(new Rect(30,350,920,250), "", "NormalSizeDilogBox");
				}

				//Display character's name
				GUI.Label(new Rect(120,400,725,100), m_loadSceneData.GetCharacterName(m_currentScene), "CharacterNameColor");
				GUI.Label(new Rect(120,418,725,100), m_loadSceneData.LoadVoiceText(m_currentScene));
				//Answer box
				GUI.Label(new Rect(180,470,620,135), "", "NormalSizeDilogBox");
			}
			else{
				GUI.Label(new Rect(0.0f, 0.0f, 960.0f, 600.0f), "", "FullSizeDialogBox");
				//Display character's name
				GUI.Label(new Rect(120,400,725,100), m_loadSceneData.GetCharacterName(m_currentScene), "CharacterNameColor");
				GUI.Label(new Rect(200.0f, 150.0f, 600.0f, 300.0f), m_loadSceneData.LoadVoiceText(m_currentScene));
			}

			//Options
			if(m_loadSceneData.SubMultiChoiceGetQuestionOptions(m_currentScene, "A") != ""){
				if(GUI.Toggle(new Rect(235, 500, 480, 20), m_bNpcSubMultiChoiceOptionA, m_loadSceneData.SubMultiChoiceGetQuestionOptions(m_currentScene, "A")) || Global.multiChoice == "A"){
					if(Global.multiChoice == "A"){
						Global.multiChoice = "";
					}
					m_bNpcSubMultiChoiceOptionA = true;
					m_bNpcSubMultiChoiceOptionB = false;
					m_bNpcSubMultiChoiceOptionC = false;
					m_bNpcSubMultiChoiceOptionD = false;

					if (m_currentScene == "L4GuideScene24")
					{
						L4EnterTexts.m_bTriggered = true;
					}

					//For level 7 only
					m_bL7NeedHelpOptionA = true;
					m_bL7UnNeedHelpOptionB = false;

					//This code is used to record which option is being selected
					OptionSelecetionReset(0);

					tempSceneName = m_loadSceneData.GetSubMultichoiceNextSceneName(m_currentScene, "A");
				}
			}
			string optionA = m_loadSceneData.SubMultiChoiceGetQuestionOptions(m_currentScene, "A");
			Rect Bbox = new Rect(235, 520, 480, 20);
			if (optionA.Contains("\n"))
			{
				Bbox = new Rect(235, 540, 480, 20);
			}
			if(m_loadSceneData.SubMultiChoiceGetQuestionOptions(m_currentScene, "B") != ""){
				if(GUI.Toggle(Bbox, m_bNpcSubMultiChoiceOptionB, m_loadSceneData.SubMultiChoiceGetQuestionOptions(m_currentScene, "B")) || Global.multiChoice == "B"){
					if(Global.multiChoice == "B"){
						Global.multiChoice = "";
					}
					m_bNpcSubMultiChoiceOptionA = false;
					m_bNpcSubMultiChoiceOptionB = true;
					m_bNpcSubMultiChoiceOptionC = false;
					m_bNpcSubMultiChoiceOptionD = false;

					if (m_currentScene == "L4GuideScene24")
					{
						L4EnterTexts.m_bTriggered = false;
					}

					//For level 7 only
					m_bL7UnNeedHelpOptionB = true;
					m_bL7NeedHelpOptionA = false;

					//This code is used to record which option is being selected
					OptionSelecetionReset(1);

					tempSceneName = m_loadSceneData.GetSubMultichoiceNextSceneName(m_currentScene, "B");
				}
			}
			if(m_loadSceneData.SubMultiChoiceGetQuestionOptions(m_currentScene, "C") != ""){
				if(GUI.Toggle(new Rect(235, 540, 480, 20), m_bNpcSubMultiChoiceOptionC, m_loadSceneData.SubMultiChoiceGetQuestionOptions(m_currentScene, "C")) || Global.multiChoice == "C"){
					if(Global.multiChoice == "C"){
						Global.multiChoice = "";
					}
					m_bNpcSubMultiChoiceOptionA = false;
					m_bNpcSubMultiChoiceOptionB = false;
					m_bNpcSubMultiChoiceOptionC = true;
					m_bNpcSubMultiChoiceOptionD = false;

					//This code is used to record which option is being selected
					OptionSelecetionReset(2);

					tempSceneName = m_loadSceneData.GetSubMultichoiceNextSceneName(m_currentScene, "C");
				}
			}
			if(m_loadSceneData.SubMultiChoiceGetQuestionOptions(m_currentScene, "D") != ""){
				if(GUI.Toggle(new Rect(235, 560, 480, 20), m_bNpcSubMultiChoiceOptionD, m_loadSceneData.SubMultiChoiceGetQuestionOptions(m_currentScene, "D")) || Global.multiChoice == "D"){
					if(Global.multiChoice == "D"){
						Global.multiChoice = "";
					}
					m_bNpcSubMultiChoiceOptionA = false;
					m_bNpcSubMultiChoiceOptionB = false;
					m_bNpcSubMultiChoiceOptionC = false;
					m_bNpcSubMultiChoiceOptionD = true;

					//This code is used to record which option is being selected
					OptionSelecetionReset(3);

					tempSceneName = m_loadSceneData.GetSubMultichoiceNextSceneName(m_currentScene, "D");
				}
			}

			//Previous button
			if(m_loadSceneData.PreviousButtonExist(m_currentScene) == true){
				if(GUI.Button(new Rect(113, 495, 60, 60), "", "dilogBoxPreButton") || Global.arrowKey == "left"){

					//Stop the orginal voice, and loads up new voice
					m_characterVoiceAS.Stop();
					Debug.Log ("CHARVIOCE 2433 - STOPED!");
					m_currentScene = m_loadSceneData.GetPreSceneName(m_currentScene);	
					m_voiceCanStart = true;
				}
			}
			//Next button
			if(m_loadSceneData.NextButtonExist(m_currentScene)){
				if(IsOptionsSelected()){
					Rect rectQuetionBox;
					if(m_currentScene != "L7PlayerScene1"){
						rectQuetionBox = new Rect(800, 495, 60, 60);
					}
					else{
						rectQuetionBox = new Rect(720, 510, 60, 60);
					}
					//					Debug.Log ("Length A: " + m_loadSceneData.SubMultiChoiceGetQuestionOptions(m_currentScene, "A").Length);
					//					Debug.Log ("Length B: " + m_loadSceneData.SubMultiChoiceGetQuestionOptions(m_currentScene, "B").Length);
					//					Debug.Log ("Length C: " + m_loadSceneData.SubMultiChoiceGetQuestionOptions(m_currentScene, "C").Length);
					//					Debug.Log ("Length D: " + m_loadSceneData.SubMultiChoiceGetQuestionOptions(m_currentScene, "D").Length);

					//if (GUI.Button (rectQuetionBox, "", "dilogBoxNextButton") || Global.arrowKey == "right" && m_currentScene.Substring(0,7) != "L4Spark") {
						if (GUI.Button (rectQuetionBox, "", "dilogBoxNextButton") || Global.arrowKey == "right") {
							Global.arrowKey = "";

							//The voice will be played again if the question type is statement or subMultichoice
							//Stop the orginal voice, and loads up new voice
							m_characterVoiceAS.Stop ();
							m_voiceCanStart = true;


							string selectedLabel = "";

							if (m_bNpcSubMultiChoiceOptionA) {
								selectedLabel = "A";
							}
							if (m_bNpcSubMultiChoiceOptionB) {
								selectedLabel = "B";
							}
							if (m_bNpcSubMultiChoiceOptionC) {
								selectedLabel = "C";
							}
							if (m_bNpcSubMultiChoiceOptionD) {
								selectedLabel = "D";
							}

							if (m_loadSceneData.GetQuestionType (m_currentScene).Equals ("SubMultiChoiceQuestion")) {
								if (m_loadSceneData.GetSubMultichoiceGotoType (m_currentScene, selectedLabel).Equals ("Imag")) {
									//Load the script
									m_loadSceneData.SwitchScriptByName (m_loadSceneData.GetSubMultichoiceNextSceneName (m_currentScene, selectedLabel));
								} else {
									//Load to another scene
									m_currentScene = tempSceneName;
								}
							}

							if (m_currentScene == "L7PlayerScene1" && selectedLabel == "A") {
								Global.GetPlayer().GetComponent<PlayerMovement>().
								MoveToPosition(Global.GetObjectPositionByID("TalkToPeople"),
								Global.GetObjectPositionByID("LookAtMiniGameOne"));	
								Global.m_bPlayerAcceptHelp = true;
							}

							if (m_currentScene == "L7CanyonDwellerScene2_1" ) {
								Global.m_bPlayerAfterFire = true;
							}

							//Reset the variables
							m_bNpcSubMultiChoiceOptionA = false;
							m_bNpcSubMultiChoiceOptionB = false;
							m_bNpcSubMultiChoiceOptionC = false;
							m_bNpcSubMultiChoiceOptionD = false;

							m_bL7NeedHelpOptionA = false;
							m_bL7UnNeedHelpOptionB = false;
						}
				}
			}
		}
	}

	public void OptionSelecetionReset(int _selectIndex)
	{
		//Only for web player build
		if(m_bChiceSelectedDetector.Length == 0){
			m_bChiceSelectedDetector = new bool[4];
		}
		
		for(int i = 0; i < m_bChiceSelectedDetector.Length; i++){
			if(i != _selectIndex){
				m_bChiceSelectedDetector[i] = false;
			}
			else{
				m_bChiceSelectedDetector[i] = true;
			}
		}
	}
	
	/*
	*
	* This function is used to check whether any of the option is being selected
	* If any of the option is being selected, then it needs to be reset.
	*
	*/

	public bool IsOptionsSelected()
	{
		bool isSelected = false;
		
		if(m_bMultiChoice1OptionA || m_bMultiChoice1OptionB || m_bMultiChoice1OptionC || m_bMultiChoice1OptionD){
			isSelected = true;
		}
		
		if(m_bTuiSubMultiChoiceOptionA || m_bTuiSubMultiChoiceOptionB || m_bTuiSubMultiChoiceOptionC || m_bTuiSubMultiChoiceOptionD){
			isSelected = true;
		}
		
		if(m_bNpcSubMultiChoiceOptionA || m_bNpcSubMultiChoiceOptionB || m_bNpcSubMultiChoiceOptionC || m_bNpcSubMultiChoiceOptionD){
			isSelected = true;
//			if(m_bNpcSubMultiChoiceOptionA){
//				Debug.Log ("A");
//			}else if(m_bNpcSubMultiChoiceOptionB){
//				Debug.Log ("B");
//			}else if(m_bNpcSubMultiChoiceOptionC){
//				Debug.Log ("C");
//			}else if(m_bNpcSubMultiChoiceOptionD){
//				Debug.Log ("D");
//			}else{
//				Debug.Log("Nothing");
//			}
		}
		
		return isSelected;
	}
	
	/*
	*
	* This function loads all the conversation of one perticular level
	*
	*/

	public void LoadLevelConversation(int _levelNum)
	{
		//Load the public conversation for all the levels
		//Guide conversation data
		m_guideXmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/guide_" + _levelNum.ToString(), typeof(TextAsset));
		//Mentor conversation data
		m_mentorXMLFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/mentor_" + _levelNum.ToString(), typeof(TextAsset));
		//Hope conversation data
		m_hopeXmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/hope_" + _levelNum.ToString(), typeof(TextAsset));


		//Other character conversation data in different levels
		if(_levelNum == 1){
			//Load the Island character conversation data
			m_guardianXmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/guardian_" + _levelNum.ToString(), typeof(TextAsset));

			//Load the level 1 caver character conversation data
			m_1npcXmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/cavenpc_" + _levelNum.ToString(), typeof(TextAsset));
			m_travelerXmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/traveller_" + _levelNum.ToString(), typeof(TextAsset));

			Debug.Log ("Level 1 conversation data is loaded!");

			L1AudioFileList[0] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_0_guide");
			L1AudioFileList[1] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_0_guide_movingon");
			L1AudioFileList[2] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_0_mentor");
			L1AudioFileList[3] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_1_guide_end");
			L1AudioFileList[4] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_1_guide_greetings");
			L1AudioFileList[5] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_1_guide_start");
			L1AudioFileList[6] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_1_guide_start_2");
			L1AudioFileList[7] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_1_mentor");
			L1AudioFileList[8] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_1_ranger");
			L1AudioFileList[9] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_1_snpc");
			L1AudioFileList[10] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_1_traveller");
			L1AudioFileList[11] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_1_tui");
			L1AudioFileList[12] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_guide_PHQ");
			L1AudioFileList[13] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_guide_quit");
			L1AudioFileList[14] = (AudioClip)Resources.Load("AudioSounds/Voice/SPARX_R/L1.006_SPARX-R");
			L1AudioFileList[15] = (AudioClip)Resources.Load("AudioSounds/Voice/SPARX_R/L1.040_SPARX-R");
			L1AudioFileList[16] = (AudioClip)Resources.Load("AudioSounds/Voice/SPARX_R/L1.044_SPARX-R");
			L1AudioFileList[17] = (AudioClip)Resources.Load("AudioSounds/Voice/SPARX_R/L1.084_SPARX-R");
			L1AudioFileList[18] = (AudioClip)Resources.Load("AudioSounds/Voice/SPARX_R/L1.085_SPARX-R");
			L1AudioFileList[19] = (AudioClip)Resources.Load("AudioSounds/Voice/SPARX_R/L1.086_SPARX-R");
			L1AudioFileList[20] = (AudioClip)Resources.Load("AudioSounds/Voice/SPARX_R/L1.087_SPARX-R");
			L1AudioFileList[21] = (AudioClip)Resources.Load("AudioSounds/Voice/SPARX_R/L1.088_SPARX-R");
			L1AudioFileList[22] = (AudioClip)Resources.Load("AudioSounds/Voice/SPARX_R/L1.090_SPARX-R");
			L1AudioFileList[23] = (AudioClip)Resources.Load("AudioSounds/Voice/SPARX_R/L1.092_SPARX-R");
			L1AudioFileList[24] = (AudioClip)Resources.Load("AudioSounds/Voice/SPARX_R/L1.093_SPARX-R");
			L1AudioFileList[25] = (AudioClip)Resources.Load("AudioSounds/Voice/SPARX_R/L1.094_SPARX-R");
			L1AudioFileList[26] = (AudioClip)Resources.Load("AudioSounds/Voice/SPARX_R/L1.096_SPARX-R");
			L1AudioFileList[27] = (AudioClip)Resources.Load("AudioSounds/Voice/SPARX_R/L1.097_SPARX-R");
			L1AudioFileList[28] = (AudioClip)Resources.Load("AudioSounds/Voice/SPARX_R/L1.098_SPARX-R");
			L1AudioFileList[29] = (AudioClip)Resources.Load("AudioSounds/Voice/SPARX_R/L1.106_SPARX-R");
			L1AudioFileList[30] = (AudioClip)Resources.Load("AudioSounds/Voice/SPARX_R/L1.107_SPARX-R");
			L1AudioFileList[31] = (AudioClip)Resources.Load("AudioSounds/Voice/SPARX_R/L1.110_SPARX-R");

			Debug.Log ("Level 1 conversation sound is loaded!");

			m_SoundFilesAlreadyLoaded = true;
		}

		if(_levelNum == 2){
			//Load the level 2 cave character data
			m_cassXmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/cass_" + _levelNum.ToString(), typeof(TextAsset));
			m_fireXmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/fire_" + _levelNum.ToString(), typeof(TextAsset));	
			m_yetiXmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/yeti_" + _levelNum.ToString(), typeof(TextAsset));
			m_fireperson2XmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/fire2_" + _levelNum.ToString(), typeof(TextAsset));

			Debug.Log ("Level 2 conversation data is loaded!");

			L2AudioFileList[0] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_2_cass");
			L2AudioFileList[1] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_2_cassfire");
			L2AudioFileList[2] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_2_fireperson");
			L2AudioFileList[3] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_2_guide_end");
			L2AudioFileList[4] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_2_guide_start");
			L2AudioFileList[5] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_2_mentor");
			L2AudioFileList[6] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_2_npc");
			L2AudioFileList[7] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_2_tui");
			L2AudioFileList[8] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_2_yeti");
			L2AudioFileList[9] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_guide_quit");
			L2AudioFileList[10] = (AudioClip)Resources.Load("AudioSounds/Voice/SPARX_R/L2.004_SPARX-R");
			L2AudioFileList[11] = (AudioClip)Resources.Load("AudioSounds/Voice/SPARX_R/L2.017_SPARX-R");
			L2AudioFileList[12] = (AudioClip)Resources.Load("AudioSounds/Voice/SPARX_R/L2.019_SPARX-R");
			L2AudioFileList[13] = (AudioClip)Resources.Load("AudioSounds/Voice/SPARX_R/L2.020_SPARX-R");
			L2AudioFileList[14] = (AudioClip)Resources.Load("AudioSounds/Voice/SPARX_R/L2.025_SPARX-R");
			L2AudioFileList[15] = (AudioClip)Resources.Load("AudioSounds/Voice/SPARX_R/L2.071_SPARX-R");

			Debug.Log ("Level 2 conversation sound is loaded!");

			m_SoundFilesAlreadyLoaded = true;


		}

		if(_levelNum == 3){
			//Load the level 3 cave character data
			m_firespiritXmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/firespirit_" + _levelNum.ToString(), typeof(TextAsset));
			m_npcXmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/npc_" + _levelNum.ToString(), typeof(TextAsset));	
			m_npc2XmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/npc2_" + _levelNum.ToString(), typeof(TextAsset));

			Debug.Log ("Level 3 conversation data is loaded!");

			L3AudioFileList[0] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_3_ash");
			L3AudioFileList[1] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_3_firespirits");
			L3AudioFileList[2] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_3_guide_end");
			L3AudioFileList[3] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_3_guide_start");
			L3AudioFileList[4] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_3_mentor");
			L3AudioFileList[5] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_3_tui");
			L3AudioFileList[6] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_3_vinn");
			L3AudioFileList[7] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_guide_quit");
			L3AudioFileList[8] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_0_guide");

			Debug.Log ("Level 3 conversation sound is loaded!");

			m_SoundFilesAlreadyLoaded = true;

		}

		if(_levelNum == 4){
			//Load the level 4 cave character data
			m_darroXmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/darro_" + _levelNum.ToString(), typeof(TextAsset));
			//m_gnatsXmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/gnats_" + _levelNum.ToString(), typeof(TextAsset));
			m_sparksXmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/sparks_" + _levelNum.ToString(), typeof(TextAsset));

			Debug.Log ("Level 4 conversation data is loaded!");

			L4AudioFileList[0] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_4_darro");
			L4AudioFileList[1] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_4_gnats");
			L4AudioFileList[2] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_4_guide_end");
			L4AudioFileList[3] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_4_guide_start");
			L4AudioFileList[4] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_4_mentor");
			L4AudioFileList[5] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_4_sparks");
			L4AudioFileList[6] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_4_tui");
			L4AudioFileList[7] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_guide_PHQ");
			L4AudioFileList[8] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_guide_quit");
			L4AudioFileList[9] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_0_guide");
			L4AudioFileList[10] = (AudioClip)Resources.Load("AudioSounds/Voice/SPARX_R/L4.045_SPARX-R");
			L4AudioFileList[11] = (AudioClip)Resources.Load("AudioSounds/Voice/SPARX_R/L4.046_SPARX-R");
			L4AudioFileList[12] = (AudioClip)Resources.Load("AudioSounds/Voice/SPARX_R/L4.050_SPARX-R");
			L4AudioFileList[13] = (AudioClip)Resources.Load("AudioSounds/Voice/SPARX_R/L4.110_SPARX-R");

			Debug.Log ("Level 4 conversation sound is loaded!");

			m_SoundFilesAlreadyLoaded = true;

		}

		if(_levelNum == 5){
			//Load the level 5 cave character data
			m_swapguyXmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/swampguy_" + _levelNum.ToString(), typeof(TextAsset));
			//NOTE: Other characters' conversations are being hard coded in the mini game

			Debug.Log ("Level 5 conversation data is loaded!");

			L5AudioFileList[0] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_5_gnats");
			L5AudioFileList[1] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_5_guide_end");
			L5AudioFileList[2] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_5_guide_start");
			L5AudioFileList[3] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_5_innkeeper");
			L5AudioFileList[4] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_5_mentor");
			L5AudioFileList[5] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_5_sparks");
			L5AudioFileList[6] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_5_tui");
			L5AudioFileList[7] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_guide_quit");
			L5AudioFileList[8] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_0_guide");
			L5AudioFileList[9] = (AudioClip)Resources.Load("AudioSounds/Voice/SPARX_R/L5.034_SPARX-R");


			Debug.Log ("Level 5 conversation sound is loaded!");

			m_SoundFilesAlreadyLoaded = true;

		}

		if(_levelNum == 6){
			//Load the level 6 cave character data
			m_bridgeWomanXmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/WomanoftheBridgeland_" + _levelNum.ToString(), typeof(TextAsset));
			m_templeGuidianXmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/templeGudian_" + _levelNum.ToString(), typeof(TextAsset));

			Debug.Log ("Level 6 conversation data is loaded!");

			L6AudioFileList[0] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_6_gnats");
			L6AudioFileList[1] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_6_guide_end");
			L6AudioFileList[2] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_6_guide_start");
			L6AudioFileList[3] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_6_mentor");
			L6AudioFileList[4] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_6_npcgate");
			L6AudioFileList[5] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_6_npctemple");
			L6AudioFileList[6] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_6_tui");
			L6AudioFileList[7] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_guide_quit");
			L6AudioFileList[8] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_0_guide");
			L6AudioFileList[9] = (AudioClip)Resources.Load("AudioSounds/Voice/SPARX_R/L6.094_SPARX-R");
			L6AudioFileList[10] = (AudioClip)Resources.Load("AudioSounds/Voice/SPARX_R/L6.100_SPARX-R");
			L6AudioFileList[11] = (AudioClip)Resources.Load("AudioSounds/Voice/SPARX_R/L6.102_SPARX-R");
			L6AudioFileList[12] = (AudioClip)Resources.Load("AudioSounds/Voice/SPARX_R/L6.103_SPARX-R");
			L6AudioFileList[13] = (AudioClip)Resources.Load("AudioSounds/Voice/SPARX_R/L6.105_SPARX-R");
			L6AudioFileList[14] = (AudioClip)Resources.Load("AudioSounds/Voice/SPARX_R/L6.108_SPARX-R");
			L6AudioFileList[15] = (AudioClip)Resources.Load("AudioSounds/Voice/SPARX_R/L6.113_SPARX-R");
			L6AudioFileList[16] = (AudioClip)Resources.Load("AudioSounds/Voice/SPARX_R/L6.121_SPARX-R");


			Debug.Log ("Level 6 conversation sound is loaded!");

			m_SoundFilesAlreadyLoaded = true;

		}

		if(_levelNum == 7){
			//Load the level 7 cave character data
			m_examinerXmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/npc_" + _levelNum.ToString(), typeof(TextAsset));
			m_swapGuyXmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/guard_" + _levelNum.ToString(), typeof(TextAsset));
			m_playerNeedsHelpTextAsset = (TextAsset)Resources.Load ("LevelData/Level 7/player_7", typeof(TextAsset));
			m_cassL7XmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/cass_" + _levelNum.ToString(), typeof(TextAsset));
			m_darroL7XmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/darro_" + _levelNum.ToString(), typeof(TextAsset));
			m_firepersonL7XmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/fireman_" + _levelNum.ToString(), typeof(TextAsset));
			m_kogXmlFilePathTextAsset = (TextAsset)Resources.Load ("LevelData/Level " + _levelNum.ToString() + "/kog_" + _levelNum.ToString(), typeof(TextAsset));

			Debug.Log ("Level 7 conversation data is loaded!");

			L7AudioFileList[0] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_7_canyon");
			L7AudioFileList[1] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_7_cass");
			L7AudioFileList[2] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_7_darro");
			L7AudioFileList[3] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_7_examiner");
			L7AudioFileList[4] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_7_gnats");
			L7AudioFileList[5] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_7_guard");
			L7AudioFileList[6] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_7_guide_end");
			L7AudioFileList[7] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_7_guide_start");
			L7AudioFileList[8] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_7_kog");
			L7AudioFileList[9] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_7_mentor");
			L7AudioFileList[10] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_7_sparks");
			L7AudioFileList[11] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_7_tui");
			L7AudioFileList[12] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_guide_PHQ");
			L7AudioFileList[13] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_guide_quit");
			L7AudioFileList[14] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_0_guide");
			L7AudioFileList[15] = (AudioClip)Resources.Load("AudioSounds/Voice/voice_0_guide_movingon");
			L7AudioFileList[16] = (AudioClip)Resources.Load("AudioSounds/Voice/SPARX_R/L7.136_SPARX-R");
			L7AudioFileList[17] = (AudioClip)Resources.Load("AudioSounds/Voice/SPARX_R/L7.141_SPARX-R");
			L7AudioFileList[18] = (AudioClip)Resources.Load("AudioSounds/Voice/SPARX_R/L7.151_SPARX-R");


								Debug.Log ("Level 7 conversation sound is loaded!");

			m_SoundFilesAlreadyLoaded = true;

		}
	}

	
	/*
	*
	* This function is used to get the audio clip base on the clip name
	* in the XML data files.
	*
	*/

	public AudioClip GetCharacterVoiceFile(string _audioNameFromXML)
	{
		AudioClip tempAudioClip = new AudioClip();
		int i;
		
		//Audio files for level 1
		if(Global.CurrentLevelNumber == 1){
			for (i = 0; i < L1AudioFileList.Length; ++i){
				if (_audioNameFromXML == L1AudioFileList[i].name)
				{
					tempAudioClip = L1AudioFileList[i];
					break;
				}
			}
		}
		//Audio files for level 2
		else if(Global.CurrentLevelNumber == 2){
			for (i = 0; i < L2AudioFileList.Length; ++i){
				if (_audioNameFromXML == L2AudioFileList[i].name)
				{
					tempAudioClip = L2AudioFileList[i];
					break;
				}
			}
		}
		//Audio files for level 3
		else if(Global.CurrentLevelNumber == 3){
			for (i = 0; i < L3AudioFileList.Length; ++i){
				if (_audioNameFromXML == L3AudioFileList[i].name)
				{
					tempAudioClip = L3AudioFileList[i];
					break;
				}
			}
		}
		//Audio files for level 4
		else if(Global.CurrentLevelNumber == 4){
			for (i = 0; i < L4AudioFileList.Length; ++i){
				if (_audioNameFromXML == L4AudioFileList[i].name)
				{
					tempAudioClip = L4AudioFileList[i];
					break;
				}
			}
		}
		//Audio files for level 5
		else if(Global.CurrentLevelNumber == 5){
			for (i = 0; i < L5AudioFileList.Length; ++i){
				if (_audioNameFromXML == L5AudioFileList[i].name)
				{
					tempAudioClip = L5AudioFileList[i];
					break;
				}
			}
		}
		//Audio files for level 6
		else if(Global.CurrentLevelNumber == 6){
			for (i = 0; i < L6AudioFileList.Length; ++i){
				if (_audioNameFromXML == L6AudioFileList[i].name)
				{
					tempAudioClip = L6AudioFileList[i];
					break;
				}
			}
		}
		//Audio files for level 7
		else if(Global.CurrentLevelNumber == 7){
			for (i = 0; i < L7AudioFileList.Length; ++i){
				if (_audioNameFromXML == L7AudioFileList[i].name)
				{
					tempAudioClip = L7AudioFileList[i];
					break;
				}
			}
		}
		
		return tempAudioClip;
	}
	
	/*
	*
	* This function plays the voice of the guide of the current scene
	*
	*/

	public void PlayVoice(string _theSceneName, string _soundFileName)
	{
		Debug.Log("-------CurrentSounfilename:" + _soundFileName);
		//m_characterVoice = Resources.Load ("AudioSounds/Voice/" + _soundFileName) as AudioClip;
        m_characterVoiceAS.clip = GetCharacterVoiceFile(_soundFileName);
        //m_characterVoiceAS.clip = GetCharacterVoiceFile(_soundFileName);
		m_characterVoiceAS.time = 0;
		Debug.Log("-------Length Soundfile:" + m_characterVoiceAS.maxDistance);
		//Get the voice start time of the current scene
        //m_characterVoiceAS.time = m_loadSceneData.GetVoiceStartTime(_theSceneName);
        m_characterVoiceAS.time = m_loadSceneData.GetVoiceStartTime(_theSceneName);
        Debug.Log("-------SceneName:" + _theSceneName);
        Debug.Log("-------CurrentTime:" + m_characterVoiceAS.time);
			
		m_characterVoiceAS.Play();
		m_bjustPlay = true;//FIX_D
		/*if (m_BoolSkipVoiceErrorLevel4) {
			m_BoolSkipVoiceErrorLevel4Pass = true;
			Debug.Log (m_currentScene + "<>" + m_currentScene == "L4SparkScene1" + "<>" + GameObject.Find ("GUI").GetComponent<TerminateTalkScene> ().enabled);
			//m_bjustPlay = false;
			m_characterVoiceAS.Pause ();
		} /*else {*/
		/*}*/

		//m_BoolSkipVoiceErrorLevel4 = false;

		if (Global.CurrentInteractNPC != "Hope" && !Global.CurrentInteractNPC.Contains("Firespirit") && 
			!Global.CurrentInteractNPC.Contains("Spark") && Global.CurrentInteractNPC != "Boy" && Global.CurrentInteractNPC != "Girl")
			//&& Global.CurrentInteractNPC != "KingOfGnats")
		{
			GameObject interact = GameObject.Find (Global.CurrentInteractNPC);
			//Play talk animation
			if (!(Global.CurrentInteractNPC == "guardian" && interact.GetComponent<Animation>().IsPlaying("point")) && 
				!(Global.CurrentInteractNPC == "Mentor" && interact.GetComponent<Animation>().IsPlaying("give")) &&
				!(Global.CurrentInteractNPC == "Traveller" && interact.GetComponent<Animation>().IsPlaying("breath")))
			{
				GameObject.Find (Global.CurrentInteractNPC).GetComponent<Animation>().Play("talk");
			}
			if(Global.CurrentInteractNPC == "Swampguy"){
				GameObject.Find (Global.CurrentInteractNPC).GetComponent<Animation>().Play("talk");
			}
		}
	}
	
	/*
	*
	* This function reruns whether is scene is auto scene,
	* if the scene is an auto scene, then it will play automatically. 
	*
	*/

	public bool IsAutoTalkScene(string _currentSceneName)
	{
		bool isAutoScene = false;
		
		//If the scene is a auto scene
		if(_currentSceneName.Substring(0, 4) == "Auto"){
			isAutoScene = true;
		}
		return isAutoScene;
	}
	
	/*
	*
	* This function plays scene information automatically
	* If the current scene continue with sutomatic scenes, then
	* the continues scene will pop up automatically.
	* 
	* _startSceneName: The scene starts the auto talk scene
	* _firstDelayTime: The time delay of the first auto conversation
	* _secondDelayTime: The time delay of the second auto conversation
	*
	*/

	public void PlayAutoTalkScenes(string _autoSceneName, float _eachConversationTime)
	{
		//Define the scenes that needs auto talk scene
		if(Time.time - m_autoSceneStartTime > _eachConversationTime){
			//Traveller
			if(Global.CurrentLevelNumber == 1){
				if(_autoSceneName == "AutoTravellerScene4_2"){
					GameObject.Find ("Traveller").GetComponent<Animation>().Play("talk");
				} 
			}
			//Change the talking character(Cass & Fireman)
			if(Global.CurrentLevelNumber == 2){
				if(Global.CurrentInteractNPC == "Cass"){
					Global.CurrentInteractNPC = "Fireperson";
					GameObject.Find("Cass").GetComponent<Animation>().Play("idle");
					GameObject.Find("Fireperson").GetComponent<Animation>().Play("talk");
					GameObject.Find("Cass").GetComponent<CharacterInteraction>().enabled = false;
				}
				else if (Global.CurrentInteractNPC == "Fireperson"){
					Global.CurrentInteractNPC = "Cass";
					GameObject.Find("Cass").GetComponent<Animation>().Play("talk");
					GameObject.Find("Fireperson").GetComponent<Animation>().Play("idle");
					if(Global.PreviousSceneName == "AutoL2FireScene5"){
						GameObject.Find("Cass").GetComponent<Animation>().Play("take");
					}
				}
				if(m_currentScene == "AutoL2FireScene5"){//Last sentense of fireman
					GameObject.Find ("L2GUI").GetComponent<TerminateCassFiremanScene>().m_firePersonConversationFinished = true;
					Global.NPCGiveAwayObject("Fireperson", "give", "Ember");
					Global.m_pickNPCName = "Cass";
					Global.m_PickAniName = "take";
					m_voiceCanStart = true;
					Debug.Log ("Fireman gives ember to Cass..........");
				}
			}
			
			//Change the scene name when time is up
			m_currentScene = m_loadSceneData.GetNextSceneName(_autoSceneName);
			m_bAutoConversationUnFinished = true;
			
			//The player walk to the gate and breath
			if(m_currentScene == "TerminateTalkScene"){
				Global.GetPlayer().GetComponent<Animation>().Play("walk");
				
				Global.GetPlayer().transform.LookAt(Global.GetObjectPositionByID("37004"));
				Global.GetPlayer().GetComponent<PlayerMovement>().
					MoveToPosition(Global.GetObjectPositionByID("37004"), Global.GetObjectPositionByID("37001"));
			}
		}
		else{
			m_bAutoConversationUnFinished = false;
		}
	}
	
	/*
	*
	* This function handles all the auto conversation in each level 
	*
	*/

	public void AutoConversationSceneSwitch()
	{
		//Level 1 travel auto talk------------------------------------------------------
		if(Global.CurrentLevelNumber == 1){
			//Travel auto conversation
			if(Global.CurrentInteractNPC == "Traveller"){
				if(m_currentScene == "AutoTravellerScene4"){
					if(m_bAutoConversationUnFinished){
						m_autoSceneStartTime = Time.time;
						m_bAutoConversationUnFinished = false;
						//Player breath animation
						Global.GetPlayer().GetComponent<Animation>().Play("breath");
						//Global.GetPlayer().animation["breath"].normalizedSpeed = 1.1f;
						SoundPlayer.PlaySound("sfx_breath", 0.3f);
					}
					PlayAutoTalkScenes(m_currentScene, 6.8f);
				}
				if(m_currentScene == "AutoTravellerScene4_1"){
					if(m_bAutoConversationUnFinished){
						m_autoSceneStartTime = Time.time;
						m_bAutoConversationUnFinished = false;
					}
					PlayAutoTalkScenes(m_currentScene, 3.2f);
				}
				if(m_currentScene == "AutoTravellerScene4_2"){
					if(m_bAutoConversationUnFinished){
						m_autoSceneStartTime = Time.time;
						m_bAutoConversationUnFinished = false;
					}
					PlayAutoTalkScenes(m_currentScene, 5.1f);
				}
			}
		}
		//Level 2 Cass and fireman auto talk---------------------------------------------
		else if(Global.CurrentLevelNumber == 2){
			//Cass auto conversation
			if(Global.CurrentInteractNPC == "Cass"){
				if(m_currentScene == "AutoL2CassScene2"){
					if(m_bAutoConversationUnFinished){
						m_autoSceneStartTime = Time.time;
						m_bAutoConversationUnFinished = false;
						
						m_bCassFiremanTalkStart = true;
					}
					PlayAutoTalkScenes(m_currentScene, 3.9f);
				}
				
				if(m_currentScene == "AutoL2CassScene4"){
					if(m_bAutoConversationUnFinished){
						m_autoSceneStartTime = Time.time;
						m_bAutoConversationUnFinished = false;
					}
					PlayAutoTalkScenes(m_currentScene, 6.0f);
				}
			}
			//Firman 1 auto conversation
			if(Global.CurrentInteractNPC == "Fireperson"){
				if(m_currentScene == "AutoL2FireScene3"){
					if(m_bAutoConversationUnFinished){
						m_autoSceneStartTime = Time.time;
						m_bAutoConversationUnFinished = false;
					}
					PlayAutoTalkScenes(m_currentScene, 4.9f);
				}
				
				if(m_currentScene == "AutoL2FireScene5"){
					if(m_bAutoConversationUnFinished){
						m_autoSceneStartTime = Time.time;
						m_bAutoConversationUnFinished = false;
					}
					PlayAutoTalkScenes(m_currentScene, 5.0f);
				}
			}
		}
		//Level 7 examiner auto talk------------------------------------------------------
		else if(Global.CurrentLevelNumber == 7){
			//The Examiner auto conversation
			if(Global.CurrentInteractNPC == "Traveller"){
				if(m_currentScene == "AutoL7ExaminerScene3"){
					if(m_bAutoConversationUnFinished){
						m_autoSceneStartTime = Time.time;
						m_bAutoConversationUnFinished = false;
					}
					PlayAutoTalkScenes(m_currentScene, 6.0f);
				}
				
				if(m_currentScene == "AutoL7ExaminerScene4"){
					if(m_bAutoConversationUnFinished){
						m_autoSceneStartTime = Time.time;
						m_bAutoConversationUnFinished = false;
					}
					PlayAutoTalkScenes(m_currentScene, 3.0f);
				}
				
				if(m_currentScene == "AutoL7ExaminerScene5"){
					if(m_bAutoConversationUnFinished){
						m_autoSceneStartTime = Time.time;
						m_bAutoConversationUnFinished = false;
					}
					PlayAutoTalkScenes(m_currentScene, 10.0f);
				}
			}
		}
	}
	
	public void SwitchCharacterNameColor()
	{
		Color playerNameColor = new Color(0.3f, 0.08f, 0.5f, 1.0f);
		Color guideNameColor = new Color(0.07f, 0.14f, 0.4f, 1.0f);
		Color mentorNameColor = new Color(0.64f, 0.0f, 0.0f, 1.0f);
		Color otherNPCNameColor = new Color(0.82f, 0.0f, 0.54f, 1.0f);
		
		//Player's information
		
		if(Global.CurrentInteractNPC == "Guide"){
			GUI.skin.customStyles[30].normal.textColor = guideNameColor;
		}
		else if(Global.CurrentInteractNPC == "Mentor"){
			GUI.skin.customStyles[30].normal.textColor = mentorNameColor;
		}
		else{
			GUI.skin.customStyles[30].normal.textColor = otherNPCNameColor;
		}
	}

}
