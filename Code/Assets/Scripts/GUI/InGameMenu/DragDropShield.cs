using UnityEngine;
using System.Collections;

public struct DraggableObject
{
	public Rect original;
	public Rect movable;
	public string words;
	
	public DraggableObject(Rect _originalAndMovable, string _words)
	{
		original = _originalAndMovable;
		movable = _originalAndMovable;
		words = _words;
	}
	
	public DraggableObject(Rect _original, Rect _movable, string _words)
	{
		original = _original;
		movable = _movable;
		words = _words;
	}
	
	public DraggableObject(Rect _original, Rect _movable)
	{
		original = _original;
		movable = _movable;
		words = "";
	}
};

public class DragDropShield : MonoBehaviour 
{
	//Public variables
	public GUISkin m_skin;
	public GameObject m_shieldPrf = null;
	
	//Private variables
	private Vector3 m_vShieldPosition;
	private GameObject m_shieldObjectClone = null;
	
	private string m_conversationBoxText = "";
	
	static private float TextWidth = 120.0f;
	static private float TextHeight = 30.0f;
	
	private string[] childObjectNames = 
	{
		"_[id]2001_[bx]1_[by]10_[bz]10_[mini]2_[pick]_",
		"_[id]2002_[bx]1_[by]10_[bz]10_[mini]2_[pick]_",
		"_[id]2003_[bx]1_[by]10_[bz]10_[mini]2_[pick]_",
		"_[id]2004_[bx]1_[by]10_[bz]10_[mini]2_[pick]_",
		"_[id]2005_[bx]1_[by]10_[bz]10_[mini]2_[pick]_",
		"_[id]2006_[bx]1_[by]10_[bz]10_[mini]2_[pick]_"
	};
	
	private string[] shieldNames = 
	{
		"SORT IT",
		"SPOT IT",
		"SWAP IT",
		"SOLVE IT",
		"DO IT",
		"RELAX"
	};
	
	private Rect[] shieldNameRects = 
	{
		new Rect(Screen.width*0.625f, Screen.height*0.1563f, TextWidth, TextHeight),
		new Rect(Screen.width*0.7617f, Screen.height*0.4948f, TextWidth, TextHeight),
		new Rect(Screen.width*0.6348f, Screen.height*0.7142f, TextWidth, TextHeight),
		new Rect(Screen.width*0.2148f, Screen.height*0.7142f, TextWidth, TextHeight),
		new Rect(Screen.width*0.1172f, Screen.height*0.4948f, TextWidth, TextHeight),
		new Rect(Screen.width*0.2246f, Screen.height*0.1563f, TextWidth, TextHeight),
/*		new Rect(570.0f, 95.0f, TextWidth, TextHeight),
		new Rect(660.0f, 260.0f, TextWidth, TextHeight),
		new Rect(530.0f, 455.0f, TextWidth, TextHeight),
		new Rect(290.0f, 455.0f, TextWidth, TextHeight),
		new Rect(190.0f, 260.0f, TextWidth, TextHeight),
		new Rect(260.0f, 95.0f, TextWidth, TextHeight),*/
		/*new Rect(Screen.width*0.625f, Screen.height*0.1563f, TextWidth, TextHeight),
		new Rect(Screen.width*0.7617f, Screen.height*0.4948f, TextWidth, TextHeight),
		new Rect(Screen.width*0.6348f, Screen.height*0.7942f, TextWidth, TextHeight),
		new Rect(Screen.width*0.2148f, Screen.height*0.7942f, TextWidth, TextHeight),
		new Rect(Screen.width*0.1172f, Screen.height*0.4948f, TextWidth, TextHeight),
		new Rect(Screen.width*0.2246f, Screen.height*0.1563f, TextWidth, TextHeight),*/
	};
	
	private bool[] displayName = 
	{
		false,
		false,
		false,
		false,
		false,
		false
	};
	
	private Color[] displayColor = 
	{
		new Color(0.0f, 0.83f, 1.0f, 1.0f),
		new Color(1.0f, 0.0f, 0.0f, 1.0f),
		new Color(1.0f, 0.5f, 0.0f, 1.0f),
		new Color(0.0f, 0.82f, 0.0f, 1.0f),
		new Color(1.0f, 1.0f, 0.0f, 1.0f),
		new Color(1.0f, 0.0f, 0.66f, 1.0f)
	};
	
	static private float m_collisiondimension = 10.0f;
	static private float m_movingTextwidth = 200.0f;
	static private float m_movingTextHeight = 40.0f;
	
	private DraggableObject[] m_wordsRects = 
	{
		new DraggableObject(new Rect(125.0f, 140.0f, m_movingTextwidth, m_movingTextHeight), 
			"Control your breathing"),
		new DraggableObject(new Rect(640.0f, 140.0f, m_movingTextwidth, m_movingTextHeight), 
			"Find and defeat the Gnats"),
	};
	
	static float m_targetDim = 70.0f;
	private Rect[] m_destinations = 
	{
		new Rect(510.0f, 130.0f, m_targetDim, m_targetDim),
		new Rect(570.0f, 240.0f, m_targetDim, m_targetDim),
		new Rect(510.0f, 350.0f, m_targetDim, m_targetDim),
		new Rect(380.0f, 350.0f, m_targetDim, m_targetDim),
		new Rect(320.0f, 240.0f, m_targetDim, m_targetDim),
		new Rect(380.0f, 130.0f, m_targetDim, m_targetDim)
/*		new Rect(Screen.width*0.5469f, Screen.height*0.2344f, m_targetDim, m_targetDim),
		new Rect(Screen.width*0.625f, Screen.height*0.4167f, m_targetDim, m_targetDim),
		new Rect(Screen.width*0.5469f, Screen.height*0.6120f, m_targetDim, m_targetDim),
		new Rect(Screen.width*0.3906f, Screen.height*0.6120f, m_targetDim, m_targetDim),
		new Rect(Screen.width*0.3125f, Screen.height*0.4167f, m_targetDim, m_targetDim),
		new Rect(Screen.width*0.3809f, Screen.height*0.2474f, m_targetDim, m_targetDim)*/
	};
	
	private int[] m_correctIndex = 
	{
		-1, 
		1, 
		-1, 
		-1, 
		-1,
		0
	};
	
	private string m_currentDragText = "";
	private int m_currentDragIndex = -1;
	private bool m_dragging = false;
	
	public Texture colourTexture = null;
	public Texture colourFrameTexture = null;
	public Texture greyScaleTexture = null;
	public Texture2D m_tConversationBackground = null;
	
	// Use this for initialization
	void Start () 
	{
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		
		//colourTexture             = (Texture2D)Resources.Load ("Textures/UI/shield_new_color", typeof(Texture2D));
		//greyScaleTexture          = (Texture2D)Resources.Load ("Textures/UI/shield_new_grey", typeof(Texture2D));
		//m_tConversationBackground = (Texture2D)Resources.Load ("UI/talk_paper", typeof(Texture2D));
	}
	
	// Update is called once per frame
	void Update () 
	{
		//The default conversation box text
		m_conversationBoxText = WordsHoverOverText("");
		
		for (int i = 0; i < m_wordsRects.Length; ++i)
		{
			if(m_wordsRects[i].original.Contains(new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{
		         m_conversationBoxText = WordsHoverOverText(m_wordsRects[i].words);
			}
		}
		
		if (m_dragging)
		{
			// save the mouse pos for ease of access
			float mouseX = Input.mousePosition.x;
			float mouseY = Input.mousePosition.y;
			m_wordsRects[m_currentDragIndex].movable.Set(mouseX - m_collisiondimension/2, 
												     mouseY * (-1) + Screen.height - m_collisiondimension/2, 
												     m_collisiondimension, 
												     m_collisiondimension);
			// Color the frame of each part if mouse hover over
			for (int i = 0; i < m_destinations.Length; ++i)
			{
				// check if this destination box contains the current clicked keyword box
				if(m_destinations[i].Contains(new Vector2(mouseX, Screen.height - mouseY)))
				{
					ChangeFrameColour(i);
				}
			}

		}
		else
		{
			KeyWordDrag();
		}
		
		DestinationDrop();
	}
	
	void OnEnable()
	{
		//The shield against depression position
		m_vShieldPosition = new Vector3(0.0f, 0.03f, 0.70f);
		//The shield object clone
		m_shieldObjectClone = (GameObject) Instantiate(m_shieldPrf, m_vShieldPosition, Quaternion.identity);
		
		GameObject.Find ("CharacterCustomiseBackground").GetComponent<GUITexture>().enabled = true;
		
	}
	
	void OnGUI()
	{
		GUI.depth = -1;
		GUI.skin = m_skin;
		
		// shield of depression text
		GUI.Label(new Rect(310.0f, 50.0f, 350.0f, 50.0f), "SHIELD AGAINST FEELING DOWN", "Titles");
		
		bool done = true;
		for (int i = 0; i < m_wordsRects.Length; ++i)
		{
			GUI.Label(m_wordsRects[i].original, m_wordsRects[i].words, "CenteredFont");
			//GUI.Box(m_wordsRects[i].original, "");
			if (m_wordsRects[i].words != "")
			{
				done = false;
			}
		}
		
		done = (m_currentDragText=="" && done)?done:false;
		
		if (done)
		{
			//The next button
			//if(GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
			if(GUI.Button(new Rect(800, 495, 60, 60), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
					Global.arrowKey = "";

				this.enabled = false;
				
				//Destroy the shield clone
				GameObject.Destroy(m_shieldObjectClone);
				GameObject.Find ("CharacterCustomiseBackground").GetComponent<GUITexture>().enabled = false;
				
				GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;
				UpdateCurrentSceneName();
			}
		}
		
		// The current movable keyword box and text
		if (m_currentDragText != "")
		{
			Rect holder = new Rect(m_wordsRects[m_currentDragIndex].movable.x - m_movingTextwidth/2, 
								   m_wordsRects[m_currentDragIndex].movable.y - m_movingTextHeight/2,
								   m_movingTextwidth,
								   m_movingTextHeight);
			GUI.Label(holder, m_currentDragText, "CenteredFont");
		}
		
		//The text on the shield
		GUI.Label(new Rect(10.0f, -300.0f, 800.0f, 500.0f), "Get help when you need it");
		
		//Conversation box
		//GUI.Label(new Rect(Screen.width*0.3418f,Screen.height*0.7813f,Screen.width*0.2832f,Screen.height*0.1253f), "", "MiddleAreaBackGround");
		//GUI.Label(new Rect(Screen.width*0.3661f,Screen.height*0.7713f,Screen.width*0.2246f,Screen.height*0.1693f), m_conversationBoxText, "CenteredFont");
		GUI.Label(new Rect(265,470,440,135), "", "MiddleAreaBackGround");
		GUI.Label(new Rect(300.0f, 497.0f, 360.0f, 80.0f), m_conversationBoxText, "CenteredFont");

		GUIStyle style = m_skin.GetStyle("ShieldText");
		
		// loop through all the shild names rects
		for (int i = 0; i < shieldNameRects.Length; ++i)
		{
			// change the text colour and then display the text
			style.normal.textColor = displayColor[i];
			GUI.Label(shieldNameRects[i], shieldNames[i], "ShieldText");
			//GUI.Box(m_destinations[i], "");
			ChangePieceColour(i);
		}
		
		style.normal.textColor = new Color(0.0f, 0.0f, 0.0f, 1.0f);
	}
	
	void ChangePieceColour(int _index)
	{
		// get the child of teh shiled that we are changing
		Transform child = m_shieldObjectClone.transform.FindChild(childObjectNames[_index]);
			
		// if it is being displayed then the colour needs active
		if (displayName[_index])
		{
			child.GetComponent<MeshRenderer>().material.SetTexture("_MainTex", colourTexture);
		}
		// fi it is not being displayed then the coulour needs to be grey scale
		else
		{
			child.GetComponent<MeshRenderer>().material.SetTexture("_MainTex", greyScaleTexture);
		}
	}

	void ChangeFrameColour(int _index)
	{
		// get the child of teh shiled that we are changing
		Transform child = m_shieldObjectClone.transform.FindChild (childObjectNames [_index]);
		
		// if it is being displayed then the colour needs active
		//if (displayName[_index])
		//{
		child.GetComponent<MeshRenderer> ().material.SetTexture ("_MainTex", colourFrameTexture);
		//}
		// if it is not being displayed then the coulour needs to be grey scale
		//else
		//{
		//child.GetComponent<MeshRenderer> ().material.SetTexture ("_MainTex", greyScaleTexture);
		//}
	}
	
	/*
	 * 
	 * Thie function drag the keyword and move with the mouse.
	 * 
	*/
	public void KeyWordDrag()
	{
		// release early if there is no active keyword box being moved.
		if (m_currentDragIndex != -1) { return ;}
		
		//If the mouse button is clicked and if the mouse position is on the keywords
		if(Input.GetMouseButton(0))
		{
			// save the mouse pos for ease of access
			float mouseX = Input.mousePosition.x;
			float mouseY = Input.mousePosition.y;
			
			// boolean to see if a box has been clicked on.
			bool clicked = false;
			
			// loop through all the original rects to see if the user has clicked on one of the boxes.
			for (int i = 0; i < m_wordsRects.Length; ++i)
			{
				if (m_wordsRects[i].original.Contains(new Vector2(mouseX, Screen.height - mouseY)))
				{
					// set the current active key text
					// check if the current text has been set
					if (m_currentDragText == "")
					{
						m_currentDragText = m_wordsRects[i].words;
						m_wordsRects[i].words = "";
						m_currentDragIndex = i;
						clicked = true;
						m_dragging = true;
						break;
					}
				}
			}	
			
			// this is an early release from the function
			if (clicked == true) { return ;}
		}
	}
	
	/*
	 * 
	 * Thie function drop the keyword to the destination area.
	 * 
	*/
	public void DestinationDrop()
	{
		// release early if there is no active keyword box being moved.
		if (m_currentDragIndex == -1) { return ;}
		
		//If the mouse button is UnClicked and if  the mouse position is on the fill field
		if(Input.GetMouseButtonUp(0))
		{
			m_dragging = false;
			
			// boolean for if the text was placed in the box.
			bool successful = false;
			
			// loop through the destination rects to see which box the word is being placed in
			for (int i = 0; i < m_destinations.Length; ++i)
			{
				Rect moving = m_wordsRects[m_currentDragIndex].movable;
				Vector2 min = new Vector2(moving.xMin, moving.yMin);
				Vector2 max = new Vector2(moving.xMax, moving.yMax);
				
				// check if this destination box contains the current clicked keyword box
				if(m_destinations[i].Contains(min) && m_destinations[i].Contains(max))
				{
					// if the destination text is empty
					if(m_correctIndex[i] == m_currentDragIndex)
					{
						// set the destination text and remove the clicked text.
						m_currentDragText = "";
						m_currentDragIndex = -1;
						displayName[i] = true;
						successful = true;
						break;
					}
				}
			}
			
			// another early release because everything is done already
			if (successful) { return ;}
			
			// execute this code if the user missed the destination box.
			ResetCurrentString();
		}
	}
	
	/*
	 * 
	 * This function resets the missed keyword text and text rect back to their original positions
	 * 
	 */
	private void ResetCurrentString()
	{
		// set the movable rect to the original area, set the keyword back to the current,
		// reset the current keyword and break the loop.
		m_wordsRects[m_currentDragIndex].movable = m_wordsRects[m_currentDragIndex].original;
		m_wordsRects[m_currentDragIndex].words = m_currentDragText;
		m_currentDragText = "";
		m_currentDragIndex = -1;
	}
	
	/*
	*
	* This function updates the current scene name after click the next button on the image
	* ThoughsAndFeeling image screen.
	*
	*/
	public void UpdateCurrentSceneName()
	{
		//level 1
		if(Global.CurrentLevelNumber == 1)
		{
			//Depend on the different scene, the shield is different as well
			if(Global.PreviousSceneName == "GuideScene29")
			{
				GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentScene = "GuideScene30";
			}
		}
	}
	
	public string WordsHoverOverText(string _keyWord)
	{
		string keyWordExplaination = "";
			
		switch(_keyWord){
			case "Control your breathing":
				keyWordExplaination = "Slowing down your breathing is not just a game trick. It is a simple but powerful thing that you can do when you feel stressed, worried or angry.";
				break;
			case "Find and defeat the Gnats":
				keyWordExplaination = "Gnats are like negative thoughts and you need to learn to spot and swap them. You'll learn more about this next time.";
				break;
			default:
			    keyWordExplaination = "Match the Cave Province skills to the right part of the shield.";
				break;
		}
			
		return keyWordExplaination;
	}
}
