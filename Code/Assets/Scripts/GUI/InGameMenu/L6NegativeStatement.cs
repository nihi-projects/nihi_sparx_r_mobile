using UnityEngine;
using System.Collections;

public class L6NegativeStatement : MonoBehaviour {
	
	public GUISkin m_skin = null;
	public bool[] m_bNegativeStatementSelectList = new bool[5];
	static public string m_sChoice = "";
	
	// Use this for initialization
	public void Start () {
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		
		m_bNegativeStatementSelectList = new bool[5];
	}
	
	// Update is called once per frame
	public void Update () {
	
	}
	
	public void OnGUI () 
	{		
		GUI.skin = m_skin;
		GUI.depth = -1;
		
		GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
		GUI.Label(new Rect(180.0f,50.0f,600.0f,200.0f), "Choose a Gnat (negative thought) or use your own negative statement.", "Titles");
		
		if(GUI.Toggle(new Rect(370.0f,230.0f,220.0f,20.0f), false, "My life sucks...")){
			this.enabled = false;
			GetComponent<L6AnalyseNegativeStatement>().enabled = true;
			m_bNegativeStatementSelectList[0] = true;
			m_sChoice = "My life sucks...";
		}
		if(GUI.Toggle(new Rect(370.0f,270.0f,220.0f,20.0f), false, "Nobody likes me...")){
			this.enabled = false;
			GetComponent<L6AnalyseNegativeStatement>().enabled = true;
			m_bNegativeStatementSelectList[1] = true;
			m_sChoice = "Nobody likes me...";
		}
		if(GUI.Toggle(new Rect(370.0f,310.0f,220.0f,20.0f), false, "SPARX is useless...")){
			this.enabled = false;
			GetComponent<L6AnalyseNegativeStatement>().enabled = true;
			m_bNegativeStatementSelectList[2] = true;
			m_sChoice = "SPARX is useless...";
		}
		if(GUI.Toggle(new Rect(370.0f,350.0f,220.0f,20.0f), false, "I'll never feel good again...")){
			this.enabled = false;
			GetComponent<L6AnalyseNegativeStatement>().enabled = true;
			m_bNegativeStatementSelectList[3] = true;
			m_sChoice = "I'll never feel good again...";
		}
		if(GUI.Toggle(new Rect(370.0f,390.0f,220.0f,20.0f), false, "Your own 'problem thought'.")){
			this.enabled = false;
			GetComponent<L6AnalyseNegativeStatement>().enabled = true;
			m_bNegativeStatementSelectList[4] = true;
			m_sChoice = "Your own 'problem thought'.";
		}
	}
}
