using UnityEngine;
using System.Collections;

public class L2NoteBook : MonoBehaviour 
{
	// publics
	public GUISkin m_skin = null;
	
	
	// privates
	private string text1 = "Click here to enter text";
	private string text2 = "Click here to enter text";
	private string text3 = "Click here to enter text";
	
	private CapturedDataInput instance;
	public Texture2D m_tNoteBook;
	
	private int m_iIndex = 0;

	// Use this for initialization
	void Start () 
	{
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		
		//Connecting to the server
		//instance    = CapturedDataInput.GetInstance;
		instance = GameObject.Find("CapturedDataInputHolder").GetComponent<CapturedDataInput>();
		
		//m_tNoteBook = (Texture2D)Resources.Load ("UI/nbook_bg", typeof(Texture2D));
	}
	
	// Update is called once per frame
	void Update () 
	{

	}
	
	void OnGUI()
	{
		GUI.skin = m_skin;
		GUI.depth = -1;
		
		GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "NoteBook");
		
		GUI.Box(new Rect(150.0f, 65.0f, 100.0f, 25.0f), "In a nutshell", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 95.0f, 310.0f, 180.0f), "DO IT:\n" +
			"-When you feel down, stressed or bad it's easy to feel unmotivated, tired and do less of the things that you normally enjoy. This leaves you feeling even worse.\n" +
			"-Do more = feel better.\n" +
			"-Each day do one thing you enjoy, one thing that makes you feel like you've achieved something and add some exercise.", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 260.0f, 300.0f, 200.0f), "RELAX:\n" +
			"-Stress can make your body feel tired and tense.\n" +
			"-Relaxed muscles: Tense your muscles & relax them - feel how nice it is once tension has gone.\n" +
			"Start with your fist or arms - tense them for 10 seconds and relax completely. Repeat for your back and shoulders, legs and feet.", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 450.0f, 300.0f, 80.0f), "SORT IT:\n" +
			"-Hardly anyone is born confident but you can always pretend! Smile, make eye contact, say hello loud and clearly.", "NoteBookFont");

		
		GUI.Box(new Rect(560.0f, 85.0f, 200.0f, 25.0f), "Things I am going to do:", "NoteBookFont");
		GUI.Box(new Rect(560.0f, 275.0f, 250.0f, 25.0f), "Things that I find relaxing:", "NoteBookFont");
		
		float y = 110.0f;
		for (int i = 0; i < L2DragAndDropWords.m_L2destinationText.Length; ++i)
		{
			if (L2DragAndDropWords.m_L2destinationText[i] != "")
			{
				GUI.Box(new Rect(560.0f, y, 150.0f, 20.0f), L2DragAndDropWords.m_L2destinationText[i], "NoteBookFont");
				y += 25.0f;
			}
		}
		
		text1 = GUI.TextField(new Rect(560.0f, 290.0f, 300.0f, 25.0f), text1, 32);
		text2 = GUI.TextField(new Rect(560.0f, 320.0f, 300.0f, 25.0f), text2, 32);
		text3 = GUI.TextField(new Rect(560.0f, 350.0f, 300.0f, 25.0f), text3, 32);
		
		if(GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
					Global.arrowKey = "";

			SaveL2NotebookData();

			this.enabled = false;
			GameObject.Find ("GUI").GetComponent<HelpLine>().enabled = true;
			
			//SAVE LEVEL END EVENT
			StartCoroutine(instance.SaveLevelStartEndEventToServer_NewJsonSchema(3, Global.CurrentLevelNumber));
			
			//SAVE PROGRESS
			int severityScore = 0;
			if(Global.CurrentLevelNumber == 1){severityScore = Global.userFeedBackScore[0];}
			if(Global.CurrentLevelNumber == 4){severityScore = Global.userFeedBackScore[1];}
			if(Global.CurrentLevelNumber == 7){severityScore = Global.userFeedBackScore[2];}
			
			StartCoroutine(instance.SaveUserProcessToServer_NewJsonSchema(2, severityScore));
		}
	}

	void SaveL2NotebookData()
	{
		for (int i = 0; i < L2DragAndDropWords.m_L2destinationText.Length; ++i)
		{
			//if (L2DragAndDropWords.m_L2destinationText[i] != "")
			//{
				Global.userL2NoteBookDataBlocks[i] = L2DragAndDropWords.m_L2destinationText[i];
			//}
		}

		Global.userL2NoteBookManualBlocks[0] = text1;
		Global.userL2NoteBookManualBlocks[1] = text2;
		Global.userL2NoteBookManualBlocks[2] = text3;
	}
}
