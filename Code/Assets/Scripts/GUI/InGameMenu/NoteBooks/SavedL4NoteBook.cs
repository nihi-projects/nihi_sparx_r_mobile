using UnityEngine;
using System.Collections;

public class SavedL4NoteBook : MonoBehaviour 
{
	// publics
	public GUISkin m_skin = null;

	
	// privates
	private string text1 = "Say what the problem is";
	private string text2 = "Think of solutions";
	private string text3 = "Examine each solution";
	private string text4 = "Pick one and try it";
	private string text5 = "See what happens";
	
	private CapturedDataInput instance;
	
	public Texture2D m_tNoteBook;
	
	// Use this for initialization
	void Start () 
	{
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		
		//Connecting to the server
		//instance    = CapturedDataInput.GetInstance;
		instance = GameObject.Find("CapturedDataInputHolder").GetComponent<CapturedDataInput>();
		
		//m_tNoteBook = (Texture2D)Resources.Load ("UI/nbook_bg", typeof(Texture2D));
	}
	
	// Update is called once per frame
	void Update () 
	{

	}
	
	void OnGUI()
	{
		GUI.skin = m_skin;
		GUI.depth = -1;
		
		GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "NoteBook");
		
		GUI.Box(new Rect(150.0f, 85.0f, 100.0f, 25.0f), "In a nutshell", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 110.0f, 280.0f, 60.0f), "SOLVE IT:\n" +
			"-Problems can seem like mountains - too hard to climb.", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 180.0f, 280.0f, 200.0f), "SOLVE IT WITH STEPS:\n" +
		        "-SAY what the problem is.\nBe clear and specific as you can.\n" +
		        "-THINK of solutions. Lots of different\nones. Even ones you wouldn't do.\n" +
		        "-EXAMINE these ideas - look  at the\npros and cons of the best ones.\n" +
		        "-PICK one and try it!\n" +
		        "-SEE what happens. Try again if that\nsolution didn't work.", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 390.0f, 300.0f, 120.0f), "SPOT IT\n" +
		        "-Sparks are positive or helpful thoughts\nabout you and your future. They make you \nfeel good.\n" +
		        "-Spot Sparks in your life. Believe them.\nKeep them.", "NoteBookFont");
		
		GUI.Box(new Rect(560.0f, 85.0f, 200.0f, 25.0f), "My problem is:", "NoteBookFont");
		GUI.Box(new Rect(560.0f, 155.0f, 280.0f, 25.0f), "Im going to figure it out using STEPS:", "NoteBookFont");
		GUI.Box(new Rect(560.0f, 175.0f, 20.0f, 25.0f), "S", "NoteBookFont");
		GUI.Box(new Rect(560.0f, 200.0f, 20.0f, 25.0f), "T", "NoteBookFont");
		GUI.Box(new Rect(560.0f, 225.0f, 20.0f, 25.0f), "E", "NoteBookFont");
		GUI.Box(new Rect(560.0f, 250.0f, 20.0f, 25.0f), "P", "NoteBookFont");
		GUI.Box(new Rect(560.0f, 275.0f, 20.0f, 25.0f), "S", "NoteBookFont");
		GUI.Box(new Rect(560.0f, 320.0f, 200.0f, 25.0f), "My Sparks are:", "NoteBookFont");
		
		float y = 110.0f;
		int index = 0;
		for (int i = 0; i < Global.userL4NoteBookDataBlockOne.Length; ++i)
		{
			if (Global.userL4NoteBookDataBlockOne[i] != "")
			{
				GUI.Box(new Rect(560.0f, y, 200.0f, 25.0f), Global.userL4NoteBookDataBlockOne[i], "NoteBookFont");
				y += 25.0f;
			}
		}

		text1 = Global.userL4NoteBookStepsBlocks [0];
		text2 = Global.userL4NoteBookStepsBlocks [1];
		text3 = Global.userL4NoteBookStepsBlocks [2];
		text4 = Global.userL4NoteBookStepsBlocks [3];
		text5 = Global.userL4NoteBookStepsBlocks [4];

		if (text1 == null)
			text1 = "";
		if (text2 == null)
			text2 = "";
		if (text3 == null)
			text3 = "";
		if (text4 == null)
			text4 = "";
		if (text5 == null)
			text5 = "";

		text1 = GUI.TextField(new Rect(570.0f, 177.0f, 300.0f, 25.0f), text1, 32);
		text2 = GUI.TextField(new Rect(570.0f, 202.0f, 300.0f, 25.0f), text2, 32);
		text3 = GUI.TextField(new Rect(570.0f, 227.0f, 300.0f, 25.0f), text3, 32);
		text4 = GUI.TextField(new Rect(570.0f, 252.0f, 300.0f, 25.0f), text4, 32);
		text5 = GUI.TextField(new Rect(570.0f, 277.0f, 300.0f, 25.0f), text5, 32);
		
		y = 360.0f;
		for (int i = 0; i < Global.userL4NoteBookDataBlockTwo.Length; ++i)
		{
			if (Global.userL4NoteBookDataBlockTwo[i] != "")
			{
				GUI.Box(new Rect(560.0f, y, 150.0f, 25.0f), Global.userL4NoteBookDataBlockTwo[i], "NoteBookFont");
				y += 25.0f;
			}
		}

		//if(Global.CurrentLevelNumber > 5 && GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
			if(Global.CurrentLevelNumber > 5 && GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
					Global.arrowKey = "";

			if(Global.CurrentLevelNumber >= 5){
				this.enabled = false;
				GameObject.Find ("GUI").GetComponent<SavedL5NoteBook>().enabled = true;
			}
			else{
				this.enabled = false;
			}
		}
	}
	
	/*
	 * Record the time when the game is finished,
	 * so the play time for the game can be calculated.
	 * */
	void OnDisable()
	{
		
	}
}
