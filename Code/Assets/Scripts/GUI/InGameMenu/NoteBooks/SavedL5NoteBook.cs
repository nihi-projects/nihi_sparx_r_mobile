using UnityEngine;
using System.Collections;

public class SavedL5NoteBook : MonoBehaviour 
{
	// publics
	public GUISkin m_skin = null;
	
	
	// privates
	private string text1 = "";
	private string text2 = "";
	private string text3 = "";
	
	private CapturedDataInput instance;
	
	public Texture2D m_tNoteBook;
	
	// Use this for initialization
	void Start () 
	{
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		
		//Connecting to the server
		//instance    = CapturedDataInput.GetInstance;
		instance = GameObject.Find("CapturedDataInputHolder").GetComponent<CapturedDataInput>();
		
		//m_tNoteBook = (Texture2D)Resources.Load ("UI/nbook_bg", typeof(Texture2D));
	}
	
	// Update is called once per frame
	void Update () 
	{

	}
	
	void OnGUI()
	{
		GUI.skin = m_skin;
		GUI.depth = -1;
		
		GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "NoteBook");
		
		GUI.Box(new Rect(150.0f, 85.0f, 100.0f, 25.0f), "In a nutshell", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 125.0f, 280.0f, 60.0f), "SPOT IT\n" +
		        "-Spot the Gnats (Gloomy Negative\n Automatic Thoughts).\n" +
		        "-Why? Because Gnats are toxic.", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 200.0f, 320.0f, 260.0f), "HOW TO SPOT A GNAT?\n" +
				"-DOWNER: Am I looking on the downside\n and ignoring the positive.\n" +
		        "-PERFECTIONIST: Am I  expecting myself\n to be perfect?\n" +
		        "-MIND READER: Am I trying to read\n people's minds or predict what will happen?\n" +
		        "-GUILTY: Am I thinking this is my fault\n even it's not up to me?\n" +
		        "-DISASTER: Am I making a bigger deal out\n of it than it really is?\n" +
		        "-ALL-OR-NOTHING: Am I seeing things as\n extreme with nothing in between?", "NoteBookFont");
		GUI.Box(new Rect(560.0f, 85.0f, 300.0f, 40.0f), "I SPOT these annoying Gnats\n(Gloomy Negative Automatic Thoughts):", "NoteBookFont");
		GUI.Box(new Rect(560.0f, 275.0f, 250.0f, 25.0f), "Things that stood out for me today:", "NoteBookFont");
		
		float y = 115.0f;
		for (int i = 0; i < Global.userL5NoteBookDataBlocks.Length; ++i)
		{
			if (Global.userL5NoteBookDataBlocks[i] != "Click here to enter text.")
			{
				GUI.Box(new Rect(560.0f, y, 320.0f, 25.0f), Global.userL5NoteBookDataBlocks[i], "NoteBookFont");
				y += 25.0f;
			}
		}
		
		if(Global.userL5NoteBookManualBlocks[0] != "" && Global.userL5NoteBookManualBlocks[0] != null){
			text1 = GUI.TextField(new Rect(560.0f, 305.0f, 300.0f, 25.0f), Global.userL5NoteBookManualBlocks[0], 32);
		}
		if(Global.userL5NoteBookManualBlocks[1] != "" && Global.userL5NoteBookManualBlocks[1] != null){
			text2 = GUI.TextField(new Rect(560.0f, 335.0f, 300.0f, 25.0f), Global.userL5NoteBookManualBlocks[1], 32);
		}
		if(Global.userL5NoteBookManualBlocks[2] != "" && Global.userL5NoteBookManualBlocks[2] != null){
			Debug.Log (Global.userL5NoteBookManualBlocks[2]);
			Debug.Log (text3);
			text3 = GUI.TextField(new Rect(560.0f, 365.0f, 300.0f, 25.0f), Global.userL5NoteBookManualBlocks[2], 32);
		}

		//if(Global.CurrentLevelNumber >= 6 && GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
			if(Global.CurrentLevelNumber >= 6 && GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
					Global.arrowKey = "";

			if(Global.CurrentLevelNumber >= 6){
				this.enabled = false;
				GameObject.Find ("GUI").GetComponent<SavedL6NoteBook>().enabled = true;
			}
			else{
				this.enabled = false;
			}
		}
	}
	
	/*
	 * Record the time when the game is finished,
	 * so the play time for the game can be calculated.
	 * */
	void OnDisable()
	{
		
	}
}
