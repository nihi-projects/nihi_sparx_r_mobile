using UnityEngine;
using System.Collections;

public class SavedL1NoteBook : MonoBehaviour 
{
	// publics
	public GUISkin m_skin = null;
	
	
	// privates

	
	private CapturedDataInput instance;
	
	public Texture2D m_tNoteBook;
	
	// Use this for initialization
	void Start () 
	{
		//m_skin      = (GUISkin) Resources.Load("Skins/SparxSkin");
		//m_tNoteBook = (Texture2D)Resources.Load ("UI/nbook_bg", typeof(Texture2D));
		
		//Connecting to the server
		//instance    = CapturedDataInput.GetInstance;
		instance = GameObject.Find("CapturedDataInputHolder").GetComponent<CapturedDataInput>();
	}
	
	// Update is called once per frame
	void Update () 
	{

	}
	
	void OnGUI()
	{
		GUI.skin = m_skin;
		GUI.depth = -1;
		
		GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "NoteBook");
		
/*		GUI.Box(new Rect(150.0f, 80.0f, 100.0f, 25.0f), "In a nutshell", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 105.0f, 215.0f, 25.0f), "-1 IN 6 GET FEEL DOWN", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 130.0f, 260.0f, 60.0f), "Know that you are not the only one!\nLots of people suffer from depression \nand they recover.", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 195.0f, 300.0f, 60.0f), "-HAVE HOPE\nRemind yourself that things will get better.\nYou'll get through it.", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 260.0f, 300.0f, 80.0f), "-RELAX. USE YOUR BREATHING:\nTest out the simple but powerful skill of\nslow controlled breathing.\nIt will help you to calm down.", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 345.0f, 250.0f, 60.0f), "-MIND POWER:\nStart changing  your thoughts.\nYour feelings will follow.", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 395.0f, 250.0f, 60.0f), "\n\n\n\n\nCHANGE WHAT YOU THINK.\n\nCHANGE WHAT YOU DO.\n\nYOUR FEELINGS WILL\nCHANGE TOO.", "CenteredFont");
		GUI.Box(new Rect(560.0f, 90.0f, 200.0f, 25.0f), "Things I want to change:", "NoteBookFont");
		GUI.Box(new Rect(560.0f, 310.0f, 250.0f, 25.0f), "Things that stood out for me today:", "NoteBookFont");
*/
		GUI.Box(new Rect(150.0f, 70.0f, 100.0f, 25.0f), "In a nutshell", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 105.0f, 300.0f, 80.0f), "People go up and down: if you have hard times you’re not the only one! Lots of people get down, stressed, really angry or feel no good but they find a way through.", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 200.0f, 300.0f, 60.0f), "-HAVE HOPE\nRemind yourself that things will get better.\nYou'll get through it.", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 275.0f, 300.0f, 80.0f), "-RELAX. USE YOUR BREATHING:\nTest out the simple but powerful skill of\nslow controlled breathing.\nIt will help you to calm down.", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 375.0f, 250.0f, 60.0f), "-MIND POWER:\nStart changing  your thoughts.\nYour feelings will follow.", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 395.0f, 250.0f, 60.0f), "\n\n\n\n\n\n\n\n\n\nCHANGE WHAT YOU THINK.\n\nCHANGE WHAT YOU DO.\n\nYOUR FEELINGS WILL\n\nCHANGE TOO.", "CenteredFont");
		GUI.Box(new Rect(560.0f, 90.0f, 200.0f, 25.0f), "Things I want to change:", "NoteBookFont");
		GUI.Box(new Rect(560.0f, 310.0f, 250.0f, 25.0f), "Things that stood out for me today:", "NoteBookFont");

		float y = 120.0f;
		for (int i = 0; i < DragAndDropWords.m_L1destinationText.Length; ++i){
			if (Global.userL1NoteBookDataBlocks[i] != ""){
				GUI.Box(new Rect(560.0f, y, 150.0f, 25.0f), Global.userL1NoteBookDataBlocks[i], "NoteBookFont");
				y += 25.0f;
			}
		}
		
		GUI.Label(new Rect(560.0f, 340.0f, 300.0f, 25.0f), Global.userL1NoteBookManualBlocks[0], "NoteBookFont");
		GUI.Label(new Rect(560.0f, 370.0f, 300.0f, 25.0f), Global.userL1NoteBookManualBlocks[1], "NoteBookFont");
		GUI.Label(new Rect(560.0f, 400.0f, 300.0f, 25.0f), Global.userL1NoteBookManualBlocks[2], "NoteBookFont");
		
		//if(Global.CurrentLevelNumber > 2 && GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
			if(Global.CurrentLevelNumber > 2 && GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
					Global.arrowKey = "";

			if(Global.CurrentLevelNumber >= 2){
				this.enabled = false;
				GameObject.Find ("GUI").GetComponent<SavedL2NoteBook>().enabled = true;
			}
			else{
				this.enabled = false;
			}
		}
	}
	
	/*
	 * Record the time when the game is finished,
	 * so the play time for the game can be calculated.
	 * */
	void OnDisable()
	{
		
	}
}
