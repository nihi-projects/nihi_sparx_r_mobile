using UnityEngine;
using System.Collections;

public class SavedL3NoteBook : MonoBehaviour 
{
	// publics
	public GUISkin m_skin = null;
	
	
	// privates
	private CapturedDataInput instance;
	public Texture2D m_tNoteBook;
	
	// Use this for initialization
	void Start () 
	{
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		
		//Connecting to the server
		//instance    = CapturedDataInput.GetInstance;
		instance = GameObject.Find("CapturedDataInputHolder").GetComponent<CapturedDataInput>();
		
		//m_tNoteBook = (Texture2D)Resources.Load ("UI/nbook_bg", typeof(Texture2D));
	}
	
	// Update is called once per frame
	void Update () 
	{

	}
	
	void OnGUI()
	{
		GUI.skin = m_skin;
		GUI.depth = -1;
		
		GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "NoteBook");
		
		GUI.Box(new Rect(150.0f, 65f, 100.0f, 25.0f), "In a nutshell", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 90.0f, 100.0f, 25.0f), "SPOT IT", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 122.0f, 280.0f, 40.0f), "Spot feelings of anger or hurt and choose\nhow you react.", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 162.0f, 320.0f, 60.0f), "Level 1: DISTRACTION (walk away; count;\nlisten to music; make up rhymes;\ntake time out).", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 220.0f, 280.0f, 40.0f), "Level 2: STOP IT; TRASH IT,\nTURN IT DOWN.", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 257.0f, 280.0f, 60.0f), "Level 3: SORT IT! If it's a real problem,\ncalm down, pick a good time and\nSORT IT.", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 315.0f, 250.0f, 120.0f), "LISTEN WITH BLINC \n    Bite your tongue \n    Look at the speaker be Interested \n    No interruptions \n    Check you understand", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 438.0f, 300.0f, 60.0f), "BE ASSERTIVE \nNot aggro, not a push over. Say what you\nneed in a calm way.", "NoteBookFont");

		GUI.Box(new Rect(530.0f, 65.0f, 150.0f, 25.0f), "My triggers are:", "NoteBookFont");
		GUI.Box(new Rect(530.0f, 240.0f, 180.0f, 25.0f), "My distraction skills:", "NoteBookFont");
		GUI.Box(new Rect(530.0f, 415.0f, 280.0f, 25.0f), "What I would like to try out from today:", "NoteBookFont");
		GUI.Box(new Rect(530.0f, 470.0f, 300.0f, 80.0f), "Its OK to have strong feelings but it's\nwhat you do with the feelings that\nmatter. Don't hurt yourself, hurt others, or\ndamage other people's property.", "NoteBookFont");

		float y = 85.0f;
		int index = 0;
		for (int i = 0; i < Global.userL3NoteBookDataBlockOne.Length; ++i)
		{
			if (Global.userL3NoteBookDataBlockOne[i] != "")
			{
				GUI.Box(new Rect(530.0f, y, 150.0f, 25.0f), Global.userL3NoteBookDataBlockOne[i], "NoteBookFont");
				y += 23.0f;
			}
			++index;
		}
		
		y = 265.0f;
		for (int i = 0; i < Global.userL3NoteBookDataBlockTwo.Length; ++i)
		{
			if (Global.userL3NoteBookDataBlockTwo[i] != "")
			{
				GUI.Box(new Rect(530.0f, y, 150.0f, 25.0f), Global.userL3NoteBookDataBlockTwo[i], "NoteBookFont");
				y += 23.0f;
			}
			++index;
		}
		
		y = 440.0f;
		if (Global.userL2NoteBookDataBlocks[0] != "")
		{
			GUI.Box(new Rect(530.0f, y, 240.0f, 25.0f), Global.userL3NoteBookDataBlockThree[0], "NoteBookFont");
			y += 25.0f;
		}
	
		//if(Global.CurrentLevelNumber > 4 && GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
		if(Global.CurrentLevelNumber > 4 && GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
					Global.arrowKey = "";

			if(Global.CurrentLevelNumber >= 4){
				this.enabled = false;
				GameObject.Find ("GUI").GetComponent<SavedL4NoteBook>().enabled = true;
			}
			else{
				this.enabled = false;
			}
		}
	}
	
	/*
	 * Record the time when the game is finished,
	 * so the play time for the game can be calculated.
	 * */
	void OnDisable()
	{
		
	}
}
