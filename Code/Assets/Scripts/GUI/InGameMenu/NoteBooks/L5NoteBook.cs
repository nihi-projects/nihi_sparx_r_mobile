using UnityEngine;
using System.Collections;

public class L5NoteBook : MonoBehaviour 
{
	// publics
	public GUISkin m_skin = null;
	
	
	// privates
	private string text1 = "Click here to enter text";
	private string text2 = "Click here to enter text";
	private string text3 = "Click here to enter text";
	
	private CapturedDataInput instance;
	
	public Texture2D m_tNoteBook;

	private int m_iIndex = 0;
	
	// Use this for initialization
	void Start () 
	{
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		
		//Connecting to the server
		//instance    = CapturedDataInput.GetInstance;
		instance = GameObject.Find("CapturedDataInputHolder").GetComponent<CapturedDataInput>();
		
		//m_tNoteBook = (Texture2D)Resources.Load ("UI/nbook_bg", typeof(Texture2D));
	}
	
	// Update is called once per frame
	void Update () 
	{

	}
	
	void OnGUI()
	{
		GUI.skin = m_skin;
		GUI.depth = -1;
		
		GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "NoteBook");
		
		GUI.Box(new Rect(150.0f, 85.0f, 100.0f, 25.0f), "In a nutshell", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 125.0f, 280.0f, 60.0f), "SPOT IT\n" +
														"-Spot the Gnats (Gloomy Negative\n Automatic Thoughts).\n" +
														"-Why? Because Gnats are toxic.", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 200.0f, 320.0f, 260.0f), "HOW TO SPOT A GNAT?\n" +
														"-DOWNER: Am I looking on the downside\n and ignoring the positive.\n" +
														 "-PERFECTIONIST: Am I  expecting myself\n to be perfect?\n" +
														 "-MIND READER: Am I trying to read\n people's minds or predict what will happen?\n" +
														 "-GUILTY: Am I thinking this is my fault\n even it's not up to me?\n" +
														 "-DISASTER: Am I making a bigger deal out\n of it than it really is?\n" +
														 "-ALL-OR-NOTHING: Am I seeing things as\n extreme with nothing in between?", "NoteBookFont");
		GUI.Box(new Rect(560.0f, 85.0f, 300.0f, 40.0f), "I SPOT these annoying Gnats\n(Gloomy Negative Automatic Thoughts):", "NoteBookFont");
		GUI.Box(new Rect(560.0f, 275.0f, 250.0f, 25.0f), "Things that stood out for me today:", "NoteBookFont");
		
		float y = 115.0f;
		for (int i = 0; i < L5EnterTexts.m_inputTexts.Length; ++i)
		{
			if (L5EnterTexts.m_inputTexts[i] != "Click here to enter text.")
			{
				GUI.Box(new Rect(560.0f, y, 320.0f, 25.0f), L5EnterTexts.m_inputTexts[i], "NoteBookFont");
				y += 25.0f;
			}
		}
		GUI.SetNextControlName ("text1");
		text1 = GUI.TextField(new Rect(560.0f, 305.0f, 300.0f, 25.0f), text1, 32);
		GUI.SetNextControlName ("text2");
		text2 = GUI.TextField(new Rect(560.0f, 335.0f, 300.0f, 25.0f), text2, 32);
		GUI.SetNextControlName ("text3");
		text3 = GUI.TextField(new Rect(560.0f, 365.0f, 300.0f, 25.0f), text3, 32);

		if (GUI.GetNameOfFocusedControl () != "") {
			text1 = Global.doTextFieldCheck("text1", text1, new Rect(560.0f, 305.0f, 300.0f, 25.0f));
			text2 = Global.doTextFieldCheck("text2", text2, new Rect(560.0f, 335.0f, 300.0f, 25.0f));
			text3 = Global.doTextFieldCheck("text3", text3, new Rect(560.0f, 365.0f, 300.0f, 25.0f));
		}
		if(GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
					Global.arrowKey = "";

			SaveL5NotebookData();

			this.enabled = false;
			GameObject.Find ("GUI").GetComponent<HelpLine>().enabled = true;
			
			//SAVE LEVEL END EVENT
			StartCoroutine(instance.SaveLevelStartEndEventToServer_NewJsonSchema(3, Global.CurrentLevelNumber));
			
			//SAVE PROGRESS
			int severityScore = 0; 
			if(Global.CurrentLevelNumber == 1){severityScore = Global.userFeedBackScore[0];}
			if(Global.CurrentLevelNumber == 4){severityScore = Global.userFeedBackScore[1];}
			if(Global.CurrentLevelNumber == 7){severityScore = Global.userFeedBackScore[2];}
			
			StartCoroutine(instance.SaveUserProcessToServer_NewJsonSchema(5, severityScore));
		}
	}

	void SaveL5NotebookData()
	{
		for (int i = 0; i < L5EnterTexts.m_inputTexts.Length; ++i)
		{
			if (L5EnterTexts.m_inputTexts[i] != "Click here to enter text.")
			{
				Global.userL5NoteBookDataBlocks[m_iIndex] = L5EnterTexts.m_inputTexts[i];
				m_iIndex++;
			}
		}

		Global.userL5NoteBookManualBlocks [0] = text1;
		Global.userL5NoteBookManualBlocks [1] = text2;
		Global.userL5NoteBookManualBlocks [2] = text3;
	}
}
