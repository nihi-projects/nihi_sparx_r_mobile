using UnityEngine;
using System.Collections;

public class L1NoteBook : MonoBehaviour 
{
	// publics
	public GUISkin m_skin = null;
	
	
	// privates
	private string text1 = "Click here to enter text";
	private string text2 = "Click here to enter text";
	private string text3 = "Click here to enter text";
	
	private CapturedDataInput instance;
	
	public Texture2D m_tNoteBook;

	private int m_iIndex = 0;
	
	// Use this for initialization
	void Start () 
	{
		//m_skin      = (GUISkin) Resources.Load("Skins/SparxSkin");
		//m_tNoteBook = (Texture2D)Resources.Load ("UI/nbook_bg", typeof(Texture2D));
		
		//Connecting to the server
		//instance    = CapturedDataInput.GetInstance;
		instance = GameObject.Find("CapturedDataInputHolder").GetComponent<CapturedDataInput>();
	}
	
	// Update is called once per frame
	void Update () 
	{

	}
	
	void OnGUI()
	{
		GUI.skin = m_skin;
		GUI.depth = -1;
		
		GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "NoteBook");
		
		GUI.Box(new Rect(150.0f, 70.0f, 100.0f, 25.0f), "In a nutshell", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 105.0f, 300.0f, 80.0f), "People go up and down: if you have hard times you’re not the only one! Lots of people get down, stressed, really angry or feel no good but they find a way through.", "NoteBookFont");
		//GUI.Box(new Rect(150.0f, 175.0f, 300.0f, 80.0f), "Know that you are not the only one!\nLots of people suffer from depression \nand they recover.", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 200.0f, 300.0f, 60.0f), "-HAVE HOPE\nRemind yourself that things will get better.\nYou'll get through it.", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 275.0f, 300.0f, 80.0f), "-RELAX. USE YOUR BREATHING:\nTest out the simple but powerful skill of\nslow controlled breathing.\nIt will help you to calm down.", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 375.0f, 250.0f, 60.0f), "-MIND POWER:\nStart changing  your thoughts.\nYour feelings will follow.", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 395.0f, 250.0f, 60.0f), "\n\n\n\n\n\n\n\nCHANGE WHAT YOU THINK.\nCHANGE WHAT YOU DO.\nYOUR FEELINGS WILL\nCHANGE TOO.", "CenteredFont");
		GUI.Box(new Rect(560.0f, 90.0f, 200.0f, 25.0f), "Things I want to change:", "NoteBookFont");
		GUI.Box(new Rect(560.0f, 310.0f, 250.0f, 25.0f), "Things that stood out for me today:", "NoteBookFont");
		
		float y = 120.0f;
		for (int i = 0; i < DragAndDropWords.m_L1destinationText.Length; ++i){
			if (DragAndDropWords.m_L1destinationText[i] != ""){
				GUI.Box(new Rect(560.0f, y, 150.0f, 25.0f), DragAndDropWords.m_L1destinationText[i], "NoteBookFont");
				y += 25.0f;
			}
		}
		
		text1 = GUI.TextField(new Rect(560.0f, 340.0f, 300.0f, 25.0f), text1, 32);
		text2 = GUI.TextField(new Rect(560.0f, 370.0f, 300.0f, 25.0f), text2, 32);
		text3 = GUI.TextField(new Rect(560.0f, 400.0f, 300.0f, 25.0f), text3, 32);
		
		if(GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
					Global.arrowKey = "";

			SaveL1NotebookData ();

			this.enabled = false;
			GameObject.Find ("GUI").GetComponent<HelpLine>().enabled = true;
			
			//SAVE LEVEL END EVENT (For the US developers, )	
			StartCoroutine(instance.SaveLevelStartEndEventToServer_NewJsonSchema(3, Global.CurrentLevelNumber));
			
			//SAVE PROGRESS
			int severityScore = 0; 
			if(Global.CurrentLevelNumber == 1){severityScore = Global.userFeedBackScore[0];}
			if(Global.CurrentLevelNumber == 4){severityScore = Global.userFeedBackScore[1];}
			if(Global.CurrentLevelNumber == 7){severityScore = Global.userFeedBackScore[2];}
			
			StartCoroutine(instance.SaveUserProcessToServer_NewJsonSchema(1, severityScore));
		}
	}

	void SaveL1NotebookData()
	{
		for (int i = 0; i < DragAndDropWords.m_L1destinationText.Length; ++i){
			//if (DragAndDropWords.m_L1destinationText[i] != "" && DragAndDropWords.m_L1destinationText[i] != " " && DragAndDropWords.m_L1destinationText[i] != null){
			if (DragAndDropWords.m_L1destinationText[i] != null){
				//Save notbook data to server
				Global.userL1NoteBookDataBlocks[m_iIndex] = DragAndDropWords.m_L1destinationText[i];
				m_iIndex++;
			}
		}

		//Save notbook data to server
		Global.userL1NoteBookManualBlocks[0] = text1;
		Global.userL1NoteBookManualBlocks[1] = text2;
		Global.userL1NoteBookManualBlocks[2] = text3;
	}
}
