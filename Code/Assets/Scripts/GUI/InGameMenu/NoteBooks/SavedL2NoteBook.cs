using UnityEngine;
using System.Collections;

public class SavedL2NoteBook : MonoBehaviour 
{
	// publics
	public GUISkin m_skin = null;
	
	
	// privates

	
	private CapturedDataInput instance;
	public Texture2D m_tNoteBook;
	
	
	// Use this for initialization
	void Start () 
	{
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		
		//Connecting to the server
		//instance    = CapturedDataInput.GetInstance;
		instance = GameObject.Find("CapturedDataInputHolder").GetComponent<CapturedDataInput>();
		
		//m_tNoteBook = (Texture2D)Resources.Load ("UI/nbook_bg", typeof(Texture2D));
	}
	
	// Update is called once per frame
	void Update () 
	{

	}
	
	void OnGUI()
	{
		GUI.skin = m_skin;
		GUI.depth = -1;
		
		GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "NoteBook");
/*
		GUI.Box(new Rect(150.0f, 65.0f, 100.0f, 25.0f), "In a nutshell", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 85.0f, 310.0f, 180.0f), "DO IT:\n" +
				"-When you feel down, stressed or bad it's easy to feel\n unmotivated, tired and do less of the things\n that you normally enjoy. This leaves you feeling even worse.\n" +
		        "-Do more = feel better.\n" +
		        "-Each day do one thing you enjoy, one thing\nthat makes you feel like you've achieved\nsomething and add some exercise.", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 260.0f, 300.0f, 200.0f), "RELAX:\n" +
		        "-Stress can make your body feel tired\nand tense.\n" +
		        "-Relaxed muscles: Tense your muscles &\nrelax them - feel how nice it is once\ntension has gone.\n" +
		        "Start with your fist or arms - tense them\nfor 10 seconds and relax completely.\nRepeat for your back and shoulders,\nlegs and feet.", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 460.0f, 300.0f, 80.0f), "SORT IT\n" +
		        "-Hardly anyone is born confident but you\ncan always pretend! Smile, make eye\ncontact, say hello loud and clearly.", "NoteBookFont");
*/
		GUI.Box(new Rect(150.0f, 65.0f, 100.0f, 25.0f), "In a nutshell", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 95.0f, 310.0f, 180.0f), "DO IT:\n" +
			"-When you feel down, stressed or bad it's easy to feel unmotivated, tired and do less of the things that you normally enjoy. This leaves you feeling even worse.\n" +
			"-Do more = feel better.\n" +
			"-Each day do one thing you enjoy, one thing that makes you feel like you've achieved something and add some exercise.", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 260.0f, 300.0f, 200.0f), "RELAX:\n" +
			"-Stress can make your body feel tired and tense.\n" +
			"-Relaxed muscles: Tense your muscles & relax them - feel how nice it is once tension has gone.\n" +
			"Start with your fist or arms - tense them for 10 seconds and relax completely. Repeat for your back and shoulders, legs and feet.", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 450.0f, 300.0f, 80.0f), "SORT IT:\n" +
			"-Hardly anyone is born confident but you can always pretend! Smile, make eye contact, say hello loud and clearly.", "NoteBookFont");


		GUI.Box(new Rect(560.0f, 85.0f, 200.0f, 25.0f), "Things I am going to do:", "NoteBookFont");
		GUI.Box(new Rect(560.0f, 275.0f, 250.0f, 25.0f), "Things that I find relaxing:", "NoteBookFont");

		float y = 110.0f;
		for (int i = 0; i < Global.userL2NoteBookDataBlocks.Length; ++i)
		{
			if (Global.userL2NoteBookDataBlocks[i] != "")
			{
				GUI.Box(new Rect(560.0f, y, 150.0f, 20.0f), Global.userL2NoteBookDataBlocks[i], "NoteBookFont");
				y += 25.0f;
			}
		}
		
		GUI.Label(new Rect(560.0f, 300.0f, 300.0f, 25.0f), Global.userL2NoteBookManualBlocks[0], "NoteBookFont");
		GUI.Label(new Rect(560.0f, 330.0f, 300.0f, 25.0f), Global.userL2NoteBookManualBlocks[1], "NoteBookFont");
		GUI.Label(new Rect(560.0f, 360.0f, 300.0f, 25.0f), Global.userL2NoteBookManualBlocks[2], "NoteBookFont");
		
		//if(Global.CurrentLevelNumber > 3 && GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
		if(Global.CurrentLevelNumber > 3 && GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
					Global.arrowKey = "";

			if(Global.CurrentLevelNumber >= 3){
				this.enabled = false;
				GameObject.Find ("GUI").GetComponent<SavedL3NoteBook>().enabled = true;
			}
			else{
				this.enabled = false;
			}
		}
	}
	
	/*
	 * Record the time when the game is finished,
	 * so the play time for the game can be calculated.
	 * */
	void OnDisable()
	{
		
	}
}
