using UnityEngine;
using System.Collections;

public class L6NoteBook : MonoBehaviour 
{
	// publics
	public GUISkin m_skin = null;
	
	
	// privates
	private string text1 = "Click here to enter text";
	private string text2 = "Click here to enter text";
	private string text3 = "Click here to enter text";
	
	private CapturedDataInput instance;
	
	public Texture2D m_tNoteBook;

	private int m_iIndex = 0;
	
	// Use this for initialization
	void Start () 
	{
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		
		//Connecting to the server
		//instance    = CapturedDataInput.GetInstance;
		instance = GameObject.Find("CapturedDataInputHolder").GetComponent<CapturedDataInput>();
		
		//m_tNoteBook = (Texture2D)Resources.Load ("UI/nbook_bg", typeof(Texture2D));
	}
	
	// Update is called once per frame
	void Update () 
	{

	}
	
	void OnGUI()
	{
		GUI.skin = m_skin;
		GUI.depth = -1;
		
		//GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "NoteBook");
		GUI.Label(new Rect(0.0f, 0.0f, 960.0f, 600.0f), "", "NoteBook");

		GUI.Box(new Rect(150.0f, 85.0f, 100.0f, 25.0f), "In a nutshell", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 110.0f, 300.0f, 100.0f), "SWAP IT\n" +
														"-Thoughts are not facts. You can SWAP\nthem.\n" +
														"-Spot a negative thought and swap it for\nsomething better!", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 220.0f, 300.0f, 180.0f), "RAPA = 4 steps to SWAP a thought.\n" +
														 "R=REALITY CHECK. How do you know\nyour thought is true?\n" +
														 "A=ANOTHER VIEW. Is there another\nway to think about it?\n" +
														 "P=PERSPECTIVE. Is it really as bad as\nyou think?\n" +
														 "A=THINK 'ACTION! Think solutions,\nnot problems.", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 410.0f, 310.0f, 80.0f), "SORT IT\n" +
														 "-Negotiate a deal. Listen, explain what you\nneed, give a little, take a little and aim for\n a compromise.", "NoteBookFont");
		
		GUI.Box(new Rect(560.0f, 85.0f, 200.0f, 25.0f), "A thought I'd like to swap:", "NoteBookFont");
		GUI.Box(new Rect(560.0f, 145.0f, 200.0f, 25.0f), "REALITY CHECK", "NoteBookFont");
		GUI.Box(new Rect(560.0f, 205.0f, 200.0f, 25.0f), "ANOTHER VIEW", "NoteBookFont");
		GUI.Box(new Rect(560.0f, 265.0f, 200.0f, 25.0f), "Put it into PERSPECTIVE", "NoteBookFont");
		GUI.Box(new Rect(560.0f, 325.0f, 200.0f, 25.0f), "THINK ACTION!", "NoteBookFont");
		GUI.Box(new Rect(560.0f, 400.0f, 280.0f, 25.0f), "What I would like to try out from today:", "NoteBookFont");
	
		if (L6NegativeStatement.m_sChoice != "")
		{
			GUI.Box(new Rect(560.0f, 115.0f, 300.0f, 25.0f), L6NegativeStatement.m_sChoice, "NoteBookFont");
		}
		
		float y = 170.0f;
		for (int i = 0; i < L6AnalyseNegativeStatement.m_sChoices.Length; ++i)
		{
			GUI.Box(new Rect(560.0f, y, 300.0f, 25.0f), L6AnalyseNegativeStatement.m_sChoices[i], "NoteBookFont");
			y += 60;
		}
		
		text1 = GUI.TextField(new Rect(560.0f, 425.0f, 300.0f, 25.0f), text1, 32);
		text2 = GUI.TextField(new Rect(560.0f, 455.0f, 300.0f, 25.0f), text2, 32);
		text3 = GUI.TextField(new Rect(560.0f, 485.0f, 300.0f, 25.0f), text3, 32);
		
		//if(GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
		if(GUI.Button(new Rect(800, 495, 60, 60), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
			Global.arrowKey = "";

			SaveL6NotebookData();

			this.enabled = false;
			GameObject.Find ("GUI").GetComponent<HelpLine>().enabled = true;
			
			//SAVE LEVEL END EVENT
			StartCoroutine(instance.SaveLevelStartEndEventToServer_NewJsonSchema(3, Global.CurrentLevelNumber));
			
			
			//SAVE PROGRESS
			int severityScore = 0; 
			if(Global.CurrentLevelNumber == 1){severityScore = Global.userFeedBackScore[0];}
			if(Global.CurrentLevelNumber == 4){severityScore = Global.userFeedBackScore[1];}
			if(Global.CurrentLevelNumber == 7){severityScore = Global.userFeedBackScore[2];}
			
			StartCoroutine(instance.SaveUserProcessToServer_NewJsonSchema(6, severityScore));
		}
	}

	void SaveL6NotebookData()
	{
		Global.userL6NoteBookDataBlocks[0] = L6NegativeStatement.m_sChoice;

		for (int i = 0; i < L6AnalyseNegativeStatement.m_sChoices.Length; ++i)
		{
			Global.userL6NoteBookRapaBlocks[m_iIndex] = L6AnalyseNegativeStatement.m_sChoices[i];
			m_iIndex++;
		}
		
		Global.userL6NoteBookManualBlocks [0] = text1;
		Global.userL6NoteBookManualBlocks [1] = text2;
		Global.userL6NoteBookManualBlocks [2] = text3;
	}
}
