using UnityEngine;
using System.Collections;

public class SavedL6NoteBook : MonoBehaviour 
{
	// publics
	public GUISkin m_skin = null;
	
	
	// privates
	private string text1 = "";
	private string text2 = "";
	private string text3 = "";
	
	private CapturedDataInput instance;
	
	public Texture2D m_tNoteBook;
	
	// Use this for initialization
	void Start () 
	{
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		
		//Connecting to the server
		//instance    = CapturedDataInput.GetInstance;
		instance = GameObject.Find("CapturedDataInputHolder").GetComponent<CapturedDataInput>();
		
		//m_tNoteBook = (Texture2D)Resources.Load ("UI/nbook_bg", typeof(Texture2D));
	}
	
	// Update is called once per frame
	void Update () 
	{

	}
	
	void OnGUI()
	{
		GUI.skin = m_skin;
		GUI.depth = -1;
		
		GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "NoteBook");
		
		GUI.Box(new Rect(150.0f, 85.0f, 100.0f, 25.0f), "In a nutshell", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 110.0f, 300.0f, 100.0f), "SWAP IT\n" +
		        "-Thoughts are not facts. You can SWAP\nthem.\n" +
		        "-Spot a negative thought and swap it for\nsomething better!", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 220.0f, 300.0f, 180.0f), "RAPA = 4 steps to SWAP a thought.\n" +
		        "R=REALITY CHECK. How do you know\nyour thought is true?\n" +
		        "A=ANOTHER VIEW. Is there another\nway to think about it?\n" +
		        "P=PERSPECTIVE. Is it really as bad as\nyou think?\n" +
		        "A=THINK 'ACTION! Think solutions,\nnot problems.", "NoteBookFont");
		GUI.Box(new Rect(150.0f, 410.0f, 310.0f, 80.0f), "SORT IT\n" +
		        "-Negotiate a deal. Listen, explain what you\nneed, give a little, take a little and aim for\n a compromise.", "NoteBookFont");
		
		GUI.Box(new Rect(560.0f, 85.0f, 200.0f, 25.0f), "A thought I'd like to swap:", "NoteBookFont");
		GUI.Box(new Rect(560.0f, 145.0f, 200.0f, 25.0f), "REALITY CHECK", "NoteBookFont");
		GUI.Box(new Rect(560.0f, 205.0f, 200.0f, 25.0f), "ANOTHER VIEW", "NoteBookFont");
		GUI.Box(new Rect(560.0f, 265.0f, 200.0f, 25.0f), "Put it into PERSPECTIVE", "NoteBookFont");
		GUI.Box(new Rect(560.0f, 325.0f, 200.0f, 25.0f), "THINK ACTION!", "NoteBookFont");
		GUI.Box(new Rect(560.0f, 400.0f, 280.0f, 25.0f), "What I would like to try out from today:", "NoteBookFont");
	
		if (Global.userL6NoteBookDataBlocks[0] != "")
		{
			GUI.Box(new Rect(560.0f, 115.0f, 300.0f, 25.0f), Global.userL6NoteBookDataBlocks[0], "NoteBookFont");
		}
		
		float y = 170.0f;
		for (int i = 0; i < Global.userL6NoteBookRapaBlocks.Length; ++i)
		{
			GUI.Box(new Rect(560.0f, y, 300.0f, 25.0f), Global.userL6NoteBookRapaBlocks[i], "NoteBookFont");
			y += 60;
		}
		if(Global.userL6NoteBookManualBlocks[0] != "" && Global.userL6NoteBookManualBlocks[0] != null){
			text1 = GUI.TextField(new Rect(560.0f, 425.0f, 300.0f, 25.0f), Global.userL6NoteBookManualBlocks[0], 32);
		}
		if(Global.userL6NoteBookManualBlocks[1] != "" && Global.userL6NoteBookManualBlocks[1] != null){
			text2 = GUI.TextField(new Rect(560.0f, 455.0f, 300.0f, 25.0f), Global.userL6NoteBookManualBlocks[1], 32);
		}
		if(Global.userL6NoteBookManualBlocks[2] != "" && Global.userL6NoteBookManualBlocks[2] != null){
			text3 = GUI.TextField(new Rect(560.0f, 485.0f, 300.0f, 25.0f), Global.userL6NoteBookManualBlocks[2], 32);
		}

		//if(Global.CurrentLevelNumber > 6 && GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
		if(Global.CurrentLevelNumber > 6 && GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
			Global.arrowKey = "";

			if(Global.CurrentLevelNumber >= 6){
				this.enabled = false;
				GameObject.Find ("Bag").GetComponent<Bag>().m_bNotebookUp = false;
			}
			else{
				this.enabled = false;
				GameObject.Find ("Bag").GetComponent<Bag>().m_bNotebookUp = false;
			}
		}

	}
	
	/*
	 * Record the time when the game is finished,
	 * so the play time for the game can be calculated.
	 * */
	void OnDisable()
	{
		
	}
}
