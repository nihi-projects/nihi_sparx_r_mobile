using UnityEngine;
using System.Collections;

public class L3StopItWithText : MonoBehaviour {

	public GUISkin m_skin = null;
	
	private float m_fTimer = 0.0f;
	
	public Texture2D m_tStopItTexture;
	
	
	// Use this for initialization
	void Start () {
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		
		//m_tStopItTexture = (Texture2D)Resources.Load ("UI/mg_l3_e_mg1", typeof(Texture2D));
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	/*
	*
	* OnGUI Function.
	*
	*/
	public void OnGUI () 
	{		
		GUI.skin = m_skin;
		GUI.depth = -1;
		
		GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
		GUI.Label(new Rect(155.0f, 55.0f, 650.0f, 501.0f), m_tStopItTexture);
		GUI.Label(new Rect(180, 65, 600, 30), "Unhelpful thought? STOP IT. Think of something else. For example:", "Titles");
		
		m_fTimer += Time.deltaTime;
		
		if (m_fTimer > 7.5f)
		{
			GUI.Label(new Rect(280.0f, 440.0f, 400.0f, 40.0f), "Try it now", "CenteredFontBright");
		}
		if (m_fTimer > 5.0f)
		{
			GUI.Label(new Rect(280.0f, 390.0f, 400.0f, 40.0f), "Words - Come up with some rhymes.", "CenteredFontBright");
		}
		if (m_fTimer > 3.5f)
		{
			GUI.Label(new Rect(280.0f, 321.0f, 400.0f, 40.0f), "Sports - Quiz yourself on players' names.", "CenteredFontBright");
		}
		if (m_fTimer > 2.0f)
		{
			GUI.Label(new Rect(280.0f, 248.0f, 400.0f, 40.0f), "Music - Make up lyrics to a favourite song.", "CenteredFontBright");
		}
		if (m_fTimer > 0.5f)
		{
			GUI.Label(new Rect(280.0f, 175.0f, 400.0f, 40.0f), "Maths - Count backwards or multiply.", "CenteredFontBright");
		}
		
		if(GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
					Global.arrowKey = "";

			this.enabled = false;
			
			GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;
			//Update the current scene name
			GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentScene = "L3GuideScene30";
		}
	}
}
