using UnityEngine;
using System.Collections;

public class L3DragDropWords : MonoBehaviour 
{
	Rect k_zeroRect = new Rect(0.0f, 0.0f, 0.0f, 0.0f);
	// publics
	public GUISkin m_skin = null;
	
	
	// privates
	private string m_conversationBoxText = "";
	 
	static private float m_keywordWidth = 100.0f;
	static private float m_keywordHeight = 30.0f;
	private Vector2[] m_KeywordDimensions = 
	{
		new Vector2(150.0f, 30.0f), // Heavy
		new Vector2(70.0f, 30.0f), // 'Sore' heart
		new Vector2(130.0f, 30.0f), // No Energy
		new Vector2(180.0f, 30.0f), // Shaky
		new Vector2(100.0f, 30.0f), // Numb
		new Vector2(120.0f, 30.0f), // Deflated
		new Vector2(160.0f, 30.0f), // Cry

		new Vector2(140.0f, 30.0f), // Sick
		new Vector2(120.0f, 30.0f), // Selfharm
		new Vector2(150.0f, 30.0f), // Headache
		new Vector2(120.0f, 30.0f), // Voilent
		new Vector2(90.0f, 30.0f), // Loneliness
		new Vector2(120.0f, 30.0f), // Voilent
		new Vector2(150.0f, 30.0f), // Loneliness

	};
	
	private Rect[] m_myOriRect; 
	
	private string[] m_keyWordText = {"I get a headache", "I cry", "I yell/swear", "My blood is pumping", 
		"I feel sick", "I want to fight", "I am short of breath", "I have a red face",
		"I bite my nails ", "I tense my shoulders","I clench my fists", "I get shaky","I want to leave", "My heart is racing"};
	
	//KeyWord destination positions 
	static private float m_dextinationRectWidth = 240.0f;
	static private float m_dextinationRectHeight = 35.0f;
	
	private Rect[] m_destinationOriKeyWordRect = 
	{
		new Rect(360.0f, 175.0f, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(360.0f, 210.0f, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(360.0f, 245.0f, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(360.0f, 280.0f, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(360.0f, 315.0f, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(360.0f, 345.0f, m_dextinationRectWidth, m_dextinationRectHeight)
	};

	private string[] m_destinationText = {"", "", "", "", "", ""};

	private Rect[] m_destinationRect = 
	{
		new Rect(410.0f, 175.0f, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(410.0f, 210.0f, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(410.0f, 245.0f, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(410.0f, 280.0f, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(410.0f, 315.0f, m_dextinationRectWidth, m_dextinationRectHeight),
		new Rect(410.0f, 345.0f, m_dextinationRectWidth, m_dextinationRectHeight)
	};
	
	private Rect[] m_movableStringRect;
	private Rect m_currentClickedStringRect;
	
	private string m_currentClickedString = "";
	
	private int m_currentIndex = -1;
	
	private bool b_dragging = false;
	
	public Texture2D m_tFillFieldBackground;
	public Texture2D m_tDestinationBackground;
	public Texture2D m_tConversationBackground;
	
	// Use this for initialization
	void Start () 
	{
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
		Global.levelStarted = false;
		m_myOriRect = new Rect[] 
		{	
			new Rect(680.0f, 100.0f, m_KeywordDimensions[0].x, m_KeywordDimensions[0].y), // Violent
			new Rect(680.0f, 150.0f, m_KeywordDimensions[1].x, m_KeywordDimensions[1].y), // 'Sore' heart
			new Rect(680.0f, 205.0f, m_KeywordDimensions[2].x, m_KeywordDimensions[2].y), // No Energy
			new Rect(680.0f, 260.0f, m_KeywordDimensions[3].x, m_KeywordDimensions[3].y), // Shaky
			new Rect(680.0f, 315.0f, m_KeywordDimensions[4].x, m_KeywordDimensions[4].y), // Heavy
			new Rect(680.0f, 370.0f, m_KeywordDimensions[5].x, m_KeywordDimensions[5].y), // Numb
			new Rect(680.0f, 435.0f, m_KeywordDimensions[6].x, m_KeywordDimensions[6].y), // Violent
			new Rect(180.0f, 100.0f, m_KeywordDimensions[7].x, m_KeywordDimensions[7].y), // Loneliness
			new Rect(180.0f, 150.0f, m_KeywordDimensions[8].x, m_KeywordDimensions[8].y), // Headache
			new Rect(180.0f, 205.0f, m_KeywordDimensions[9].x, m_KeywordDimensions[9].y), // Selfharm
			new Rect(180.0f, 260.0f, m_KeywordDimensions[10].x, m_KeywordDimensions[10].y), // Sick
			new Rect(180.0f, 315.0f, m_KeywordDimensions[11].x, m_KeywordDimensions[11].y), // Cry
			new Rect(180.0f, 370.0f, m_KeywordDimensions[12].x, m_KeywordDimensions[12].y), // Deflated
			new Rect(180.0f, 435.0f, m_KeywordDimensions[13].x, m_KeywordDimensions[13].y), // Loneliness

		};
		
		m_movableStringRect = new Rect[m_myOriRect.Length];
		for (int i = 0; i < m_myOriRect.Length; ++i)
		{
			m_movableStringRect[i] = m_myOriRect[i];
		}
		
		m_currentClickedStringRect = k_zeroRect;
		
		//m_tFillFieldBackground     = (Texture2D)Resources.Load ("UI/edittext", typeof(Texture2D));
		//m_tDestinationBackground   = (Texture2D)Resources.Load ("UI/talk_paper", typeof(Texture2D));
		//m_tConversationBackground  = (Texture2D)Resources.Load ("UI/talk_paper", typeof(Texture2D));
	}
	
	// Update is called once per frame
	void Update () 
	{
		//The default conversation box text
		m_conversationBoxText = WordsHoverOverText("");
		
		for (int i = 0; i < m_myOriRect.Length; ++i)
		{
			if(m_myOriRect[i].Contains(new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{
		         m_conversationBoxText = WordsHoverOverText(m_keyWordText[i]);
			}
		}
		
		if (b_dragging)
		{
			// check to see if the moveable rect is found
			if (m_movableStringRect[m_currentIndex] == m_currentClickedStringRect)
			{
				// save the mouse pos for ease of access
				float mouseX = Input.mousePosition.x;
				float mouseY = Input.mousePosition.y;
				m_movableStringRect[m_currentIndex].Set(mouseX - m_KeywordDimensions[m_currentIndex].x/2, 
												   mouseY * (-1) + Screen.height - m_KeywordDimensions[m_currentIndex].y/2, 
												   m_KeywordDimensions[m_currentIndex].x, 
												   m_KeywordDimensions[m_currentIndex].y);
				
				m_currentClickedStringRect = m_movableStringRect[m_currentIndex];
			}
		}
		else
		{
			KeyWordDrag();
		}
		
		DestinationDrop();
	}
	
	public void OnGUI () 
	{		
		GUI.skin = m_skin;
		GUI.depth = -1;
		
		// the background of the mingame
		GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
		
		// The middle area
		GUI.Label(new Rect(330, 120, 300, 300), "", "SmallBoarderPanel");
		GUI.Label(new Rect(345, 145, 270, 40), "I can spot feeling angry when:", "Titles");
		bool done = false;
		for (int i = 0; i < m_destinationRect.Length; ++i)
		{
			GUI.Label(m_destinationRect[i], m_tFillFieldBackground);
			GUI.Label(m_destinationOriKeyWordRect[i], m_destinationText[i], "CenteredFont");
			//GUI.Box(m_destinationRect[i], m_tFillFieldBackground);
			//GUI.Box(m_destinationOriKeyWordRect[i], m_destinationText[i]);
			
			if (m_destinationText[i] != "")
			{
				done = true;
			}
		}
		
		// the circle of words
		for (int i = 0; i < m_myOriRect.Length; ++i)
		{
			GUI.Label(m_myOriRect[i], m_keyWordText[i], "CenteredFont");
			//GUI.Box(m_myOriRect[i], m_keyWordText[i], "CenteredFont");
		}
		
		// The current movable keyword box and text
		if (m_currentClickedString != "")
		{
			GUI.Label(m_currentClickedStringRect, m_currentClickedString, "CenteredFont");
			//GUI.Box(m_currentClickedStringRect, m_currentClickedString);
		}
		
		//Conversation box
		GUI.Label(new Rect(265,470,620,135), m_tConversationBackground);
		GUI.Label(new Rect(275.0f, 497.0f, 400.0f, 80.0f), m_conversationBoxText, "CenteredFont");
		
		//The next button
		if (true == done)
		{
			if(GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
					Global.arrowKey = "";

				this.enabled = false;
				
				GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;
				GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentScene = "L3GuideScene17";
			}
		}
	}
	
	/*
	 * 
	 * Thie function drag the keyword and move with the mouse.
	 * 
	*/
	public void KeyWordDrag()
	{
		// release early if there is no active keyword box being moved.
		if (m_currentIndex != -1) { return ;}
		
		//If the mouse button is clicked and if the mouse position is on the keywords
		if(Input.GetMouseButton(0))
		{
			// save the mouse pos for ease of access
			float mouseX = Input.mousePosition.x;
			float mouseY = Input.mousePosition.y;
			
			// boolean to see if a box has been clicked on.
			bool clicked = false;
			
			// loop through all the original rects to see if the user has clicked on one of the boxes.
			for (int i = 0; i < m_myOriRect.Length; ++i)
			{
				if (m_movableStringRect[i].Contains(new Vector2(mouseX, Screen.height - mouseY)))
				{
					// set the current active key text
					// check if the current text has been set
					if (m_currentClickedString == "")
					{
						// if the keyword does not exist that means that it is in one of the destination boxes
						if (m_keyWordText[i] == "")
						{
							// loop through the destination boxes to find which contains the key word
							for (int j = 0; j < m_destinationOriKeyWordRect.Length; ++j)
							{
								// set the min and max of the box
								Vector2 min = new Vector2(m_movableStringRect[i].xMin, m_movableStringRect[i].yMin);
								Vector2 max = new Vector2(m_movableStringRect[i].xMax, m_movableStringRect[i].yMax);
								
								// check if this destination box contains the current movable box
								if(m_destinationOriKeyWordRect[j].Contains(min) &&
									m_destinationOriKeyWordRect[j].Contains(max))
								{
									// set the destination text and remove the clicked text.
									m_keyWordText[i] = m_destinationText[j];
									m_destinationText[j] = "";
									break;
								}
							}
						}
						
						m_currentClickedString = m_keyWordText[i];
						m_keyWordText[i] = "";
						m_currentIndex = i;
						clicked = true;
						b_dragging = true;
						m_currentClickedStringRect = m_movableStringRect[m_currentIndex];
						break;
					}
				}
			}	
			
			// this is an early release from the function
			if (clicked == true) { return ;}
		}
	}
	
	/*
	 * 
	 * Thie function drop the keyword to the destination area.
	 * 
	*/
	public void DestinationDrop()
	{
		// release early if there is no active keyword box being moved.
		if (m_currentIndex == -1) { return ;}
		
		//If the mouse button is UnClicked and if  the mouse position is on the fill field
		if(Input.GetMouseButtonUp(0))
		{
			b_dragging = false;
			
			// boolean for if the text was placed in the box.
			bool successful = false;
			
			// loop through the destination rects to see which box the word is being placed in
			for (int i = 0; i < m_destinationOriKeyWordRect.Length; ++i)
			{
				// save the mouse pos for ease of access
				float mouseX = Input.mousePosition.x;
				float mouseY = Input.mousePosition.y;
				
				// check if this destination box contains the current clicked keyword box
				if(m_destinationOriKeyWordRect[i].Contains(new Vector2(mouseX, Screen.height - mouseY)))
				{
					// if the destination text is empty
					if(m_destinationText[i] == "")
					{
						// set the destination text and remove the clicked text.
						m_destinationText[i] = m_currentClickedString;
						m_currentClickedString = "";
						successful = true;
						
						float width = m_dextinationRectWidth - 2;
						float height = m_dextinationRectHeight - 2;
						m_movableStringRect[m_currentIndex].Set(
							m_destinationOriKeyWordRect[i].center.x - width/2, 
							m_destinationOriKeyWordRect[i].center.y - height/2, 
							width, 
							height);

						m_currentClickedStringRect = k_zeroRect;
						m_currentIndex = -1;
					}
				}
			}
			
			// another early release because everything is done already
			if (successful) { return ;}
			
			// execute this code if the user missed the destination box.
			ResetCurrentString();
		}
	}
	
	/*
	 * 
	 * This function resets the missed keyword text and text rect back to their original positions
	 * 
	 */
	private void ResetCurrentString()
	{
		// set the movable rect to the original area, set the keyword back to the current,
		// reset the current keyword and break the loop.
		m_movableStringRect[m_currentIndex] = m_myOriRect[m_currentIndex];
		m_keyWordText[m_currentIndex] = m_currentClickedString;
		m_currentClickedStringRect = k_zeroRect;
		m_currentClickedString = "";
		m_currentIndex = -1;
	}
	
	public string WordsHoverOverText(string _keyWord)
	{
		string keuWordExplaination = "";
			
		switch(_keyWord)
		{
			case "Headache":
				keuWordExplaination = "I get a headache.";
				break;
			case "Butterflies":
				keuWordExplaination = "I have butterflies in my stomach.";
				break;
			case "Bite my nails":
				keuWordExplaination = "I bite my nails.";
				break;
			case "Tensed shoulders":
				keuWordExplaination = "I tense my shoulders.";
				break;
			case "Clenched jaw":
				keuWordExplaination = "I get a clenched jaw.";
				break;
			case "Shaky":
				keuWordExplaination = "I get shaky.";
				break;
			case "Leave":
				keuWordExplaination = "I want to leave.";
				break;
			case "Heart racing":
				keuWordExplaination = "My heart is racing.";
				break;
			case "Red face":
				keuWordExplaination = "I have a red face.";
				break;
			case "Short of breath":
				keuWordExplaination = "I am short of breath.";
				break;
			case "Fight":
				keuWordExplaination = "I want to fight.";
				break;
			case "Weak knees":
				keuWordExplaination = "I am weak at the knees.";
				break;
			case "Crying":
				keuWordExplaination = "I start crying.";
				break;
			case "Stomach-ache":
				keuWordExplaination = "I get a stomach-ache.";
				break;
			case "Yell/Swear":
				keuWordExplaination = "I yell/swear.";
				break;
			case "Blood pumping":
				keuWordExplaination = "My blood is pumping.";
				break;
			default:
			    keuWordExplaination = "Drag and drop all the ways you can SPOT\nfeeling angry.";
				break;
		}
			
		return keuWordExplaination;
	}
}
