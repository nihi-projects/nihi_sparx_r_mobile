using UnityEngine;
using System.Collections;

public class L2RelaxYourMuscles : MonoBehaviour 
{
	
	// publics
	public GUISkin m_skin = null;
	
	public Texture2D[] m_tMuscles;
	
	// privates
	private float m_fTimer = 0.0f;
	private string[] m_strNames = 
	{
		"RELAX YOUR MUSCLES",
		"Arms",
		"Legs", 
		"Back and Chest"
	};
	private int m_iCurrentImage = 0;
	
	// Use this for initialization
	void Start () 
	{
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	
	/*
	*
	* OnGUI Function.
	*
	*/
	public void OnGUI () 
	{		
		GUI.skin = m_skin;
		GUI.depth = -1;
		
		GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
		GUI.Label(new Rect(180.0f, 75.0f, 600.0f, 50.0f), m_strNames[m_iCurrentImage], "Titles");
		
		UpdateTexture();
		GUI.Label(new Rect(170.0f, 70.0f, 600.0f, 500.0f), m_tMuscles[m_iCurrentImage]);
		
		if(m_fTimer >= 12.0f)
		{
			if(GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
					Global.arrowKey = "";

				this.enabled = false;
				
				GameObject.Find("GUI").GetComponent<TalkScenes>().enabled = true;
				GameObject.Find("GUI").GetComponent<TalkScenes>().m_currentScene = "L2GuideScene32";
			}
		}
	}
	
	/*
	*
	* OnEnable Function, this function will be called everytime when the script is enabled.
	*
	*/
	public void UpdateTexture()
	{
		m_fTimer += Time.deltaTime;
		
		if (m_iCurrentImage < 3 && m_fTimer >= 3.0f * (float)(m_iCurrentImage + 1))
		{
			++m_iCurrentImage;
		}
	}
}
