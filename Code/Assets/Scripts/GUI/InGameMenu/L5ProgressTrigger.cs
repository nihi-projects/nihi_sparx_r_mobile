﻿using UnityEngine;
using System.Collections;

public class L5ProgressTrigger : MonoBehaviour {

	bool startTalk = false;
	public GUISkin m_skin = null;
	Vector3 tempPos;

	// Use this for initialization
	void Start () {

		tempPos.y = -1.5f;
		GameObject.Find ("5_barrels").GetComponent<Transform>().position += tempPos;
		GameObject.Find ("BarrelGame").GetComponent<Transform>().position += tempPos;

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other){
		startTalk = true;
	}

	void OnTriggerExit(Collider other) {
		startTalk = false;
	}

	void OnGUI(){
		GUI.depth = -1;
		GUI.skin = m_skin;
		if(startTalk){
			GUI.Label(new Rect(0,Screen.height*0.63f,Screen.width,Screen.height*0.3f), "", "NormalSizeDilogBox");
			GUI.Label(new Rect(Screen.width*0.16f,Screen.height*0.72f,700,100), "I can't go this way yet. The Innkeeper might be able to help.");
		}
	}
}
