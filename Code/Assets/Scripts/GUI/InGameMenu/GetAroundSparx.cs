using UnityEngine;
using System.Collections;

public class GetAroundSparx : MonoBehaviour {

	public GUISkin m_skin = null;
	public GameObject m_theGuardian;
	public GameObject m_theMontor;
	
	//Small icon images in the dialog box
	public Texture2D m_mouseCorsorImage;
	public Texture2D m_closeImage;
	public Texture2D m_backPackImage;
	public Texture2D m_sparxManulImage;
	public Texture2D loadingIcon;
	
	private GameObject m_gardianClone;
	private GameObject m_mentorClone;
	
	private TalkScenes m_talkScene;
	
	public GameObject m_gGardianObject = null;
	//private float fTime 		  = 0.0f;
	
	// Use this for initialization
	void Start () 
	{
		//m_mouseCorsorImage = (Texture2D)Resources.Load ("UI/corsor_1", typeof(Texture2D));
		m_closeImage       = (Texture2D)Resources.Load ("UI/exit", typeof(Texture2D));
		//m_backPackImage    = (Texture2D)Resources.Load ("UI/backpack", typeof(Texture2D));
		//m_sparxManulImage  = (Texture2D)Resources.Load ("UI/sbook_icon", typeof(Texture2D));
	}
	
	// Update is called once per frame
	void Update () 
	{
		GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled = false;

		Global.GetPlayer().GetComponent<PlayerMovement>().m_bMovement = false;
		Global.GetPlayer().GetComponent<PlayerMovement>().m_bMovingToPosition = false;

		GameObject.Find("Main Camera").GetComponent<CameraMovement>().m_bCameraLookAt = false;
		GameObject.Find("Main Camera").GetComponent<CameraMovement>().m_bFollow = true;

	}
	
	void OnEnable(){

		m_gGardianObject = GameObject.Find ("guardian") as GameObject;
		
		//Other levels does not have the guardian object
		if(Global.CurrentLevelNumber != 1 || Global.LevelSVisited == true){
			m_gGardianObject.SetActive(false);
			this.enabled = false;
		}
	}
	
	/*
	*
	* OnGUI function
	*
	*//*
	void OnGUI()
	{
		if(Global.CurrentLevelNumber == 1){
			GUI.depth = -1;
			GUI.skin = m_skin;
	
			GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
			GUI.Label(new Rect(0.0f, 80.0f, 960.0f, 40.0f), "GETTING AROUND SPARX", "Titles");
			
			GUI.Label(new Rect(150.0f, 135.0f, 36.0f, 36.0f), m_mouseCorsorImage);
			GUI.Label(new Rect(200.0f, 135.0f, 655.0f, 150.0f), "If a rainbow cursor appears, click on the object to see what happens.");
			GUI.Label(new Rect(150.0f, 185.0f, 705.0f, 150.0f), "If you are lost, explore the environment to see if this cursor appears.");
			GUI.Label(new Rect(150.0f, 245.0f, 705.0f, 150.0f), "Use the Arrow keys or WASD keys to walk around.");
			GUI.Label(new Rect(150.0f, 300.0f, 705.0f, 150.0f), "If you want to run, hold the shift button while walking.");
			
			//GUI.Label(new Rect(130.0f, 260.0f, 36.0f, 36.0f), m_closeImage);
			//GUI.Label(new Rect(180.0f, 260.0f, 655.0f, 150.0f), "If you want to leave at any time you can press this key on the keyboard or the exit icon in the top right corner of the screen. When you come back you will start you from the beginning of that level again.");
			
			GUI.Label(new Rect(150.0f, 355.0f, 36.0f, 36.0f), m_backPackImage);
			GUI.Label(new Rect(200.0f, 355.0f, 655.0f, 150.0f), "Your backpack has these instructions and other useful things like your Notebook.");
			
			GUI.Label(new Rect(150.0f, 425.0f, 36.0f, 36.0f), m_sparxManulImage);
			GUI.Label(new Rect(200.0f, 425.0f, 655.0f, 150.0f), "Your Notebook is for you to use. It has summaries of each Level and things you write in it.");

			
			GUI.Label(new Rect(150.0f, 480.0f, 655.0f, 150.0f), "You can use the Left and Right Arrow keys to move forward and back in conversations.");
			GUI.Label(new Rect(150.0f, 525.0f, 655.0f, 150.0f), "You can also use A,B,C,D or 1,2,3,4 for multichoice questions.");


			GUI.Label(new Rect(150.0f, 560.0f, 80.0f, 80.0f), loadingIcon);
			GUI.Label(new Rect(210.0f, 585.0f, 655.0f, 150.0f), "This symbol indicates the game is auto-saving your progress.");
			
			//Next button
			if(Time.timeSinceLevelLoad > 2f)
			{
				if(GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
					Global.arrowKey = "";

					Global.GetPlayer().GetComponent<PlayerMovement>().m_bMovement = true;
					GameObject.Find ("guardian").GetComponent<CharacterInteraction>().InteractionRadius = 6;
					this.enabled = false;
				}
			}
			/*else
			{
				fTime += Time.deltaTime;
			}*/
	/*	}
		else{
			this.enabled = false;
		}
	}*/

		void OnGUI()
		{
			if(Global.CurrentLevelNumber == 1){
				GUI.depth = -1;
				GUI.skin = m_skin;

				GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), "", "FullSizeDialogBox");
				GUI.Label(new Rect(0.0f, 80.0f, 960.0f, 40.0f), "GETTING AROUND SPARX", "Titles");

				GUI.Label(new Rect(150.0f, 120.0f, 36.0f, 36.0f), m_mouseCorsorImage);
				GUI.Label(new Rect(200.0f, 130.0f, 655.0f, 150.0f), "If a rainbow cursor appears, click on the object to see what happens.");
				GUI.Label(new Rect(150.0f, 160.0f, 705.0f, 150.0f), "If you are lost, explore the environment to see if this cursor appears.");
				GUI.Label(new Rect(150.0f, 200.0f, 705.0f, 150.0f), "Use the Arrow keys or WASD keys to walk around.");
				GUI.Label(new Rect(150.0f, 240.0f, 705.0f, 150.0f), "If you want to run, hold the shift button while walking.");

				//GUI.Label(new Rect(130.0f, 260.0f, 36.0f, 36.0f), m_closeImage);
				//GUI.Label(new Rect(180.0f, 260.0f, 655.0f, 150.0f), "If you want to leave at any time you can press this key on the keyboard or the exit icon in the top right corner of the screen. When you come back you will start you from the beginning of that level again.");

				GUI.Label(new Rect(150.0f, 270.0f, 36.0f, 36.0f), m_backPackImage);
				GUI.Label(new Rect(200.0f, 280.0f, 655.0f, 150.0f), "Your backpack has these instructions and other useful things like your Notebook.");

				GUI.Label(new Rect(150.0f, 320.0f, 36.0f, 36.0f), m_sparxManulImage);
				GUI.Label(new Rect(200.0f, 330.0f, 655.0f, 150.0f), "Your Notebook is for you to use. It has summaries of each Level and things you write in it.");


				GUI.Label(new Rect(150.0f, 370.0f, 655.0f, 150.0f), "You can use the Left and Right Arrow keys to move forward and back in conversations.");
				GUI.Label(new Rect(150.0f, 410.0f, 655.0f, 150.0f), "You can also use A,B,C,D or 1,2,3,4 for multichoice questions.");


				GUI.Label(new Rect(150.0f, 430.0f, 80.0f, 80.0f), loadingIcon);
				GUI.Label(new Rect(210.0f, 450.0f, 655.0f, 150.0f), "This symbol indicates the game is auto-saving your progress.");

				//Next button
				if(Time.timeSinceLevelLoad > 2f)
				{
					if(GUI.Button(new Rect(Screen.width * 0.82f, Screen.height * 0.7813f, Screen.width*0.0586f, Screen.height*0.0781f), "", "dilogBoxNextButton") || Global.arrowKey == "right"){
						Global.arrowKey = "";

						Global.GetPlayer().GetComponent<PlayerMovement>().m_bMovement = true;
						GameObject.Find ("guardian").GetComponent<CharacterInteraction>().InteractionRadius = 6;
						this.enabled = false;
					}
				}
				/*else
			{
				fTime += Time.deltaTime;
			}*/
			}
			else{
				this.enabled = false;
			}
		}





}
