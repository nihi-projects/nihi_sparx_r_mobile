using UnityEngine;
using System.Collections;

public enum GnatsAction
{
	MOVING,
	CIRCLING,
	MAX_ACTION,
};

public class GnatsInteraction : MonoBehaviour {
	public int IndexOfAction = 0;
	public GnatsAction[] Actions;
	public Transform[] TriggerPoints;
	public string CameraID;
	// Use this for initialization
	void Start () {
		if(Actions.GetLength(0) != TriggerPoints.GetLength(0))
		{
			Debug.LogError("Actions should match the trigger points!!!");
		}
		GetComponent<GnatsCombat>().Gnats = GetComponent<GnatsMovement>().Gnats;
		GetComponent<GnatsCombat>().CombatTargets = GetComponent<GnatsMovement>().BattleFormation;
	}
	
	// Update is called once per frame
	void Update () {
		if(Actions.Length == 0)
		{
			return;
		}
		switch(Actions[IndexOfAction])
		{
		case GnatsAction.MOVING:
			CheckMovement();
			break;
		case GnatsAction.CIRCLING:
			CheckAttack();
			break;
		default:break;
		}
	}
	
	void SetToNextAction(){
		if(IndexOfAction < Actions.Length -1)
		{
			IndexOfAction += 1;
		}
		if(Actions[IndexOfAction] == GnatsAction.CIRCLING)
		{
			GetComponent<GnatsCombat>().enabled =true;
		}
		if(Actions[IndexOfAction] == GnatsAction.MOVING)
		{
			GetComponent<GnatsMovement>().enabled = true;
		}
	}
	
	void CheckMovement()
	{
		if(!GetComponent<GnatsMovement>().IsTriggered())
		{
			// Check triggering it
			if(CheckTrigger())
			{
				GetComponent<GnatsMovement>().SetTriggered();
			}
			else
			{
				// if movement is already triggered,
				// check if finished moving
				if(GetComponent<GnatsMovement>().IsMovementComplete())
				{
					// Start to prepare for the next action
					SetToNextAction();
				}
			}
		}
	}
	
	void CheckAttack()
	{
		if(CheckTrigger() && !GetComponent<GnatsCombat>().IsTriggered()
			&& !GetComponent<GnatsCombat>().bCombatEnded)
		{
			// start attack
			GetComponent<GnatsCombat>().SetTriggered();
			
			// Stops the Gnats movement
			GetComponent<GnatsMovement>().enabled = false;
			
			// stops movement.
			Global.GetPlayer ().GetComponent<PlayerMovement>().m_bMovement = false;
			// set the camera.
			Global.MoveCameraBasedOnID(CameraID);
			
			// player animation.
			Global.GetPlayer ().GetComponent<Animation>().Play ("aim idle");
			// fireball
			Global.GetPlayerEffect().SetFireBallActive(true);
			
			Vector3 look = Global.GetPlayer().transform.position - GameObject.Find("Level1_Chest").transform.position;
			Global.GetPlayer().transform.rotation = Quaternion.LookRotation(new Vector3(look.x, Global.GetPlayer().transform.position.y, look.z));
		}
	}
	
	bool CheckTrigger()
	{
		if(Vector3.Distance(Global.GetPlayer().transform.position, TriggerPoints[IndexOfAction].position) < 2.5f)
		{
			return(true);
		}
		return(false);
	}
}
