using UnityEngine;
using System.Collections;

public class FireFountainRoutine : MonoBehaviour {
	
	// all the fountains 
	GameObject[] Fountains;
	
	// The timer for each fountain, determine how long they stay 'alive'
	float[] OnTime;
	
	// The timer for each fountain, determine how long they stay 'dead'
	float[] OffTime;
	
	public float fMin;
	public float fMax;
	// Use this for initialization
	void Start () {
		
		Fountains = new GameObject[transform.childCount];
		OnTime = new float[transform.childCount];
		OffTime = new float[transform.childCount];
		for(int i = 0; i < transform.childCount; ++i)
		{
			Fountains[i] = transform.GetChild(i).gameObject;
			OnTime[i] = Random.Range (fMin, fMax);
			OffTime[i] = -1;
		}
	}
	
	// Update is called once per frame
	void Update () {
		for(int i = 0; i < Fountains.Length; ++i)
		{
			if(OffTime[i] == -1)
			{
				// this means currently we are timing the 
				// activeness
				OnTime[i] -= Time.deltaTime;
				if(OnTime[i] <= 0.0f)
				{
					OnTime[i] = -1;
					OffTime[i] = Random.Range (2.0f, 4.0f);
					Fountains[i].GetComponent<ParticleSystem>().enableEmission = false;
					Fountains[i].GetComponent<Collider>().enabled = false;
				}
			}
			if(OnTime[i] == -1)
			{
				OffTime[i] -= Time.deltaTime;
				if(OffTime[i] <= 0.0f)
				{
					OffTime[i] = -1;
					OnTime[i] = Random.Range (fMin, fMax);
					Fountains[i].GetComponent<ParticleSystem>().enableEmission = true;
					Fountains[i].GetComponent<Collider>().enabled = true;
				}
			}
		}
		
	}
}
