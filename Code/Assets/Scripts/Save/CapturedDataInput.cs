using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using Boomlagoon.JSON;
using System.Collections.Generic;

public class CapturedDataInput : MonoBehaviour {

	private static CapturedDataInput instanceRef;

	//public variables
	public string m_sGameLoginToken   = "";
	public string m_sGameDynamicToken = "";
	//Forth version of networking
	//public static string m_sDynamicURL       = "http://sparx.cre072.co.nz/gameapi/";
	//public static string m_sDynamicURL       = "https://www.sparx.org.nz/gameapi/";
	//public static string m_sDynamicURL       = "http://192.168.2.22/sparx/01/"; // FOR EDITOR TESTING ONLY
	//public static string m_sDynamicURL       = "https://sparxapi.nihi.auckland.ac.nz/apiv1/sparxer/jstr069/"; //CHANGES_KEY_NEW_JSON_SCHEMA [THIS URL TO CALL THE CONFIGURATION METHOD TO GET THE DYNAMIC URL]
	//public static string m_sDynamicURL       = "https://sparxapi.nihi.auckland.ac.nz/apiv3/game/"; //CHANGES_KEY_NEW_JSON_SCHEMA [THIS URL TO CALL THE CONFIGURATION METHOD TO GET THE DYNAMIC URL]

	public string m_sDynamicURL   = "";

	public string m_sDynamicURLTest   = "";

	public int m_iServerLevelNum      = 0;
	public string m_sSaveProgressURL  = "";
	public string m_sReadProgressURL  = "";
	
	public WWW m_logInDataRetriever;
	public WWW m_dynamiURLRetriever;
	
	//Progress
	public WWW m_ProcessDataPostRetriever;
	public WWW m_ProcessDataGetRetriever;
	public WWW m_ProcessNotebookDataGetRetriever;
	//Level
	public WWW m_LevelDataPostRetriever;
	public WWW m_LevelDataGetRetriever;
	//Event
	public WWW m_EventLevelStartEndDataPostRetriever;
	public WWW m_EventPHQADataPostRetriever;

	public WWW m_sLoadURLReturn;
	public WWW m_sSaveURLReturn;

	public bool m_bFoundLevelNumber            = false;
	
	//private variables
	private bool m_bLoggedInConfirmed          = false;
	private bool m_dynamicLoginSuccessful      = false;
	private bool m_loginSuccessful             = false;
	private string m_sNotebookHeader           = "";
	
	
	//Implementation
	//private static CapturedDataInput instance = new CapturedDataInput();
	
	private CapturedDataInput() {}
	
	/*public static CapturedDataInput GetInstance
	{
		get{
			return (instance);
		}
	}*/
	
	void Awake()
	{
		//if (Application.isEditor){
		//	m_sDynamicURL   =  "http://192.168.2.22/sparx/01/";
		//}
		//DontDestroyOnLoad(gameObject);//FIX DUPLICATE AND INDISTICT KILLING OBJECTS
		if (instanceRef == null) {
			instanceRef = this;
			DontDestroyOnLoad (gameObject);
		}else{
			DestroyImmediate (gameObject);
		}

	}
	
	// Use this for initialization
	void Start () 
	{

	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
	
	//--------------------------------------------------------Server Login--------------------------------------------------

	/*//CHANGES_KEY_NEW_JSON_SCHEMA 
	public IEnumerator GetUserLogInToServer()
	{		
		if(Application.isEditor){
			string email = "testmetia";
			string password = "testmetia";
			string urlLogin = m_sDynamicURL + "login?jsondata=";
			string urlCredentials = WWW.EscapeURL("{\"email\":\""+ email + "\",\"password\":\"" + password + "\"}");
			//URL for Log in
			string m_sLoginURL = urlLogin + urlCredentials;
			//Connect to the server and log into the server
			WWWForm form = new WWWForm();
			form.AddField("fake", "xy");
			m_logInDataRetriever = new WWW(m_sLoginURL, form);
			Debug.Log ("The log in url is = " + m_sLoginURL);
			Debug.Log ("The log in url is = " + m_logInDataRetriever.url);
			//Debug.Log("WWW Error: " + m_logInDataRetriever.error);
			yield return m_logInDataRetriever;
		}else{
			string email = "hch67";
			string password = "2776956";
			string urlLogin = m_sDynamicURL + "login?jsondata=";
			string urlCredentials = WWW.EscapeURL("{\"email\":\""+ email + "\",\"password\":\"" + password + "\"}");
			//URL for Log in
			string m_sLoginURL = urlLogin + urlCredentials;
			//Connect to the server and log into the server
			WWWForm form = new WWWForm();
			m_logInDataRetriever = new WWW(m_sLoginURL, form);
			Debug.Log ("The log in url is = " + m_sLoginURL);
			
			yield return m_logInDataRetriever;
		}
	}*///CHANGES_KEY_NEW_JSON_SCHEMA 

	public IEnumerator GetUserLogInToServer_NewSchema() //IS JUST CALLED BY EDITOR
	{		
		//string email = "hch67";
		//string password = "2776956";
		//https://sparxapi.nihi.auckland.ac.nz/apiv1/sparxer/jstr069/getToken?apiKey=b86d44affef10fdf566d4b488aecef809b7684932bf073e28d6a3741d326f320bbfd1216aef54992466ec72d31bd102f5fc09633c17b4aefaf382ca272d37a2a
		//string urlLogin = "https://sparxapi.nihi.auckland.ac.nz/apiv1/sparxer/jstr069/" + "getToken?apiKey=b86d44affef10fdf566d4b488aecef809b7684932bf073e28d6a3741d326f320bbfd1216aef54992466ec72d31bd102f5fc09633c17b4aefaf382ca272d37a2a";
		string urlLogin = "http://testmeany.nihi.auckland.ac.nz:3001/apiv1/sparxer/diego/getToken?apiKey=f5ea74aee87ecd3d52d8c0d52e2594f3b500c0abf692c17263003532d0827c207d3c96a3f3e2e17c91a0752ee385d0960343b5d69fa4604de41ffbdda62a8ef7";
		//string urlLogin = "https://sparxapi.nihi.auckland.ac.nz/apiv3/game/config?sid=8239f3bf16a8eab5d17fe0550f85fbbb";	
		//string urlCredentials = WWW.EscapeURL("{\"email\":\""+ email + "\",\"password\":\"" + password + "\"}");
			//URL for Log in
			string m_sLoginURL = urlLogin + m_sGameDynamicToken;
			//Connect to the server and log into the server
			//WWWForm form = new WWWForm();
			//form.AddField("sid", "8239f3bf16a8eab5d17fe0550f85fbbb");
			m_logInDataRetriever = new WWW(m_sLoginURL);
			Debug.Log ("The Configuration url is = " + m_sLoginURL);

			yield return m_logInDataRetriever;
		if (m_logInDataRetriever.isDone) {
			Debug.Log ("<Login for Editor>:" + m_logInDataRetriever.text);
		}
	}
	/*//CHANGES_KEY_NEW_JSON_SCHEMA 
	public void GetTokenAfterLogin()
	{
		//Debug.Log ("THIS IS BEING CALLED (DEBUG CALL @ LINE 119)");
		if(Application.isEditor){
			if(!m_bLoggedInConfirmed){
				if(m_logInDataRetriever.isDone){
					m_sGameLoginToken = m_logInDataRetriever.text;
					Debug.Log ("Log in data retriver: " + m_logInDataRetriever.text);
					//For Unity Editor testing only, this token will not be used in web player build
					//m_sGameLoginToken = "c92fb78b7d03b6e58ad88cf95ee78cb7";
					m_loginSuccessful = m_sGameLoginToken.Contains("\"code\":200");
					if(m_loginSuccessful){
						Debug.Log ("Server login is successful...");
						m_bLoggedInConfirmed = true;
						string trim1, trim2;
						trim1 = "{\"code\":200,\"data\":{\"token\":\"";
						trim2 = "\"}}";
						//Get the taken after the successful login
						m_sGameLoginToken = m_sGameLoginToken.Remove (0, trim1.Length);
						m_sGameLoginToken = m_sGameLoginToken.TrimEnd(trim2.ToCharArray());
						Debug.Log ("Game Login Token: " + m_sGameLoginToken);
						m_sGameDynamicToken = m_sGameLoginToken; // For testing only. Remove later.
					}
					else{
						//m_sGameLoginToken = "c92fb78b7d03b6e58ad88cf95ee78cb7";// For testing only. Remove later.
						Debug.Log ("Server login is unsuccessful...");
					}
				}
			}
		}else{
			if(!m_bLoggedInConfirmed){
				if(m_logInDataRetriever.isDone){
					m_sGameLoginToken = m_logInDataRetriever.text;
					//For Unity Editor testing only, this token will not be used in web player build
					m_loginSuccessful = m_sGameLoginToken.Contains("\"code\":200");
					if(m_loginSuccessful){
						Debug.Log ("Server login is successful...");
						m_bLoggedInConfirmed = true;
						string trim1, trim2;
						trim1 = "{\"code\":200,\"data\":{\"token\":\"";
						trim2 = "\"}}";
						//Get the taken after the successful login
						m_sGameLoginToken = m_sGameLoginToken.Remove (0, trim1.Length);
						m_sGameLoginToken = m_sGameLoginToken.TrimEnd(trim2.ToCharArray());
					}
					else{
						Debug.Log ("Server login is unsuccessful...");
					}
				}

				m_sGameLoginToken = "4d5dac7301188267e7efa157a2f0e38d";
			}
		}
	}
*///CHANGES_KEY_NEW_JSON_SCHEMA 
	public void GetTokenAfterLogin_NewJsonSchema() //IS JUST CALLED WHEN EDITOR
	{
		//Debug.Log ("THIS IS BEING CALLED (DEBUG CALL @ LINE 119)");

		if(!m_bLoggedInConfirmed){
			if(m_logInDataRetriever.isDone){
				m_sGameLoginToken = m_logInDataRetriever.text;
				Debug.Log ("Log in data retriver: " + m_logInDataRetriever.text);
				//For Unity Editor testing only, this token will not be used in web player build
				//m_sGameLoginToken = "c92fb78b7d03b6e58ad88cf95ee78cb7";

				//----------------------------------------------------------------------------------------------------------
				//----------------------------------------------------------------------------------------------------------

				string tempNum;
				string tempFloat;
				string tempString;
				string[] tempStringArray;
				JSONValue tempVal;
				JSONArray tempArray;
				JSONObject json = new JSONObject();
				json = JSONObject.Parse(m_sGameLoginToken);
				m_sGameLoginToken = json.GetObject ("token").GetString ("token");

					//----------------------------------------------------------------------------------------------------------
					//----------------------------------------------------------------------------------------------------------

				if (m_sGameLoginToken.Length == 32) {
					m_loginSuccessful = true;//FIXED TEST LOGIN
				}else{
					m_loginSuccessful = false;//FIXED TEST LOGIN
				}
				if(m_loginSuccessful){
					Debug.Log ("Server login is successful...");
					m_bLoggedInConfirmed = true;
					//string trim1, trim2;
					//trim1 = "{\"code\":200,\"data\":{\"token\":\"";
					//trim2 = "\"}}";
					//Get the taken after the successful login
					//m_sGameLoginToken = m_sGameLoginToken.Remove (0, trim1.Length);
					//m_sGameLoginToken = m_sGameLoginToken.TrimEnd(trim2.ToCharArray());
					Debug.Log ("Game Login Token: " + m_sGameLoginToken);
					m_sGameDynamicToken = m_sGameLoginToken; // For testing only. Remove later.
				}
				else{
					//m_sGameLoginToken = "c92fb78b7d03b6e58ad88cf95ee78cb7";// For testing only. Remove later.
					Debug.Log ("Server login is unsuccessful...");
				}
			}
		}

	}

	//--------------------------------------------------------Get Dynamic URL--------------------------------------------------
	/*//CHANGES_KEY_NEW_JSON_SCHEMA 
	public IEnumerator SendOffGameConfigURL(string _url)
	{
		WWWForm dynamicUrlForm = new WWWForm();
		m_dynamiURLRetriever = new WWW(_url, dynamicUrlForm);	
		
		Debug.Log ("The web page url is = " + _url);
		yield return m_dynamiURLRetriever;
	}
  *///CHANGES_KEY_NEW_JSON_SCHEMA 
	public IEnumerator SendOffGameConfigURL_NewJsonSchema(string _url)
	{
		WWWForm dynamicUrlForm = new WWWForm();
		if(Application.isEditor){  
			_url = "http://testmeany.nihi.auckland.ac.nz:3001/apiv3/game/config?sid=" + m_sGameLoginToken;
			m_dynamiURLRetriever = new WWW(_url);		
		}else{
			//_url = _url WEB GL ALREADY PASS THE COMPLETE URL + TOKEN TO THIS FUNCTION
			m_dynamiURLRetriever = new WWW(_url);		
		}
		//m_sDynamicURL = ""; //Clearing URL to make sure it has to take it 
		yield return m_dynamiURLRetriever;
		//Debug.Log ("The web page Dynamic url call is = " + _url);
		if (m_dynamiURLRetriever.isDone) {
			Debug.Log ("<URL>:" + m_dynamiURLRetriever.text);
		}

	}

	/*//CHANGES_KEY_NEW_JSON_SCHEMA 
	public void GetTokenAndDynamicURL()
	{
		string wholeDynamicURLString = "";
			
		if(m_dynamiURLRetriever.isDone){
			Debug.Log ("Dynamic URL login is successful..." + m_dynamiURLRetriever.text);
			wholeDynamicURLString = m_dynamiURLRetriever.text;
			GameObject.Find ("GUI").GetComponent<TalkScenes>().m_sTestWholeDynamic = wholeDynamicURLString;
			m_dynamicLoginSuccessful = wholeDynamicURLString.Contains("\"code\":200");
			if(m_dynamicLoginSuccessful){
				Debug.Log ("Dynamic URL login is successful...");
				string trimToken, trimDynamicURL;
				trimToken = "{\"code\":200,\"data\":{\"token\":\"";
				trimDynamicURL = "\"}}";
				//Get the taken after the successful dynamic login
				//           {"code":200,"data":{"token":"c92fb78b7d03b6e58ad88cf95ee78cb7","serviceURL":"https:\/\/www.sparx.org.nz\/gameapi\/"}}
				//Template = {"code":200,"data":{"token":"a2a6bae8fc753c152e71a48efd1bf7ca","serviceURL":"http:\/\/sparx.saltwebsites.com\/gameapi\/"}}
				m_sGameDynamicToken = wholeDynamicURLString.Substring(trimToken.Length, 32);
				
				//Reset the dynamic URL
				m_sDynamicURL = wholeDynamicURLString.Substring(trimToken.Length + 48);
				m_sDynamicURL = m_sDynamicURL.TrimEnd(trimDynamicURL.ToCharArray());
				m_sDynamicURL = m_sDynamicURL.Replace(@"\/", @"/");
				Debug.Log("DEBUG @ GetTokenAndDynamicURL(): "+m_sDynamicURL);
				//m_sDynamicURLTest = m_sDynamicURL; // For testing only. Remove later.
				
				//For web player testing purpose
				//GameObject.Find ("GUI").GetComponent<TalkScenes>().m_sTestDynamicToken = m_sGameDynamicToken;
				//GameObject.Find ("GUI").GetComponent<TalkScenes>().m_sTestDynamicURL = m_sDynamicURL;
			}
			else{
				Debug.Log ("Dynamic URL login is unsuccessful...");
			}
		}
	}
	*///CHANGES_KEY_NEW_JSON_SCHEMA 

	public void GetTokenAndDynamicURL_NewJsonSchema()
	{
		//m_sDynamicURL = ""; //Clearing URL to make sure it has to take it 
		string wholeDynamicURLString = "";

		if(m_dynamiURLRetriever.isDone){
			Debug.Log ("Dynamic URL login is " + m_dynamiURLRetriever.text);
			wholeDynamicURLString = m_dynamiURLRetriever.text;
			Debug.Log ("Dynamic URL login is successful..." + m_dynamiURLRetriever.text);
			JSONObject json = new JSONObject();
			//Debug.Log ("UNO");
			json = JSONObject.Parse(wholeDynamicURLString);
			//Debug.Log ("DOS");
			GameObject.Find("GUI").GetComponent<TalkScenes>().m_sTestWholeDynamic = wholeDynamicURLString;
			m_dynamicLoginSuccessful = json.GetValue("code").ToString().Contains ("200");//wholeDynamicURLString.Contains("\"code\":200");
			//Debug.Log ("TRES"); 
			if(m_dynamicLoginSuccessful){
				//Debug.Log ("CUATRO");
				if (null != json) {
					//Debug.Log ("CINCO");
					m_sGameDynamicToken = json.GetObject ("data").GetString("token");
					m_sDynamicURL = json.GetObject ("data").GetString("serviceURL");
				}

			}
			else{
				Debug.Log ("Dynamic URL login is unsuccessful...");
			}
		}
	}



	//--------------------------------------------------------PROCESS--------------------------------------------------
	/*
	 * This function is used to save the progress of user level
	 * */
	/*//CHANGES_KEY_NEW_JSON_SCHEMA 
	public string GetUserProcessPostURL(int _levelNum, int _userFeedbackScore)
	{
		string info = "{\"token\":\"";
		if(Application.isEditor){
			info += m_sGameLoginToken;
		}
		else if(!Application.isEditor){
			info += m_sGameDynamicToken;	
		}
		info += "\",\"UserResponse\":{";
		info += GetUserProcessData(_levelNum, _userFeedbackScore);
		info += "}}";
		string urlLevel = m_sDynamicURL + "progress?jsondata=";

		Debug.Log (info);

		string m_sProgressURL = urlLevel + WWW.EscapeURL(info);
		
		return m_sProgressURL;
	}
	*///CHANGES_KEY_NEW_JSON_SCHEMA 
	public string GetUserProcessPostURL_NewJsonSchema(int _levelNum, int _userFeedbackScore)
	{
		string info = "";
		/*if(Application.isEditor){
			info += m_sGameLoginToken;
		}
		else if(!Application.isEditor){
			info += m_sGameDynamicToken;	
		}*/
		info += "{\"UserResponse\":{";
				
		info += GetUserProcessData_NewJsonSchema(_levelNum, _userFeedbackScore);
		info += "}}";
		//string urlLevel = m_sDynamicURL + "progress?jsondata=";

		Debug.Log (info);

		string m_sProgressURL = info;

		return m_sProgressURL;
	}
	/*//CHANGES_KEY_NEW_JSON_SCHEMA 
	public string GetUserProcessGetURL(int _levelNum)
	{
		string info = "{\"token\":\"";
		if(Application.isEditor){
			info += m_sGameLoginToken;
		}
		else if(!Application.isEditor){
			info += m_sGameDynamicToken;	
		}
		info += "\"}";
		string urlLevel = m_sDynamicURL + "progress?jsondata=";
	
		string m_sProgressURL = urlLevel + WWW.EscapeURL(info);
		
		return m_sProgressURL;
	}
	*///CHANGES_KEY_NEW_JSON_SCHEMA 
	public string GetUserProcessGetURL_NewJsonSchema(int _levelNum)
	{
		string info = "";
		//string info;
		if(Application.isEditor){
			info += m_sGameLoginToken;
		}
		else if(!Application.isEditor){
			info += m_sGameDynamicToken;	

		}
		string urlLevel = m_sDynamicURL + "UserResponse?sid=" + info;
		string m_sProgressURL = urlLevel;
		Debug.Log ("<URL CALL TO SERVER>" + m_sProgressURL);
		return m_sProgressURL;
	}

	/*
	 * This function saves the "game level number" and "feedback score" to
	 * the server
	 * */
	/*//CHANGES_KEY_NEW_JSON_SCHEMA 
	public string GetUserProcessData(int _levelNum, int _userFeedbackScore)
	{	
		string processDataString = "";
		
		processDataString += "\"LevelNumber\":" + _levelNum.ToString();
		processDataString += ", \"FeedbackScore\":[" + Global.userFeedBackScore[0].ToString();
		processDataString += ", " + Global.userFeedBackScore[1].ToString();
		processDataString += ", " + Global.userFeedBackScore[2].ToString() + "], ";
		if(_levelNum > 1){
			Global.lastLevelNoteBookData = Global.lastLevelNoteBookData.TrimEnd(']');
			Global.lastLevelNoteBookData += ",";
			processDataString += "\"Notebook\":" + Global.lastLevelNoteBookData;
		}
		processDataString += NoteBookDataBlocksToPost(_levelNum);
		processDataString += "\"]}]";
		Debug.Log ("processDataString: " + processDataString);
		return processDataString;
	}	
	*///CHANGES_KEY_NEW_JSON_SCHEMA 
	public string GetUserProcessData_NewJsonSchema(int _levelNum, int _userFeedbackScore)
	{	
		string processDataString = "";

		processDataString += "\"LevelNumber\":" + _levelNum.ToString();
		processDataString += ", \"FeedBackScore\":[" + Global.userFeedBackScore[0].ToString();
		processDataString += ", " + Global.userFeedBackScore[1].ToString();
		processDataString += ", " + Global.userFeedBackScore[2].ToString() + "], ";
		if(_levelNum > 1){
			Global.lastLevelNoteBookData = Global.lastLevelNoteBookData.TrimEnd(']');
			Global.lastLevelNoteBookData += ",";
			processDataString += "\"NoteBook\":" + Global.lastLevelNoteBookData; //DIEGO FIX NOTEBOOK
		}
		processDataString += NoteBookDataBlocksToPost(_levelNum);
		processDataString += "\"]}]";
		Debug.Log ("processDataString: " + processDataString);
		return processDataString;
	}	

	//CHANGES_KEY_NEW_JSON_SCHEMA - NOTHING CHANGES IN THIS ROUTINE
	public string NoteBookDataBlocksToPost(int _currentLevelNum)
	{
		int i = 0;
		int j = 0;

		switch(_currentLevelNum){
			case 1://Level 1
			m_sNotebookHeader = "\"NoteBook\":[";
				for(j=0;j<2;j++){
					if(j == 0){
						m_sNotebookHeader += "{\"Question\": \"" + Global.questionName[j] + "\",";
					}else{
						m_sNotebookHeader += "\"]},{\"Question\": \"" + Global.questionName[j] + "\",";
					}
					m_sNotebookHeader += "\"Answer\": [\"";
					if(j == 0){
					//Debug.Log ("Global.userL1NoteBookDataBlocks.Length: " + Global.userL1NoteBookDataBlocks.Length);
						for(i = 0; i < Global.userL1NoteBookDataBlocks.Length; i++){
							if(i == 0 && Global.userL1NoteBookDataBlocks[i] != "" && Global.userL1NoteBookDataBlocks[i] != " " && Global.userL1NoteBookDataBlocks[i] != null){
							m_sNotebookHeader += Global.userL1NoteBookDataBlocks[i].Replace("\"","");
							}else if(i < Global.userL1NoteBookDataBlocks.Length - 1 && Global.userL1NoteBookDataBlocks[i] != "" && Global.userL1NoteBookDataBlocks[i] != " " && Global.userL1NoteBookDataBlocks[i] != null){
							m_sNotebookHeader += "\", \"" + Global.userL1NoteBookDataBlocks[i].Replace("\"","");
							}else if(i == Global.userL1NoteBookDataBlocks.Length - 1 && Global.userL1NoteBookDataBlocks[i] != "" && Global.userL1NoteBookDataBlocks[i] != " " && Global.userL1NoteBookDataBlocks[i] != null){
							m_sNotebookHeader += "\", \"" + Global.userL1NoteBookDataBlocks[i].Replace("\"","");
							}else{
								m_sNotebookHeader += "";
							}
						}
					}else{
						for(i = 0; i < Global.userL1NoteBookManualBlocks.Length; i++){
							if(i == 0 && Global.userL1NoteBookManualBlocks[i] != "Click here to enter text"  && Global.userL1NoteBookManualBlocks[i] != "" && Global.userL1NoteBookManualBlocks[i] != " " && Global.userL1NoteBookManualBlocks[i] != null){
							m_sNotebookHeader += Global.userL1NoteBookManualBlocks[i].Replace("\"","");		
							}else if(i < Global.userL1NoteBookManualBlocks.Length - 1 && Global.userL1NoteBookManualBlocks[i] != "Click here to enter text" && Global.userL1NoteBookManualBlocks[i] != "" && Global.userL1NoteBookManualBlocks[i] != " " && Global.userL1NoteBookManualBlocks[i] != null){
							m_sNotebookHeader += "\", \"" + Global.userL1NoteBookManualBlocks[i].Replace("\"","");
							}else if(i == Global.userL1NoteBookManualBlocks.Length - 1 && Global.userL1NoteBookManualBlocks[i] != "Click here to enter text" && Global.userL1NoteBookManualBlocks[i] != "" && Global.userL1NoteBookManualBlocks[i] != " " && Global.userL1NoteBookManualBlocks[i] != null){
							m_sNotebookHeader += "\", \"" + Global.userL1NoteBookManualBlocks[i].Replace("\"","");
							}else{
								m_sNotebookHeader += "";
							}
						}
					}
				}
			break;
			case 2://Level 2
				for(j=2;j<4;j++){
					if(j == 2){
						m_sNotebookHeader += "{\"Question\": \"" + Global.questionName[j] + "\",";
					}else{
						m_sNotebookHeader += "\"]},{\"Question\": \"" + Global.questionName[j] + "\",";
					}
					m_sNotebookHeader += "\"Answer\": [\"";
					if(j == 2){
						for(i = 0; i < Global.userL2NoteBookDataBlocks.Length; i++){
							if(i == 0){
							m_sNotebookHeader += Global.userL2NoteBookDataBlocks[i].Replace("\"","");		
							}else if(i < Global.userL2NoteBookDataBlocks.Length - 1 && Global.userL2NoteBookDataBlocks[i] != "Click here to enter text" && Global.userL2NoteBookDataBlocks[i] != "" && Global.userL2NoteBookDataBlocks[i] != " " && Global.userL2NoteBookDataBlocks[i] != null){
							m_sNotebookHeader += "\", \"" + Global.userL2NoteBookDataBlocks[i].Replace("\"","");
							}else if(i == Global.userL2NoteBookDataBlocks.Length - 1 && Global.userL2NoteBookDataBlocks[i] != "Click here to enter text" && Global.userL2NoteBookDataBlocks[i] != "" && Global.userL2NoteBookDataBlocks[i] != " " && Global.userL2NoteBookDataBlocks[i] != null){
							m_sNotebookHeader += "\", \"" + Global.userL2NoteBookDataBlocks[i].Replace("\"","");
							}else{
								m_sNotebookHeader += "";
							}
						}
					}else{
						for(i = 0; i < Global.userL2NoteBookManualBlocks.Length; i++){
							if(i == 0 && Global.userL2NoteBookManualBlocks[i] != "Click here to enter text"  && Global.userL2NoteBookManualBlocks[i] != "" && Global.userL2NoteBookManualBlocks[i] != " " && Global.userL2NoteBookManualBlocks[i] != null){
							m_sNotebookHeader += Global.userL2NoteBookManualBlocks[i].Replace("\"","");		
							}else if(i < Global.userL2NoteBookManualBlocks.Length - 1 && Global.userL2NoteBookManualBlocks[i] != "Click here to enter text" && Global.userL2NoteBookManualBlocks[i] != "" && Global.userL2NoteBookManualBlocks[i] != " " && Global.userL2NoteBookManualBlocks[i] != null){
							m_sNotebookHeader += "\", \"" + Global.userL2NoteBookManualBlocks[i].Replace("\"","");
							}else if(i == Global.userL2NoteBookManualBlocks.Length - 1 && Global.userL2NoteBookManualBlocks[i] != "Click here to enter text" && Global.userL2NoteBookManualBlocks[i] != "" && Global.userL2NoteBookManualBlocks[i] != " " && Global.userL2NoteBookManualBlocks[i] != null){
							m_sNotebookHeader += "\", \"" + Global.userL2NoteBookManualBlocks[i].Replace("\"","");
							}else{
								m_sNotebookHeader += "";
							}
						}
					}
				}
			break;
			case 3://Level 3
				for(j=4;j<7;j++){
					if(j == 4){
						m_sNotebookHeader += "{\"Question\": \"" + Global.questionName[j] + "\",";
					}else{
						m_sNotebookHeader += "\"]},{\"Question\": \"" + Global.questionName[j] + "\",";
					}
					m_sNotebookHeader += "\"Answer\": [\"";
					if(j == 4){
						for(i = 0; i < Global.userL3NoteBookDataBlockOne.Length; i++){
							if(i == 0){
							m_sNotebookHeader += Global.userL3NoteBookDataBlockOne[i].Replace("\"","");
							}else if(i < Global.userL3NoteBookDataBlockOne.Length - 1 && Global.userL3NoteBookDataBlockOne[i] != "Click here to enter text" && Global.userL3NoteBookDataBlockOne[i] != "" && Global.userL3NoteBookDataBlockOne[i] != " " && Global.userL3NoteBookDataBlockOne[i] != null){
							m_sNotebookHeader += "\", \"" + Global.userL3NoteBookDataBlockOne[i].Replace("\"","");
							}else if(i == Global.userL3NoteBookDataBlockOne.Length - 1 && Global.userL3NoteBookDataBlockOne[i] != "Click here to enter text" && Global.userL3NoteBookDataBlockOne[i] != "" && Global.userL3NoteBookDataBlockOne[i] != " " && Global.userL3NoteBookDataBlockOne[i] != null){
							m_sNotebookHeader += "\", \"" + Global.userL3NoteBookDataBlockOne[i].Replace("\"","");
							}else{
								m_sNotebookHeader += "";
							}
						}
					}else if(j == 5){
						for(i = 0; i < Global.userL3NoteBookDataBlockTwo.Length; i++){
							if(i == 0){
							m_sNotebookHeader += Global.userL3NoteBookDataBlockTwo[i].Replace("\"","");
							}else if(i < Global.userL3NoteBookDataBlockTwo.Length - 1 && Global.userL3NoteBookDataBlockTwo[i] != "Click here to enter text" && Global.userL3NoteBookDataBlockTwo[i] != "" && Global.userL3NoteBookDataBlockTwo[i] != " " && Global.userL3NoteBookDataBlockTwo[i] != null){
							m_sNotebookHeader += "\", \"" + Global.userL3NoteBookDataBlockTwo[i].Replace("\"","");
							}else if(i == Global.userL3NoteBookDataBlockTwo.Length - 1 && Global.userL3NoteBookDataBlockTwo[i] != "Click here to enter text" && Global.userL3NoteBookDataBlockTwo[i] != "" && Global.userL3NoteBookDataBlockTwo[i] != " " && Global.userL3NoteBookDataBlockTwo[i] != null){
							m_sNotebookHeader += "\", \"" + Global.userL3NoteBookDataBlockTwo[i].Replace("\"","");
							}else{
								m_sNotebookHeader += "";
							}
						}
					}else if(j == 6){
						for(i = 0; i < Global.userL3NoteBookDataBlockThree.Length; i++){
							if(i == 0){
							m_sNotebookHeader += Global.userL3NoteBookDataBlockThree[i].Replace("\"","");
							}else if(i < Global.userL3NoteBookDataBlockThree.Length - 1 && Global.userL3NoteBookDataBlockThree[i] != "Click here to enter text" && Global.userL3NoteBookDataBlockThree[i] != "" && Global.userL3NoteBookDataBlockThree[i] != " " && Global.userL3NoteBookDataBlockThree[i] != null){
							m_sNotebookHeader += "\", \"" + Global.userL3NoteBookDataBlockThree[i].Replace("\"","");
							}else if(i == Global.userL3NoteBookDataBlockThree.Length - 1 && Global.userL3NoteBookDataBlockThree[i] != "Click here to enter text" && Global.userL3NoteBookDataBlockThree[i] != "" && Global.userL3NoteBookDataBlockThree[i] != " " && Global.userL3NoteBookDataBlockThree[i] != null){
							m_sNotebookHeader += "\", \"" + Global.userL3NoteBookDataBlockThree[i].Replace("\"","");
							}else{
								m_sNotebookHeader += "";
							}
						}
					}
				}
			break;
			case 4://Level 4
				for(j=7;j<10;j++){
					if(j == 7){
						m_sNotebookHeader += "{\"Question\": \"" + Global.questionName[j] + "\",";
					}else{
						m_sNotebookHeader += "\"]},{\"Question\": \"" + Global.questionName[j] + "\",";
					}
					m_sNotebookHeader += "\"Answer\": [\"";
					if(j == 7){
						for(i = 0; i < Global.userL4NoteBookDataBlockOne.Length; i++){
							if(i == 0){
							m_sNotebookHeader += Global.userL4NoteBookDataBlockOne[i].Replace("\"","");		
							}else if(i < Global.userL4NoteBookDataBlockOne.Length - 1 && Global.userL4NoteBookDataBlockOne[i] != "Click here to enter text" && Global.userL4NoteBookDataBlockOne[i] != "" && Global.userL4NoteBookDataBlockOne[i] != " " && Global.userL4NoteBookDataBlockOne[i] != null){
							m_sNotebookHeader += "\", \"" + Global.userL4NoteBookDataBlockOne[i].Replace("\"","");
							}else if(i == Global.userL4NoteBookDataBlockOne.Length - 1 && Global.userL4NoteBookDataBlockOne[i] != "Click here to enter text" && Global.userL4NoteBookDataBlockOne[i] != "" && Global.userL4NoteBookDataBlockOne[i] != " " && Global.userL4NoteBookDataBlockOne[i] != null){
							m_sNotebookHeader += "\", \"" + Global.userL4NoteBookDataBlockOne[i].Replace("\"","");
							}else{
								m_sNotebookHeader += "";
							}
						}
					}else if(j == 8){
						for(i = 0; i < Global.userL4NoteBookStepsBlocks.Length; i++){
							if(i == 0){
								m_sNotebookHeader += Global.userL4NoteBookStepsBlocks[i].Replace("\"","");		
							}else if(i < Global.userL4NoteBookStepsBlocks.Length - 1 && Global.userL4NoteBookStepsBlocks[i] != "Click here to enter text" && Global.userL4NoteBookStepsBlocks[i] != "" && Global.userL4NoteBookStepsBlocks[i] != " " && Global.userL4NoteBookStepsBlocks[i] != null){
								m_sNotebookHeader += "\", \"" + Global.userL4NoteBookStepsBlocks[i].Replace("\"","");
							}else if(i == Global.userL4NoteBookStepsBlocks.Length - 1 && Global.userL4NoteBookStepsBlocks[i] != "Click here to enter text" && Global.userL4NoteBookStepsBlocks[i] != "" && Global.userL4NoteBookStepsBlocks[i] != " " && Global.userL4NoteBookStepsBlocks[i] != null){
								m_sNotebookHeader += "\", \"" + Global.userL4NoteBookStepsBlocks[i].Replace("\"","");
							}else{
								m_sNotebookHeader += "";
							}
						}
					}else if(j == 9){
						for(i = 0; i < Global.userL4NoteBookDataBlockTwo.Length; i++){
							if(i == 0){
								m_sNotebookHeader += Global.userL4NoteBookDataBlockTwo[i].Replace("\"","");		
							}else if(i < Global.userL4NoteBookDataBlockTwo.Length - 1 && Global.userL4NoteBookDataBlockTwo[i] != "Click here to enter text" && Global.userL4NoteBookDataBlockTwo[i] != "" && Global.userL4NoteBookDataBlockTwo[i] != " " && Global.userL4NoteBookDataBlockTwo[i] != null){
								m_sNotebookHeader += "\", \"" + Global.userL4NoteBookDataBlockTwo[i].Replace("\"","");
							}else if(i == Global.userL4NoteBookDataBlockTwo.Length - 1 && Global.userL4NoteBookDataBlockTwo[i] != "Click here to enter text" && Global.userL4NoteBookDataBlockTwo[i] != "" && Global.userL4NoteBookDataBlockTwo[i] != " " && Global.userL4NoteBookDataBlockTwo[i] != null){
								m_sNotebookHeader += "\", \"" + Global.userL4NoteBookDataBlockTwo[i].Replace("\"","");
							}else{
								m_sNotebookHeader += "";
							}
						}
					}
				}
			break;
			case 5://Level 5
				for(j=10;j<12;j++){
					if(j == 10){
						m_sNotebookHeader += "{\"Question\": \"" + Global.questionName[j] + "\",";
					}else{
						m_sNotebookHeader += "\"]},{\"Question\": \"" + Global.questionName[j] + "\",";
					}
					m_sNotebookHeader += "\"Answer\": [\"";
					if(j == 10){
						for(i = 0; i < Global.userL5NoteBookDataBlocks.Length; i++){
							if(i == 0 && Global.userL5NoteBookDataBlocks[i] != "Click here to enter text" && Global.userL5NoteBookDataBlocks[i] != "" && Global.userL5NoteBookDataBlocks[i] != " " && Global.userL5NoteBookDataBlocks[i] != null){
							m_sNotebookHeader += Global.userL5NoteBookDataBlocks[i].Replace("\"","");		
							}else if(i < Global.userL5NoteBookDataBlocks.Length - 1 && Global.userL5NoteBookDataBlocks[i] != "Click here to enter text" && Global.userL5NoteBookDataBlocks[i] != "" && Global.userL5NoteBookDataBlocks[i] != " " && Global.userL5NoteBookDataBlocks[i] != null){
							m_sNotebookHeader += "\", \"" + Global.userL5NoteBookDataBlocks[i].Replace("\"","");
							}else if(i == Global.userL5NoteBookDataBlocks.Length - 1 && Global.userL5NoteBookDataBlocks[i] != "Click here to enter text" && Global.userL5NoteBookDataBlocks[i] != "" && Global.userL5NoteBookDataBlocks[i] != " " && Global.userL5NoteBookDataBlocks[i] != null){
							m_sNotebookHeader += "\", \"" + Global.userL5NoteBookDataBlocks[i].Replace("\"","");
							}else{
								m_sNotebookHeader += "";
							}
						}
					}else if(j == 11){
						for(i = 0; i < Global.userL5NoteBookManualBlocks.Length; i++){
							if(i == 0 && Global.userL5NoteBookManualBlocks[i] != "Click here to enter text" && Global.userL5NoteBookManualBlocks[i] != "" && Global.userL5NoteBookManualBlocks[i] != " " && Global.userL5NoteBookManualBlocks[i] != null){
							m_sNotebookHeader += Global.userL5NoteBookManualBlocks[i].Replace("\"","");		
							}else if(i < Global.userL5NoteBookManualBlocks.Length - 1 && Global.userL5NoteBookManualBlocks[i] != "Click here to enter text" && Global.userL5NoteBookManualBlocks[i] != "" && Global.userL5NoteBookManualBlocks[i] != " " && Global.userL5NoteBookManualBlocks[i] != null){
							m_sNotebookHeader += "\", \"" + Global.userL5NoteBookManualBlocks[i].Replace("\"","");
							}else if(i == Global.userL5NoteBookManualBlocks.Length - 1 && Global.userL5NoteBookManualBlocks[i] != "Click here to enter text" && Global.userL5NoteBookManualBlocks[i] != "" && Global.userL5NoteBookManualBlocks[i] != " " && Global.userL5NoteBookManualBlocks[i] != null){
							m_sNotebookHeader += "\", \"" + Global.userL5NoteBookManualBlocks[i].Replace("\"","");
							}else{
								m_sNotebookHeader += "";
							}
						}
					}
				}
			break;
			case 6://Level 6
				for(j=12;j<15;j++){
					if(j == 12){
						m_sNotebookHeader += "{\"Question\": \"" + Global.questionName[j] + "\",";
					}else{
						m_sNotebookHeader += "\"]},{\"Question\": \"" + Global.questionName[j] + "\",";
					}
					m_sNotebookHeader += "\"Answer\": [\"";
					if(j == 12){
						for(i = 0; i < Global.userL6NoteBookDataBlocks.Length; i++){
							if(i == 0 && Global.userL6NoteBookDataBlocks[i] != "Click here to enter text" && Global.userL6NoteBookDataBlocks[i] != "" && Global.userL6NoteBookDataBlocks[i] != " " && Global.userL6NoteBookDataBlocks[i] != null){
							m_sNotebookHeader += Global.userL6NoteBookDataBlocks[i].Replace("\"","");		
							}else if(i < Global.userL6NoteBookDataBlocks.Length - 1 && Global.userL6NoteBookDataBlocks[i] != "Click here to enter text" && Global.userL6NoteBookDataBlocks[i] != "" && Global.userL6NoteBookDataBlocks[i] != " " && Global.userL6NoteBookDataBlocks[i] != null){
							m_sNotebookHeader += "\", \"" + Global.userL6NoteBookDataBlocks[i].Replace("\"","");
							}else if(i == Global.userL6NoteBookDataBlocks.Length - 1 && Global.userL6NoteBookDataBlocks[i] != "Click here to enter text" && Global.userL6NoteBookDataBlocks[i] != "" && Global.userL6NoteBookDataBlocks[i] != " " && Global.userL6NoteBookDataBlocks[i] != null){
							m_sNotebookHeader += "\", \"" + Global.userL6NoteBookDataBlocks[i].Replace("\"","");
							}else{
								m_sNotebookHeader += "";
							}
						}
					}else if(j == 13){
						for(i = 0; i < Global.userL6NoteBookRapaBlocks.Length; i++){
							if(i == 0 && Global.userL6NoteBookRapaBlocks[i] != "Click here to enter text" && Global.userL6NoteBookRapaBlocks[i] != "" && Global.userL6NoteBookRapaBlocks[i] != " " && Global.userL6NoteBookRapaBlocks[i] != null){
								m_sNotebookHeader += Global.userL6NoteBookRapaBlocks[i].Replace("\"","");		
							}else if(i < Global.userL6NoteBookRapaBlocks.Length - 1 && Global.userL6NoteBookRapaBlocks[i] != "Click here to enter text" && Global.userL6NoteBookRapaBlocks[i] != "" && Global.userL6NoteBookRapaBlocks[i] != " " && Global.userL6NoteBookRapaBlocks[i] != null){
								m_sNotebookHeader += "\", \"" + Global.userL6NoteBookRapaBlocks[i].Replace("\"","");
							}else if(i == Global.userL6NoteBookRapaBlocks.Length - 1 && Global.userL6NoteBookRapaBlocks[i] != "Click here to enter text" && Global.userL6NoteBookRapaBlocks[i] != "" && Global.userL6NoteBookRapaBlocks[i] != " " && Global.userL6NoteBookRapaBlocks[i] != null){
								m_sNotebookHeader += "\", \"" + Global.userL6NoteBookRapaBlocks[i].Replace("\"","");
							}else{
								m_sNotebookHeader += "";
							}
						}					
					}else if(j == 14){
						for(i = 0; i < Global.userL6NoteBookManualBlocks.Length; i++){
							if(i == 0 && Global.userL6NoteBookManualBlocks[i] != "Click here to enter text" && Global.userL6NoteBookManualBlocks[i] != "" && Global.userL6NoteBookManualBlocks[i] != " " && Global.userL6NoteBookManualBlocks[i] != null){
								m_sNotebookHeader += Global.userL6NoteBookManualBlocks[i].Replace("\"","");		
							}else if(i < Global.userL6NoteBookManualBlocks.Length - 1 && Global.userL6NoteBookManualBlocks[i] != "Click here to enter text" && Global.userL6NoteBookManualBlocks[i] != "" && Global.userL6NoteBookManualBlocks[i] != " " && Global.userL6NoteBookManualBlocks[i] != null){
								m_sNotebookHeader += "\", \"" + Global.userL6NoteBookManualBlocks[i].Replace("\"","");
							}else if(i == Global.userL6NoteBookManualBlocks.Length - 1 && Global.userL6NoteBookManualBlocks[i] != "Click here to enter text" && Global.userL6NoteBookManualBlocks[i] != "" && Global.userL6NoteBookManualBlocks[i] != " " && Global.userL6NoteBookManualBlocks[i] != null){
								m_sNotebookHeader += "\", \"" + Global.userL6NoteBookManualBlocks[i].Replace("\"","");
							}else{
								m_sNotebookHeader += "";
							}
						}

					}
				}
			break;
			case 7://Level 7
				m_sNotebookHeader += "{\"Question\": \"" + Global.questionName[15] + "\",";
				m_sNotebookHeader += "\"Answer\": [\"";
				for(i = 0; i < Global.userL7NoteBookDataBlocks.Length; i++){
					if(i == 0){
					m_sNotebookHeader += Global.userL7NoteBookDataBlocks[i].Replace("\"","");		
					}else if(i < Global.userL7NoteBookDataBlocks.Length - 1){
					m_sNotebookHeader += "\", \"" + Global.userL7NoteBookDataBlocks[i].Replace("\"","");
					}else if(i == Global.userL7NoteBookDataBlocks.Length - 1){
					m_sNotebookHeader += "\", \"" + Global.userL7NoteBookDataBlocks[i].Replace("\"","");
					}else{
						m_sNotebookHeader += "";
					}
				}
			break;			
		}
		m_sNotebookHeader = m_sNotebookHeader.Replace("\"\",","");
		return m_sNotebookHeader;
	}
	
	/*
	 * This function send game progress save request to the server
	 * */
	/*//CHANGES_KEY_NEW_JSON_SCHEMA 
	public IEnumerator SaveUserProcessToServer(int _levelNum, int _userFeedbackScore)
	{	
		WWWForm form = new WWWForm();
		form.AddField("fake", "xy");
		yield return m_ProcessDataPostRetriever = new WWW(GetUserProcessPostURL(_levelNum, _userFeedbackScore), form);
		Debug.Log ("The user process data POST URL is = " + GetUserProcessPostURL(_levelNum, _userFeedbackScore));
	}	
	*///CHANGES_KEY_NEW_JSON_SCHEMA 


	public IEnumerator SaveUserProcessToServer_NewJsonSchema(int _levelNum, int _userFeedbackScore)
	{	
	//	WWWForm form = new WWWForm();
	//	form.AddField("fake", "xy");
	
		var postScoreURL = m_sDynamicURL + "UserResponse?sid=" + m_sGameDynamicToken;
		var jsonString = GetUserProcessPostURL_NewJsonSchema(_levelNum, _userFeedbackScore);
		var encoding = new System.Text.UTF8Encoding();
		Dictionary<string, string> postHeader = new Dictionary<string, string>();
		postHeader["Content-Type"] = "application/json";
		//postHeader ["Content-Length"] = jsonString.Length.ToString();

		yield return m_ProcessDataPostRetriever = new WWW(postScoreURL, encoding.GetBytes(jsonString), postHeader);
		Debug.Log ("<SAVING PROGRESS AND NOTEBOOK>:STRING" + jsonString);
		Debug.Log ("<URL CALL TO SERVER>" + postScoreURL);
		if (m_ProcessDataPostRetriever.isDone) {
			Debug.Log ("<SAVING PROGRESS AND NOTEBOOK>:" + m_ProcessDataPostRetriever.text);
		}

		//yield return m_ProcessDataPostRetriever = new WWW(GetUserProcessPostURL(_levelNum, _userFeedbackScore), form);
		//Debug.Log ("The user process data POST URL is = " + GetUserProcessPostURL_NewJsonSchema(_levelNum, _userFeedbackScore));
	}	

	/*
	 * This function reads game progress from the server by send request
	 * */
	/*//CHANGES_KEY_NEW_JSON_SCHEMA 
	public IEnumerator ReadUserProgressDataFromServer(int _levelNum)
	{
		WWWForm form = new WWWForm();
		form.AddField("fakeformProgress", "xyxy");
		Debug.Log ("The proggress string to the server is = " + GetUserProcessGetURL(_levelNum));
		yield return m_ProcessDataGetRetriever = new WWW(GetUserProcessGetURL(_levelNum), form);
		
		string test = m_ProcessDataGetRetriever.text;
		test = test.Replace(@"\", "");
		Debug.Log ("The proggress data from the server is = " + test);
		m_sReadProgressURL = test;
		JSONObject json = new JSONObject();

		json = JSONObject.Parse(test);
		string tempNum;
		string tempFloat;
		string tempString;
		JSONValue tempVal;

		if (null != json){
			json = json.GetObject("UserResponse").GetObject("progress");

			m_bFoundLevelNumber = true;

			tempNum = json.GetValue("LevelNumber").ToString();
			Global.CurrentLevelNumber = int.Parse(tempNum);
			if(Application.isEditor && Global.TestingOnline == true){
				Global.CurrentLevelNumber = Global.TestProgress;
			}
			Debug.Log ("Old server level number = " + Global.CurrentLevelNumber);

			if(Global.CurrentLevelNumber < 1 || Global.CurrentLevelNumber == 7){
				Global.CurrentLevelNumber = 1;
				Debug.Log ("Server level number = " + Global.CurrentLevelNumber);
			}
			else{
				Global.CurrentLevelNumber++; 
				Debug.Log ("Server level number = " + Global.CurrentLevelNumber);
			}

			if(Global.CurrentLevelNumber > 1){
				JSONArray tempArray = json.GetArray("FeedbackScore");
				tempNum = tempArray[0].ToString();
				Global.userFeedBackScore[0] = int.Parse(tempNum);
				tempNum = tempArray[1].ToString();
				Global.userFeedBackScore[1] = int.Parse(tempNum);
				tempNum = tempArray[2].ToString();
				Global.userFeedBackScore[2] = int.Parse(tempNum);
			}
		}
	}
	*///CHANGES_KEY_NEW_JSON_SCHEMA 
//---------------------------------------------------------------------------------
//------------------------------------------------------------------------------
	public IEnumerator ReadUserProgressDataFromServer_NewJsonSchema(int _levelNum)
	{
		WWWForm form = new WWWForm();
		form.AddField("fakeformProgress", "xyxy");
		Debug.Log ("The proggress string to the server is = " + GetUserProcessGetURL_NewJsonSchema(_levelNum));
		yield return m_ProcessDataGetRetriever = new WWW(GetUserProcessGetURL_NewJsonSchema(_levelNum));

		if (m_ProcessDataGetRetriever.isDone){
			//m_ProcessDataGetRetriever.text = m_ProcessDataGetRetriever.text.Replace("\"SavePoint\":null","\"SavePoint\":\"levelEnd"");
		
		string test = m_ProcessDataGetRetriever.text;

		if (test.Contains ("{\"UserResponse\":null}")) {
			Debug.Log ("Replace String:" + "{\"UserResponse\":{\"LevelNumber\":1,\"SavePoint\":\"\",\"FeedbackScore\":[0,0,0],\"Notebook\":null}}");
			test = "{\"UserResponse\":{\"LevelNumber\":1,\"SavePoint\":\"\",\"FeedbackScore\":[0,0,0],\"Notebook\":null}}";
		}

		Debug.Log ("<PROGRESS>:" + test);
		
		//test = "{\"UserResponse\":null}";

		test = test.Replace(@"\", "");
		
		Debug.Log ("The proggress data from the server is = " + test);

		if (test.Contains ("{\"UserResponse\":null}")) {
			Debug.Log ("Replace String:" + "{\"UserResponse\":{\"LevelNumber\":1,\"SavePoint\":\"\",\"FeedbackScore\":[0,0,0],\"Notebook\":null}}");
			test = "{\"UserResponse\":{\"LevelNumber\":1,\"SavePoint\":\"\",\"FeedbackScore\":[0,0,0],\"Notebook\":null}}";
		}

		m_sReadProgressURL = test;

		JSONObject json = new JSONObject();
		json = JSONObject.Parse(test);
		/*try{
			json = JSONObject.Parse(test);
		}catch(System.Exception e){
			test = "{\"UserResponse\":{\"LevelNumber\":1,\"SavePoint\":\"\",\"FeedbackScore\":[0,0,0],\"Notebook\":null}}";
			json = JSONObject.Parse(test);
		}*/

		string tempNum;
		string tempFloat;
		string tempString;
		string SavePoint;
		JSONValue tempVal;

		if (null != json){
			//json = json.GetObject("UserResponse").GetObject("progress");

			m_bFoundLevelNumber = true;

			tempNum = json.GetObject("UserResponse").GetNumber("LevelNumber").ToString();

				if (json.GetObject ("UserResponse").GetString ("SavePoint") == null) {// ANGELA ASKED FOR SWAP NULL FOR levelEnd
					SavePoint = "levelEnd";// ANGELA ASKED FOR SWAP NULL FOR levelEnd
			} else {
					SavePoint = json.GetObject("UserResponse").GetString("SavePoint").ToString();
			}

			Global.CurrentLevelNumber = int.Parse(tempNum);
			if(Application.isEditor && Global.TestingOnline == true){
				Global.CurrentLevelNumber = Global.TestProgress;
			}
			Debug.Log ("Old server level number = " + Global.CurrentLevelNumber);

			if(Global.CurrentLevelNumber < 1 || (Global.CurrentLevelNumber == 7 && SavePoint.Contains("levelEnd"))){
				Global.CurrentLevelNumber = 1;
				Debug.Log ("Server level number = " + Global.CurrentLevelNumber);
			}
			else{
				if (SavePoint.Contains("levelEnd")) {
					Global.CurrentLevelNumber++;// TEST DIEGO //CHANGES_KEY_NEW_JSON_SCHEMA
					Debug.Log ("Server level number = " + Global.CurrentLevelNumber);
				} else {
					Debug.Log ("Server level number = " + Global.CurrentLevelNumber);
				}
			}

			if(Global.CurrentLevelNumber > 1){
				JSONArray tempArray = json.GetObject("UserResponse").GetArray("FeedBackScore");
				tempNum = tempArray[0].ToString();
				Global.userFeedBackScore[0] = int.Parse(tempNum);
				tempNum = tempArray[1].ToString();
				Global.userFeedBackScore[1] = int.Parse(tempNum);
				tempNum = tempArray[2].ToString();
				Global.userFeedBackScore[2] = int.Parse(tempNum);
			}
		}
		}
	}

	/*//CHANGES_KEY_NEW_JSON_SCHEMA 
	public IEnumerator ReadUserProgressNoteBookDataFromServer(int _levelNum)
	{
		WWWForm form = new WWWForm();
		form.AddField("fakeformNotebook", "xyxy");

		yield return m_ProcessNotebookDataGetRetriever = new WWW(GetUserProcessGetURL(_levelNum), form);
		
		string test = m_ProcessNotebookDataGetRetriever.text;
		Debug.Log ("The data from server is === " + test);
		JSONObject json = new JSONObject();
		string tempNum;
		string tempFloat;
		string tempString;
		string[] tempStringArray;
		JSONValue tempVal;
		JSONArray tempArray;
		json = JSONObject.Parse(test);
		json = json.GetObject("UserResponse").GetObject("progress");
		Global.lastLevelNoteBookData = json.GetArray("Notebook").ToString();
		if (null != json){

			if(_levelNum == 2){
				tempString = json.GetArray("Notebook")[1].ToString();
				tempString = tempString.Replace("{\"Answer\":[", "");
				tempString = tempString.Replace("]}", "");
				tempStringArray = tempString.Split(',');
				for(int i = 0; i < tempStringArray.Length ; i++){
					Global.userL1NoteBookDataBlocks[i] = tempStringArray[i];
				}
				tempString = json.GetArray("Notebook")[3].ToString();
				tempString = tempString.Replace("{\"Answer\":[", "");
				tempString = tempString.Replace("]}", "");
				tempStringArray = tempString.Split(',');
				for(int i = 0; i < tempStringArray.Length ; i++){
					Global.userL1NoteBookManualBlocks[i] = tempStringArray[i];
				}
			}
			if(_levelNum >= 3){
				tempArray = json.GetArray("Notebook");
				//Debug.Log (tempArray);
				int j = 0;
				if(_levelNum == 3){
					j = 1;
				}else if(_levelNum == 4){
					j = 2;
				}else if(_levelNum == 5){
					j = 3;
				}else if(_levelNum == 6){
					j = 4;
				}else if(_levelNum == 7){
					j = 5;
				}
				switch(j)
				{
				case 1://Show in Level 3
					// Level 1 Data
					tempString = json.GetArray("Notebook")[1].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL1NoteBookDataBlocks[i] = tempStringArray[i];
					}
					tempString = json.GetArray("Notebook")[3].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL1NoteBookManualBlocks[i] = tempStringArray[i];
					}
					
					// Level 2 Data
					tempString = json.GetArray("Notebook")[5].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL2NoteBookDataBlocks[i] = tempStringArray[i];
					}
					tempString = json.GetArray("Notebook")[7].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL2NoteBookManualBlocks[i] = tempStringArray[i];
					}
					break;
				case 2://Show in Level 4
					// Level 1 Data
					tempString = json.GetArray("Notebook")[1].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL1NoteBookDataBlocks[i] = tempStringArray[i];
					}
					tempString = json.GetArray("Notebook")[3].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL1NoteBookManualBlocks[i] = tempStringArray[i];
					}

					// Level 2 Data
					tempString = json.GetArray("Notebook")[5].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL2NoteBookDataBlocks[i] = tempStringArray[i];
					}
					tempString = json.GetArray("Notebook")[7].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL2NoteBookManualBlocks[i] = tempStringArray[i];
					}

					//Level 3 data
					tempString = json.GetArray("Notebook")[9].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL3NoteBookDataBlockOne[i] = tempStringArray[i];
					}
					tempString = json.GetArray("Notebook")[11].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL3NoteBookDataBlockTwo[i] = tempStringArray[i];
					}
					tempString = json.GetArray("Notebook")[13].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL3NoteBookDataBlockThree[i] = tempStringArray[i];
					}
					break;
				case 3://Show in Level 5
					// Level 1 Data
					tempString = json.GetArray("Notebook")[1].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL1NoteBookDataBlocks[i] = tempStringArray[i];
					}
					tempString = json.GetArray("Notebook")[3].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL1NoteBookManualBlocks[i] = tempStringArray[i];
					}
					
					// Level 2 Data
					tempString = json.GetArray("Notebook")[5].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL2NoteBookDataBlocks[i] = tempStringArray[i];
					}
					tempString = json.GetArray("Notebook")[7].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL2NoteBookManualBlocks[i] = tempStringArray[i];
					}
					
					//Level 3 data
					tempString = json.GetArray("Notebook")[9].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL3NoteBookDataBlockOne[i] = tempStringArray[i];
					}
					tempString = json.GetArray("Notebook")[11].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL3NoteBookDataBlockTwo[i] = tempStringArray[i];
					}
					tempString = json.GetArray("Notebook")[13].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL3NoteBookDataBlockThree[i] = tempStringArray[i];
					}

					//Level 4 data
					tempString = json.GetArray("Notebook")[15].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL4NoteBookDataBlockOne[i] = tempStringArray[i];
					}
					tempString = json.GetArray("Notebook")[17].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL4NoteBookDataBlockTwo[i] = tempStringArray[i];
					}
					tempString = json.GetArray("Notebook")[19].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL4NoteBookStepsBlocks[i] = tempStringArray[i];
					}
					break;
				case 4://Show in Level 6
					// Level 1 Data
					tempString = json.GetArray("Notebook")[1].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL1NoteBookDataBlocks[i] = tempStringArray[i];
					}
					tempString = json.GetArray("Notebook")[3].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL1NoteBookManualBlocks[i] = tempStringArray[i];
					}
					
					// Level 2 Data
					tempString = json.GetArray("Notebook")[5].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL2NoteBookDataBlocks[i] = tempStringArray[i];
					}
					tempString = json.GetArray("Notebook")[7].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL2NoteBookManualBlocks[i] = tempStringArray[i];
					}
					
					//Level 3 data
					tempString = json.GetArray("Notebook")[9].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL3NoteBookDataBlockOne[i] = tempStringArray[i];
					}
					tempString = json.GetArray("Notebook")[11].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL3NoteBookDataBlockTwo[i] = tempStringArray[i];
					}
					tempString = json.GetArray("Notebook")[13].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL3NoteBookDataBlockThree[i] = tempStringArray[i];
					}
					
					//Level 4 data
					tempString = json.GetArray("Notebook")[15].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL4NoteBookDataBlockOne[i] = tempStringArray[i];
					}
					tempString = json.GetArray("Notebook")[17].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL4NoteBookDataBlockTwo[i] = tempStringArray[i];
					}
					tempString = json.GetArray("Notebook")[19].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL4NoteBookStepsBlocks[i] = tempStringArray[i];
					}

					// Level 5 Data
					tempString = json.GetArray("Notebook")[21].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL5NoteBookDataBlocks[i] = tempStringArray[i];
					}
					tempString = json.GetArray("Notebook")[23].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL5NoteBookManualBlocks[i] = tempStringArray[i];
					}
					break;
				case 5://Show in Level 7
					//Debug.Log ("Triggered Case 5");
					// Level 1 Data
					tempString = json.GetArray("Notebook")[1].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL1NoteBookDataBlocks[i] = tempStringArray[i];
					}
					tempString = json.GetArray("Notebook")[3].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL1NoteBookManualBlocks[i] = tempStringArray[i];
					}
					
					// Level 2 Data
					tempString = json.GetArray("Notebook")[5].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL2NoteBookDataBlocks[i] = tempStringArray[i];
					}
					tempString = json.GetArray("Notebook")[7].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL2NoteBookManualBlocks[i] = tempStringArray[i];
					}
					
					//Level 3 data
					tempString = json.GetArray("Notebook")[9].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL3NoteBookDataBlockOne[i] = tempStringArray[i];
					}
					tempString = json.GetArray("Notebook")[11].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL3NoteBookDataBlockTwo[i] = tempStringArray[i];
					}
					tempString = json.GetArray("Notebook")[13].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL3NoteBookDataBlockThree[i] = tempStringArray[i];
					}
					
					//Level 4 data
					tempString = json.GetArray("Notebook")[15].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL4NoteBookDataBlockOne[i] = tempStringArray[i];
					}
					tempString = json.GetArray("Notebook")[17].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL4NoteBookDataBlockTwo[i] = tempStringArray[i];
					}
					tempString = json.GetArray("Notebook")[19].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL4NoteBookStepsBlocks[i] = tempStringArray[i];
					}
					
					// Level 5 Data
					tempString = json.GetArray("Notebook")[21].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL5NoteBookDataBlocks[i] = tempStringArray[i];
					}
					tempString = json.GetArray("Notebook")[23].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL5NoteBookManualBlocks[i] = tempStringArray[i];
					}
					
					// Level 6 Data
					tempString = json.GetArray("Notebook")[25].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL6NoteBookDataBlocks[i] = tempStringArray[i];
					}
					tempString = json.GetArray("Notebook")[27].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL6NoteBookManualBlocks[i] = tempStringArray[i];
					}
					tempString = json.GetArray("Notebook")[29].ToString();
					tempString = tempString.Replace("{\"Answer\":[", "");
					tempString = tempString.Replace("]}", "");
					tempStringArray = tempString.Split(',');
					for(int i = 0; i < tempStringArray.Length ; i++){
						Global.userL6NoteBookRapaBlocks[i] = tempStringArray[i];
					}
					break;
				};
			}
		}
	}
	*///CHANGES_KEY_NEW_JSON_SCHEMA 

	public IEnumerator ReadUserProgressNoteBookDataFromServer_NewJsonSchema(int _levelNum)
	{
		WWWForm form = new WWWForm ();
		form.AddField ("fakeformNotebook", "xyxy");

		yield return m_ProcessNotebookDataGetRetriever = new WWW (GetUserProcessGetURL_NewJsonSchema (_levelNum));

		if (m_ProcessNotebookDataGetRetriever.isDone) {
			Debug.Log ("<NOTEBOOK>:" + m_ProcessNotebookDataGetRetriever.text);
		}

		string test = m_ProcessNotebookDataGetRetriever.text;
		Debug.Log ("The data from server is === " + test);
		JSONObject json = new JSONObject ();
		JSONObject jsonNotebookItem = new JSONObject ();
		string tempNum;
		string tempFloat;
		string tempString;
		string[] tempStringArray;
		JSONArray tempJSONStringArray;
		JSONValue tempVal;
		JSONArray tempArray;
		json = JSONObject.Parse (test);
		Global.lastLevelNoteBookData = "";
		json = json.GetObject ("UserResponse");
		if (null != json.GetArray("NoteBook")) {
		Global.lastLevelNoteBookData = json.GetArray ("NoteBook").ToString ();
		
		if (null != json) {


			if (_levelNum == 2) {
					// Level 1 Data
					if (json.GetArray ("NoteBook").Length > 0) {
						tempString = json.GetArray ("NoteBook") [0].ToString ();
						jsonNotebookItem = JSONObject.Parse (tempString);
						tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
						for (int i = 0; i < tempJSONStringArray.Length; i++) {
							Global.userL1NoteBookDataBlocks [i] = tempJSONStringArray [i].ToString();
						}
					}
					if (json.GetArray ("NoteBook").Length > 1) {
						tempString = json.GetArray ("NoteBook") [1].ToString ();
						jsonNotebookItem = JSONObject.Parse (tempString);
						tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
						for (int i = 0; i < tempJSONStringArray.Length; i++) {
							Global.userL1NoteBookManualBlocks [i] = tempJSONStringArray [i].ToString();
						}
					}
			}
			if (_levelNum >= 3) {
				tempArray = json.GetArray ("NoteBook");
				//Debug.Log (tempArray);
				int j = 0;
				if (_levelNum == 3) {
					j = 1;
				} else if (_levelNum == 4) {
					j = 2;
				} else if (_levelNum == 5) {
					j = 3;
				} else if (_levelNum == 6) {
					j = 4;
				} else if (_levelNum == 7) {
					j = 5;
				}
				switch (j) {
				case 1://Show in Level 3
						// Level 1 Data
						if (json.GetArray ("NoteBook").Length > 0) {
							tempString = json.GetArray ("NoteBook") [0].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
							for (int i = 0; i < tempJSONStringArray.Length; i++) {
								Global.userL1NoteBookDataBlocks [i] = tempJSONStringArray [i].ToString();
							}
						}
						if (json.GetArray ("NoteBook").Length > 1) {
							tempString = json.GetArray ("NoteBook") [1].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
							for (int i = 0; i < tempJSONStringArray.Length; i++) {
								Global.userL1NoteBookManualBlocks [i] = tempJSONStringArray [i].ToString();
							}
						}
						// Level 2 Data
						if (json.GetArray ("NoteBook").Length > 2) {
							tempString = json.GetArray ("NoteBook") [2].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
							for (int i = 0; i < tempJSONStringArray.Length; i++) {
								Global.userL2NoteBookDataBlocks [i] = tempJSONStringArray [i].ToString();
							}
						}
						if (json.GetArray ("NoteBook").Length > 3) {
							tempString = json.GetArray ("NoteBook") [3].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
							for (int i = 0; i < tempJSONStringArray.Length; i++) {
								Global.userL2NoteBookManualBlocks [i] = tempJSONStringArray [i].ToString();
							}
						}
					break;
				case 2://Show in Level 4
						// Level 1 Data
						if (json.GetArray ("NoteBook").Length > 0) {
							tempString = json.GetArray ("NoteBook") [0].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
							for (int i = 0; i < tempJSONStringArray.Length; i++) {
								Global.userL1NoteBookDataBlocks [i] = tempJSONStringArray [i].ToString();
							}
						}
						if (json.GetArray ("NoteBook").Length > 1) {
							tempString = json.GetArray ("NoteBook") [1].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
							for (int i = 0; i < tempJSONStringArray.Length; i++) {
								Global.userL1NoteBookManualBlocks [i] = tempJSONStringArray [i].ToString();
							}
						}
						// Level 2 Data
						if (json.GetArray ("NoteBook").Length > 2) {
							tempString = json.GetArray ("NoteBook") [2].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
							for (int i = 0; i < tempJSONStringArray.Length; i++) {
								Global.userL2NoteBookDataBlocks [i] = tempJSONStringArray [i].ToString();
							}
						}
						if (json.GetArray ("NoteBook").Length > 3) {
							tempString = json.GetArray ("NoteBook") [3].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
							for (int i = 0; i < tempJSONStringArray.Length; i++) {
								Global.userL2NoteBookManualBlocks [i] = tempJSONStringArray [i].ToString();
							}
						}
						//Level 3 data
						if (json.GetArray ("NoteBook").Length > 4) {
							tempString = json.GetArray ("NoteBook") [4].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
							for (int i = 0; i < tempJSONStringArray.Length; i++) {
								Global.userL3NoteBookDataBlockOne [i] = tempJSONStringArray [i].ToString();
							}
						}
						if (json.GetArray ("NoteBook").Length > 5) {
							tempString = json.GetArray ("NoteBook") [5].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
							for (int i = 0; i < tempJSONStringArray.Length; i++) {
								Global.userL3NoteBookDataBlockTwo [i] = tempJSONStringArray [i].ToString();
							}
						}
						if (json.GetArray ("NoteBook").Length > 6) {
							tempString = json.GetArray ("NoteBook") [6].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
							for (int i = 0; i < tempJSONStringArray.Length; i++) {
								Global.userL3NoteBookDataBlockThree [i] = tempJSONStringArray [i].ToString();
							}
						}					break;
				case 3://Show in Level 5
					// Level 1 Data
					if (json.GetArray ("NoteBook").Length > 0) {
						tempString = json.GetArray ("NoteBook") [0].ToString ();
						jsonNotebookItem = JSONObject.Parse (tempString);
						tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
						for (int i = 0; i < tempJSONStringArray.Length; i++) {
							Global.userL1NoteBookDataBlocks [i] = tempJSONStringArray [i].ToString();
						}
					}
					if (json.GetArray ("NoteBook").Length > 1) {
						tempString = json.GetArray ("NoteBook") [1].ToString ();
						jsonNotebookItem = JSONObject.Parse (tempString);
						tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
						for (int i = 0; i < tempJSONStringArray.Length; i++) {
							Global.userL1NoteBookManualBlocks [i] = tempJSONStringArray [i].ToString();
						}
					}
					// Level 2 Data
					if (json.GetArray ("NoteBook").Length > 2) {
						tempString = json.GetArray ("NoteBook") [2].ToString ();
						jsonNotebookItem = JSONObject.Parse (tempString);
						tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
						for (int i = 0; i < tempJSONStringArray.Length; i++) {
							Global.userL2NoteBookDataBlocks [i] = tempJSONStringArray [i].ToString();
						}
					}
						if (json.GetArray ("NoteBook").Length > 3) {
						tempString = json.GetArray ("NoteBook") [3].ToString ();
						jsonNotebookItem = JSONObject.Parse (tempString);
						tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
						for (int i = 0; i < tempJSONStringArray.Length; i++) {
							Global.userL2NoteBookManualBlocks [i] = tempJSONStringArray [i].ToString();
						}
					}
					//Level 3 data
					if (json.GetArray ("NoteBook").Length > 4) {
						tempString = json.GetArray ("NoteBook") [4].ToString ();
						jsonNotebookItem = JSONObject.Parse (tempString);
						tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
						for (int i = 0; i < tempJSONStringArray.Length; i++) {
							Global.userL3NoteBookDataBlockOne [i] = tempJSONStringArray [i].ToString();
						}
					}
					if (json.GetArray ("NoteBook").Length > 5) {
						tempString = json.GetArray ("NoteBook") [5].ToString ();
						jsonNotebookItem = JSONObject.Parse (tempString);
						tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
						for (int i = 0; i < tempJSONStringArray.Length; i++) {
							Global.userL3NoteBookDataBlockTwo [i] = tempJSONStringArray [i].ToString();
						}
					}
					if (json.GetArray ("NoteBook").Length > 6) {
						tempString = json.GetArray ("NoteBook") [6].ToString ();
						jsonNotebookItem = JSONObject.Parse (tempString);
						tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
						for (int i = 0; i < tempJSONStringArray.Length; i++) {
							Global.userL3NoteBookDataBlockThree [i] = tempJSONStringArray [i].ToString();
						}
					}
					//Level 4 data
					if (json.GetArray ("NoteBook").Length > 7) {
						tempString = json.GetArray ("NoteBook") [7].ToString ();
						jsonNotebookItem = JSONObject.Parse (tempString);
						tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
						for (int i = 0; i < tempJSONStringArray.Length; i++) {
							Global.userL4NoteBookDataBlockOne [i] = tempJSONStringArray [i].ToString();
						}
					}
					if (json.GetArray ("NoteBook").Length > 8) {
						tempString = json.GetArray ("NoteBook") [8].ToString ();
						jsonNotebookItem = JSONObject.Parse (tempString);
						tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
						for (int i = 0; i < tempJSONStringArray.Length; i++) {
							Global.userL4NoteBookStepsBlocks [i] = tempJSONStringArray [i].ToString();
						}
					}
					if (json.GetArray ("NoteBook").Length > 9) {
						tempString = json.GetArray ("NoteBook") [9].ToString ();
						jsonNotebookItem = JSONObject.Parse (tempString);
						tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
						for (int i = 0; i < tempJSONStringArray.Length; i++) {
							Global.userL4NoteBookDataBlockTwo [i] = tempJSONStringArray [i].ToString();
						}
					}
					break;
				case 4://Show in Level 6
						// Level 1 Data
						if (json.GetArray ("NoteBook").Length > 0) {
							tempString = json.GetArray ("NoteBook") [0].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
							for (int i = 0; i < tempJSONStringArray.Length; i++) {
								Global.userL1NoteBookDataBlocks [i] = tempJSONStringArray [i].ToString();
							}
						}
						if (json.GetArray ("NoteBook").Length > 1) {
							tempString = json.GetArray ("NoteBook") [1].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
							for (int i = 0; i < tempJSONStringArray.Length; i++) {
								Global.userL1NoteBookManualBlocks [i] = tempJSONStringArray [i].ToString();
							}
						}
						// Level 2 Data
						if (json.GetArray ("NoteBook").Length > 2) {
							tempString = json.GetArray ("NoteBook") [2].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
							for (int i = 0; i < tempJSONStringArray.Length; i++) {
								Global.userL2NoteBookDataBlocks [i] = tempJSONStringArray [i].ToString();
							}
						}
						if (json.GetArray ("NoteBook").Length > 3) {
							tempString = json.GetArray ("NoteBook") [3].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
							for (int i = 0; i < tempJSONStringArray.Length; i++) {
								Global.userL2NoteBookManualBlocks [i] = tempJSONStringArray [i].ToString();
							}
						}
						//Level 3 data
						if (json.GetArray ("NoteBook").Length > 4) {
							tempString = json.GetArray ("NoteBook") [4].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
							for (int i = 0; i < tempJSONStringArray.Length; i++) {
								Global.userL3NoteBookDataBlockOne [i] = tempJSONStringArray [i].ToString();
							}
						}
						if (json.GetArray ("NoteBook").Length > 5) {
							tempString = json.GetArray ("NoteBook") [5].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
							for (int i = 0; i < tempJSONStringArray.Length; i++) {
								Global.userL3NoteBookDataBlockTwo [i] = tempJSONStringArray [i].ToString();
							}
						}
						if (json.GetArray ("NoteBook").Length > 6) {
							tempString = json.GetArray ("NoteBook") [6].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
							for (int i = 0; i < tempJSONStringArray.Length; i++) {
								Global.userL3NoteBookDataBlockThree [i] = tempJSONStringArray [i].ToString();
							}
						}
						//Level 4 data
						if (json.GetArray ("NoteBook").Length > 7) {
							tempString = json.GetArray ("NoteBook") [7].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
							for (int i = 0; i < tempJSONStringArray.Length; i++) {
								Global.userL4NoteBookDataBlockOne [i] = tempJSONStringArray [i].ToString();
							}
						}
						if (json.GetArray ("NoteBook").Length > 8) {
							tempString = json.GetArray ("NoteBook") [8].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
							for (int i = 0; i < tempJSONStringArray.Length; i++) {
								Global.userL4NoteBookStepsBlocks [i] = tempJSONStringArray [i].ToString();
							}
						}
						if (json.GetArray ("NoteBook").Length > 9) {
							tempString = json.GetArray ("NoteBook") [9].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
							for (int i = 0; i < tempJSONStringArray.Length; i++) {
								Global.userL4NoteBookDataBlockTwo [i] = tempJSONStringArray [i].ToString();
							}
						}
						// Level 5 Data
						if (json.GetArray ("NoteBook").Length > 10) {
							tempString = json.GetArray ("NoteBook") [10].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
							for (int i = 0; i < tempJSONStringArray.Length; i++) {
								Global.userL5NoteBookDataBlocks [i] = tempJSONStringArray [i].ToString();
							}
						}
						if (json.GetArray ("NoteBook").Length > 11) {
							tempString = json.GetArray ("NoteBook") [11].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
							for (int i = 0; i < tempJSONStringArray.Length; i++) {
								Global.userL5NoteBookManualBlocks [i] = tempJSONStringArray [i].ToString();
							}
						}
					break;
				case 5://Show in Level 7
					//Debug.Log ("Triggered Case 5");
					// Level 1 Data
					if (json.GetArray ("NoteBook").Length > 0) {
						tempString = json.GetArray ("NoteBook") [0].ToString ();
						jsonNotebookItem = JSONObject.Parse (tempString);
						tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
						for (int i = 0; i < tempJSONStringArray.Length; i++) {
							Global.userL1NoteBookDataBlocks [i] = tempJSONStringArray [i].ToString();
						}
					}
					if (json.GetArray ("NoteBook").Length > 1) {
						tempString = json.GetArray ("NoteBook") [1].ToString ();
						jsonNotebookItem = JSONObject.Parse (tempString);
						tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
						for (int i = 0; i < tempJSONStringArray.Length; i++) {
							Global.userL1NoteBookManualBlocks [i] = tempJSONStringArray [i].ToString();
						}
					}
					// Level 2 Data
					if (json.GetArray ("NoteBook").Length > 2) {
						tempString = json.GetArray ("NoteBook") [2].ToString ();
						jsonNotebookItem = JSONObject.Parse (tempString);
						tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
						for (int i = 0; i < tempJSONStringArray.Length; i++) {
							Global.userL2NoteBookDataBlocks [i] = tempJSONStringArray [i].ToString();
						}
					}
					if (json.GetArray ("NoteBook").Length > 3) {
						tempString = json.GetArray ("NoteBook") [3].ToString ();
						jsonNotebookItem = JSONObject.Parse (tempString);
						tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
						for (int i = 0; i < tempJSONStringArray.Length; i++) {
							Global.userL2NoteBookManualBlocks [i] = tempJSONStringArray [i].ToString();
						}
					}
					//Level 3 data
					if (json.GetArray ("NoteBook").Length > 4) {
						tempString = json.GetArray ("NoteBook") [4].ToString ();
						jsonNotebookItem = JSONObject.Parse (tempString);
						tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
						for (int i = 0; i < tempJSONStringArray.Length; i++) {
							Global.userL3NoteBookDataBlockOne [i] = tempJSONStringArray [i].ToString();
						}
					}
					if (json.GetArray ("NoteBook").Length > 5) {
						tempString = json.GetArray ("NoteBook") [5].ToString ();
						jsonNotebookItem = JSONObject.Parse (tempString);
						tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
						for (int i = 0; i < tempJSONStringArray.Length; i++) {
							Global.userL3NoteBookDataBlockTwo [i] = tempJSONStringArray [i].ToString();
						}
					}
					if (json.GetArray ("NoteBook").Length > 6) {
						tempString = json.GetArray ("NoteBook") [6].ToString ();
						jsonNotebookItem = JSONObject.Parse (tempString);
						tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
						for (int i = 0; i < tempJSONStringArray.Length; i++) {
							Global.userL3NoteBookDataBlockThree [i] = tempJSONStringArray [i].ToString();
						}
					}
					//Level 4 data
					if (json.GetArray ("NoteBook").Length > 7) {
						tempString = json.GetArray ("NoteBook") [7].ToString ();
						jsonNotebookItem = JSONObject.Parse (tempString);
						tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
						for (int i = 0; i < tempJSONStringArray.Length; i++) {
							Global.userL4NoteBookDataBlockOne [i] = tempJSONStringArray [i].ToString();
						}
					}
					if (json.GetArray ("NoteBook").Length > 8) {
						tempString = json.GetArray ("NoteBook") [8].ToString ();
						jsonNotebookItem = JSONObject.Parse (tempString);
						tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
						for (int i = 0; i < tempJSONStringArray.Length; i++) {
							Global.userL4NoteBookStepsBlocks [i] = tempJSONStringArray [i].ToString();
						}
					}
					if (json.GetArray ("NoteBook").Length > 9) {
						tempString = json.GetArray ("NoteBook") [9].ToString ();
						jsonNotebookItem = JSONObject.Parse (tempString);
						tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
						for (int i = 0; i < tempJSONStringArray.Length; i++) {
							Global.userL4NoteBookDataBlockTwo [i] = tempJSONStringArray [i].ToString();
						}
					}
					// Level 5 Data
					if (json.GetArray ("NoteBook").Length > 10) {
						tempString = json.GetArray ("NoteBook") [10].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
						for (int i = 0; i < tempJSONStringArray.Length; i++) {
								Global.userL5NoteBookDataBlocks [i] = tempJSONStringArray [i].ToString();
						}
					}
					if (json.GetArray ("NoteBook").Length > 11) {
						tempString = json.GetArray ("NoteBook") [11].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
						for (int i = 0; i < tempJSONStringArray.Length; i++) {
								Global.userL5NoteBookManualBlocks [i] = tempJSONStringArray [i].ToString();
						}
					}
					// Level 6 Data
					if (json.GetArray ("NoteBook").Length > 12) {
						tempString = json.GetArray ("NoteBook") [12].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
						for (int i = 0; i < tempJSONStringArray.Length; i++) {
								Global.userL6NoteBookDataBlocks [i] = tempJSONStringArray [i].ToString();
						}
					}
					if (json.GetArray ("NoteBook").Length > 13) {
						tempString = json.GetArray ("NoteBook") [13].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
						for (int i = 0; i < tempJSONStringArray.Length; i++) {
								Global.userL6NoteBookRapaBlocks [i] = tempJSONStringArray [i].ToString();
						}
					}
					if (json.GetArray ("NoteBook").Length > 14) {
						tempString = json.GetArray ("NoteBook") [14].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempJSONStringArray = jsonNotebookItem.GetArray("Answer");//tempString.Split (',');
						for (int i = 0; i < tempJSONStringArray.Length; i++) {
								Global.userL6NoteBookManualBlocks [i] = tempJSONStringArray [i].ToString();
						}
					}
					break;
				}
				;
			}
		}

	  }

	}
	/*
	public IEnumerator ReadUserProgressNoteBookDataFromServer_NewJsonSchema(int _levelNum)
	{
		WWWForm form = new WWWForm ();
		form.AddField ("fakeformNotebook", "xyxy");

		yield return m_ProcessNotebookDataGetRetriever = new WWW (GetUserProcessGetURL_NewJsonSchema (_levelNum));

		if (m_ProcessNotebookDataGetRetriever.isDone) {
			Debug.Log ("<NOTEBOOK>:" + m_ProcessNotebookDataGetRetriever.text);
		}

		string test = m_ProcessNotebookDataGetRetriever.text;
		Debug.Log ("The data from server is === " + test);
		JSONObject json = new JSONObject ();
		JSONObject jsonNotebookItem = new JSONObject ();
		string tempNum;
		string tempFloat;
		string tempString;
		string[] tempStringArray;
		JSONValue tempVal;
		JSONArray tempArray;
		json = JSONObject.Parse (test);
		Global.lastLevelNoteBookData = "";
		json = json.GetObject ("UserResponse");
		if (null != json.GetArray("NoteBook")) {
			Global.lastLevelNoteBookData = json.GetArray ("NoteBook").ToString ();

			if (null != json) {


				if (_levelNum == 2) {
					if (json.GetArray ("NoteBook").Length > 1) {
						tempString = json.GetArray ("NoteBook") [0].ToString ();
						jsonNotebookItem = JSONObject.Parse (tempString);
						tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
						tempString = tempString.Replace ("[", "");
						tempString = tempString.Replace ("]", "");
						tempStringArray = tempString.Split (',');
						for (int i = 0; i < tempStringArray.Length; i++) {
							Global.userL1NoteBookDataBlocks [i] = tempStringArray [i];
						}
					}
					if (json.GetArray ("NoteBook").Length > 1) {
						tempString = json.GetArray ("NoteBook") [1].ToString ();
						jsonNotebookItem = JSONObject.Parse (tempString);
						tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
						tempString = tempString.Replace ("[", "");
						tempString = tempString.Replace ("]", "");
						tempStringArray = tempString.Split (',');
						for (int i = 0; i < tempStringArray.Length; i++) {
							Global.userL1NoteBookManualBlocks [i] = tempStringArray [i];
						}
					}
				}
				if (_levelNum >= 3) {
					tempArray = json.GetArray ("NoteBook");
					//Debug.Log (tempArray);
					int j = 0;
					if (_levelNum == 3) {
						j = 1;
					} else if (_levelNum == 4) {
						j = 2;
					} else if (_levelNum == 5) {
						j = 3;
					} else if (_levelNum == 6) {
						j = 4;
					} else if (_levelNum == 7) {
						j = 5;
					}
					switch (j) {
					case 1://Show in Level 3
						// Level 1 Data
						if (json.GetArray ("NoteBook").Length > 1) {
							tempString = json.GetArray ("NoteBook") [1].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL1NoteBookDataBlocks [i] = tempStringArray [i];
							}
						}
						if (json.GetArray ("NoteBook").Length > 3) {
							tempString = json.GetArray ("NoteBook") [3].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL1NoteBookManualBlocks [i] = tempStringArray [i];
							}
						}
						// Level 2 Data
						if (json.GetArray ("NoteBook").Length > 5) {
							tempString = json.GetArray ("NoteBook") [5].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL2NoteBookDataBlocks [i] = tempStringArray [i];
							}
						}
						if (json.GetArray ("NoteBook").Length > 7) {
							tempString = json.GetArray ("NoteBook") [7].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL2NoteBookManualBlocks [i] = tempStringArray [i];
							}
						}
						break;
					case 2://Show in Level 4
						// Level 1 Data
						if (json.GetArray ("NoteBook").Length > 1) {
							tempString = json.GetArray ("NoteBook") [1].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL1NoteBookDataBlocks [i] = tempStringArray [i];
							}
						}
						if (json.GetArray ("NoteBook").Length > 3) {
							tempString = json.GetArray ("NoteBook") [3].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL1NoteBookManualBlocks [i] = tempStringArray [i];
							}
						}
						// Level 2 Data
						if (json.GetArray ("NoteBook").Length > 5) {
							tempString = json.GetArray ("NoteBook") [5].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL2NoteBookDataBlocks [i] = tempStringArray [i];
							}
						}
						if (json.GetArray ("NoteBook").Length > 7) {
							tempString = json.GetArray ("NoteBook") [7].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL2NoteBookManualBlocks [i] = tempStringArray [i];
							}
						}
						//Level 3 data
						if (json.GetArray ("NoteBook").Length > 9) {
							tempString = json.GetArray ("NoteBook") [9].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL3NoteBookDataBlockOne [i] = tempStringArray [i];
							}
						}
						if (json.GetArray ("NoteBook").Length > 11) {
							tempString = json.GetArray ("NoteBook") [11].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL3NoteBookDataBlockTwo [i] = tempStringArray [i];
							}
						}
						if (json.GetArray ("NoteBook").Length > 13) {
							tempString = json.GetArray ("NoteBook") [13].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL3NoteBookDataBlockThree [i] = tempStringArray [i];
							}
						}
						break;
					case 3://Show in Level 5
						// Level 1 Data
						if (json.GetArray ("NoteBook").Length > 1) {
							tempString = json.GetArray ("NoteBook") [1].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL1NoteBookDataBlocks [i] = tempStringArray [i];
							}
						}
						if (json.GetArray ("NoteBook").Length > 3) {
							tempString = json.GetArray ("NoteBook") [3].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL1NoteBookManualBlocks [i] = tempStringArray [i];
							}
						}
						// Level 2 Data
						if (json.GetArray ("NoteBook").Length > 5) {
							tempString = json.GetArray ("NoteBook") [5].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL2NoteBookDataBlocks [i] = tempStringArray [i];
							}
						}
						if (json.GetArray ("NoteBook").Length > 7) {
							tempString = json.GetArray ("NoteBook") [7].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL2NoteBookManualBlocks [i] = tempStringArray [i];
							}
						}
						//Level 3 data
						if (json.GetArray ("NoteBook").Length > 9) {
							tempString = json.GetArray ("NoteBook") [9].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL3NoteBookDataBlockOne [i] = tempStringArray [i];
							}
						}
						if (json.GetArray ("NoteBook").Length > 11) {
							tempString = json.GetArray ("NoteBook") [11].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL3NoteBookDataBlockTwo [i] = tempStringArray [i];
							}
						}
						if (json.GetArray ("NoteBook").Length > 13) {
							tempString = json.GetArray ("NoteBook") [13].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL3NoteBookDataBlockThree [i] = tempStringArray [i];
							}
						}
						//Level 4 data
						if (json.GetArray ("NoteBook").Length > 15) {
							tempString = json.GetArray ("NoteBook") [15].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL4NoteBookDataBlockOne [i] = tempStringArray [i];
							}
						}
						if (json.GetArray ("NoteBook").Length > 17) {
							tempString = json.GetArray ("NoteBook") [17].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL4NoteBookDataBlockTwo [i] = tempStringArray [i];
							}
						}
						if (json.GetArray ("NoteBook").Length > 19) {
							tempString = json.GetArray ("NoteBook") [19].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL4NoteBookStepsBlocks [i] = tempStringArray [i];
							}
						}
						break;
					case 4://Show in Level 6
						// Level 1 Data
						if (json.GetArray ("NoteBook").Length > 1) {
							tempString = json.GetArray ("NoteBook") [1].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL1NoteBookDataBlocks [i] = tempStringArray [i];
							}
						}
						if (json.GetArray ("NoteBook").Length > 3) {
							tempString = json.GetArray ("NoteBook") [3].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL1NoteBookManualBlocks [i] = tempStringArray [i];
							}
						}
						// Level 2 Data
						if (json.GetArray ("NoteBook").Length > 5) {
							tempString = json.GetArray ("NoteBook") [5].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL2NoteBookDataBlocks [i] = tempStringArray [i];
							}
						}
						if (json.GetArray ("NoteBook").Length > 7) {
							tempString = json.GetArray ("NoteBook") [7].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL2NoteBookManualBlocks [i] = tempStringArray [i];
							}
						}
						//Level 3 data
						if (json.GetArray ("NoteBook").Length > 9) {
							tempString = json.GetArray ("NoteBook") [9].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL3NoteBookDataBlockOne [i] = tempStringArray [i];
							}
						}
						if (json.GetArray ("NoteBook").Length > 11) {
							tempString = json.GetArray ("NoteBook") [11].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL3NoteBookDataBlockTwo [i] = tempStringArray [i];
							}
						}
						if (json.GetArray ("NoteBook").Length > 13) {
							tempString = json.GetArray ("NoteBook") [13].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL3NoteBookDataBlockThree [i] = tempStringArray [i];
							}
						}
						//Level 4 data
						if (json.GetArray ("NoteBook").Length > 15) {
							tempString = json.GetArray ("NoteBook") [15].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL4NoteBookDataBlockOne [i] = tempStringArray [i];
							}
						}
						if (json.GetArray ("NoteBook").Length > 17) {
							tempString = json.GetArray ("NoteBook") [17].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL4NoteBookDataBlockTwo [i] = tempStringArray [i];
							}
						}
						if (json.GetArray ("NoteBook").Length > 19) {
							tempString = json.GetArray ("NoteBook") [19].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL4NoteBookStepsBlocks [i] = tempStringArray [i];
							}
						}
						// Level 5 Data
						if (json.GetArray ("NoteBook").Length > 21) {
							tempString = json.GetArray ("NoteBook") [21].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL5NoteBookDataBlocks [i] = tempStringArray [i];
							}
						}
						if (json.GetArray ("NoteBook").Length > 23) {
							tempString = json.GetArray ("NoteBook") [23].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL5NoteBookManualBlocks [i] = tempStringArray [i];
							}
						}
						break;
					case 5://Show in Level 7
						//Debug.Log ("Triggered Case 5");
						// Level 1 Data
						if (json.GetArray ("NoteBook").Length > 1) {
							tempString = json.GetArray ("NoteBook") [1].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL1NoteBookDataBlocks [i] = tempStringArray [i];
							}
						}
						if (json.GetArray ("NoteBook").Length > 3) {
							tempString = json.GetArray ("NoteBook") [3].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL1NoteBookManualBlocks [i] = tempStringArray [i];
							}
						}
						// Level 2 Data
						if (json.GetArray ("NoteBook").Length > 5) {
							tempString = json.GetArray ("NoteBook") [5].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL2NoteBookDataBlocks [i] = tempStringArray [i];
							}
						}
						if (json.GetArray ("NoteBook").Length > 7) {
							tempString = json.GetArray ("NoteBook") [7].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL2NoteBookManualBlocks [i] = tempStringArray [i];
							}
						}
						//Level 3 data
						if (json.GetArray ("NoteBook").Length > 9) {
							tempString = json.GetArray ("NoteBook") [9].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL3NoteBookDataBlockOne [i] = tempStringArray [i];
							}
						}
						if (json.GetArray ("NoteBook").Length > 11) {
							tempString = json.GetArray ("NoteBook") [11].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL3NoteBookDataBlockTwo [i] = tempStringArray [i];
							}
						}
						if (json.GetArray ("NoteBook").Length > 13) {
							tempString = json.GetArray ("NoteBook") [13].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL3NoteBookDataBlockThree [i] = tempStringArray [i];
							}
						}
						//Level 4 data
						if (json.GetArray ("NoteBook").Length > 15) {
							tempString = json.GetArray ("NoteBook") [15].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL4NoteBookDataBlockOne [i] = tempStringArray [i];
							}
						}
						if (json.GetArray ("NoteBook").Length > 17) {
							tempString = json.GetArray ("NoteBook") [17].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL4NoteBookDataBlockTwo [i] = tempStringArray [i];
							}
						}
						if (json.GetArray ("NoteBook").Length > 19) {
							tempString = json.GetArray ("NoteBook") [19].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL4NoteBookStepsBlocks [i] = tempStringArray [i];
							}
						}
						// Level 5 Data
						if (json.GetArray ("NoteBook").Length > 21) {
							tempString = json.GetArray ("NoteBook") [21].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL5NoteBookDataBlocks [i] = tempStringArray [i];
							}
						}
						if (json.GetArray ("NoteBook").Length > 23) {
							tempString = json.GetArray ("NoteBook") [23].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL5NoteBookManualBlocks [i] = tempStringArray [i];
							}
						}
						// Level 6 Data
						if (json.GetArray ("NoteBook").Length > 25) {
							tempString = json.GetArray ("NoteBook") [25].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL6NoteBookDataBlocks [i] = tempStringArray [i];
							}
						}
						if (json.GetArray ("NoteBook").Length > 27) {
							tempString = json.GetArray ("NoteBook") [27].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL6NoteBookManualBlocks [i] = tempStringArray [i];
							}
						}
						if (json.GetArray ("NoteBook").Length > 29) {
							tempString = json.GetArray ("NoteBook") [29].ToString ();
							jsonNotebookItem = JSONObject.Parse (tempString);
							tempString = jsonNotebookItem.GetValue ("Answer").ToString ();
							tempString = tempString.Replace ("[", "");
							tempString = tempString.Replace ("]", "");
							tempStringArray = tempString.Split (',');
							for (int i = 0; i < tempStringArray.Length; i++) {
								Global.userL6NoteBookRapaBlocks [i] = tempStringArray [i];
							}
						}
						break;
					}
					;
				}
			}

		}

	}
	*/

	//--------------------------------------------------------LEVELS--------------------------------------------------
	/*
	 * This function is used to save the levels of user level
	 * */
	/*//CHANGES_KEY_NEW_JSON_SCHEMA 
	public string GetUserLevePostURL(int _levelNum)
	{
		string info = "{\"token\":\"";
		if(Application.isEditor){
			info += m_sGameLoginToken;
		}
		else if(!Application.isEditor){
			info += m_sGameDynamicToken;	
		}
		info += "\",\"levelID\":";
		info += _levelNum;
		info += ",";
		info += GetUserLevelData();
		info += "}";
		string urlLevel = m_sDynamicURL + "levels?jsondata=";
	
		string m_sLevelsURL = urlLevel + WWW.EscapeURL(info);
		
		return m_sLevelsURL;
	}
	*///CHANGES_KEY_NEW_JSON_SCHEMA 
	public string GetUserLevePostURL_NewJsonSchema(int _levelNum)
	{
		string info = "{";//\"token\":\"";
		/*if(Application.isEditor){
			info += m_sGameLoginToken;
		}
		else if(!Application.isEditor){
			info += m_sGameDynamicToken;	
		}*/
		//info += "\",\"levelID\":";
		//info += _levelNum;
		//info += ",";
		info += GetUserLevelData();
		info += "}";
		string urlLevel = "";//m_sDynamicURL + "Character?sid=" ;

		string m_sLevelsURL =info;

		return m_sLevelsURL;
	}

	/*//CHANGES_KEY_NEW_JSON_SCHEMA 
	public string GetUserLeveGetURL(int _levelNum)
	{
		string info = "{\"token\":\"";
		if(Application.isEditor){
			info += m_sGameLoginToken;
		}
		else if(!Application.isEditor){
			info += m_sGameDynamicToken;	
		}
		info += "}";
		string urlLevel = m_sDynamicURL + "levels?jsondata=";
	
		string m_sLevelsURL = urlLevel + WWW.EscapeURL(info);

		Debug.Log ("m_sLevelsURL: " + m_sLevelsURL);

		return m_sLevelsURL;
	}
*///CHANGES_KEY_NEW_JSON_SCHEMA 
	public string GetUserLeveGetURL_NewJsonSchema(int _levelNum)
	{
		
		string info = "";
		if(Application.isEditor){
			info += m_sGameLoginToken;
		}
		else if(!Application.isEditor){
			info += m_sGameDynamicToken;	
		}
		info += "";
		string urlLevel = m_sDynamicURL + "Character?sid=" + m_sGameLoginToken;

		string m_sLevelsURL = urlLevel;

		Debug.Log ("m_sLevelsURL: " + m_sLevelsURL);

		return m_sLevelsURL;
	}

	/*
	 * This function saves the user data to the server
	 * */
	//CHANGES_KEY_NEW_JSON_SCHEMA - Nothing changes
	public string GetUserLevelData()
	{	
		string levelDataString = "\"Character\":{";
		levelDataString += "\"userGender\":" + "\"" + Global.CurrentPlayerName.ToString() + "\",";
		levelDataString += "\"userHairStyle\":" + Global.userHairStyleNum.ToString() + ",";
		levelDataString += "\"userHairColor\":[" + Global.userHairColor.r.ToString()
			+ "," + Global.userHairColor.g.ToString()
			+ "," + Global.userHairColor.b.ToString()
			+ "," + Global.userHairColor.a.ToString();
		levelDataString += "],\"userSkinColor\":[" + Global.userSkinColor.r.ToString()
			+ "," + Global.userSkinColor.g.ToString()
			+ "," + Global.userSkinColor.b.ToString()
			+ "," + Global.userSkinColor.a.ToString();
		levelDataString += "],\"userEyeColor\":[" + Global.userEyeColor.r.ToString()
			+ "," + Global.userEyeColor.g.ToString()
			+ "," + Global.userEyeColor.b.ToString()
			+ "," + Global.userEyeColor.a.ToString();
		levelDataString += "],\"userClothColor\":[" + Global.userClothColor.r.ToString()
			+ "," + Global.userClothColor.g.ToString()
			+ "," + Global.userClothColor.b.ToString()
			+ "," + Global.userClothColor.a.ToString();
		levelDataString += "],\"userTrimTextureNum\":" + Global.userTrimTextureNum.ToString();
		levelDataString += ",\"userTrimColor\":[" + Global.userTrimColor.r.ToString()
			+ "," + Global.userTrimColor.g.ToString()
			+ "," + Global.userTrimColor.b.ToString()
			+ "," + Global.userTrimColor.a.ToString();
		levelDataString += "],\"userBackPackNum\":" + Global.userBackPackNum.ToString() + "}";
						
		return levelDataString;
	}	

	/*//CHANGES_KEY_NEW_JSON_SCHEMA 
	public IEnumerator SaveUserLevelToServer(int _levelNum)//DIEGO TEST
	{	
		WWWForm form = new WWWForm();
		form.AddField("id", m_sGameDynamicToken);
		yield return m_LevelDataPostRetriever = new WWW(GetUserLevePostURL_NewJsonSchema(_levelNum));
		Debug.Log ("The user level data POST URL is = " + GetUserLevePostURL_NewJsonSchema(_levelNum));
	}	//DIEGO TEST
	*///CHANGES_KEY_NEW_JSON_SCHEMA 

	public IEnumerator SaveUserLevelToServer_NewJsonSchema(int _levelNum)
	{	

		var postScoreURL = m_sDynamicURL + "Character?sid=" + m_sGameDynamicToken;
		Debug.Log ("<URL CALL TO SERVER>" + postScoreURL);
		var jsonString = GetUserLevePostURL_NewJsonSchema (_levelNum);
		var encoding = new System.Text.UTF8Encoding();
		Dictionary<string, string> postHeader = new Dictionary<string, string>();
		postHeader["Content-Type"] = "application/json";
		//postHeader ["Content-Length"] = jsonString.Length.ToString();

		yield return m_LevelDataPostRetriever = new WWW(postScoreURL, encoding.GetBytes(jsonString), postHeader);

	//	Debug.Log ("The user level data POST URL is = " + postScoreURL);

	}

	/*//CHANGES_KEY_NEW_JSON_SCHEMA 
	public IEnumerator ReadUserLevelDataFromServer(int _levelNum)
	{
		Debug.Log("Started Player Data Retrival");
		WWWForm form = new WWWForm();
		form.AddField("fakefake", "xyxy");

		yield return m_LevelDataGetRetriever = new WWW(GetUserLeveGetURL(_levelNum), form);
		
		string test = m_LevelDataGetRetriever.text;

		JSONObject json = new JSONObject();
		json = JSONObject.Parse(test);
		if (null != json){
			Debug.Log ("THIS IS THE LABEL -->" + json);
			//json = JSONObject.Parse(test);
			string tempNum;
			string tempFloat;
			string tempString;
			JSONValue tempVal;
			tempVal= json.GetObject("data").GetArray("levels")[0];
			tempString = tempVal.ToString();
			json = JSONObject.Parse(tempString);
			json = json.GetObject("levelData");

			Global.CurrentPlayerName = json.GetString("userGender");
			Debug.Log("Player Data loaded: " + Global.CurrentPlayerName);

			tempNum = json.GetValue("userHairStyle").ToString();
			Global.userHairStyleNum = int.Parse(tempNum);

			JSONArray hairColor = json.GetArray("userHairColor");
			tempFloat = hairColor[0].ToString();
			Global.userHairColor.r = float.Parse(tempFloat);
			tempFloat = hairColor[1].ToString();
			Global.userHairColor.g = float.Parse(tempFloat);
			tempFloat = hairColor[2].ToString();
			Global.userHairColor.b = float.Parse(tempFloat);
			tempFloat = hairColor[3].ToString();
			Global.userHairColor.a = float.Parse(tempFloat);
			
			JSONArray skinColor = json.GetArray("userSkinColor");
			tempFloat = skinColor[0].ToString();
			Global.userSkinColor.r = float.Parse(tempFloat);
			tempFloat = skinColor[1].ToString();
			Global.userSkinColor.g = float.Parse(tempFloat);
			tempFloat = skinColor[2].ToString();
			Global.userSkinColor.b = float.Parse(tempFloat);
			tempFloat = skinColor[3].ToString();
			Global.userSkinColor.a = float.Parse(tempFloat);
			
			JSONArray eyeColor = json.GetArray("userEyeColor");
			tempFloat = eyeColor[0].ToString();
			Global.userEyeColor.r = float.Parse(tempFloat);
			tempFloat = eyeColor[1].ToString();
			Global.userEyeColor.g = float.Parse(tempFloat);
			tempFloat = eyeColor[2].ToString();
			Global.userEyeColor.b = float.Parse(tempFloat);
			tempFloat = eyeColor[3].ToString();
			Global.userEyeColor.a = float.Parse(tempFloat);
			
			JSONArray clothColor = json.GetArray("userClothColor");
			tempFloat = clothColor[0].ToString();
			Global.userClothColor.r = float.Parse(tempFloat);
			tempFloat = clothColor[1].ToString();
			Global.userClothColor.g = float.Parse(tempFloat);
			tempFloat = clothColor[2].ToString();
			Global.userClothColor.b = float.Parse(tempFloat);
			tempFloat = clothColor[3].ToString();
			Global.userClothColor.a = float.Parse(tempFloat);
			
			tempNum = json.GetValue("userTrimTextureNum").ToString();
			Global.userTrimTextureNum = int.Parse(tempNum);
			
			JSONArray trimColor = json.GetArray("userTrimColor");
			tempFloat = trimColor[0].ToString();
			Global.userTrimColor.r = float.Parse(tempFloat);
			tempFloat = trimColor[1].ToString();
			Global.userTrimColor.g = float.Parse(tempFloat);
			tempFloat = trimColor[2].ToString();
			Global.userTrimColor.b = float.Parse(tempFloat);
			tempFloat = trimColor[3].ToString();
			Global.userTrimColor.a = float.Parse(tempFloat);
			
			tempNum = json.GetValue("userBackPackNum").ToString();
			Global.userBackPackNum = int.Parse(tempNum);
		}
	}
	*///CHANGES_KEY_NEW_JSON_SCHEMA 

	public IEnumerator ReadUserLevelDataFromServer_NewJsonSchema(int _levelNum)
	{
		Debug.Log("Started Player Data Retrival");
		WWWForm form = new WWWForm();
		form.AddField("fakefake", "xyxy");

		yield return m_LevelDataGetRetriever = new WWW(GetUserLeveGetURL_NewJsonSchema(_levelNum));

		if (m_LevelDataGetRetriever.isDone){
			
			Debug.Log ("<CHARACTER>:" + m_LevelDataGetRetriever.text);
		

		string test = m_LevelDataGetRetriever.text;

			if (test == "{}" || String.IsNullOrEmpty (test)) {
				return false;
			}

		JSONObject json = new JSONObject();
		json = JSONObject.Parse(test);
		if (null != json){
			//json = JSONObject.Parse(test);
			string tempNum;
			string tempFloat;
			string tempString;
			JSONValue tempVal;
			//tempVal= json.GetObject("data").GetArray("levels")[0];
			//tempString = tempVal.ToString();
			//json = JSONObject.Parse(tempString);
			//json = json.GetObject("levelData");

			Global.CurrentPlayerName = json.GetObject("Character").GetString("userGender");
			Debug.Log("Player Data loaded: " + Global.CurrentPlayerName); 

			tempNum = json.GetObject("Character").GetNumber("userHairStyle").ToString();
			Global.userHairStyleNum = int.Parse(tempNum);

			JSONArray hairColor = json.GetObject("Character").GetArray("userHairColor");
			tempFloat = hairColor[0].ToString();
			Global.userHairColor.r = float.Parse(tempFloat);
			tempFloat = hairColor[1].ToString();
			Global.userHairColor.g = float.Parse(tempFloat);
			tempFloat = hairColor[2].ToString();
			Global.userHairColor.b = float.Parse(tempFloat);
			tempFloat = hairColor[3].ToString();
			Global.userHairColor.a = float.Parse(tempFloat);

			JSONArray skinColor = json.GetObject("Character").GetArray("userSkinColor");
			tempFloat = skinColor[0].ToString();
			Global.userSkinColor.r = float.Parse(tempFloat);
			tempFloat = skinColor[1].ToString();
			Global.userSkinColor.g = float.Parse(tempFloat);
			tempFloat = skinColor[2].ToString();
			Global.userSkinColor.b = float.Parse(tempFloat);
			tempFloat = skinColor[3].ToString();
			Global.userSkinColor.a = float.Parse(tempFloat);

			JSONArray eyeColor = json.GetObject("Character").GetArray("userEyeColor");
			tempFloat = eyeColor[0].ToString();
			Global.userEyeColor.r = float.Parse(tempFloat);
			tempFloat = eyeColor[1].ToString();
			Global.userEyeColor.g = float.Parse(tempFloat);
			tempFloat = eyeColor[2].ToString();
			Global.userEyeColor.b = float.Parse(tempFloat);
			tempFloat = eyeColor[3].ToString();
			Global.userEyeColor.a = float.Parse(tempFloat);

			JSONArray clothColor = json.GetObject("Character").GetArray("userClothColor");
			tempFloat = clothColor[0].ToString();
			Global.userClothColor.r = float.Parse(tempFloat);
			tempFloat = clothColor[1].ToString();
			Global.userClothColor.g = float.Parse(tempFloat);
			tempFloat = clothColor[2].ToString();
			Global.userClothColor.b = float.Parse(tempFloat);
			tempFloat = clothColor[3].ToString();
			Global.userClothColor.a = float.Parse(tempFloat);

			tempNum = json.GetObject("Character").GetValue("userTrimTextureNum").ToString();
			Global.userTrimTextureNum = int.Parse(tempNum);

			JSONArray trimColor = json.GetObject("Character").GetArray("userTrimColor");
			tempFloat = trimColor[0].ToString();
			Global.userTrimColor.r = float.Parse(tempFloat);
			tempFloat = trimColor[1].ToString();
			Global.userTrimColor.g = float.Parse(tempFloat);
			tempFloat = trimColor[2].ToString();
			Global.userTrimColor.b = float.Parse(tempFloat);
			tempFloat = trimColor[3].ToString();
			Global.userTrimColor.a = float.Parse(tempFloat);

			tempNum = json.GetObject("Character").GetValue("userBackPackNum").ToString();
			Global.userBackPackNum = int.Parse(tempNum);
			}
		}
	}
	//--------------------------------------------------------LEVEL START END EVENT--------------------------------------------------
	/*
	 * This function is used to save the PHQA of user level
	 * */
	/*//CHANGES_KEY_NEW_JSON_SCHEMA 
	public string GetUserLevelStartEndEventPostURL(int _eventID, int _levelNum)
	{
		string ComputerTimeStamp = System.DateTime.UtcNow.Hour.ToString() + ":" + System.DateTime.UtcNow.Minute.ToString()
														+ ":" + System.DateTime.UtcNow.Second.ToString();
		
		string info = "{\"token\":\"";
		if(Application.isEditor){
			info += m_sGameLoginToken;
		}
		else if(!Application.isEditor){
			info += m_sGameDynamicToken;	
		}
		info += "\",\"events\":[{\"eventID\":";
		info += _eventID;
		info += ",\"ts\":\"";
		info += ComputerTimeStamp + "\"";
		info += ",\"data\":";
		info += _levelNum;
		info += "}]}";
		string urlEvent = m_sDynamicURL + "event?jsondata=";
	
		string m_sEventURL = urlEvent + WWW.EscapeURL(info);
		
		return m_sEventURL;
	}
	*///CHANGES_KEY_NEW_JSON_SCHEMA 

	public string GetUserLevelStartEndEventPostURL_NewJsonSchema(int _eventID, int _levelNum)
	{

		#if UNITY_WEBGL && !UNITY_EDITOR
		string ComputerTimeStamp =   System.DateTime.UtcNow.AddHours(0).Year.ToString().PadLeft(2,'0') + "-" + System.DateTime.UtcNow.AddHours(0).Month.ToString().PadLeft(2,'0') + "-" + System.DateTime.UtcNow.AddHours(0).Day.ToString().PadLeft(2,'0') + "T" + System.DateTime.UtcNow.AddHours(0).Hour.ToString().PadLeft(2,'0') + ":" + System.DateTime.UtcNow.AddHours(0).Minute.ToString().PadLeft(2,'0') + ":" + System.DateTime.UtcNow.AddHours(0).Second.ToString().PadLeft(2,'0');
		#else
		string ComputerTimeStamp =   System.DateTime.UtcNow.Year.ToString().PadLeft(2,'0') + "-" + System.DateTime.UtcNow.Month.ToString().PadLeft(2,'0') + "-" + System.DateTime.UtcNow.Day.ToString().PadLeft(2,'0') + "T" + System.DateTime.UtcNow.Hour.ToString().PadLeft(2,'0') + ":" + System.DateTime.UtcNow.Minute.ToString().PadLeft(2,'0') + ":" + System.DateTime.UtcNow.Second.ToString().PadLeft(2,'0');		
		#endif

		string info = "{\"userEvent\":{";
		info += "\"type\":";
		info += _eventID;
		//info += "\",\"data\":{\"answers\":[";
		//info += _eventID;
		//info += "]},\"level\":\"";
		info += ",\"level\":";
		info += _levelNum;
		info += ",\"timestamp\":\"";
		info += ComputerTimeStamp;
		info += "\"}}";
		//string urlEvent = m_sDynamicURL + "event?jsondata=";

		string m_sEventURL = info;

		return m_sEventURL;
	}


	public string GetUserLevelSavePointEventPostURL_NewJsonSchema(int _eventID, int _levelNum, string SavingPoint)
	{
		#if UNITY_WEBGL && !UNITY_EDITOR 
		string ComputerTimeStamp =   System.DateTime.UtcNow.AddHours(0).Year.ToString().PadLeft(2,'0') + "-" + System.DateTime.UtcNow.AddHours(0).Month.ToString().PadLeft(2,'0') + "-" + System.DateTime.UtcNow.AddHours(0).Day.ToString().PadLeft(2,'0') + "T" + System.DateTime.UtcNow.AddHours(0).Hour.ToString().PadLeft(2,'0') + ":" + System.DateTime.UtcNow.AddHours(0).Minute.ToString().PadLeft(2,'0') + ":" + System.DateTime.UtcNow.AddHours(0).Second.ToString().PadLeft(2,'0');
		#else
		string ComputerTimeStamp =   System.DateTime.UtcNow.Year.ToString().PadLeft(2,'0') + "-" + System.DateTime.UtcNow.Month.ToString().PadLeft(2,'0') + "-" + System.DateTime.UtcNow.Day.ToString().PadLeft(2,'0') + "T" + System.DateTime.UtcNow.Hour.ToString().PadLeft(2,'0') + ":" + System.DateTime.UtcNow.Minute.ToString().PadLeft(2,'0') + ":" + System.DateTime.UtcNow.Second.ToString().PadLeft(2,'0');		
		#endif

		string info = "{\"userEvent\":{";
		info += "\"type\":";
		info += _eventID;
		//info += "\",\"data\":{\"answers\":[";
		//info += _eventID;
		//info += "]},\"level\":\"";
		info += ",\"level\":";
		info += _levelNum;
		info += ",\"timestamp\":\"";
		info += ComputerTimeStamp;
		info += "\"";
		info += ",\"data\": {";
		info += "\"savepoint\": \"";
		info += SavingPoint;
		info += "\"}}}";

		//string urlEvent = m_sDynamicURL + "event?jsondata=";

		string m_sEventURL = info;

		return m_sEventURL;
	}


	/*//CHANGES_KEY_NEW_JSON_SCHEMA 
	public IEnumerator SaveLevelStartEndEventToServer(int _eventID, int _levelNum)
	{
		WWWForm form = new WWWForm();
		form.AddField("fake", "xy");

		string nameurl = GetUserLevelStartEndEventPostURL(_eventID, _levelNum);
		yield return m_EventLevelStartEndDataPostRetriever = new WWW(nameurl, form);
		Debug.Log ("Game Login Token: " + m_sGameLoginToken);
		Debug.Log ("The user level start end event data POST URL is = " + nameurl);
	}
	
	*///CHANGES_KEY_NEW_JSON_SCHEMA 



	public IEnumerator SaveLevelStartEndEventToServer_NewJsonSchema(int _eventID, int _levelNum)
	{
		Application.ExternalCall("gamelog", "Middle... = " + _eventID);
		var postScoreURL = m_sDynamicURL + "event?sid=" + m_sGameDynamicToken;
		Debug.Log ("<URL CALL TO SERVER>" + postScoreURL);
		var jsonString =GetUserLevelStartEndEventPostURL_NewJsonSchema(_eventID, _levelNum);
		var encoding = new System.Text.UTF8Encoding ();
		Dictionary<string, string> postHeader = new Dictionary<string, string> ();
		postHeader ["Content-Type"] = "application/json";
		//postHeader ["Content-Length"] = jsonString.Length.ToString ();

		m_sSaveURLReturn = null;
		yield return m_sSaveURLReturn = new WWW (postScoreURL, encoding.GetBytes (jsonString), postHeader);
		Debug.Log ("<EVENT LEVEL START / END>STRING:" +jsonString);

		Debug.Log ("Game Login Token: " + m_sGameLoginToken);
		Debug.Log ("The user level start end event data POST URL is = " + jsonString);

		if(m_sSaveURLReturn.isDone){
			Debug.Log ("<EVENT LEVEL START / END>:" + m_sSaveURLReturn.text);
		}
	}

	////CHANGES_KEY_NEW_JSON_SCHEMA 
	public IEnumerator SavePointEventToServer_NewJsonSchema(int _eventID, int _levelNum, string SavingPoint)
	{
		Application.ExternalCall("gamelog", "Middle... = " + _eventID);
		var postScoreURL = m_sDynamicURL + "event?sid=" + m_sGameDynamicToken;
		Debug.Log ("<URL CALL TO SERVER>" + postScoreURL);
		var jsonString = GetUserLevelSavePointEventPostURL_NewJsonSchema (_eventID, _levelNum, SavingPoint);
		var encoding = new System.Text.UTF8Encoding ();
		Dictionary<string, string> postHeader = new Dictionary<string, string> ();
		postHeader ["Content-Type"] = "application/json";
		//postHeader ["Content-Length"] = jsonString.Length.ToString ();

		m_sSaveURLReturn = null;
		yield return m_sSaveURLReturn = new WWW (postScoreURL, encoding.GetBytes (jsonString), postHeader);
		Debug.Log ("<EVENT LEVEL START / END>STRING:" +jsonString);

		Debug.Log ("Game Login Token: " + m_sGameLoginToken);
		Debug.Log ("The user level start end event data POST URL is = " + jsonString);

		if(m_sSaveURLReturn.isDone){
			Debug.Log ("<EVENT LEVEL START / END>:" + m_sSaveURLReturn.text);
		}
	}


	//--------------------------------------------------------PHQA EVENT--------------------------------------------------
	
	/*
	 * This function is used to save the PHQA of user level
	 * */
	/*//CHANGES_KEY_NEW_JSON_SCHEMA 
	public string GetUserPHQAEventPostURL(int _eventID, int _levelNum, int _Q1A, int _Q2A, int _Q3A, int _Q4A, int _Q5A, int _Q6A, int _Q7A, int _Q8A, int _Q9A)
	{
		string ComputerTimeStamp = System.DateTime.UtcNow.Hour.ToString() + ":" + System.DateTime.UtcNow.Minute.ToString()
														+ ":" + System.DateTime.UtcNow.Second.ToString();

		string info = "{\"token\":\"";
		if(Application.isEditor){
			info += m_sGameLoginToken;
		}
		else if(!Application.isEditor){
			info += m_sGameDynamicToken;	
		}
		info += "\",\"events\":[{\"eventID\":";
		info += _eventID;
		info += ",\"ts\":";
		info += "\"" + ComputerTimeStamp + "\"";
		info += ",\"data\":{";
		info += "\"level\":";
		info += _levelNum;
		info += ",\"answers\":[";
		info += _Q1A + "," + _Q2A + "," + _Q3A + "," + _Q4A + "," + _Q5A + "," + _Q6A + "," + _Q7A + "," + _Q8A + "," + _Q9A;
		info += "]}}]}";
		string urlPHQAEvent = m_sDynamicURL + "event?jsondata=";
		
		string m_sPHQAEventURL = urlPHQAEvent + WWW.EscapeURL(info);
		Debug.Log("m_sPHQAEventURL: " + m_sPHQAEventURL);
		return m_sPHQAEventURL;
	}
	*///CHANGES_KEY_NEW_JSON_SCHEMA 
	/*//CHANGES_KEY_NEW_JSON_SCHEMA 
	public IEnumerator SaveUserPHQAEventToServer(int _eventID, int _levelNum, int _Q1A, int _Q2A, int _Q3A, int _Q4A, int _Q5A, int _Q6A, int _Q7A, int _Q8A, int _Q9A)
	{
		Application.ExternalCall("gamelog", "Middle... = " + _eventID);
		WWWForm formSpecial = new WWWForm();
		formSpecial.AddField("fakeSpecial", "xxyy");
		
		string nameurl = GetUserPHQAEventPostURL(_eventID, _levelNum, _Q1A, _Q2A,  _Q3A,  _Q4A,  _Q5A,  _Q6A,  _Q7A,  _Q8A, _Q9A);
		yield return m_EventPHQADataPostRetriever = new WWW(nameurl, formSpecial);
		Debug.Log ("The user level start end event data POST URL is = " + nameurl);
	}
	*///CHANGES_KEY_NEW_JSON_SCHEMA 
	public string GetUserPHQAEventPostURL_NewJsonSchema(int _eventID, int _levelNum, int _Q1A, int _Q2A, int _Q3A, int _Q4A, int _Q5A, int _Q6A, int _Q7A, int _Q8A, int _Q9A)
	{

		#if UNITY_WEBGL && !UNITY_EDITOR 
			string ComputerTimeStamp =   System.DateTime.UtcNow.AddHours(0).Year.ToString().PadLeft(2,'0') + "-" + System.DateTime.UtcNow.AddHours(0).Month.ToString().PadLeft(2,'0') + "-" + System.DateTime.UtcNow.AddHours(0).Day.ToString().PadLeft(2,'0') + "T" + System.DateTime.UtcNow.AddHours(0).Hour.ToString().PadLeft(2,'0') + ":" + System.DateTime.UtcNow.AddHours(0).Minute.ToString().PadLeft(2,'0') + ":" + System.DateTime.UtcNow.AddHours(0).Second.ToString().PadLeft(2,'0');
		#else
			string ComputerTimeStamp =   System.DateTime.UtcNow.Year.ToString().PadLeft(2,'0') + "-" + System.DateTime.UtcNow.Month.ToString().PadLeft(2,'0') + "-" + System.DateTime.UtcNow.Day.ToString().PadLeft(2,'0') + "T" + System.DateTime.UtcNow.Hour.ToString().PadLeft(2,'0') + ":" + System.DateTime.UtcNow.Minute.ToString().PadLeft(2,'0') + ":" + System.DateTime.UtcNow.Second.ToString().PadLeft(2,'0');
		#endif

		string info = "{\"userEvent\":{";
		info += "\"type\":";
		info += _eventID;
		info += ",\"data\":{\"answers\":[";
		info += _Q1A + "," + _Q2A + "," + _Q3A + "," + _Q4A + "," + _Q5A + "," + _Q6A + "," + _Q7A + "," + _Q8A + "," + _Q9A;
		info += "]},\"level\":";
		info += _levelNum;
		info += ",\"timestamp\":\"";
		info += ComputerTimeStamp;
		info += "\"}}";
		string m_sPHQAEventURL = info;
		Debug.Log("m_sPHQAEventURL: " + m_sPHQAEventURL);
		return m_sPHQAEventURL;
	}

	public IEnumerator SaveUserPHQAEventToServer_NewJsonSchema(int _eventID, int _levelNum, int _Q1A, int _Q2A, int _Q3A, int _Q4A, int _Q5A, int _Q6A, int _Q7A, int _Q8A, int _Q9A)
	{
		Application.ExternalCall("gamelog", "Middle... = " + _eventID);
		var postScoreURL = m_sDynamicURL + "event?sid=" + m_sGameDynamicToken;
		Debug.Log ("<URL CALL TO SERVER>" + postScoreURL);
		var jsonString = GetUserPHQAEventPostURL_NewJsonSchema(_eventID, _levelNum, _Q1A, _Q2A,  _Q3A,  _Q4A,  _Q5A,  _Q6A,  _Q7A,  _Q8A, _Q9A);
		var encoding = new System.Text.UTF8Encoding ();
		Dictionary<string, string> postHeader = new Dictionary<string, string> ();
		postHeader ["Content-Type"] = "application/json";
		//postHeader ["Content-Length"] = jsonString.Length.ToString ();

		yield return m_sSaveURLReturn = new WWW (postScoreURL, encoding.GetBytes (jsonString), postHeader);
		Debug.Log ("<EVENT_PHQ>STRING:" + jsonString);

		if(m_sSaveURLReturn.isDone){
			Debug.Log ("<EVENT_PHQ>:" + m_sSaveURLReturn.text);
		}

		//Debug.Log ("The user level start end event data POST URL is = " + jsonString);
	}

	/*
	 **************

		In level save points save and load functions
		
	 **************
	*/
	/*//CHANGES_KEY_NEW_JSON_SCHEMA 
	public string savePointURL(int _levelNum, string _feedback, string _savePoint, bool _levelComplete){
				
		string urlSave = m_sDynamicURL + "save?jsondata=";
		
		string saveurl = "{\"token\":\"";
		if(Application.isEditor){
			saveurl += m_sGameLoginToken;
		}
		else if(!Application.isEditor){
			saveurl += m_sGameDynamicToken;	
		}
		saveurl += "\",\"data\":{\"levelNumber\":";
		saveurl += _levelNum;
		saveurl += ",\"savePoint\": \"";
		saveurl += _savePoint + "\"";
		saveurl += ",\"feedback\":";
		saveurl += _feedback;
		saveurl += ",\"levelComplete\":";
		saveurl += "\"" + _levelComplete + "\"";
		saveurl += "}}";
		
		Debug.Log(saveurl);
		string m_sSaveURI = urlSave + WWW.EscapeURL(saveurl);
		return m_sSaveURI;
	}
	*///CHANGES_KEY_NEW_JSON_SCHEMA 
	public string savePointURL_NewJsonSchema(int _levelNum, string _feedback, string _savePoint, bool _levelComplete){


		string saveurl = "";

		saveurl += "{\"UserResponse\":{\"levelNumber\":";
		saveurl += _levelNum;
		saveurl += ",\"SavePoint\": \"";
		saveurl += _savePoint + "\"";
		saveurl += ",\"FeedBackScore\":";
		saveurl += _feedback;
		saveurl += "}}";
		//saveurl = "{\"UserResponse\":{\"LevelNumber\": 7,\"SavePoint\": \"uno\"}}";
		Debug.Log(saveurl);
		string m_sSaveURI = saveurl;
		return m_sSaveURI;
	}
	/*//CHANGES_KEY_NEW_JSON_SCHEMA 
	public IEnumerator savePointToServer(int _levelNum, string _feedback, string _savePoint, bool _levelComplete){
		Global.toggleGUI = true;

		WWWForm saveForm = new WWWForm();
		//saveForm.AddField("saveFake", "xyxyx");

		string m_sSaveURL = savePointURL_NewJsonSchema(_levelNum, _feedback, _savePoint, _levelComplete);
		Debug.Log("saving Save point data.");
		
		//yield return new WaitForSeconds(3);
		Global.toggleGUI = false;

		yield return m_sSaveURLReturn = new WWW(m_sSaveURL, saveForm);

		if(m_sSaveURLReturn.isDone){
			Debug.Log("<SAVEPOINT>:" + m_sSaveURLReturn.text);
		}
	}
	*///CHANGES_KEY_NEW_JSON_SCHEMA 
	public IEnumerator savePointToServer_NewJsonSchema(int _levelNum, string _feedback, string _savePoint, bool _levelComplete){
		Global.toggleGUI = true;
		//m_sGameDynamicToken = "0ee6e5774c4d860c6af44c4fe7c0dd0f";
		var postScoreURL = m_sDynamicURL + "UserResponse?sid=" + m_sGameDynamicToken;
		Debug.Log ("<URL CALL TO SERVER>" + postScoreURL);
		var jsonString = savePointURL_NewJsonSchema (_levelNum, _feedback, _savePoint, _levelComplete);
		var encoding = new System.Text.UTF8Encoding ();
		Dictionary<string, string> postHeader = new Dictionary<string, string> ();
		postHeader ["Content-Type"] = "application/json";
		//postHeader ["Content-Length"] = jsonString.Length.ToString ();

		yield return m_sSaveURLReturn = new WWW (postScoreURL, encoding.GetBytes (jsonString), postHeader);
		Debug.Log ("<SAVED POINT>STRING:" + jsonString);
		//Debug.Log ("saving Save point data = " + postScoreURL);
		if (m_sSaveURLReturn.isDone) {
			Debug.Log ("<SAVED POINT>:" + m_sSaveURLReturn.text);
			Global.toggleGUI = false;
		}
	}

	public string loadSavePointURL(){
		string urlLoad = m_sDynamicURL + "save?jsondata=";

		string loadData = "{\"token\":\"";
		if(Application.isEditor){
			loadData += m_sGameLoginToken;
		}
		else if(!Application.isEditor){
			loadData += m_sGameDynamicToken;
		}
		loadData += "}}";

		string m_sLoadURI = urlLoad + WWW.EscapeURL(loadData);
		return m_sLoadURI;
	}

	public string loadSavePointURL_NewJsonSchema(){
		
		string urlLoad = m_sDynamicURL + "UserResponse?sid=";
	
		string loadData = "";
		if(Application.isEditor){
			loadData += m_sGameLoginToken;
		}
		else if(!Application.isEditor){
			loadData += m_sGameDynamicToken;
		}
		//loadData += "";

		Debug.Log ("<m_sGameLoginToken>" + m_sGameLoginToken);
		Debug.Log ("<m_sGameDynamicToken>" + m_sGameDynamicToken);

		string m_sLoadURI = urlLoad + loadData;
		Debug.Log ("<URL CALL TO SERVER>" + m_sLoadURI);
		return m_sLoadURI;
	}
	/*//CHANGES_KEY_NEW_JSON_SCHEMA 
	public IEnumerator loadSavePointFromServer(){
		Debug.Log ("On loadSavePointFromServer");
		WWWForm loadForm = new WWWForm();
		loadForm.AddField("loadFake", "xyxyxus");

		string m_sLoadURL = loadSavePointURL();

		yield return m_sLoadURLReturn = new WWW(m_sLoadURL, loadForm);
		
		
		if(m_sLoadURLReturn.isDone){
			string test = m_sLoadURLReturn.text;

			JSONObject json = new JSONObject();
			json = JSONObject.Parse(test);
			string tempNum;
			string tempFloat;
			string tempString;
			bool tempBool;
			JSONValue tempVal;
			Debug.Log ("THIS IS THE LABEL 2 -->" + json);
			if (null != json){
				tempVal= json.GetObject("data").GetObject("save");
				tempString = tempVal.ToString();
				json = JSONObject.Parse(tempString);

				tempString = json.GetString("levelComplete");
				if(tempString == "False"){
					tempBool = false;
				}else{
					tempBool = true;
				}
				if(Application.isEditor && Global.TestingOnline == true){
					tempBool = false;
				}
				if(!tempBool){
					Debug.Log("level not yet ended");
					JSONArray tempArray = json.GetArray("feedback");
					tempNum = tempArray[0].ToString();
					Global.userFeedBackScore[0] = int.Parse(tempNum);
					tempNum = tempArray[1].ToString();
					Global.userFeedBackScore[1] = int.Parse(tempNum);
					tempNum = tempArray[2].ToString();
					Global.userFeedBackScore[2] = int.Parse(tempNum);
					if(json.GetString("savePoint") == null){
						Global.loadedSavePoint = "";
					}else{
					Global.loadedSavePoint = json.GetString("savePoint");
					}
					Global.levelComplete = tempBool;

					tempNum = json.GetValue("levelNumber").ToString();
					Global.savePointLoadLevelNumber = int.Parse(tempNum);
					if(Application.isEditor && Global.TestingOnline == true){
						Global.loadedSavePoint = Global.TestSavePoint;
					}
				}else{
					Debug.Log ("Previous level complete, loading new level.");
					Global.loadedSavePoint = "";
				}
				Debug.Log("The save point form the server is: " + Global.loadedSavePoint);
			}
		}
	} 
	*///CHANGES_KEY_NEW_JSON_SCHEMA 
	public IEnumerator loadSavePointFromServer_NewJsonSchema(){
		Debug.Log ("On loadSavePointFromServer");
		WWWForm loadForm = new WWWForm();
		loadForm.AddField("loadFake", "xyxyxus");

		string m_sLoadURL = loadSavePointURL_NewJsonSchema();

		yield return m_sLoadURLReturn = new WWW(m_sLoadURL);


		if(m_sLoadURLReturn.isDone){

			Debug.Log ("<SAVEPOINT>:" + m_sLoadURLReturn.text);

			string test = m_sLoadURLReturn.text;

			if (test.Contains ("{\"UserResponse\":null}")) {
				Debug.Log ("Replace String:" + "{\"UserResponse\":{\"LevelNumber\":1,\"SavePoint\":\"\",\"FeedbackScore\":[0,0,0],\"Notebook\":null}}");
				test = "{\"UserResponse\":{\"LevelNumber\":1,\"SavePoint\":\"\",\"FeedbackScore\":[0,0,0],\"Notebook\":null}}";
			}

			JSONObject json = new JSONObject();

			json = JSONObject.Parse(test);

			string tempNum;
			string tempFloat;
			string tempString;
			bool tempBool;
			JSONValue tempVal;
			Debug.Log ("THIS IS THE LABEL 2 -->" + json);
			if (null != json){
				tempVal= json.GetObject("UserResponse");
				tempString = tempVal.ToString();
				json = JSONObject.Parse(tempString); 

				if (json.GetString ("SavePoint") == null ) {
					tempString = "levelEnd";// ANGELA ASK FOR SWAP NULL FOR levelEnd
				} else {
					tempString = json.GetString ("SavePoint");
				}
				/*if(tempString == "False"){// TEST DIEGO //CHANGES_KEY_NEW_JSON_SCHEMA
					tempBool = false;
				}else{
					tempBool = true;
				}*/// TEST DIEGO //CHANGES_KEY_NEW_JSON_SCHEMA
				if (tempString == null) {
						tempString = "";
						tempBool = false;
				}else{
					if (!tempString.Contains ("levelEnd")) {// TEST DIEGO //CHANGES_KEY_NEW_JSON_SCHEMA
						tempBool = false;
					} else {
						tempBool = true;
					}
				}
				if(Application.isEditor && Global.TestingOnline == true){
					tempBool = false;
				}
				if(!tempBool){
					Debug.Log("level not yet ended");
					JSONArray tempArray = json.GetArray("FeedBackScore");
					tempNum = tempArray[0].ToString();
					Global.userFeedBackScore[0] = int.Parse(tempNum);
					tempNum = tempArray[1].ToString();
					Global.userFeedBackScore[1] = int.Parse(tempNum);
					tempNum = tempArray[2].ToString();
					Global.userFeedBackScore[2] = int.Parse(tempNum);

					Global.loadedSavePoint = tempString;

					Global.levelComplete = tempBool;

					tempNum = json.GetValue("LevelNumber").ToString();
					Global.savePointLoadLevelNumber = int.Parse(tempNum);
					if(Application.isEditor && Global.TestingOnline == true){
						Global.loadedSavePoint = Global.TestSavePoint;
					}
				}else{
					if (Global.CurrentLevelNumber == 1) {
						Global.levelComplete = !tempBool;
						Global.loadedSavePoint =  "";
					} else {
						Global.levelComplete = tempBool;
						Global.loadedSavePoint =  tempString;
					}
					Debug.Log ("Previous level complete, loading new level.");

				}
				Debug.Log("The save point form the server is: " + Global.loadedSavePoint);
			}
		}
	}

	public void OnGUI ()
	{
		//GUI.Label (new Rect(10, 10, 1000, 2000), m_sDynamicURL);	
	}
}