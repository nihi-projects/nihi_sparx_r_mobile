﻿using UnityEngine;
using System.Collections;

public class levelSaveOnLoad : MonoBehaviour {
	
	private CapturedDataInput instance;
	private string currentSavePoint;
	private bool levelComplete;
	private string feedbackData;
	public static GameObject saveTriggerToggle;

	// Use this for initialization
	void Start () {
		//Connecting to the server
		//instance    = CapturedDataInput.GetInstance;
		instance = GameObject.Find("CapturedDataInputHolder").GetComponent<CapturedDataInput>();
		SaveToServer();

		Debug.Log ("TEST @ line 18");
		/*saveTriggerToggle = GameObject.Find("saveTrigger1");
		if(Application.loadedLevel == 3){
			saveTriggerToggle.SetActive(false);
		}*/
		//why was that there??
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void SaveToServer(){


		Global.levelSSavePoint(Global.CurrentLevelNumber);
		currentSavePoint = Global.m_sSavePointCode;
		
		levelComplete = false;
		
		feedbackData = "[" + Global.userFeedBackScore[0] + ", " + Global.userFeedBackScore[1] + ", " + Global.userFeedBackScore[2] + "]";
		if(!Global.loadedSavePoint.Contains(currentSavePoint)){
			Debug.Log("starting save point saving.");
			StartCoroutine(instance.SavePointEventToServer_NewJsonSchema(5,Global.CurrentLevelNumber,currentSavePoint));
			StartCoroutine(instance.savePointToServer_NewJsonSchema(Global.CurrentLevelNumber, feedbackData, currentSavePoint, levelComplete));
		}
	}
}
