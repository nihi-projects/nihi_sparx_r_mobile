using UnityEngine;
using System.Collections;

public class LadderInteraction : MonoBehaviour {
	
	float fLengthOfLadder;
	public float fPlayerPositionOnLadder;
	
	public bool m_bCorrectLadder;
	public Transform EndPos;
	public GameObject Trigger;
	public bool m_bOnThisLadder;
	
	bool m_bClimbing = false;
	bool m_bReachedTop = false;
	
	// Use this for initialization
	void Start () {
		fLengthOfLadder = this.GetComponent<Renderer>().bounds.extents.y * 2.0f + 1.0f; 
		fPlayerPositionOnLadder = Trigger.transform.position.y;
	}
	
	// Update is called once per frame
	void Update () {
		if(m_bOnThisLadder)
		{
			m_bClimbing = false;
			m_bReachedTop = false;
			if(Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
			{
				ClimbUp();
				if (!m_bReachedTop)
				{
					SoundPlayer.PlayLoopSound("sfx-ladder", 1.0f);
				}
				m_bClimbing = true;
			}
			else if(Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
			{
				ClimbDown();
				SoundPlayer.PlayLoopSound("sfx-ladder", 1.0f);
				m_bClimbing = true;
			}
			
			if (!m_bClimbing)
			{
				Global.GetPlayer().GetComponent<Animation>().enabled = false;
				SoundPlayer.StopLoop("sfx-ladder");
				//Global.GetPlayer().animation.Play ("idle");
			}
			else
			{
				Global.GetPlayer().GetComponent<Animation>().enabled = true;
			}
		}
	}
		
	void ClimbUp()
	{
		float Estimate = fPlayerPositionOnLadder + (LevelFourScript.m_fClimbSpeed * Time.deltaTime);
		if(Estimate >= (transform.position.y + fLengthOfLadder * 0.5f) - LevelFourScript.PlayerHeight * 0.5f)
		{
			if(m_bCorrectLadder && EndPos)
			{
				if(EndPos.name.Contains("Move"))
				{
					// set the player pos to center of the collider.
					Global.GetPlayer().transform.position = EndPos.GetComponent<Collider>().bounds.center;
				}
				else
				{
					// set the player pos to transform pos;
					Global.GetPlayer().transform.position = EndPos.position;
				}
				//Player reaches the top of the ladder
				Global.GetPlayer().GetComponent<Animation>().Play ("idle");
				Global.GetPlayer ().GetComponent<PlayerMovement>().m_bMovement = true;
				Global.CameraSnapBackToPlayer();
				m_bOnThisLadder = false;
			}
			else
			{
				m_bReachedTop = true;
			}
			
			return;
		}
		// if the player is NOT playing climb, or if the player is playing 
		// climb backwards
		if(!Global.GetPlayer().GetComponent<Animation>().IsPlaying("climb") ||
			(Global.GetPlayer ().GetComponent<Animation>().IsPlaying("climb") && 
			 Global.GetPlayer ().GetComponent<Animation>()["climb"].speed == -1.0f))
		{
			Global.GetPlayer ().GetComponent<Animation>()["climb"].speed = 1.0f;
			Global.GetPlayer ().GetComponent<Animation>()["climb"].time = 0;
			Global.GetPlayer ().GetComponent<Animation>().Play ("climb");
		}
		Global.GetPlayer ().transform.position += Vector3.up * LevelFourScript.m_fClimbSpeed * Time.deltaTime;
		fPlayerPositionOnLadder += LevelFourScript.m_fClimbSpeed * Time.deltaTime;
	}
	
	void ClimbDown()
	{
		float Estimate = fPlayerPositionOnLadder - (LevelFourScript.m_fClimbSpeed * Time.deltaTime);
		if(Estimate <=  Trigger.transform.position.y)
		{
			Global.GetPlayer().GetComponent<PlayerMovement>().m_bMovement = true;
			Global.GetPlayer().GetComponent<Animation>().Play ("idle");
			m_bOnThisLadder = false;
			Global.CameraSnapBackToPlayer();
			return;	
		}
		// if the player is NOT playing climb, or if the player is playing 
		// climb forward
		if(!Global.GetPlayer ().GetComponent<Animation>().IsPlaying("climb") || 
			(Global.GetPlayer ().GetComponent<Animation>().IsPlaying("climb") && 
			 Global.GetPlayer ().GetComponent<Animation>()["climb"].speed == 1.0f))
		{
			Global.GetPlayer ().GetComponent<Animation>()["climb"].speed = -1.0f;
			Global.GetPlayer ().GetComponent<Animation>()["climb"].time = Global.GetPlayer ().GetComponent<Animation>()["climb"].length;
			Global.GetPlayer ().GetComponent<Animation>().Play ("climb");
		}
		Global.GetPlayer ().transform.position -= Vector3.up * LevelFourScript.m_fClimbSpeed * Time.deltaTime;
		fPlayerPositionOnLadder -= LevelFourScript.m_fClimbSpeed * Time.deltaTime;
	}
}
