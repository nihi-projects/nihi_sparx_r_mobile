using UnityEngine;
using System.Collections;

public class IceDoorObjMovement : MonoBehaviour {

	//Public variables
	public bool bRotation = false;
	public Vector3 m_vTarget;
	public Vector3 m_vMovement;
	public bool m_bStartMoving = false;
	public Vector3 m_vRotation;
	public bool m_bMovementComplete = false;
	
	//Private variables
	private Vector3 m_vOriginalPosition;
	private float m_fMovementProgress = 0.0f;
	private float m_fRotateYValue = 0.0f;
	private float m_fAngle = 0.0f;
	
	// Use this for initialization
	void Start () {
		if(m_vMovement != Vector3.zero)
		{
			m_vTarget = transform.position + m_vMovement;
		}
		m_vOriginalPosition = transform.position;
		m_fRotateYValue = transform.rotation.y;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(m_bStartMoving && !m_bMovementComplete)
		{
			//Left ice door
			if(gameObject.name.Equals(Global.GetGameObjectByID("32009").name)){//Left door
				//Rotating Forwards
				if(bRotation){
					m_fAngle -= 0.25f;
//					if(m_fAngle >= -7.0f){
//						transform.Rotate(new Vector3(0.0f, 0.0f, 1.0f), m_fAngle);
//					}
					if(!GameObject.Find ("LeftDoorTrigger").GetComponent<LeftDoorTriggerEnter>().m_leftDoorTriggerEntered){//If the left door is still not open
						transform.Rotate(new Vector3(0.0f, 0.0f, 1.0f), m_fAngle);
					}
					else{
						m_bMovementComplete = true;
					}
				}
			}
			//Right ice door
			else if(gameObject.name.Equals(Global.GetGameObjectByID("32010").name)){//Right door
				//Rotating Forwards
				if(bRotation){
					m_fAngle += 0.25f;
//					if(m_fAngle < 7.0f){
//						transform.Rotate(new Vector3(0.0f, 0.0f, 1.0f), m_fAngle);
//					}
					if(!GameObject.Find ("RightDoorTrigger").GetComponent<RightDoorTriggerEnter>().m_rightDoorTriggerEntered){//If the right door is still not open
						transform.Rotate(new Vector3(0.0f, 0.0f, 1.0f), m_fAngle);
					}
					else{
						m_bMovementComplete = true;
					}
				}
			}
		}
	}
	
	public void RecalcMovement()
	{
		m_fMovementProgress = 0.0f;
		m_vOriginalPosition = transform.position;
		
		if(m_vMovement != Vector3.zero){
			m_vTarget = transform.position + m_vMovement;
		}
	}
	
	public void ResetOriginalPos()
	{
		m_vOriginalPosition = transform.position;
	}
}
