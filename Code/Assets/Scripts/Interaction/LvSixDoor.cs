using UnityEngine;
using System.Collections;

public class LvSixDoor : MonoBehaviour {
	
	public GameObject LeftDoor;
	public GameObject RightDoor;
	public bool bOpen = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(bOpen)
		{
			MoveWood();
			if(LeftDoor.transform.Find ("wood").
			   GetComponent<ObjectMovement>().
			   m_bMovementComplete)
			{
				OpenDoor();
				if(LeftDoor.GetComponent<ObjectMovement>().m_bMovementComplete)
				{
					this.enabled = false;
				}
			}
		}
	}
	void MoveWood()
	{
		LeftDoor.transform.Find ("wood").GetComponent<ObjectMovement>().m_bStartMoving = true;
	}
	
	void OpenDoor()
	{
		LeftDoor.GetComponent<ObjectMovement>().m_bStartMoving = true;
		RightDoor.GetComponent<ObjectMovement>().m_bStartMoving = true;
	}
}
