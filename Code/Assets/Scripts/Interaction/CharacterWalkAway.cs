using UnityEngine;
using System.Collections;

public class CharacterWalkAway : MonoBehaviour {

	public Vector3 m_vTargetPosition;
	public bool m_bMovingToTarget;
	public float m_fWalkingSpeed = 0.0001f;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update (){
		if(m_bMovingToTarget)
		{
			//Hope look at
			gameObject.transform.LookAt(m_vTargetPosition);
			
			//Flying
			transform.position = Vector3.Lerp (transform.position, m_vTargetPosition, Time.deltaTime);
			
			//Destination is reached
			if(Vector3.Distance(transform.position, m_vTargetPosition) <= 0.5f)
			{
				m_bMovingToTarget = false;
				gameObject.GetComponent<Animation>().Play("idle");
				gameObject.transform.LookAt(Global.GetPlayer().transform.position);
			}
		}
	}
	
	/*
	 * This function set the interacting character to a certain point
	 * */
	public void InteractCharacterWalkTo(string _walkTargetPositionID)
	{
		GetComponent<Animation>().Play ("walk");
		
		m_vTargetPosition = Global.GetObjectPositionByID(_walkTargetPositionID);
		m_bMovingToTarget = true;
	}
}
