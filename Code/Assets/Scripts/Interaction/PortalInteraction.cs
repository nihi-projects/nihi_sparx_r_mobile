using UnityEngine;
using System.Collections;

public class PortalInteraction : MonoBehaviour {
	public Vector3 CameraPosition;
	private float InteractionRadius      = 0.5f;
	private float m_fTransitionStartTime = 0.0f;
	private float m_fTransitionTime      = 2.0f;
	private bool IsAutomatic             = false;
	private bool Interacting             = false;
	private bool m_bTransitionStarts     = false;
	private bool m_playerStartMoving     = false;
	
	// Use this for initialization
	void Start () {
	}
	
	void OnEnable() 
	{
		SoundPlayer.PlayLoopSound("sfx-potal", 1.0f);
	}
	
	// Update is called once per frame
	void Update () 
	{
		//When the player finish moving the cerntre point of the portal
		if(m_playerStartMoving && !Global.GetPlayer().GetComponent<PlayerMovement>().m_bMovingToPosition){
			Interacting = true;
		}
		
		float fDistance= Vector3.Distance(Global.GetPlayer().transform.position, transform.position);

		if(Global.m_bGemIsFullyReleased){
			if(fDistance <= InteractionRadius){	
				if(!Interacting){
					Global.GetPlayer().GetComponent<PlayerMovement>().MoveToPosition(transform.position + (transform.forward * 0.5f),
							  									 						transform.position);
					m_playerStartMoving = true;
				}
				else if(Interacting && !Global.GetPlayer().GetComponent<PlayerMovement>().m_bMovingToPosition){
					if(Global.GetPlayer ().GetComponent<Animation>().IsPlaying("idle") && !m_bTransitionStarts){
						Global.GetPlayer().GetComponent<PlayerMovement>().m_bMovement = false;
						GameObject.Find ("Portal Transition Particle").GetComponent<ParticleSystem>().Play();
						Global.CurrentInteractNPC = "Guide";
						m_bTransitionStarts = true;
						m_fTransitionStartTime = Time.time;
					}
				}
			}
		}
		
		if(m_bTransitionStarts && (Time.time - m_fTransitionStartTime) > m_fTransitionTime){
			Application.LoadLevel("GuideScene");
		}
	}
	
}
