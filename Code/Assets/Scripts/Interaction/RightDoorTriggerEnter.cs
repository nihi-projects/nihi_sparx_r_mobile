﻿using UnityEngine;
using System.Collections;

public class RightDoorTriggerEnter : MonoBehaviour {

	public bool m_rightDoorTriggerEntered = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other) 
	{
		m_rightDoorTriggerEntered = true;
		GameObject.Find("_[id]32010_doorr").GetComponent<ObjectMovement>().m_bMovementComplete = true;
	}
}
