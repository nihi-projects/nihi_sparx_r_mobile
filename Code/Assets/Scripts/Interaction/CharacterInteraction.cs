using UnityEngine;
using System.Collections;

public class CharacterInteraction : MonoBehaviour {
	
	
	//public Vector3 CameraPosition;
	public float InteractionRadius         = 0.0f;
	public float m_fFaceToFaceTalkDistance = 0.0f;
	private float m_fPlayerNPCDistance      = 1000.0f;
	public bool IsAutomatic                = false;
	public bool Interacting                = false;
	public bool Interacted                 = false;
	
	public float m_fCassDistance           = 0.0f;
	
	private bool m_bHopeIsFlyingL2         = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(Interacted){ return;}
		
		if (Global.GetPlayer())
		{
			m_fPlayerNPCDistance = Vector3.Distance(Global.GetPlayer().transform.position, transform.position);
		}
		
		if(m_fPlayerNPCDistance <= InteractionRadius){	
			//Non automatic
			if(!IsAutomatic){
				if(!Interacting){
					if(Input.GetKey(KeyCode.Space)){
						Global.CurrentInteractNPC = this.name;
						Interacting = true;
						if(this.name != "Hope" && this.name != "FirepspiritA" && this.name != "Yeti"){
							Global.GetPlayer().GetComponent<PlayerMovement>().
							MoveToPosition(transform.position + (transform.forward * m_fFaceToFaceTalkDistance),
	  									   transform.position,
										   gameObject.name);
						}
					}
				}
			}
			//Automatic
			else{
				if(!Interacting){
					//Non Hope
					if(this.name != "Hope"){
						if (m_fPlayerNPCDistance > (m_fFaceToFaceTalkDistance + 0.1f)){
							Vector3 moveTo = new Vector3(transform.position.x, Global.GetPlayer().transform.position.y, transform.position.z);
								Global.GetPlayer().GetComponent<PlayerMovement>().
								MoveToPosition(moveTo + (transform.forward * m_fFaceToFaceTalkDistance), transform.position, gameObject.name);
						}
						else{
							Interacting = true;
							Global.CurrentInteractNPC = this.name;
							
							//Fake object for character interaction
							if(gameObject.name.Contains("Fake")){
								FakeCharacterSwitch(gameObject.name);
							}
						}
					}
					//Hope
					else{
                    	Interacting = true;
                    	Global.CurrentInteractNPC = this.name;
					}
				}
			}
			
			//Interacting
			if(Interacting){
				//When the player interacting with NPCs
				if(Global.GetPlayer() != null){
					if(Global.GetPlayer().GetComponent<Animation>().IsPlaying("idle") && !Global.CurrentInteractNPC.Contains("Fake")){
						//This code is for fixing the Hope appear delay in the L2 ice cave door
//						if(GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentScene == "L2HopeScene2"){
//							if(GameObject.Find ("Hope").GetComponent<HopeTalk>().m_bMovingToTarget){
//								m_bHopeIsFlyingL2 = true;
//							}
//							if(m_bHopeIsFlyingL2 && !GameObject.Find ("Hope").GetComponent<HopeTalk>().m_bMovingToTarget){
//								GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled = true;
//							}
//						}
//						else{
							GameObject.Find ("GUI").GetComponent<TalkScenes>().enabled = true;
						//}
						Global.GetPlayer().GetComponent<PlayerMovement>().m_bMovement = false;
						Global.GetPlayer().GetComponent<PlayerMovement>().m_bMovingToPosition = false;
						GameObject.Find ("GUI").GetComponent<TerminateTalkScene>().enabled = false;
					}
				}
			}
		}
		
		SetCharacterInteractionBack();
	}
	
	public void FakeCharacterSwitch(string _fakeCharacterName)
	{
		switch(_fakeCharacterName){
			//Level 1
			case "FakeChestCharacter":
				//Global.CurrentInteractNPC = "Hope";
				Global.m_sCurrentFakeNPC = "FakeChestCharacter";
				break;
			case "FakeHopeCharStp1":
				Global.CurrentInteractNPC = "Hope";
				Global.m_sCurrentFakeNPC = "FakeHopeCharStp1";
				break;
			case "FakeHopeCharStp2":
				Global.CurrentInteractNPC = "Hope";
				Global.m_sCurrentFakeNPC = "FakeHopeCharStp2";
				break;
			//Level 2
			case "FakeTuiIceCave":
				Global.CurrentInteractNPC = "Hope";
				Global.m_sCurrentFakeNPC = "FakeTuiIceCave";
				GameObject.Find ("GUI").GetComponent<TalkScenes>().m_currentScene = "L2HopeScene2";
				break;
			//Level 3
			case "FakeTuiVocanoStart":
				Global.CurrentInteractNPC = "Hope";
				Global.m_sCurrentFakeNPC = "FakeTuiVocanoStart";
				break;
			case "FakeTuiVocanoSprit":
				Global.CurrentInteractNPC = "Hope";
				Global.m_sCurrentFakeNPC = "FakeTuiVocanoSprit";
				break;
			case "FakeTuiBeforeMiniGames":
				//Global.CurrentInteractNPC = "Hope";
				Global.m_sCurrentFakeNPC = "FakeTuiBeforeMiniGames";
				break;
			//Level 4
			case "FakeCliffTuiFlyPos1":
				Global.CurrentInteractNPC = "Hope";
				Global.m_sCurrentFakeNPC = "FakeCliffTuiFlyPos1";
				break;
		}
	}
	
	public void SetCharacterInteractionBack()
	{
		//Level 2
		if (Global.CurrentLevelNumber == 2){
			//Before Cass talk to the fireman
			if (Global.CurrentInteractNPC == "Cass" && !GameObject.Find ("GUI").GetComponent<TalkScenes>().m_bCassFiremanTalkStart){
				m_fCassDistance = Vector3.Distance(Global.GetPlayer().transform.position, GameObject.Find ("Cass").transform.position);
				
				if(m_fCassDistance > InteractionRadius + 2){
					GameObject.Find ("Cass").GetComponent<CharacterInteraction>().Interacted = false;
				}
			}
		}
	}
}