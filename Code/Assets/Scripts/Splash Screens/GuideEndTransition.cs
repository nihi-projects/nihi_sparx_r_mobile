using UnityEngine;
using System.Collections;

public class GuideEndTransition : MonoBehaviour {

	public GUISkin m_skin         = null;

	private float yPosition       = 0.0f;
	private float fTime 		  = 0.0f;
	private PlayerInfoLoadOnLevelLoad m_PlayerInfoLevelLoad;
	
	// Use this for initialization
	void Start () {
		//m_skin = (GUISkin) Resources.Load("Skins/SparxSkin");
	}
	
	// Update is called once per frame
	void Update () {
		if(Global.GetPlayer() && fTime > 1.5f)
		{
			Global.arrowKey = "";
			this.enabled = false;
		}
		else
		{
			Global.arrowKey = "";
			fTime += Time.deltaTime;
		}
	}
	
	/*
	*
	* OnGUI Function.
	*
	*/
	public void OnGUI () {		
		GUI.skin = m_skin;
		GUI.depth = -1;
		
		GUI.Label(new Rect(0, yPosition, Screen.width, Screen.height), "", "GuideEndTransImage");
		GUI.Label(new Rect(0, yPosition + Screen.height, Screen.width, Screen.height), "", "GuideEndTransImage");
		
		yPosition -= 5.0f;
		if (yPosition < -500.0f)
		{
			yPosition = 0.0f;
		}
	}
	
	public void OnEnable()
	{
		m_PlayerInfoLevelLoad = GameObject.Find ("PlayerInfoLoad").GetComponent<PlayerInfoLoadOnLevelLoad>();
		
		Application.LoadLevel("LevelS");
	}
}
