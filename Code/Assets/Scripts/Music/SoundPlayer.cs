using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundPlayer 
{
	// publics
	
	// privates
	static private List<AudioClip> m_acSoundClips = new List<AudioClip>();
	
	private static GameObject instance;
	private static GameObject loopPlayer;
	
	private SoundPlayer() {}
	
	static GameObject GetController()
	{   
	    if (!instance) 
		{
			instance = new GameObject("SoundObject");
    		instance.AddComponent<AudioSource>();
		}
	    return instance;
	}
	
	static GameObject GetLoopController()
	{   
	    if (!loopPlayer) 
		{
			loopPlayer = new GameObject("LoopSoundObject");
    		loopPlayer.AddComponent<AudioSource>();
		}
	    return loopPlayer;
	}
	
	static public void PlaySound(string _clipName, float _volume)
	{
		if (!instance) 
		{
			instance = new GameObject("SoundObject");
    		instance.AddComponent<AudioSource>();
		}
		
		bool found = false;
		foreach (AudioClip clip in m_acSoundClips)
		{
			if ((clip != null) && (clip.name == _clipName))
			{
				if (!(instance.GetComponent<AudioSource>().clip == clip && instance.GetComponent<AudioSource>().isPlaying))
				{
					//instance.audio.PlayOneShot(clip, _volume);
					instance.GetComponent<AudioSource>().clip = clip;
					instance.GetComponent<AudioSource>().Play();
					Debug.Log("Sound played: " + clip.name);
				}
				found = true;
				break;
			}
		}
		
		if (!found)
		{
			AudioClip clip = (AudioClip)Resources.Load("AudioSounds/Sounds/" + _clipName);
			if (clip == null)
			{
				Debug.Log(_clipName);
			}
			m_acSoundClips.Add(clip);
			instance.GetComponent<AudioSource>().clip = clip;
			instance.GetComponent<AudioSource>().Play();
		}
	}
	
	static public void PlayLoopSound(string _clipName, float _volume)
	{
		if (!loopPlayer) 
		{
			loopPlayer = new GameObject("LoopSoundObject");
    		loopPlayer.AddComponent<AudioSource>();
		}
		
		bool found = false;
		foreach (AudioClip clip in m_acSoundClips)
		{
			if (clip && clip.name == _clipName)
			{
				if (!(loopPlayer.GetComponent<AudioSource>().clip == clip && loopPlayer.GetComponent<AudioSource>().isPlaying))
				{
					loopPlayer.GetComponent<AudioSource>().clip = clip;
					loopPlayer.GetComponent<AudioSource>().Play();
				}
				loopPlayer.GetComponent<AudioSource>().volume = _volume;
				found = true;
				break;
			}
		}
		
		if (!found)
		{
			AudioClip clip = (AudioClip)Resources.Load("AudioSounds/Sounds/" + _clipName);
			if (clip == null)
			{
				Debug.Log("loop" + _clipName);
			}
			m_acSoundClips.Add(clip);
			loopPlayer.GetComponent<AudioSource>().clip = clip;
			loopPlayer.GetComponent<AudioSource>().Play();
			loopPlayer.GetComponent<AudioSource>().volume = _volume;
		}
	}
	
	static public void Stop(string _clipName)
	{
		if (!instance) 
		{
			instance = new GameObject("SoundObject");
    		instance.AddComponent<AudioSource>();
		}
		
		if (instance.GetComponent<AudioSource>().clip &&
			instance.GetComponent<AudioSource>().clip.name == _clipName)
		{
			instance.GetComponent<AudioSource>().Stop();
			Debug.Log ("125AUDIO SOURCE - STOPED!");
		}
	}
	
	static public void StopLoop(string _clipName)
	{
		if (!loopPlayer) 
		{
			loopPlayer = new GameObject("LoopSoundObject");
    		loopPlayer.AddComponent<AudioSource>();
		}
		
		if (loopPlayer.GetComponent<AudioSource>().clip &&
			loopPlayer.GetComponent<AudioSource>().clip.name == _clipName)
		{
			loopPlayer.GetComponent<AudioSource>().Stop();
			//Debug.Log ("141AUDIO SOURCE - STOPED!");
		}
	}
	
	static public void SetSounds(AudioClip[] _clips)
	{
		for (int i = 0; i < _clips.Length; ++i)
		{
			m_acSoundClips.Add(_clips[i]);
		}
	}
}
