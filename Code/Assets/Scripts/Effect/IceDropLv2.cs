using UnityEngine;
using System.Collections;

public class IceDropLv2 : MonoBehaviour {
	public GameObject[] IceParts;
	public bool m_bIsDropping = false;
	public float FallSpeedMin = 2.0f;
	public float FallSpeedMax = 4.0f;
	float[] FallSpeeds;
	public float IntervalMin = 1.0f;
	public float IntervalMax = 2.5f;
	float[] Intervals;
	bool[] Dropping;
	Vector3[] OriginalPos;
	
	GameObject[] tempIceEffect = null;
	GameObject[] tempSnowEffect = null;
	
	// Use this for initialization
	void Start () {
		FallSpeeds = new float[IceParts.Length];
		Intervals = new float[IceParts.Length];
		Dropping = new bool[IceParts.Length];
		OriginalPos = new Vector3[IceParts.Length];
		
		for(uint i = 0; i < Dropping.Length; ++i)
		{
			Dropping[i] = true;
			OriginalPos[i] = IceParts[i].transform.position;
			FallSpeeds[i] = Random.Range(FallSpeedMin, FallSpeedMax);
			Intervals[i] = Random.Range(IntervalMin, IntervalMax);
		}
		
		tempIceEffect = new GameObject[IceParts.Length];
		tempSnowEffect = new GameObject[IceParts.Length];
	}
	
	// Update is called once per frame
	void Update () {
		if(m_bIsDropping)
		{
			// each individual drop
			for(uint i = 0; i < IceParts.Length; ++i)
			{
				if(Dropping[i])
				{
					IceParts[i].transform.position += Vector3.down * FallSpeeds[i] * Time.deltaTime;
					float y = IceParts[i].transform.position.y;
					// Check if dropped on the ground.
					if(y <= 0.0f)
					{
						// if yes, stop droping and calculate interval.
						Dropping[i] = false;
						IceParts[i].transform.position = OriginalPos[i];
						Intervals[i] = Random.Range(IntervalMin, IntervalMax);
						GameObject tempIce = GameObject.Find("Iceshards " + (i+1).ToString());
						GameObject tempSnow = GameObject.Find("SnowPuff " + (i+1).ToString());
						tempIceEffect[i] = (GameObject)Instantiate(tempIce, tempIce.transform.position, tempIce.transform.rotation);
						tempSnowEffect[i] = (GameObject)Instantiate(tempSnow, tempSnow.transform.position, tempSnow.transform.rotation);
						SoundPlayer.PlaySound("sfx-icecrash", 0.35f);
					}
				}
				else
				{
					Intervals[i] -= Time.deltaTime;
					
					// check if interval finished,
					if(Intervals[i] <= 0.0f)
					{
						// if yes, calculate droping speed and start dropping
						Dropping[i] = true;
						FallSpeeds[i] = Random.Range (FallSpeedMin, FallSpeedMax);
					}
				}
				
				if (tempIceEffect[i] != null)
				{
					float time = tempIceEffect[i].GetComponent<ParticleSystem>().time;
					int count = tempIceEffect[i].GetComponent<ParticleSystem>().particleCount;
					if (tempIceEffect[i].GetComponent<ParticleSystem>().time > 0.1f &&
						tempIceEffect[i].GetComponent<ParticleSystem>().particleCount == 0)
					{
						Destroy(tempIceEffect[i]);
					}
				}
				
				if (tempSnowEffect[i] != null && 
					tempSnowEffect[i].GetComponent<ParticleSystem>().time > 0.1f &&
					tempSnowEffect[i].GetComponent<ParticleSystem>().particleCount == 0)
				{
					Destroy(tempSnowEffect[i]);
				}
			}
		}
	}
	
	public void Reset()
	{
		for(uint i = 0; i < IceParts.Length; ++i)
		{
			IceParts[i].transform.position = OriginalPos[i];
		}
	}
}
