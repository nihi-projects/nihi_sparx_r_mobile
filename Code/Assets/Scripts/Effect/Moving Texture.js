#pragma strict

// Public Variables
public var m_fScrollSpeed1:float = 0.0f;
public var m_fScrollSpeed2:float = 0.0f;


/*
*
* FixedUpdate Function.
*
*/
function FixedUpdate () {
	var offset1 : float = Time.time * m_fScrollSpeed1;
	var offset2 : float = Time.time * m_fScrollSpeed2;

    GetComponent.<Renderer>().material.SetTextureOffset ("_MainTex", Vector2(offset1,offset2));
    //renderer.material.SetTextureOffset ("_Texture2", Vector2(0,));

}