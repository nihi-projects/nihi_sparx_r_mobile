using UnityEngine;
using System.Collections;

public class WaterEffect : MonoBehaviour {
	public Vector2[] Offset;
	public Vector2[] tiling;
	float AccumulatedTime = 0;
	public float Speed = 3.0f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		AccumulatedTime += Time.deltaTime * Speed;
		Matrix4x4 matrix = Matrix4x4.TRS(Offset[0], Quaternion.Euler(0, 0,AccumulatedTime), tiling[0]);
		GetComponent<Renderer>().material.SetMatrix("_Matrix1", matrix);
		matrix = Matrix4x4.TRS(Offset[1], Quaternion.Euler(0, 0, -AccumulatedTime), tiling[1]);
		GetComponent<Renderer>().material.SetMatrix("_Matrix2", matrix);
		
	}
}
